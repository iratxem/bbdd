﻿CREATE TABLE [dbo].[sga_puesto] (
    [cod_puesto]      NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [activo]          TINYINT       NOT NULL,
    [descripcion]     VARCHAR (255) NOT NULL,
    [ip]              VARCHAR (255) NULL,
    [nombre]          VARCHAR (255) NOT NULL,
    [optimistic_lock] NUMERIC (19)  NOT NULL,
    [seleccionado]    TINYINT       NULL,
    [superusuario]    TINYINT       NULL,
    PRIMARY KEY CLUSTERED ([cod_puesto] ASC)
);


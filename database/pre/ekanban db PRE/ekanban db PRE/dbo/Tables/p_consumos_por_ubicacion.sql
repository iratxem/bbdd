﻿CREATE TABLE [dbo].[p_consumos_por_ubicacion] (
    [id_sububicacion] VARCHAR (255) NOT NULL,
    [referencia_id]   VARCHAR (255) NOT NULL,
    [cantidad]        NUMERIC (19)  NULL,
    [ref_cliente]     VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id_sububicacion] ASC, [referencia_id] ASC)
);


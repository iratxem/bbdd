﻿CREATE TABLE [dbo].[sga_parametros] (
    [nombre]      VARCHAR (255) NOT NULL,
    [descripcion] VARCHAR (255) NULL,
    [valor]       INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([nombre] ASC)
);


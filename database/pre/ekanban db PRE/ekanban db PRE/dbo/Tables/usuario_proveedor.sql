﻿CREATE TABLE [dbo].[usuario_proveedor] (
    [id]           NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [id_usuario]   NUMERIC (19)  NOT NULL,
    [id_proveedor] VARCHAR (255) NOT NULL,
    [prioridad]    INT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_usuario_proveedor_sga_usuario] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario])
);


﻿CREATE TABLE [dbo].[usuario_ubicacion_flujo] (
    [IdUbicacion] VARCHAR (255) NOT NULL,
    [IdFlujo]     INT           NOT NULL,
    [IdUsuario]   NUMERIC (19)  NOT NULL,
    [prioridad]   INT           NULL,
    [descripcion] VARCHAR (255) NULL,
    CONSTRAINT [PK_usuario_ubicacion_flujo] PRIMARY KEY CLUSTERED ([IdUbicacion] ASC, [IdFlujo] ASC, [IdUsuario] ASC)
);


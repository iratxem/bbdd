﻿CREATE TABLE [dbo].[sga_puesto_privilegio] (
    [ptr_puesto]           NUMERIC (19)  NOT NULL,
    [cod_puestoprivilegio] VARCHAR (255) NULL,
    CONSTRAINT [FK4E5B5ED9CD9013F2] FOREIGN KEY ([ptr_puesto]) REFERENCES [dbo].[sga_puesto] ([cod_puesto])
);


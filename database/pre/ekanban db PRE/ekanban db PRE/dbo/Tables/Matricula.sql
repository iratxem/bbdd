﻿CREATE TABLE [dbo].[Matricula] (
    [IdMatricula]  INT           IDENTITY (1, 1) NOT NULL,
    [IdFlujo]      INT           NULL,
    [IdUsuario]    VARCHAR (100) NULL,
    [FechaInicio]  DATE          NULL,
    [FechaFin]     DATE          NULL,
    [Cancelado]    BIT           NULL,
    [Ubicacion]    VARCHAR (255) NULL,
    [SubUbicacion] VARCHAR (255) NULL,
    CONSTRAINT [PK_Matricula] PRIMARY KEY CLUSTERED ([IdMatricula] ASC)
);


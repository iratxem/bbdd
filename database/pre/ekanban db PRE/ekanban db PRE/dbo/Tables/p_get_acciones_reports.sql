﻿CREATE TABLE [dbo].[p_get_acciones_reports] (
    [id_accrep]     VARCHAR (255) NOT NULL,
    [acc_rep_des]   VARCHAR (255) NULL,
    [acc_rep_nvar]  INT           NULL,
    [prioridad]     INT           NULL,
    [acc_rep_tip]   VARCHAR (255) NULL,
    [acc_rep_v1def] VARCHAR (255) NULL,
    [acc_rep_v1txt] VARCHAR (255) NULL,
    [acc_rep_v2def] VARCHAR (255) NULL,
    [acc_rep_v2txt] VARCHAR (255) NULL,
    [acc_rep_v3def] VARCHAR (255) NULL,
    [acc_rep_v3txt] VARCHAR (255) NULL,
    [accrep_des]    VARCHAR (255) NULL,
    [accrep_nvar]   INT           NULL,
    [accrep_tip]    VARCHAR (255) NULL,
    [accrep_v1def]  VARCHAR (255) NULL,
    [accrep_v1txt]  VARCHAR (255) NULL,
    [accrep_v2def]  VARCHAR (255) NULL,
    [accrep_v2txt]  VARCHAR (255) NULL,
    [accrep_v3def]  VARCHAR (255) NULL,
    [accrep_v3txt]  VARCHAR (255) NULL,
    [accrep_ccir]   BIT           NULL,
    [accrep_qrvar]  INT           NULL,
    PRIMARY KEY CLUSTERED ([id_accrep] ASC)
);


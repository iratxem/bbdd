﻿CREATE TABLE [dbo].[Courses] (
    [id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [code]        VARCHAR (50)     NOT NULL,
    [description] VARCHAR (200)    NULL,
    [instructor]  VARCHAR (50)     NULL,
    [date]        DATE             NULL,
    [venue]       VARCHAR (50)     NULL,
    [price]       MONEY            NULL,
    [duration]    INT              NULL,
    CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED ([id] ASC, [code] ASC)
);


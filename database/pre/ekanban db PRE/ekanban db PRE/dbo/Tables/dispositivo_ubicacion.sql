﻿CREATE TABLE [dbo].[dispositivo_ubicacion] (
    [id]             NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [prioridad]      INT           NOT NULL,
    [id_dispositivo] VARCHAR (255) NOT NULL,
    [id_ubicacion]   VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_atj79pqko6guiswhr515hl5qi] FOREIGN KEY ([id_dispositivo]) REFERENCES [dbo].[dispositivo] ([id_dispositivo]),
    CONSTRAINT [FK_hsx52j13m5icy3spj1hormj1y] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion])
);


﻿CREATE TABLE [dbo].[p_pc_ta_app_versiones] (
    [num_version]    VARCHAR (255) NOT NULL,
    [descripcion]    VARCHAR (255) NULL,
    [fecha_creacion] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([num_version] ASC)
);


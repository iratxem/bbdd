﻿CREATE TABLE [dbo].[p_situacion_tarjetas] (
    [art]               VARCHAR (255) NOT NULL,
    [a]                 INT           NULL,
    [ae]                INT           NULL,
    [artcli]            VARCHAR (255) NULL,
    [r]                 INT           NULL,
    [t]                 INT           NULL,
    [v]                 INT           NULL,
    [circ]              INT           NULL,
    [circuito]          VARCHAR (255) NULL,
    [cont]              INT           NULL,
    [estado]            INT           NULL,
    [ss]                INT           NULL,
    [ultlec]            DATETIME      NULL,
    [opt]               INT           NULL,
    [transito_activado] INT           NULL,
    [ctdKan]            INT           NULL,
    PRIMARY KEY CLUSTERED ([art] ASC)
);


﻿CREATE TABLE [dbo].[p_pedidos_enviados] (
    [linenum]        FLOAT (53)    NOT NULL,
    [packingslipid]  VARCHAR (255) NOT NULL,
    [qty]            FLOAT (53)    NULL,
    [deliverydate]   DATETIME      NULL,
    [invoiceaccount] VARCHAR (255) NULL,
    [externalitemid] VARCHAR (255) NULL,
    [itemid]         VARCHAR (255) NULL,
    [inventbatchid]  VARCHAR (255) NULL,
    [deliveryname]   VARCHAR (255) NULL,
    [purchaseorder]  VARCHAR (255) NULL,
    [rownumber]      INT           NULL,
    [t]              INT           NULL,
    [color]          INT           NULL,
    [link]           INT           NULL,
    [recid]          VARCHAR (255) NULL,
    [txtval]         VARCHAR (255) NULL,
    [tip]            INT           NULL,
    PRIMARY KEY CLUSTERED ([linenum] ASC, [packingslipid] ASC)
);


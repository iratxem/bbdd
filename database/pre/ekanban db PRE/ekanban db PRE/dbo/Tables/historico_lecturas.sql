﻿CREATE TABLE [dbo].[historico_lecturas] (
    [id_lectura]      VARCHAR (255) NOT NULL,
    [antena]          VARCHAR (255) NULL,
    [estado]          VARCHAR (255) NULL,
    [fecha_historico] DATETIME      NULL,
    [fecha_lectura]   DATETIME      NULL,
    [gestionado]      VARCHAR (255) NULL,
    [id_cliente]      VARCHAR (255) NULL,
    [id_referencia]   VARCHAR (255) NULL,
    [id_ubicacion]    VARCHAR (255) NULL,
    [idcircuito]      VARCHAR (255) NULL,
    [movil]           VARCHAR (255) NULL,
    [tipo]            VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id_lectura] ASC)
);


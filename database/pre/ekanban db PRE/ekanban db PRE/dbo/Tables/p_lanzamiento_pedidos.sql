﻿CREATE TABLE [dbo].[p_lanzamiento_pedidos] (
    [cod_error] VARCHAR (255) NOT NULL,
    [msg_error] VARCHAR (255) NULL,
    [result]    VARCHAR (255) NOT NULL,
    [msg]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([cod_error] ASC)
);


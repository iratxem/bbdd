﻿CREATE TABLE [dbo].[p_valid_priv] (
    [username]       VARCHAR (255) NOT NULL,
    [cod_privilegio] VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([username] ASC, [cod_privilegio] ASC)
);


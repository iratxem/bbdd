﻿CREATE TABLE [dbo].[p_numero_optimo_con_consumos] (
    [ubi]    VARCHAR (255) NOT NULL,
    [art]    VARCHAR (255) NOT NULL,
    [etiq]   VARCHAR (255) NOT NULL,
    [cli]    VARCHAR (255) NOT NULL,
    [año]    VARCHAR (255) NULL,
    [color]  VARCHAR (255) NULL,
    [ctdsem] FLOAT (53)    NULL,
    [emp]    VARCHAR (255) NULL,
    [sem]    VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ubi] ASC, [art] ASC, [etiq] ASC, [cli] ASC)
);


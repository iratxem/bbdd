﻿CREATE TABLE [dbo].[p_acc_reg_lectura] (
    [result]       INT           NOT NULL,
    [msg]          VARCHAR (255) NULL,
    [id_circuito]  VARCHAR (255) NULL,
    [circuito]     VARCHAR (255) NULL,
    [mid_circuito] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([result] ASC)
);


﻿CREATE TABLE [dbo].[TRegistroEstPort] (
    [RegPor_Fec]    DATETIME    NULL,
    [RegPor_Dis]    NCHAR (255) NULL,
    [RegPor_Des]    DATETIME    NULL,
    [RegPor_Has]    DATETIME    NULL,
    [RegPor_Err]    BIT         NULL,
    [RegPor_IP]     NCHAR (20)  NULL,
    [RegPor_Ver]    NCHAR (10)  NULL,
    [RegPor_Est]    INT         NULL,
    [RegPor_UltIni] DATETIME    NULL
);


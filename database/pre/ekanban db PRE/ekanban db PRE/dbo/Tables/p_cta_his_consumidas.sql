﻿CREATE TABLE [dbo].[p_cta_his_consumidas] (
    [lote]          VARCHAR (255) NOT NULL,
    [estado]        VARCHAR (255) NULL,
    [fecha_lectura] DATETIME      NULL,
    [sububicacion]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([lote] ASC)
);


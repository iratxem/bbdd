﻿CREATE TABLE [dbo].[TRegistro] (
    [Reg_Fec] DATETIME    NULL,
    [Reg_Usu] NCHAR (255) NULL,
    [Reg_Ubi] NCHAR (255) NULL,
    [Reg_opt] NCHAR (255) NULL,
    [Reg_Art] NCHAR (255) NULL,
    [Reg_Cli] NCHAR (255) NULL,
    [Reg_Txt] NCHAR (255) NULL
);


GO
CREATE NONCLUSTERED INDEX [Ind_Fecha]
    ON [dbo].[TRegistro]([Reg_Fec] ASC);


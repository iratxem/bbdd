﻿CREATE VIEW dbo.VAX_EKANCONSPROG
AS
SELECT        TOP (100) PERCENT dbo.sga_usuario.username, dbo.circuito.Proveedor_id, dbo.circuito.ubicacion, dbo.circuito.id_circuito, dbo.circuito.activo, dbo.circuito.referencia_id, InventTrans_1.DATEEXPECTED, 
                         InventTrans_1.TRANSREFID, InventTrans_1.QTY, InventTrans_1.DATAAREAID, InventTrans_1.STATUSISSUE, dbo.circuito.descripcion, dbo.circuito.ref_cliente, InventTrans_1.CUSTVENDAC
FROM            SRVAXSQL.SQLAX.dbo.INVENTTRANS AS InventTrans_1 INNER JOIN
                         dbo.circuito ON InventTrans_1.ITEMID = dbo.circuito.referencia_id INNER JOIN
                         dbo.usuario_proveedor ON dbo.circuito.Proveedor_id = dbo.usuario_proveedor.id_proveedor INNER JOIN
                         dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
WHERE        (InventTrans_1.STATUSISSUE = 6) AND (dbo.circuito.activo = 1)
ORDER BY InventTrans_1.DATEEXPECTED DESC, dbo.circuito.referencia_id

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[57] 4[21] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "InventTrans_1"
            Begin Extent = 
               Top = 13
               Left = 466
               Bottom = 635
               Right = 771
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 0
               Left = 72
               Bottom = 629
               Right = 292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuario_proveedor"
            Begin Extent = 
               Top = 235
               Left = 876
               Bottom = 365
               Right = 1046
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sga_usuario"
            Begin Extent = 
               Top = 247
               Left = 1100
               Bottom = 516
               Right = 1291
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
    ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANCONSPROG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANCONSPROG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANCONSPROG';


﻿



CREATE PROCEDURE [dbo].[PCtaEnTrans] 
	(@Cir varchar(255), @USU varchar(255)='', @Dis int=0)
AS
BEGIN
	
	declare @Tipo as int


	begin
		SELECT        @Tipo=tipo_circuito
		FROM            dbo.circuito
		WHERE        id_circuito = @Cir
	end

	set @Tipo=isnull(@Tipo,1)


	--EN PROVEEDORES NO GESTIONAMOS LA CTD EN TRANSITO, O CONSUMIDO, O CONFIRMADO O EN ALMACÉN
	if @Tipo=1
	begin
		SELECT '000000' AS ALB, GETDATE() AS FEC, GETDATE() AS ENT, 0 AS CTD, 'ABC' AS LOT
	end

	--TRANSITO EN CLIENTES
	if @Tipo=3
	begin
		SELECT        dbo.VAX_EKAN_ALBA.PACKINGSLIPID AS ALB,  dbo.VAX_EKAN_ALBA.CREATEDDATETIME AS FEC,  

		--DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME))
		dbo.ufn_ADD_WORKING_DAYS(dbo.VAX_EKAN_ALBA.CREATEDDATETIME, dbo.circuito.entrega_dias, dbo.circuito.entrega_horas)
		AS ENT, 
		dbo.VAX_EKAN_ALBA.QTY AS CTD, dbo.VAX_EKAN_ALBA.INVENTBATCHID AS LOT
		FROM            dbo.circuito INNER JOIN
		dbo.VAX_EKAN_ALBA ON dbo.circuito.cliente_id = dbo.VAX_EKAN_ALBA.ORDERACCOUNT AND dbo.circuito.referencia_id = dbo.VAX_EKAN_ALBA.ITEMID AND 
		(dbo.circuito.ubicacion = SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 3) OR
		SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 2) = dbo.circuito.Cir_UbiAnt OR
		dbo.VAX_EKAN_ALBA.PURCHASEORDER LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%')
		WHERE        
		--(DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME)) > GETDATE()) 
		(dbo.ufn_ADD_WORKING_DAYS(dbo.VAX_EKAN_ALBA.CREATEDDATETIME, dbo.circuito.entrega_dias, dbo.circuito.entrega_horas) > GETDATE())
		AND (dbo.VAX_EKAN_ALBA.QTY > 0) AND (dbo.circuito.id_circuito = @Cir)
	END




END





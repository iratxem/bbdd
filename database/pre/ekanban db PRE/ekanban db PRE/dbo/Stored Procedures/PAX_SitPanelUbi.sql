﻿

-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_SitPanelUbi]
(
@Ubi varchar(255)='', @Usu AS varchar(255)='', @ST as bit=1
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
	DECLARE @Clis integer
	DECLARE @Pros integer

	set @st=isnull(@st,0)
	

	SELECT     @Clis=COUNT(DISTINCT cliente_id) , @Pros=COUNT(DISTINCT Proveedor_id) 
	FROM        dbo.circuito
	WHERE     (activo = 1) AND (ubicacion = @Ubi)

	set @Clis=isnull(@Clis,0)
	set @Pros=isnull(@Pros,0)

	if @Pros<=1
	begin

		if @ST=0 
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, 0 AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi)
		end
		else
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, dbo.Estado(id_circuito) AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi)
		end
	end
	else
	begin
		
		if @ST=0
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, 0 AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi) 
			and Proveedor_id in (
				SELECT     dbo.usuario_proveedor.id_proveedor
				FROM        dbo.usuario_proveedor INNER JOIN
				dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
				WHERE     (dbo.sga_usuario.username = @Usu)
			)
		end
		else
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, dbo.Estado(id_circuito) AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi) 
			and Proveedor_id in (
				SELECT     dbo.usuario_proveedor.id_proveedor
				FROM        dbo.usuario_proveedor INNER JOIN
				dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
				WHERE     (dbo.sga_usuario.username = @Usu)
			)
		end

	end
	--status
	--0 nada
	--1 ok
	--2 alerta
	--3 crítico

	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = @Ubi,
		@Reg_opt = N'SitPanelUbi',
		@Reg_Art = @st,
		@Reg_cli = @Clis,
		@Reg_Txt = @Pros
	COMMIT;	
END





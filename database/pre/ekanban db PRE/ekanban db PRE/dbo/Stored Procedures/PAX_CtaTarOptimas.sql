﻿
-- =============================================
-- Author:		IKER
-- Create date: 6-5-2014
-- Description:	Procedimiento para obtener la cantidad optima según circuito
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CtaTarOptimas]
(
@Circuito varchar(255)='', @Usu AS varchar(255)='', @Sal as int=0, @Reg as varchar(50)=' '
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @Tipo as int
	DECLARE @Cliente AS varchar(255)
	DECLARE @Proveedor AS varchar(255)
	DECLARE @Articulo AS varchar(255)
	DECLARE @Ubic AS varchar(255)
	DECLARE @MedDia  as real
	declare @CtdKan as int
	declare @DiaTrans as int
	declare @DiaTrans1 as int
	declare @HoraTrans as int
	declare @DefKan as int
	DECLARE @OPT AS int
	DECLARE @CONSKANDIA AS real
	DECLARE @UbiAnt as varchar(3)=''
	DECLARE @NumEnv as int=0
	

	if @sal=0
	begin
		SELECT        cliente_id AS CLI, ubicacion AS UBI, referencia_id AS ART, num_kanban AS ACT, 1 AS MEDDIA, 1 AS CTDKAN, 1 AS ConsTarDia, 1 AS NumEnvios, 1 AS DemoraDias, 1 AS TRANSD, 1 AS TRANSH, 
		num_kanbanOPT AS OPT, num_kanbanOPT - num_kanban AS DIF
		FROM            dbo.circuito
		WHERE        (id_circuito = @Circuito)

		-- si es 0, que lo muestre en pantalla. situación por defecto. En funcionamiento hasta la fecha. 27/1/2015
		--select  @Cliente as CLI, @Ubic as UBI, @Articulo as ART, @DefKan as ACT, @MedDia as MEDDIA, @CtdKan as CTDKAN, @CONSKANDIA AS ConsTarDia, @NumEnv AS NumEnvios, 5-@NumEnv as DemoraDias, @DiaTrans as TRANSD, @HoraTrans as TRANSH, @OPT as OPT, @OPT-@DefKan as DIF
	end
	else
	begin 


			set @MedDia =0

		begin
			SELECT        @Cliente= cliente_id, @Proveedor=Proveedor_id, @Articulo=referencia_id, @CtdKan=cantidad_piezas, @DiaTrans=entrega_dias, @HoraTrans=entrega_horas, @DefKan=num_kanban, @Ubic=ubicacion,  @Tipo=tipo_circuito, @UbiAnt=Cir_UbiAnt,
							@NumEnv=CASE WHEN Cir_LunM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_MarM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_MieM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_JueM = 0 THEN 1 ELSE 0 END + CASE WHEN
							  Cir_VieM = 0 THEN 1 ELSE 0 END 
			FROM            dbo.circuito
			WHERE        id_circuito = @Circuito
		end
	
			--considerando las ubicaciones y cogiendo los envíos según albarán, en vez de facturas como anteriormente.

		if @tipo=3
		--Significa que es circuito de tipo cliente. 
		begin
			SET @UBIANT=ISNULL(@UBIANT,'')
			SET @UBIANT=LTRIM(RTRIM(@UBIANT))

			SELECT         @MedDia=AVG(CtdSem) / 5 
				FROM            (
	
					SELECT        TOP (40) CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
							THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
					FROM            PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
							PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
							CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
							PREAXSQL.SQLAXBAK.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
							CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
					WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0)) OR
							(SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%') AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0))
					GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
							'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
							CUSTPACKINGSLIPTRANS.DATAAREAID
					HAVING       (SUBSTRING(CUSTPACKINGSLIPJOUR.ORDERACCOUNT,1,6) = substring(@Cliente,1,6)) 
			 AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) AND (DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE) <> DATEPART(week, GETDATE()))
					ORDER BY ART, AÑO, SEM DESC

	/**
			SELECT        TOP (40)  CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, 
									 DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
									 CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
									 THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
			FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
									 SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
									 CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
									 SRVAXSQL.SQLAX.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
									 CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
			WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) OR
									 (SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%')
			GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
									 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
									 CUSTPACKINGSLIPTRANS.DATAAREAID
			HAVING        (CUSTPACKINGSLIPJOUR.ORDERACCOUNT = @Cliente) AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) 
							AND (DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(YEAR, GETDATE()))
       					    AND (DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) <> DATEPART(week, GETDATE()))
			ORDER BY ART, AÑO, SEM DESC
			**/

			) AS ten


			end


		if @tipo=1 --tipo proveedor
		BEGIN
			SELECT         @MedDia=AVG(CtdSem) / 5 
			FROM            (
				SELECT        TOP (100) PERCENT PURCHTABLE_1.INVOICEACCOUNT AS CLI, 1 AS UBI, PURCHLINE_1.ITEMID AS ART, DATEPART(year, PURCHLINE_1.DELIVERYDATE) AS AÑO, DATEPART(week, 
				PURCHLINE_1.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, PURCHLINE_1.DELIVERYDATE))) AS ETIQ, 
				SUM(PURCHLINE_1.PURCHQTY) AS CTDSEM, '1' AS COLOR, PURCHTABLE_1.DATAAREAID AS EMP
				FROM            PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 INNER JOIN
				PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 ON PURCHTABLE_1.PURCHID = PURCHLINE_1.PURCHID
				GROUP BY PURCHTABLE_1.INVOICEACCOUNT, PURCHLINE_1.ITEMID, DATEPART(year, PURCHLINE_1.DELIVERYDATE), DATEPART(week, PURCHLINE_1.DELIVERYDATE), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
				PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, PURCHLINE_1.DELIVERYDATE))), PURCHTABLE_1.DATAAREAID
				HAVING        (PURCHTABLE_1.DATAAREAID = N'ork') AND (PURCHLINE_1.ITEMID = @Articulo) AND (DATEPART(year, PURCHLINE_1.DELIVERYDATE) = DATEPART(YEAR, GETDATE())) AND (DATEPART(week, 
				PURCHLINE_1.DELIVERYDATE) <> 53) AND (PURCHTABLE_1.INVOICEACCOUNT = @Proveedor)
 				) as a
		END


		set @MedDia=isnull(@MedDia,0)

		if @MedDia=0 set @MedDia=1

		SET @CONSKANDIA=@MedDia/@CtdKan

		set @DiaTrans1=@DiaTrans
		if @DiaTrans1=0 set @DiaTrans1=1
		if @DiaTrans1>5 set @DiaTrans1=@DiaTrans1-2

		if @tipo=1 	SET @OPT=@MedDia/@CtdKan*@DiaTrans1*1.2
		if @tipo=3 	SET @OPT=@MedDia/@CtdKan*(5 - @NumEnv + @DiaTrans1) * 1.25

		set @OPT=isnull(@OPT,0)

		if @OPT=0 set @OPT=1


	

		--para visualizar, mientras se ejecuta. 
		select  @Cliente as CLI, @Ubic as UBI, @Articulo as ART, @DefKan as ACT, @MedDia as MEDDIA, @CtdKan as CTDKAN, @CONSKANDIA AS ConsTarDia, @NumEnv AS NumEnvios, 5-@NumEnv as DemoraDias, @DiaTrans as TRANSD, @HoraTrans as TRANSH, @OPT as OPT, @OPT-@DefKan as DIF

		--Actualizamos el campo de la tabla circuito, para acceso rápido
		UPDATE [dbo].[circuito]
		SET [num_kanbanOPT] = @OPT
		WHERE [id_circuito]=@Circuito

		-- En el histórico, ponemos todo a vigente 0
		UPDATE [dbo].[TRegistroOptHis]
		SET [id_Vig] = 0       
		WHERE [id_circuito] =@Circuito
		
		--insertamos un nuevo registro con vigente 1
		INSERT INTO [dbo].[TRegistroOptHis]
           ([id_Reg]
           ,[id_Fec]
           ,[id_Vig]
           ,[id_circuito]
           ,[cliente_id]
           ,[ubicacion]
           ,[referencia_id]
           ,[num_kanbanAct]
           ,[num_kanbanOpt]
           ,[num_kanbanDif]
           ,[MedDia]
           ,[cantidad_piezas]
           ,[MedDiaTarKanban]
           ,[NumEnvios]
           ,[DemoraDias]
           ,[DiasTrans]
           ,[HorasTrans])

		select  
			  @Reg as Registro
			, getdate()
			, 1 as v
			, @Circuito as c
			, @Cliente as CLI
			, @Ubic as UBI
			, @Articulo as ART
			, @DefKan as ACT
			, @OPT as OPT
			, @OPT-@DefKan as DIF
			, @MedDia as MEDDIA
			, @CtdKan as CTDKAN
			, @CONSKANDIA AS ConsTarDia
			, @NumEnv AS NumEnvios
			, 5-@NumEnv as DemoraDias
			, @DiaTrans as TRANSD
			, @HoraTrans as TRANSH

	end



	--EXEC	[dbo].[PRegistro]
	--@Reg_Usu = N'_',
	--@Reg_Ubi = @Ubic,
	--@Reg_opt = N'Cta/Act Optimos',
	--@Reg_Art = @Articulo,
	--@Reg_cli = @Cliente,
	--@Reg_Txt = @OPT


END


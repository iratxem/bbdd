﻿/*****************************************************************************************
 *
 * setEstadoActualCircuito
 *
 * Establece el  estado actual de un circuito 
 *
 * Posibles valores: 'A' , 'R', 'V'
 *
 ******************************************************************************************/

create PROCEDURE [dbo].[alertasEstado_setEstadoActualCircuito]
	( @Circuito as nvarchar(50),
	  @estadoCircuito as VARCHAR(2)  )
AS
BEGIN
	SET NOCOUNT ON;
	-- guardar el valor del estado en la tabla que corresponda para el circuito indicado
	update THistoricoVerdes
	set estado = @estadoCircuito 
	where IdCircuito = @Circuito
	and Fecha = (select MAX(fecha) from THistoricoVerdes where IdCircuito =  @Circuito)

	print 'guardar estado ' + @Circuito + ' ' + @estadoCircuito
END
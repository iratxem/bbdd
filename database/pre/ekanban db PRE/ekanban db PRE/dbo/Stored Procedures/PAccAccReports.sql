﻿
CREATE PROCEDURE [dbo].[PAccAccReports]
(
@Usuario AS varchar(255)='', @Cliente varchar(255)='',@Ubicacion varchar(255)='', 
@Circuito varchar(255)='', @AccRep varchar(9)='', @Tipo varchar(3)='PDF', 
@Var1 varchar(255)='', @Var2 varchar(255)='', @Var3 varchar(255)=''
)
AS

BEGIN
	DECLARE @Rep varchar(250)
	DECLARE @Ser varchar(25)=''
	DECLARE @ResOK INT=0
	DECLARE @Errmsg varchar(250)=''
	DECLARE @Url varchar(250)=''
	SET NOCOUNT ON;
	
	--REPORTING
	if @Tipo='PDE' OR @Tipo='PDF' OR @Tipo='EXC'
	BEGIN
		SELECT   @ser=AccRep_Ser, @Rep=AccRep_Rep
		FROM     dbo.TAccionesReports
		WHERE   (AccRep_Idd = @AccRep)
	END
	SET @SER=ISNULL(@SER, '')
	SET @Rep=ISNULL(@Rep, '')



	--***********************************************
	/*
	SELECT *
	FROM [dbo].[TAccionesReports]
	where accrep_tip='acc'
	ORDER BY ACCREP_TIP, ACCREP_IDD
	*/

	--ACCIONES
	if @AccRep='ACTMAICON' 
	begin
		UPDATE [dbo].[circuito]
		SET [mail] = @Var1
		WHERE   (id_circuito = @Circuito)
		
		SET @ResOK=1
	end
	if @AccRep='DARCONSUM' SET @ResOK=0
	

	if @AccRep='RESETCIRC' and left(@Circuito,4)='RFID'
	begin
		DELETE FROM [dbo].[tarjeta]
		WHERE        (id_circuito = @Circuito)
		SET @ResOK=1
	end

	

	if @AccRep='UPDMODUBI' --and left(@Circuito,10)='RFIDTEST00'
	begin
		UPDATE [EKANBAN].[dbo].[ubicacion]
		SET [Ubi_Modo] = @Var1
		WHERE [id_ubicacion]=@Ubicacion
		
		SET @ResOK=1
	end



	/*
	exec SRVAXSQL.ORKLB000.dbo.[ReportPDF_Obtener_Path] @Servidor = N'SRVAX',
		@Report = N'900_Adierazleak.rpt',
		@Cliente = N'00000001',
		@Division = N'01',
		@Links = N'1',
		@Empresa = N'ORK', 
		@Formato = N'EXCEL'


	*/


	--RESULTADOS para Acciones, como para Reporting
	SELECT  @Tipo as Tipo, 
			@ResOK as ResOK, 
			@Errmsg as Errmsg,  
			@Url as Url,
			@ser AS servidor,  
			@Rep AS report,  
			@Cliente AS cliente,  
			@Ubicacion AS ubicacion,  
			@Circuito AS circuito,  
			@Usuario AS usuario,  
			'01' AS division,  
			'1' AS links,  
			'9' AS permiso,  
			'ORK' AS empresa,  
			@Tipo AS tipo,  
			@Var1 AS var1,  
			@Var2 AS var2,  
			@Var3 AS var3,  
			'4' AS var4,  
			'5' AS var5,  
			'6' AS var6,  
			'7' AS var7
		
		
		/*
		EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usuario,
		@Reg_Ubi = '',
		@Reg_opt = N'[PCtaAccReports]',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Usuario
		*/

END









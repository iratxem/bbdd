﻿/*******************************************************************************************
 *
 * sendMailAlertaEstadoVerde
 *
 * Envía un mail de alerta indicando que el estado del circuito a pasado a verde
 *
 * @Des es el destinatario/s , si son más de uno deberá ir separado por ";" (TO)
 * @Cc son las direcciones de correo a las que se envía copia, si son varios separados por ";" (CC)
 * @Cco son las direcciones de correo a las que se envía copia oculta, si son varias separadas por ";" (CCO)
 * @Tem es el título o asunto del correo electrónico
 * @Cue es el contenido del mensaje
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[alertasEstado_sendMailAlertaEstadoVerde]
	( @Circuito as nvarchar(50))
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @subject as VARCHAR(255)
	DECLARE @to as VARCHAR(1000)
	DECLARE @cc as VARCHAR(1000) = null
	DECLARE @body as VARCHAR(5000)

	select @subject = mailVerdeAsunto, @to = mailVerdePara, @cc = mailVerdeCc from alertasEstado_configuracionMail
	where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)

	if @@ROWCOUNT > 0 and (@to is not null or @cc is not null)
		begin
			SET @body = 'Se ha pasado al estado verde...'
			SET @to = RTRIM(@to)

			if (@cc is not null)
				begin
					SET	 @cc = RTRIM(@cc)
				end
			else
				begin
					SET	 @cc = ISNULL(@cc,'')
				end

			if (1 = 1) --ejecutamos en el caso de que sea preproduccion
				begin
					SET @subject = N'PREPRODUCCION ' + ISNULL(@subject,'')
				
					SET @body = N'Para: ' + @to + CHAR(13) +CHAR(10) + 
								 'Cc: ' + @cc + CHAR(13) + CHAR(10) + 
								 @body
					
					print N'alertasEstado_sendMailAlertaEstadoVerde - Body del mail ' + @body

					SET @to = 'iratxem@cyc.es;mlecea@orkli.es' --aquí se pondría el mail al que se quiere que lleguen los mensajes de preproducción
					SET @cc = Null
				end
			
			
			EXEC msdb.dbo.sp_send_dbmail 
			  @profile_name='posta',
			  @recipients = @to,
			  @copy_recipients = @cc, 
			  @subject=@subject,
			  @body=@body
		end		 
	else
		begin
			print N'alertasEstado_sendMailAlertaEstadoVerde - No se ha especificado ni to ni cc, o no existe configuracion de envío de la alerta'
		end  
		
END
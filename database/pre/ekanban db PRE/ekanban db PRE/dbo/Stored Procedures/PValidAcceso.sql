﻿


-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PValidAcceso]
(
@Usu AS varchar(255)='', @Psw AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- + ISNULL(dbo.sga_usuario.Usu_PAIS,'') 
	declare @Token varchar(50)

	if @Usu='token' 
	begin
		set @Token=@Psw
		
		SELECT        @Usu=username, @Psw=password
		FROM            dbo.sga_usuario
		WHERE        Usu_Token = @Token

		set @Usu=isnull(@Usu,'')
		set @Psw=isnull(@Psw,'')
	end





		SELECT        TOP (100) PERCENT  dbo.sga_usuario.username as usu, dbo.sga_usuario.Usu_Idi AS Usu_Idi, dbo.TEmpresa.Emp_LogURL AS Emp_Log, dbo.TEmpresa.Emp_Nom, dbo.TEmpresa.Emp_Tel, dbo.TEmpresa.Emp_EMa, dbo.sga_usuario.cod_usuario, dbo.sga_usuario.nombre, 
		dbo.sga_usuario.apellidos, dbo.sga_rol.descripcion AS RolDes, dbo.ubicacion.Ubi_DesLoc, dbo.usuario_ubicacion.id_ubicacion, dbo.ubicacion.descripcion, dbo.ubicacion.localizacion, case isnull(Usu_TimeZone,'') when '' then 'Europe/Madrid' else Usu_TimeZone end as Timezone
		FROM            dbo.sga_usuario LEFT OUTER JOIN
		dbo.usuario_ubicacion ON dbo.sga_usuario.cod_usuario = dbo.usuario_ubicacion.id_usuario LEFT OUTER JOIN

		dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion  LEFT OUTER JOIN
		dbo.TEmpresa ON dbo.sga_usuario.Usu_Emp = dbo.TEmpresa.Emp_Idd INNER JOIN
		dbo.sga_rol ON dbo.sga_usuario.ptr_rol = dbo.sga_rol.cod_rol
		WHERE        (dbo.sga_usuario.activo = 1) AND (dbo.sga_usuario.username = @Usu) AND (dbo.sga_usuario.password = @Psw)  AND (dbo.ubicacion.activo = 1)
		ORDER BY dbo.usuario_ubicacion.prioridad, dbo.ubicacion.Ubi_DesLoc, dbo.usuario_ubicacion.id_ubicacion




	UPDATE [dbo].[sga_usuario]
	SET [conectado] = 1
	,[conectUltFec] = getdate()
	WHERE [username]=@Usu



	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = '',
		@Reg_opt = N'PValidAcceso',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Psw
		
END




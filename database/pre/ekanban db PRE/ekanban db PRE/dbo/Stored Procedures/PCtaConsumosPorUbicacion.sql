﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaConsumosPorUbicacion] 
	-- Add the parameters for the stored procedure here
	@UBICACION VARCHAR(255), @Usu varchar(255) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    -- Obtenemos todos los consumos de una ubicación, agrupado por sububicaciones
    -- Se devuelven SOLO las referencias que han tenido algun consumo
	
	select C.id_circuito, C.referencia_id, C.ref_cliente, COUNT(*) AS CANTIDAD
	FROM UBICACION U INNER JOIN CIRCUITO C ON U.id_ubicacion = C.ubicacion INNER JOIN tarjeta T ON T.id_circuito = C.id_circuito
	WHERE U.id_ubicacion = @UBICACION AND T.sentido='S' AND (T.estado = 'P' OR T.estado='A') AND (T.gestionado IS NULL OR
					T.gestionado = '') and C.activo = 1
		AND C.referencia_id <> 'TRAZABILITATEA'
	group by C.id_circuito, C.referencia_id, C.ref_cliente
	order by C.referencia_id
END

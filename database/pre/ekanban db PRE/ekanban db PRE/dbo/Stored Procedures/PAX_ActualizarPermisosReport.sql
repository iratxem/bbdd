﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PAX_ActualizarPermisosReport 
	-- Add the parameters for the stored procedure here
	@idReport varchar(255),
	@usuarios varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from usuario_AccReports where id_accrep = @idReport;
	
	DECLARE @start INT, @end INT 
	declare @usuario varchar(255)
    SELECT @start = 1, @end = CHARINDEX('|', @usuarios) 
    declare @index int = 1;
    WHILE @start < LEN(@usuarios) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@usuarios) + 1
       
        set @usuario= SUBSTRING(@usuarios, @start, @end - @start) ;
        insert into usuario_AccReports (id_usuario, id_accrep) values (@usuario, @idReport);
        SET @start = @end + 1 
        SET @end = CHARINDEX('|', @usuarios, @start)
        
    END 
END

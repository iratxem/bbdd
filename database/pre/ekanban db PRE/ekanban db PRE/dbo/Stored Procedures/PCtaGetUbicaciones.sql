﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaGetUbicaciones] 
	-- Add the parameters for the stored procedure here
	@codUser numeric(19,0),
	@mode int = 0,
	@idUbic varchar(255) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    if @mode = 0
    begin
		select * 
		from usuario_ubicacion uu inner join ubicacion u on uu.id_ubicacion= u.id_ubicacion and uu.id_usuario= @codUser;
	end
	if @mode=1
	begin
		select *
		from ubicacion u1
		where u1.id_ubicacion not in (select id_ubicacion from usuario_ubicacion uu1 where uu1.id_usuario = @codUser);
	end
	
	if @mode=2
	begin
		select * from ubicacion 
	end
	if @mode=3
	begin
		select * from ubicacion where id_ubicacion = @idUbic
	end
END

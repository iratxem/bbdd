﻿
-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_SitTarjetas]
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
	DECLARE @Cliente AS varchar(255)
	DECLARE @Articulo AS varchar(255)
	DECLARE @CtdKan AS INT
	DECLARE @ArtCli as varchar(255)
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SobreStock as int
	DECLARE @PEDPTES AS INT
	DECLARE	@return_value int
	DECLARE	@stat int
	DECLARE @UltLec as datetime
	DECLARE @TipCon as integer
	DECLARE @TipCir	as integer
	DECLARE @num_kanbanOPT as int = 0
	DECLARE @Ubi as varchar(50)
	declare @transitoActivado as bit;

	
	SET @T =0
	SET @V=0
	SET @AE=0
	SET @A=0
	SET @R=0
	SET @SobreStock=0
	SET @PEDPTES=0
	SET @stat=0
	set @UltLec=getdate()
	set @TipCon=0
	set @TipCir=0

	begin
		SELECT        @Cliente= cliente_id, @Articulo=referencia_id, @T=num_kanban, @CtdKan=cantidad_piezas, @ArtCli=ref_cliente, @TipCon=tipo_contenedor, @TipCir=tipo_circuito, @num_kanbanOPT=num_kanbanOPT, @Ubi=ubicacion, @transitoActivado= TransitoActivado
		FROM            dbo.circuito
		WHERE        id_circuito = @Circuito
	end

	set @ArtCli=isnull(@ArtCli, '')
	set @transitoActivado = ISNULL(@transitoActivado, 0);
	
	SET @SobreStock=dbo.SS(@Circuito)

	SET @R=dbo.R(@Circuito)

	SET @A=dbo.A(@Circuito)--, @ubi
	
	if @transitoActivado = 1
	begin
		SET @AE=dbo.AE(@Circuito)
	end
	else
	begin
		SET @AE=0;
	end
	


	--************************************************

	declare @Modo as varchar(30)
	--SELECT @Modo=dbo.ubicacion.Ubi_Modo
	--FROM   dbo.circuito INNER JOIN
	--dbo.ubicacion ON dbo.circuito.ubicacion = dbo.ubicacion.id_ubicacion
	--WHERE (dbo.circuito.id_circuito = @Circuito)
	
	SELECT @Modo=Ubi_Modo
	FROM ubicacion
	WHERE id_ubicacion =@Ubi;

	set @Modo=isnull(@Modo, 0)

	if @Modo<>'DISP'
	BEGIN 
		--MODO EVENTOS DE CONSUMO
		SET @V=@T-@AE-@A-@R
		SET @V=@V+@SobreStock 	--todo sobrestock se suma como si fuese verde.
		IF @V<0 SET @V=0
	
	END
	ELSE
	BEGIN 
	--MODO DISPNIBILIAD
		SET @V=@R --ES EL RESULTADO QUE CONSEGUIMOS DE LAS ENTRADAS

		SET @R=@T-@AE-@A-@V
		--SET @V=@V+@SobreStock 	--todo sobrestock se suma como si fuese verde.
		IF @R<0 SET @R=0
		--XXXXXXXXXXXXXXXXXXXXXX
	END
	--************************************************
	
	
	
	
	
	
	--Consulta de situación del circuito
	SET @stat=dbo.Estado(@Circuito);
	SET @UltLec=dbo.UltLec(@Circuito);
	--RESULTADOS PARA MOSTRAR
	SELECT @Articulo AS ART, @ArtCli as ARTCLI, @T AS T,  @R AS R, @A AS A, @AE AS AE, @V AS V, @SobreStock as SS, @stat as Estado,  @Circuito as Circuito, @UltLec as UltLec, @TipCon as Cont, @TipCir as Circ, @num_kanbanOPT as OPT, @transitoActivado as Transito_Activado, @CtdKan as ctd_Kan

	
	--REGISTRAR DATOS, CONTROL DE USO
	set @Usu=isnull(@Usu,' ')

	--EXEC	[dbo].[PRegistro]
	--	@Reg_Usu = @Usu,
	--	@Reg_Ubi = '',
	--	@Reg_opt = N'SitTarjetas',
	--	@Reg_Art = @Articulo,
	--	@Reg_cli = @Cliente,
	--	@Reg_Txt = ''

	COMMIT;
END




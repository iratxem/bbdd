﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCAEAccesos]
	-- Add the parameters for the stored procedure here
	(@Cod varchar(255)='', @Sen varchar(10)='', @Man bit=0, @Ubi varchar(255)='', @Disp varchar(255)='')
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @Sentido int=1
	DECLARE @CodID varchar (50)=''

	DECLARE @UIDWorkerCtd int=0
	DECLARE @UIDWorker int=0
	DECLARE @PermanentID varchar (50)=''
	DECLARE @UIDOrganization VARCHAR (100)=''

	DECLARE @MSG VARCHAR (2000)=''

	DECLARE @OIDOrder VARCHAR (100)=''
	DECLARE @TMax datetime
	DECLARE @TMaxLim datetime
	DECLARE @Cadena varchar(300)=''
	declare @DesUbi varchar(100)=''
	declare @Acceso varchar(100)=''

	--Para gestionar si es entrada o salida. Según la antena.
	IF @Sen='Ant0' SET @Sentido=1
	IF @Sen='Ant1' SET @Sentido=2

	--UIDType	Type
	--1	Entry
	--2	Exit
	--3	Denied entry

	--goto fin
-----------------------------------------------------------------------------------------------------------------------------------
	
	set @DesUbi= [dbo].[DesUbi] (@UBI)

	if @COD='CAEIKERESTEN' or @cod='CAE999000000'--txartela eta kotxekoa
	begin
		set @Cadena= @UBI + ':' + @DesUbi + '  Sentido: ' + @Sen + ' Tarjeta:(' + @COD + ')'
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'iker@orkli.es',
		@Tem = N'Paso por arco Tarjeta CAEIKERESTEN',
		@Cue = @Cadena -- @UBI
	end
	
	if @COD='CAEALAITZARA' 
	begin
		set @Cadena= @UBI + ':' + @DesUbi + '  Sentido: ' + @Sen + ' Tarjeta:(' + @COD + ')'
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'aarandia@orkli.es; iker@orkli.es; jcamarero@orkli.es',
		@Tem = N'Paso por arco Tarjeta CAEALAITZARA',
		@Cue = @Cadena -- @UBI
	end

	if @COD='CAEJAV'
	begin
		set @Cadena= @UBI + ':' + @DesUbi + '  Sentido: ' + @Sen
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'fjgonzalez@orkli.es',
		@Tem = N'Paso por arco Tarjeta CAEJAV',
		@Cue = @Cadena -- @UBI
	end


	SET @CodID=SUBSTRING(@COD,4,3)

	--Miramos ctd de trabajadores con con código entero según RFID
	SELECT        @UIDWorkerCtd=COUNT(TWorkers_1.PermanentID) 
	FROM            UNIFIKASSQL.unifikas.MASTER.TOrganizations AS TOrganizations_1 RIGHT OUTER JOIN
							UNIFIKASSQL.unifikas.MASTER.TWorkers AS TWorkers_1 ON TOrganizations_1.UIDOrganization = TWorkers_1.UIDOrganization LEFT OUTER JOIN
							UNIFIKASSQL.unifikas.MASTER.TFactories AS TFactories_1 ON TOrganizations_1.UIDFactory = TFactories_1.UIDFactory
	WHERE        (TWorkers_1.Active = 1) AND (TWorkers_1.PermanentID = @COD) AND (TOrganizations_1.UIDFactory = 2)

	SET @UIDWorkerCtd=ISNULL(@UIDWorkerCtd,0)

	

	if @UIDWorkerCtd=1 
	begin
		SELECT        @UIDWorker= UIDWorker
		FROM            UNIFIKASSQL.unifikas.MASTER.TOrganizations AS TOrganizations_1 RIGHT OUTER JOIN
							UNIFIKASSQL.unifikas.MASTER.TWorkers AS TWorkers_1 ON TOrganizations_1.UIDOrganization = TWorkers_1.UIDOrganization LEFT OUTER JOIN
							UNIFIKASSQL.unifikas.MASTER.TFactories AS TFactories_1 ON TOrganizations_1.UIDFactory = TFactories_1.UIDFactory
		WHERE        (TWorkers_1.Active = 1) AND (TWorkers_1.PermanentID = @COD) AND (TOrganizations_1.UIDFactory = 2)

		SET @UIDWorker=ISNULL(@UIDWorker,0)

	end 

	if @UIDWorkerCtd>1 
	begin
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'iker@orkli.es',
		@Tem = N'Registros duplicadosA',
		@Cue = @COD
	end


	IF @UIDWorker=0 
	--significa que no existe ningún usuario con ese código entenro, cogemos sólo el CODID 
	BEGIN

		SELECT        @UIDWorkerCtd=COUNT(TWorkers_1.PermanentID) 
		FROM            UNIFIKASSQL.unifikas.MASTER.TOrganizations AS TOrganizations_1 RIGHT OUTER JOIN
					UNIFIKASSQL.unifikas.MASTER.TWorkers AS TWorkers_1 ON TOrganizations_1.UIDOrganization = TWorkers_1.UIDOrganization LEFT OUTER JOIN
					UNIFIKASSQL.unifikas.MASTER.TFactories AS TFactories_1 ON TOrganizations_1.UIDFactory = TFactories_1.UIDFactory
		WHERE        (TWorkers_1.Active = 1) AND (TWorkers_1.PermanentID = @CodID) AND (TOrganizations_1.UIDFactory = 2)


		SET @UIDWorkerCtd=ISNULL(@UIDWorkerCtd,0)

		if @UIDWorkerCtd=1 
		begin
			SELECT        @UIDWorker= TWorkers_1.UIDWorker
			FROM            UNIFIKASSQL.unifikas.MASTER.TOrganizations AS TOrganizations_1 RIGHT OUTER JOIN
							UNIFIKASSQL.unifikas.MASTER.TWorkers AS TWorkers_1 ON TOrganizations_1.UIDOrganization = TWorkers_1.UIDOrganization LEFT OUTER JOIN
						UNIFIKASSQL.unifikas.MASTER.TFactories AS TFactories_1 ON TOrganizations_1.UIDFactory = TFactories_1.UIDFactory
			WHERE        (TWorkers_1.Active = 1) AND (TWorkers_1.PermanentID = @CodID) AND (TOrganizations_1.UIDFactory = 2)


		/**
		--PROCEDIMIENTO PARA ACTUALIZAR EL ID PERMANENTE SEGÚN LA SERIE DE LA TARJETA.
		EXEC	UNIFIKASSQL.Unifikas.[MASTER].[TWorkers_UPDPERMID]
		@UIDWorker = @UIDWorker,
		@PermanentID = @Cod
		**/

		END
		if @UIDWorkerCtd>1 
		begin
			EXEC	[dbo].[PEnvCorreo]
			@Des = N'iker@orkli.es',
			@Tem = N'Registros duplicadosB',
			@Cue = @CodID
		end
	END 
	
	if @man=1
	begin
		--Para visualizar el usuario seleccionado
		SELECT        UIDWorker, Name, Surname
		FROM            UNIFIKASSQL.Unifikas.MASTER.TWorkers 
		where UIDWorker=@UIDWorker
	end
	-----------------------------------------------------------------------------------------------------------------------------------
	
	--para obtener el código del último trabajo
	SELECT       @OIDOrder= TOrdersWorkers_1.OIDOrder, @UIDOrganization=TOrdersWorkers_1.UIDOrganization
	FROM            UNIFIKASSQL.Unifikas.CAE.TOrdersWorkers AS TOrdersWorkers_1 INNER JOIN
					UNIFIKASSQL.Unifikas.CAE.TOrders AS TOrders_1 ON TOrdersWorkers_1.OIDOrder = TOrders_1.OIDOrder INNER JOIN
					UNIFIKASSQL.Unifikas.MASTER.TWorkers AS TWorkers_1 ON TOrdersWorkers_1.UIDWorker = TWorkers_1.UIDWorker
					INNER JOIN
                         UNIFIKASSQL.Unifikas.cae.TOrdersContracts AS TOrdersContracts ON TOrdersWorkers_1.OIDOrder = TOrdersContracts.OIDOrder
	WHERE        (TWorkers_1.UIDWorker = @UIDWorker) AND (TOrders_1.Validated = 1) AND (TOrdersContracts.Validated = 1) and (TOrdersWorkers_1.Validated = 1)
	ORDER BY TOrdersWorkers_1.OIDOrder DESC

	SET @OIDOrder=ISNULL(@OIDOrder,0)

	if @OIDOrder=0 AND left(@CodID,1)<>'5' and @CodID<>'IKE' --AND @UBI<>'U00' --        
		begin
			set @Cadena= @UBI + ':' + @DesUbi + '  Tarjeta: ' + @CodID + ' ID:' + convert(varchar(100), @UIDWorker)
			EXEC	[dbo].[PEnvCorreo]
			@Des = N'iker@orkli.es', 
			@Tem = N'No existen trabajos para usuario',
			@Cue = @Cadena --

			set @Sentido=3
		end

	
	-----------------------------------------------------------------------------------------------------------------------------------
	
	--Miramos la hora última para entrada o salida
	SELECT        @TMax=MAX(Date), @TMaxLim=DATEADD(mi, 1, MAX(Date))
	FROM            UNIFIKASSQL.Unifikas.CA.TUsersAccess AS TUsersAccess_1
	WHERE        (UIDType = @Sentido) AND (UIDWorker = @UIDWorker)

	set @TMaxLim=isnull(@TMaxLim,DATEADD(mi, -60, GETUTCDATE()))


	-----------------------------------------------------------------------------------------------------------------------------------
	--CONTROL DE DOCUMENTACIÓN ACTUALIZADA - REQUISITOS
	--ver PROCEDURE [dbo].[AccessControl_PetitionExample]

	----------------------------------------------------------------------------------------------------------------------------------
	
	if @TMaxLim < GETUTCDATE() AND (@OIDOrder<>0 or @Sentido=3)
	--Significa que no hemos registrado antes nada. Por ello registramos el movimiento
	begin
		
		/**
		EXEC	UNIFIKASSQL.[Unifikas].[CA].[TUsersAccess_INS]
		@UIDWorker = @UIDWorker,
		@OIDOrder = @OIDOrder,
		@Comments = N'Registro RFID',
		@UIDType = @Sentido,
		@UIDWorkerResponsible = 0,
		@State = 0,
		@Identification = @CodID
		**/


			set @Cadena= 'Persona: ' + dbo.nomcae(@codid) + CHAR(13) + CHAR(10) + 'Tarjeta: ' + @CodID + '   ID: ' + convert(varchar(100), @UIDWorker) + CHAR(13) + CHAR(10) + 'Sentido: ' + @Sen + CHAR(13) + CHAR(10) + @UBI + ': ' + @DesUbi
			EXEC	[dbo].[PEnvCorreo]
			@Des = N'porteria@orkli.es', --; iker@orkli.es
			@Tem = N'Registro de entrada-salida automatica',
			@Cue = @Cadena --



		if @man=1 select  'INSERTADO' AS TRAB, @TMax as TMax, @TMaxLim as TMaxLim
		   
	end


if @man=1 select @Sen as Antena, @Sentido as Sentido, @Cod as DatoRFID, @Codid as ID, @UIDWorker AS Worker, @UIDWorkerCtd as CtdWorker, @OIDOrder AS [Order], @TMax as TMax, @TMaxLim as TMaxLim
fin:


	SET XACT_ABORT OFF;
END




/**
--CONSULTA PARA VER SI HAY DUPLICADOS EN IDENTIFICADORES PERMANENTES
SELECT        TOP (100) PERCENT PermanentID
FROM            UNIFIKASSQL.Unifikas.MASTER.TWorkers AS TWorkers_1
WHERE        (Active = 1)
GROUP BY PermanentID
HAVING        (PermanentID <> '') AND (COUNT(UIDWorker) > 1)
ORDER BY PermanentID




**/
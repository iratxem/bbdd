﻿/*****************************************************************************************
 *
 * alertasEstado_calcularEstadoCircuito
 *
 * Devuelve el estado de un circuito teniendo en cuenta la configuración del cliente (o bien en número o en porcentaje)
 *
 * Posibles valores: 1, 2, 3
 *
 ******************************************************************************************/

CREATE PROCEDURE [dbo].[alertasEstado_calcularEstadoCircuito]
(@Circuito as nvarchar(50) )
AS
BEGIN
	DECLARE @estadoCircuito AS INT
	DECLARE @funcionAlertas AS VARCHAR(255)

	SET @estadoCircuito = 1 --verde por defecto
	
	select @funcionAlertas = funcionAlertasCambioEstado 
	from clientes
	where clienteId = (select cliente_id from circuito where id_circuito = @Circuito)

	-- si no existe configuracion especifica para el cliente, coger la configuración por defecto
	if @@ROWCOUNT = 0 or @funcionAlertas is null or @funcionAlertas = ''
		begin
			SET @funcionAlertas = 'alertasEstado_estadoDefecto'
			SET @estadoCircuito = 99 --verde por defecto
		end
		
	print N'alertasEstado_calcularEstadoCircuito - funcion alertas : ' + @funcionAlertas
	print N'alertasEstado_calcularEstadoCircuito - circuito : ' + CONVERT(varchar, @Circuito)

	EXEC @estadoCircuito = dbo.alertasEstado_functionDispatcher @funcion = @funcionAlertas, @parametro = @Circuito
	
	print N'alertasEstado_calcularEstadoCircuito - calcular estado circuito : ' + CONVERT(varchar, @estadoCircuito)
	
	RETURN @estadoCircuito
END
﻿
-- =============================================
-- Author:		IMR
-- Create date: 4-10-2018
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_IRA_PRUEBAS]
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
		DECLARE @V as int 
		DECLARE @AE as int
		DECLARE @A as int
		DECLARE @R as int
		DECLARE @SobreStock as int
		DECLARE @UltLec as datetime
		--DECLARE @transitoActivado as bit;

		SET @V = dbo.V(@Circuito)
		SET @A = dbo.A(@Circuito)
		SET @R = dbo.R(@Circuito)
		SET @AE = dbo.AE(@Circuito)
		SET @SobreStock=dbo.SS(@Circuito)
		
		PRINT @Circuito
		
		INSERT INTO dbo.THistoricoVerdes (IdCircuito, Fecha, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
			CtdSobreStock,tag) values (@Circuito, getdate(), @V,@A,@R, @AE, @SobreStock, null)
	COMMIT TRANSACTION;
	
END
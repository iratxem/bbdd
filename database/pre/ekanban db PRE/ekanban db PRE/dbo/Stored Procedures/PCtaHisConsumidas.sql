﻿




CREATE PROCEDURE [dbo].[PCtaHisConsumidas] 
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	SET NOCOUNT ON;

SELECT   TOP (100)  dbo.tarjeta.sububicacion, dbo.tarjeta.estado, dbo.tarjeta.id_lectura , dbo.tarjeta.fecha_lectura,
				CAST( CASE 
                  WHEN gestionado IS NULL or gestionado = '' 
                     THEN 0
                  ELSE 1
             END AS bit) as gestionado
FROM            dbo.circuito INNER JOIN
				dbo.tarjeta ON dbo.circuito.id_circuito = dbo.tarjeta.id_circuito
	WHERE      (dbo.tarjeta.sentido = 'S') AND (dbo.circuito.id_circuito = @Circuito) 
ORDER BY dbo.tarjeta.fecha_lectura DESC

END



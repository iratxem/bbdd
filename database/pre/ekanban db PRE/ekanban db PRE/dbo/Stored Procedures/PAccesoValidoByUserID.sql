﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAccesoValidoByUserID]  
	-- Add the parameters for the stored procedure here
	@userID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- + ISNULL(dbo.sga_usuario.Usu_PAIS,'') 
	declare @Token varchar(50)

	--SELECT        TOP (100) PERCENT  dbo.sga_usuario.username as usu, dbo.sga_usuario.Usu_Idi AS Usu_Idi, dbo.TEmpresa.Emp_LogURL AS Emp_Log, dbo.TEmpresa.Emp_Nom, dbo.TEmpresa.Emp_Tel, dbo.TEmpresa.Emp_EMa, dbo.sga_usuario.cod_usuario, dbo.sga_usuario.nombre, 
	--	dbo.sga_usuario.apellidos, dbo.sga_rol.descripcion AS RolDes, dbo.ubicacion.Ubi_DesLoc, dbo.usuario_ubicacion.id_ubicacion, dbo.ubicacion.descripcion, dbo.ubicacion.localizacion, case isnull(Usu_TimeZone,'') when '' then 'Europe/Madrid' else Usu_TimeZone end as Timezone
	--	FROM            dbo.sga_usuario LEFT OUTER JOIN
	--	dbo.usuario_ubicacion ON dbo.sga_usuario.cod_usuario = dbo.usuario_ubicacion.id_usuario LEFT OUTER JOIN

	--	dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion  LEFT OUTER JOIN
	--	dbo.TEmpresa ON dbo.sga_usuario.Usu_Emp = dbo.TEmpresa.Emp_Idd INNER JOIN
	--	dbo.sga_rol ON dbo.sga_usuario.ptr_rol = dbo.sga_rol.cod_rol
	--	WHERE        (dbo.sga_usuario.activo = 1) AND (dbo.sga_usuario.cod_usuario = @userID) AND (dbo.ubicacion.activo = 1)
	--	ORDER BY dbo.usuario_ubicacion.prioridad, dbo.ubicacion.Ubi_DesLoc, dbo.usuario_ubicacion.id_ubicacion
	declare @nombre varchar(255);
	declare @username  varchar(255);
	declare @idioma varchar(255);
	declare @apellidos varchar(255);
	declare @idEmpresa int;
	declare @tokenAuten int;
	declare @tokenRefresco int;
	declare @logoEmpresa varchar(255);
	declare @nombreEmpresa varchar(255);
	declare @telEmpresa varchar(255);
	declare @emailEmpresa varchar(255);
	declare @ptrRol int;
	declare @rolDesc varchar(255);
	declare @timeZone varchar(255);
	declare @pasarelaActiva bit;
	declare @ftpServer varchar(255);
	declare @ftpUsername  varchar(255);
	declare @ftpPassword varchar(255);
	declare @permisoPasarela bit;
	declare @superuser tinyint;

	select @username = username ,@nombre=nombre, @idioma = Usu_Idi, @userID = cod_usuario, @apellidos = apellidos, @idEmpresa = Usu_Emp, @tokenAuten = TokenAuten_CaducidadMins,
		@tokenRefresco = TokenRefresco_CaducidadMins, @ptrRol = ptr_rol, 
		@timeZone = case  isnull(Usu_TimeZone,'') when '' then 'Europe/Madrid' else Usu_TimeZone end, @pasarelaActiva = usu_PasarelaActivada,
		@ftpServer = FTP_Server, @ftpUsername = FTP_Username, @ftpPassword = ftp_password, @permisoPasarela = usu_subirDatosAFtp
	from sga_usuario 
	where cod_usuario = @userID AND activo = 1;

	if @idEmpresa is not null and @idEmpresa <> ''
	begin
		select @logoEmpresa = Emp_LogURL, @nombreEmpresa = Emp_Nom, @telEmpresa = Emp_Tel, @emailEmpresa = Emp_EMa from TEmpresa where Emp_Idd = @idEmpresa;
	end

	if @ptrRol is not null 
	begin
	 select @rolDesc = descripcion, @superuser = superusuario from sga_rol where cod_rol = @ptrRol;
	end

	select @username as usu, @idioma as Usu_Idi, @timeZone as Timezone, @nombre as nombre, @logoEmpresa as Emp_Log, @nombreEmpresa as Emp_Nom, @telEmpresa as Emp_Tel, @emailEmpresa as Emp_Ema, @userID as cod_usuario,
		@apellidos as apellidos, @rolDesc as RolDes, @superuser as Super_User, u.Ubi_DesLoc, u.id_ubicacion, u.descripcion, u.localizacion, su.id_subUbicacion as subUbicacion, su.descripcion as subUbicacionDesc, us.orden,
		@tokenAuten as AuthTokenExpirationMins, @tokenRefresco RefreshTokenExpirationMins, @pasarelaActiva as Pasarela_Activada, @ftpServer as servidor_FTP, @ftpUsername as ftp_Username, @ftpPassword as ftp_Password, @permisoPasarela as Permiso_Pasarela
	from (usuario_ubicacion uu inner join ubicacion u on u.id_ubicacion = uu.id_ubicacion ) LEFT OUTER JOIN 
		(ubicacion_sububicacion us  inner join subUbicacion su on us.id_subUbicacion = su.id_subUbicacion AND SU.ACTIVO = 1 and US.activo = 1) 
				on u.id_ubicacion = us.id_ubicacion 
	where  uu.id_usuario = @userID AND U.activo = 1 
	ORDER BY uu.prioridad, u.Ubi_DesLoc, uu.id_ubicacion;



	UPDATE [dbo].[sga_usuario]
	SET [conectado] = 1
	,[conectUltFec] = getdate()
	WHERE [cod_usuario]=@userID

	DECLARE @Usu varchar(255);
	declare @Psw varchar(255);
	select @Usu = username, @Psw = [password] from sga_usuario where cod_usuario = @userID;

	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = '',
		@Reg_opt = N'PValidAcceso',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Psw
END

﻿

CREATE PROCEDURE [dbo].[PTestingKK]
	AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @NBase_Cursor varchar(50)
	DECLARE	@return_value int
	
	DECLARE  CDatos CURSOR FOR
		SELECT  id_circuito --    referencia_id --
		FROM    dbo.circuito

	--	SELECT [id_ubicacion]
     --   FROM [dbo].[ubicacion]

	OPEN CDatos  -- dispone de todos los códigos de circuito a integrar.

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
	
	--***************************************************************************************
	
EXEC	@return_value = [dbo].[PCtaHisConsumidas]
		@Circuito =     @NBase_Cursor
	

	--***************************************************************************************
		
		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor

	END

close CDatos
deallocate CDatos

fin:


END



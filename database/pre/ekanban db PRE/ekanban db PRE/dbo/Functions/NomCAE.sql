﻿



CREATE FUNCTION [dbo].[NomCAE]
(@PermID as nvarchar(50) )
RETURNS varchar(300)
AS
BEGIN
	Declare @Des as varchar(300)
	
		SELECT       @Des=TWorkers_1.Name + ' ' + TWorkers_1.Surname + ' (' + rtrim(TOrganizations_1.Organization)  + ')'  --TWorkers_1.UIDWorker, TWorkers_1.Name, TWorkers_1.Surname, TWorkers_1.PermanentID, TOrganizations_1.Organization
		FROM            UNIFIKASSQL.unifikas.MASTER.TOrganizations AS TOrganizations_1 RIGHT OUTER JOIN
					UNIFIKASSQL.unifikas.MASTER.TWorkers AS TWorkers_1 ON TOrganizations_1.UIDOrganization = TWorkers_1.UIDOrganization
		WHERE        (TWorkers_1.PermanentID = @PermID) AND (TOrganizations_1.UIDFactory = 2)

	RETURN @Des
END






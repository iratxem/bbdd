﻿/*****************************************************************************************
 *
 * getEstadoActualCircuito
 *
 * Devuelve el estado actual de un circuito 
 *
 * Posibles valores: 'A' , 'R', 'V'
 *
 ******************************************************************************************/

CREATE FUNCTION [dbo].[alertasEstado_getEstadoCircuito]
(@Circuito as nvarchar(50) )
RETURNS VARCHAR(2)
AS
BEGIN
	DECLARE @estadoCircuito as VARCHAR(2)

	-- recogemos el estado de la tabla de historico verdes 

	select @estadoCircuito = estado
	from THistoricoVerdes		    
	where IdCircuito = @Circuito
	and Fecha = (select MAX(fecha) from THistoricoVerdes where IdCircuito =  @Circuito)

	RETURN @estadoCircuito

END
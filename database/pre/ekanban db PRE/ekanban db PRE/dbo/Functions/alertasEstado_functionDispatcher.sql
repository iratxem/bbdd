﻿CREATE FUNCTION [dbo].[alertasEstado_functionDispatcher](
	@funcion SYSNAME,
	@parametro as nvarchar(50)
	)
RETURNS INT
AS
BEGIN
  DECLARE @r INT;
  IF OBJECT_ID(@funcion, N'FN') IS NULL  --handling non-existing function
    RETURN 401;
  EXEC @r = @funcion @Circuito = @parametro; 
  RETURN @r;
END;
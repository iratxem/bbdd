﻿-- =============================================
-- Author:		CyC
-- Create date: 2015-14-05
-- =============================================
CREATE FUNCTION [dbo].[fAccessControl_SetWorkerExit]
(
	-- Add the parameters for the function here
	@UIDWorker int, @OIDOrder int
)
RETURNS nvarchar(4000)
AS
BEGIN

	RETURN DBO.AccessControl_SetWorkerExit('http://unifikas.orkli.com/ORKLI/WS/CAE.svc', @UIDWorker, @OIDOrder)
	
END

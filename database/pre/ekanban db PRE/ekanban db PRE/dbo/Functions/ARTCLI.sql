﻿


CREATE FUNCTION [dbo].[ARTCLI]
(@Circuito as nvarchar(50) )
RETURNS varchar(255)
AS
BEGIN
		DECLARE @ArtCli as varchar(255)
	
	begin
		SELECT        @ArtCli=ref_cliente
		FROM            dbo.circuito
		WHERE        id_circuito = @Circuito
	end
	set @ArtCli=isnull(@ArtCli,' ')
	
	RETURN @ArtCli

END





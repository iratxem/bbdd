﻿CREATE FUNCTION [dbo].[SS]
(@Circuito as nvarchar(50))
RETURNS int
AS
BEGIN
	DECLARE @SobreStock as int

	--no debería de haber, sobrestock, si hay tarjetas rojas, ya que primero tiene que eliminar estas. 
	SELECT        @SobreStock=COUNT(dbo.tarjeta.id_lectura) 
	FROM            dbo.circuito INNER JOIN
							 dbo.tarjeta ON dbo.circuito.id_circuito = dbo.tarjeta.id_circuito
	WHERE        (dbo.tarjeta.estado = 'A_MODIF') AND (dbo.tarjeta.sentido = 'S') --SOLO COGEMOS LAS SALIDAS Y NO ENTRADAS
	and (dbo.circuito.id_circuito = @Circuito) AND (dbo.tarjeta.gestionado IS NULL or dbo.tarjeta.gestionado='')
	GROUP BY dbo.circuito.id_circuito


	SET @SobreStock=ISNULL(@SobreStock,0)

	RETURN @SobreStock

END

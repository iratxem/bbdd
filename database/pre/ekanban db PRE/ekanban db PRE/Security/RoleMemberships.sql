﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'ekanban';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'orkliord\iniker5';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'muekanban';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'cycimr';


GO
EXECUTE sp_addrolemember @rolename = N'db_securityadmin', @membername = N'orkliord\iniker5';


GO
EXECUTE sp_addrolemember @rolename = N'db_ddladmin', @membername = N'orkliord\iniker5';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'ekanban';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'ek';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'orkliord\iniker5';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'LisSolutions';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'muekanban';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'ekanban';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'ek';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'orkliord\iniker5';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'muekanban';


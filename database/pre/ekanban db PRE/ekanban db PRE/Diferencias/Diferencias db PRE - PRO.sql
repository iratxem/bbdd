﻿/*
Script de implementación para EKANBAN

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "EKANBAN"
:setvar DefaultFilePrefix "EKANBAN"
:setvar DefaultDataPath "E:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "E:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
/*
Se está quitando la columna [dbo].[circuito].[LeadTime]; puede que se pierdan datos.
*/

IF EXISTS (select top 1 1 from [dbo].[circuito])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la columna [dbo].[sga_usuario].[UltimaIntegracionERP]; puede que se pierdan datos.
*/

IF EXISTS (select top 1 1 from [dbo].[sga_usuario])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[Albaranes_Incompletos]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[Albaranes_Incompletos])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[AlbaranesProcesados]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[AlbaranesProcesados])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[Aux_Lecturas]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[Aux_Lecturas])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[EnviosProcesados]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[EnviosProcesados])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[Optimos]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[Optimos])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[ReferenciasConsumoAutomatico]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[ReferenciasConsumoAutomatico])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[TOptimos]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[TOptimos])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [dbo].[TrazabilidadManualLecturas]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [dbo].[TrazabilidadManualLecturas])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
PRINT N'Quitando Permiso...';


GO
REVOKE SELECT
    ON OBJECT::[dbo].[Tarjetas_Marzo] TO [rfid] CASCADE
    AS [dbo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando Permiso...';


GO
REVOKE SELECT
    ON OBJECT::[dbo].[TRegistroLecturas_Marzo] TO [rfid] CASCADE
    AS [dbo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[ELUX_actuVSopt].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ELUX_actuVSopt';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[ELUX_actuVSopt].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ELUX_actuVSopt';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[KNIME_DesviacionSemanal].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'KNIME_DesviacionSemanal';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[KNIME_DesviacionSemanal].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'KNIME_DesviacionSemanal';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Sabaf_U09_URFID4].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Sabaf_U09_URFID4';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Sabaf_U09_URFID4].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Sabaf_U09_URFID4';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Seg_Lect_U00].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Seg_Lect_U00';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Seg_Lect_U00].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Seg_Lect_U00';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[SitTarjetasEli].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'SitTarjetasEli';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[SitTarjetasEli].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'SitTarjetasEli';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Tarjetas_Marzo].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Tarjetas_Marzo';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Tarjetas_Marzo].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Tarjetas_Marzo';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[TRegistroLecturas_Marzo].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'TRegistroLecturas_Marzo';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[TRegistroLecturas_Marzo].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'TRegistroLecturas_Marzo';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_BI_Consumos].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_BI_Consumos';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_BI_Consumos].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_BI_Consumos';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_Consumos].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_Consumos';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_Consumos].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_Consumos';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_Dispositivos].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_Dispositivos';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_Dispositivos].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_Dispositivos';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_TrazabilidadManual].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_TrazabilidadManual';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_TrazabilidadManual].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_TrazabilidadManual';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[VEtiquetasOF_vs_Lecturas].[MS_DiagramPane1]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPane1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEtiquetasOF_vs_Lecturas';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[VEtiquetasOF_vs_Lecturas].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_dropextendedproperty @name = N'MS_DiagramPaneCount', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEtiquetasOF_vs_Lecturas';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[dispositivo].[Id_dispositivo]...';


GO
DROP INDEX [Id_dispositivo]
    ON [dbo].[dispositivo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[dispositivo].[Indice_NumSerie]...';


GO
DROP INDEX [Indice_NumSerie]
    ON [dbo].[dispositivo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[DF_circuito_Cir_LecMinParaCons]...';


GO
ALTER TABLE [dbo].[circuito] DROP CONSTRAINT [DF_circuito_Cir_LecMinParaCons];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[DF_sga_usuario_Usu_Emp]...';


GO
ALTER TABLE [dbo].[sga_usuario] DROP CONSTRAINT [DF_sga_usuario_Usu_Emp];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[DF_sga_usuario_Usu_FacConsig]...';


GO
ALTER TABLE [dbo].[sga_usuario] DROP CONSTRAINT [DF_sga_usuario_Usu_FacConsig];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[DF_ubicacion_Ubi_NotEtiVacias]...';


GO
ALTER TABLE [dbo].[ubicacion] DROP CONSTRAINT [DF_ubicacion_Ubi_NotEtiVacias];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[DF_ubicacion_Ubi_Modo]...';


GO
ALTER TABLE [dbo].[ubicacion] DROP CONSTRAINT [DF_ubicacion_Ubi_Modo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_h4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[tarjeta] DROP CONSTRAINT [FK_h4a4i8acrq6mun1132lhiru9h];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_iejhi9irkh7tx1ji2y8t5k47k]...';


GO
ALTER TABLE [dbo].[circuito] DROP CONSTRAINT [FK_iejhi9irkh7tx1ji2y8t5k47k];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_3vn6jckmib1of620by1jg35a6]...';


GO
ALTER TABLE [dbo].[circuito] DROP CONSTRAINT [FK_3vn6jckmib1of620by1jg35a6];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_gpat2w8x7tswwv6tmkaq9u1po]...';


GO
ALTER TABLE [dbo].[circuito] DROP CONSTRAINT [FK_gpat2w8x7tswwv6tmkaq9u1po];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_usuario_cliente_sga_usuario1]...';


GO
ALTER TABLE [dbo].[usuario_cliente] DROP CONSTRAINT [FK_usuario_cliente_sga_usuario1];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_usuario_proveedor_sga_usuario]...';


GO
ALTER TABLE [dbo].[usuario_proveedor] DROP CONSTRAINT [FK_usuario_proveedor_sga_usuario];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FKDEDB797C40F68DC8]...';


GO
ALTER TABLE [dbo].[sga_usuario] DROP CONSTRAINT [FKDEDB797C40F68DC8];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FKDEDB797CDAB65181]...';


GO
ALTER TABLE [dbo].[sga_usuario] DROP CONSTRAINT [FKDEDB797CDAB65181];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FKEB3B4B51F04FDB86]...';


GO
ALTER TABLE [dbo].[sga_usuario_puesto] DROP CONSTRAINT [FKEB3B4B51F04FDB86];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK5577C8CE315BB8B3]...';


GO
ALTER TABLE [dbo].[usuario_ubicacion] DROP CONSTRAINT [FK5577C8CE315BB8B3];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_hsx52j13m5icy3spj1hormj1y]...';


GO
ALTER TABLE [dbo].[dispositivo_ubicacion] DROP CONSTRAINT [FK_hsx52j13m5icy3spj1hormj1y];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK5577C8CE8CC191D5]...';


GO
ALTER TABLE [dbo].[usuario_ubicacion] DROP CONSTRAINT [FK5577C8CE8CC191D5];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[FK_ubicacion_sububicacion_ubicacion]...';


GO
ALTER TABLE [dbo].[ubicacion_sububicacion] DROP CONSTRAINT [FK_ubicacion_sububicacion_ubicacion];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Aux_Lecturas]...';


GO
DROP TABLE [dbo].[Aux_Lecturas];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[EnviosProcesados]...';


GO
DROP TABLE [dbo].[EnviosProcesados];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Optimos]...';


GO
DROP TABLE [dbo].[Optimos];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[ELUX_actuVSopt]...';


GO
DROP VIEW [dbo].[ELUX_actuVSopt];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[KNIME_DesviacionSemanal]...';


GO
DROP VIEW [dbo].[KNIME_DesviacionSemanal];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Sabaf_U09_URFID4]...';


GO
DROP VIEW [dbo].[Sabaf_U09_URFID4];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Seg_Lect_U00]...';


GO
DROP VIEW [dbo].[Seg_Lect_U00];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[SitTarjetasEli]...';


GO
DROP VIEW [dbo].[SitTarjetasEli];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Tarjetas_Marzo]...';


GO
DROP VIEW [dbo].[Tarjetas_Marzo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[TRegistroLecturas_Marzo]...';


GO
DROP VIEW [dbo].[TRegistroLecturas_Marzo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_BI_Consumos]...';


GO
DROP VIEW [dbo].[V_BI_Consumos];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_Consumos]...';


GO
DROP VIEW [dbo].[V_Consumos];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_Dispositivos]...';


GO
DROP VIEW [dbo].[V_Dispositivos];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[V_TrazabilidadManual]...';


GO
DROP VIEW [dbo].[V_TrazabilidadManual];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[VEtiquetasOF_vs_Lecturas]...';


GO
DROP VIEW [dbo].[VEtiquetasOF_vs_Lecturas];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[TrazabilidadManualLecturas]...';


GO
DROP TABLE [dbo].[TrazabilidadManualLecturas];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[PAX_CalculoOptimos_V2]...';


GO
DROP PROCEDURE [dbo].[PAX_CalculoOptimos_V2];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[PAX_ConsumoAutomatico]...';


GO
DROP PROCEDURE [dbo].[PAX_ConsumoAutomatico];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[PAX_ProcesarEtiquetasRiello]...';


GO
DROP PROCEDURE [dbo].[PAX_ProcesarEtiquetasRiello];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[PAX_PruebaVerdes]...';


GO
DROP PROCEDURE [dbo].[PAX_PruebaVerdes];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[PCta_GuardarHistoricoTarjetasVerdes]...';


GO
DROP PROCEDURE [dbo].[PCta_GuardarHistoricoTarjetasVerdes];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[RoundMult]...';


GO
DROP FUNCTION [dbo].[RoundMult];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[Albaranes_Incompletos]...';


GO
DROP TABLE [dbo].[Albaranes_Incompletos];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[AlbaranesProcesados]...';


GO
DROP TABLE [dbo].[AlbaranesProcesados];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[ReferenciasConsumoAutomatico]...';


GO
DROP TABLE [dbo].[ReferenciasConsumoAutomatico];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [dbo].[TOptimos]...';


GO
DROP TABLE [dbo].[TOptimos];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [LisSolutions]...';


GO
DROP USER [LisSolutions];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [rfid]...';


GO
DROP USER [rfid];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando <sin nombre>...';


GO
EXECUTE sp_droprolemember @rolename = N'db_datawriter', @membername = N'ORKLIORD\eunanue';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando <sin nombre>...';


GO
EXECUTE sp_droprolemember @rolename = N'db_owner', @membername = N'ORKLIORD\eunanue';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando <sin nombre>...';


GO
EXECUTE sp_droprolemember @rolename = N'db_datareader', @membername = N'ORKLIORD\eunanue';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando <sin nombre>...';


GO
EXECUTE sp_droprolemember @rolename = N'db_datareader', @membername = N'precoz';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando <sin nombre>...';


GO
EXECUTE sp_droprolemember @rolename = N'db_securityadmin', @membername = N'ORKLIORD\eunanue';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [ORKLIORD\eunanue]...';


GO
DROP USER [ORKLIORD\eunanue];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Quitando [precoz]...';


GO
DROP USER [precoz];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [LisSolutions]...';


GO
CREATE USER [LisSolutions] WITHOUT LOGIN;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [muekanban]...';


GO
CREATE USER [muekanban] FOR LOGIN [muekanban];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando <sin nombre>...';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'muekanban';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando <sin nombre>...';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'muekanban';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando <sin nombre>...';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'LisSolutions';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando <sin nombre>...';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'muekanban';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Iniciando recompilación de la tabla [dbo].[circuito]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_circuito] (
    [cliente_id]              VARCHAR (255) NOT NULL,
    [id_circuito]             VARCHAR (255) NOT NULL,
    [referencia_id]           VARCHAR (255) NOT NULL,
    [activo]                  BIT           NOT NULL,
    [cantidad_piezas]         INT           NOT NULL,
    [cliente_nombre]          VARCHAR (255) NULL,
    [descripcion]             VARCHAR (255) NULL,
    [entrega_dias]            INT           NOT NULL,
    [entrega_horas]           INT           NOT NULL,
    [mail]                    VARCHAR (255) NULL,
    [num_kanban]              INT           NOT NULL,
    [ref_cliente]             VARCHAR (255) NULL,
    [reserva_auto]            BIT           NOT NULL,
    [ubicacion]               VARCHAR (255) NOT NULL,
    [tipo_circuito]           NUMERIC (19)  NOT NULL,
    [tipo_contenedor]         NUMERIC (19)  NOT NULL,
    [Cir_UbiAnt]              VARCHAR (3)   NULL,
    [Cir_LanPedAut]           BIT           NULL,
    [Cir_LunM]                INT           NULL,
    [Cir_LunT]                INT           NULL,
    [Cir_MarM]                INT           NULL,
    [Cir_MarT]                INT           NULL,
    [Cir_MieM]                INT           NULL,
    [Cir_MieT]                INT           NULL,
    [Cir_JueM]                INT           NULL,
    [Cir_JueT]                INT           NULL,
    [Cir_VieM]                INT           NULL,
    [Cir_VieT]                INT           NULL,
    [Proveedor_id]            NCHAR (20)    NULL,
    [Cir_HoraM]               TIME (7)      NULL,
    [Cir_HoraT]               TIME (7)      NULL,
    [Cir_SabM]                INT           NULL,
    [Cir_SabT]                INT           NULL,
    [Cir_DomM]                INT           NULL,
    [Cir_DomT]                INT           NULL,
    [Cir_RAnt]                INT           NULL,
    [Cir_RFec]                DATETIME      NULL,
    [Cir_Consigna]            BIT           NULL,
    [Cir_ConSalesType]        NCHAR (2)     NULL,
    [Cir_ConInventLocationId] NCHAR (25)    NULL,
    [Cir_ConFacturarAut]      INT           NULL,
    [Cir_ConNumFactCli]       NCHAR (50)    NULL,
    [Cir_StdSalesType]        NCHAR (2)     NULL,
    [Cir_StdInventLocationId] NCHAR (25)    NULL,
    [Cir_PedMarco]            NCHAR (20)    NULL,
    [Cir_AlbaranarAut]        INT           NULL,
    [Cir_IntLun]              BIT           NULL,
    [Cir_IntMar]              BIT           NULL,
    [Cir_IntMie]              BIT           NULL,
    [Cir_IntJue]              BIT           NULL,
    [Cir_IntVie]              BIT           NULL,
    [Cir_IntSab]              BIT           NULL,
    [Cir_IntDom]              BIT           NULL,
    [Cir_LanPed5MI]           BIT           NULL,
    [Cir_ConFacturarAgrup]    BIT           NULL,
    [Cir_RegistrarPedCompra]  BIT           NULL,
    [Cir_LecMinParaCons]      INT           CONSTRAINT [DF_circuito_Cir_LecMinParaCons] DEFAULT ((1)) NOT NULL,
    [num_kanbanOPT]           INT           NULL,
    [Cir_ReaprovAgrup]        BIT           NULL,
    [Cir_Empresa]             NCHAR (3)     NULL,
    [Cir_DupConsCir]          VARCHAR (255) NULL,
    [Cir_NumPedCli]           NCHAR (50)    NULL,
    [TransitoActivado]        BIT           NULL,
    [ref_IntegracionERP]      VARCHAR (255) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__circuito__BE13CAC00D7A02861] PRIMARY KEY CLUSTERED ([cliente_id] ASC, [id_circuito] ASC, [referencia_id] ASC, [ubicacion] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[circuito])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion], [activo], [cantidad_piezas], [cliente_nombre], [descripcion], [entrega_dias], [entrega_horas], [mail], [num_kanban], [ref_cliente], [reserva_auto], [tipo_circuito], [tipo_contenedor], [Cir_UbiAnt], [Cir_LanPedAut], [Cir_LunM], [Cir_LunT], [Cir_MarM], [Cir_MarT], [Cir_MieM], [Cir_MieT], [Cir_JueM], [Cir_JueT], [Cir_VieM], [Cir_VieT], [Proveedor_id], [Cir_HoraM], [Cir_HoraT], [Cir_SabM], [Cir_SabT], [Cir_DomM], [Cir_DomT], [Cir_RAnt], [Cir_RFec], [Cir_Consigna], [Cir_ConSalesType], [Cir_ConInventLocationId], [Cir_ConFacturarAut], [Cir_ConNumFactCli], [Cir_StdSalesType], [Cir_StdInventLocationId], [Cir_PedMarco], [Cir_AlbaranarAut], [Cir_IntLun], [Cir_IntMar], [Cir_IntMie], [Cir_IntJue], [Cir_IntVie], [Cir_IntSab], [Cir_IntDom], [Cir_LanPed5MI], [Cir_ConFacturarAgrup], [Cir_RegistrarPedCompra], [Cir_LecMinParaCons], [num_kanbanOPT], [Cir_ReaprovAgrup], [Cir_Empresa], [Cir_DupConsCir], [Cir_NumPedCli], [ref_IntegracionERP], [TransitoActivado])
        SELECT   [cliente_id],
                 [id_circuito],
                 [referencia_id],
                 [ubicacion],
                 [activo],
                 [cantidad_piezas],
                 [cliente_nombre],
                 [descripcion],
                 [entrega_dias],
                 [entrega_horas],
                 [mail],
                 [num_kanban],
                 [ref_cliente],
                 [reserva_auto],
                 [tipo_circuito],
                 [tipo_contenedor],
                 [Cir_UbiAnt],
                 [Cir_LanPedAut],
                 [Cir_LunM],
                 [Cir_LunT],
                 [Cir_MarM],
                 [Cir_MarT],
                 [Cir_MieM],
                 [Cir_MieT],
                 [Cir_JueM],
                 [Cir_JueT],
                 [Cir_VieM],
                 [Cir_VieT],
                 [Proveedor_id],
                 [Cir_HoraM],
                 [Cir_HoraT],
                 [Cir_SabM],
                 [Cir_SabT],
                 [Cir_DomM],
                 [Cir_DomT],
                 [Cir_RAnt],
                 [Cir_RFec],
                 [Cir_Consigna],
                 [Cir_ConSalesType],
                 [Cir_ConInventLocationId],
                 [Cir_ConFacturarAut],
                 [Cir_ConNumFactCli],
                 [Cir_StdSalesType],
                 [Cir_StdInventLocationId],
                 [Cir_PedMarco],
                 [Cir_AlbaranarAut],
                 [Cir_IntLun],
                 [Cir_IntMar],
                 [Cir_IntMie],
                 [Cir_IntJue],
                 [Cir_IntVie],
                 [Cir_IntSab],
                 [Cir_IntDom],
                 [Cir_LanPed5MI],
                 [Cir_ConFacturarAgrup],
                 [Cir_RegistrarPedCompra],
                 [Cir_LecMinParaCons],
                 [num_kanbanOPT],
                 [Cir_ReaprovAgrup],
                 [Cir_Empresa],
                 [Cir_DupConsCir],
                 [Cir_NumPedCli],
                 [ref_IntegracionERP],
                 [TransitoActivado]
        FROM     [dbo].[circuito]
        ORDER BY [cliente_id] ASC, [id_circuito] ASC, [referencia_id] ASC, [ubicacion] ASC;
    END

DROP TABLE [dbo].[circuito];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_circuito]', N'circuito';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__circuito__BE13CAC00D7A02861]', N'PK__circuito__BE13CAC00D7A0286', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[circuito].[UbicClienteRef]...';


GO
CREATE NONCLUSTERED INDEX [UbicClienteRef]
    ON [dbo].[circuito]([cliente_id] ASC, [referencia_id] ASC, [ubicacion] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[circuito].[ActivoUbicacion]...';


GO
CREATE NONCLUSTERED INDEX [ActivoUbicacion]
    ON [dbo].[circuito]([ubicacion] ASC, [activo] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[circuito].[Ubicacion]...';


GO
CREATE NONCLUSTERED INDEX [Ubicacion]
    ON [dbo].[circuito]([ubicacion] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[circuito].[activoTipoCircuito]...';


GO
CREATE NONCLUSTERED INDEX [activoTipoCircuito]
    ON [dbo].[circuito]([tipo_circuito] ASC, [activo] ASC)
    INCLUDE([cliente_id], [id_circuito], [referencia_id], [num_kanban], [ref_cliente], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[circuito].[IdCircuito]...';


GO
CREATE NONCLUSTERED INDEX [IdCircuito]
    ON [dbo].[circuito]([id_circuito] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[p_situacion_tarjetas]...';


GO
ALTER TABLE [dbo].[p_situacion_tarjetas]
    ADD [transito_activado] INT NULL,
        [ctdKan]            INT NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[p_valid_acceso]...';


GO
ALTER TABLE [dbo].[p_valid_acceso]
    ADD [auth_token_expiration_mins]    VARCHAR (255) NULL,
        [refresh_token_expiration_mins] VARCHAR (255) NULL,
        [id_sububicacion]               VARCHAR (255) NULL,
        [sub_ubicaciondesc]             VARCHAR (255) NULL,
        [authtokenexpirationmins]       VARCHAR (255) NULL,
        [refrestokenexpirationmins]     VARCHAR (255) NULL,
        [idi]                           VARCHAR (255) NULL,
        [orden]                         VARCHAR (255) NULL,
        [sububicacion]                  VARCHAR (255) NULL,
        [id_sub]                        VARCHAR (255) NULL,
        [sububicaciondesc]              VARCHAR (255) NULL,
        [refreshtokenexpirationmins]    VARCHAR (255) NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Iniciando recompilación de la tabla [dbo].[sga_usuario]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_sga_usuario] (
    [cod_usuario]                 NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [activo]                      TINYINT       NOT NULL,
    [conectado]                   TINYINT       NOT NULL,
    [nombre]                      VARCHAR (255) NULL,
    [apellidos]                   VARCHAR (255) NULL,
    [username]                    VARCHAR (255) NOT NULL,
    [mail]                        VARCHAR (255) NULL,
    [optimistic_lock]             NUMERIC (19)  NOT NULL,
    [password]                    VARCHAR (255) NULL,
    [puesto]                      NUMERIC (19)  NULL,
    [ptr_rol]                     NUMERIC (19)  NOT NULL,
    [Usu_Mov]                     NCHAR (100)   NULL,
    [Usu_MovSer]                  NCHAR (50)    NULL,
    [Usu_MovFecDesde]             DATE          NULL,
    [Usu_SIM]                     NCHAR (50)    NULL,
    [Usu_Observaciones]           NCHAR (255)   NULL,
    [Usu_Emp]                     INT           CONSTRAINT [DF_sga_usuario_Usu_Emp] DEFAULT ((1)) NULL,
    [Usu_Idi]                     NCHAR (2)     NULL,
    [conectUltFec]                DATETIME      NULL,
    [Usu_TimeZone]                VARCHAR (35)  NULL,
    [Usu_Token]                   VARCHAR (50)  NULL,
    [Usu_FacConsig]               BIT           CONSTRAINT [DF_sga_usuario_Usu_FacConsig] DEFAULT ((0)) NULL,
    [Usu_Dominio]                 NCHAR (30)    NULL,
    [TokenAuten_CaducidadMins]    INT           NULL,
    [TokenRefresco_CaducidadMins] INT           NULL,
    [Usu_PasarelaActivada]        BIT           NULL,
    [FTP_Server]                  VARCHAR (255) NULL,
    [FTP_Username]                VARCHAR (255) NULL,
    [FTP_Password]                VARCHAR (255) NULL,
    [Usu_SubirDatosAFTP]          BIT           NULL,
    [isDBAdmin]                   BIT           NULL,
    PRIMARY KEY CLUSTERED ([cod_usuario] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[sga_usuario])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_sga_usuario] ON;
        INSERT INTO [dbo].[tmp_ms_xx_sga_usuario] ([cod_usuario], [activo], [conectado], [nombre], [apellidos], [username], [mail], [optimistic_lock], [password], [puesto], [ptr_rol], [Usu_Mov], [Usu_MovSer], [Usu_MovFecDesde], [Usu_SIM], [Usu_Observaciones], [Usu_Emp], [Usu_Idi], [conectUltFec], [Usu_TimeZone], [Usu_Token], [Usu_FacConsig], [Usu_Dominio], [Usu_PasarelaActivada], [FTP_Server], [FTP_Username], [FTP_Password], [Usu_SubirDatosAFTP], [TokenAuten_CaducidadMins], [TokenRefresco_CaducidadMins], [isDBAdmin])
        SELECT   [cod_usuario],
                 [activo],
                 [conectado],
                 [nombre],
                 [apellidos],
                 [username],
                 [mail],
                 [optimistic_lock],
                 [password],
                 [puesto],
                 [ptr_rol],
                 [Usu_Mov],
                 [Usu_MovSer],
                 [Usu_MovFecDesde],
                 [Usu_SIM],
                 [Usu_Observaciones],
                 [Usu_Emp],
                 [Usu_Idi],
                 [conectUltFec],
                 [Usu_TimeZone],
                 [Usu_Token],
                 [Usu_FacConsig],
                 [Usu_Dominio],
                 [Usu_PasarelaActivada],
                 [FTP_Server],
                 [FTP_Username],
                 [FTP_Password],
                 [Usu_SubirDatosAFTP],
                 [TokenAuten_CaducidadMins],
                 [TokenRefresco_CaducidadMins],
                 [isDBAdmin]
        FROM     [dbo].[sga_usuario]
        ORDER BY [cod_usuario] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_sga_usuario] OFF;
    END

DROP TABLE [dbo].[sga_usuario];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_sga_usuario]', N'sga_usuario';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[THistoricoVerdes]...';


GO
ALTER TABLE [dbo].[THistoricoVerdes]
    ADD [origen]  VARCHAR (255) NULL,
        [usuario] VARCHAR (255) NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Iniciando recompilación de la tabla [dbo].[ubicacion]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_ubicacion] (
    [id_ubicacion]     VARCHAR (255) NOT NULL,
    [activo]           TINYINT       NOT NULL,
    [descripcion]      VARCHAR (255) NULL,
    [localizacion]     VARCHAR (255) NULL,
    [Ubi_DesLoc]       VARCHAR (255) NULL,
    [Ubi_MosPenFac]    BIT           NULL,
    [Ubi_MsgPenFac]    NCHAR (150)   NULL,
    [Ubi_RegTrazAX]    BIT           NULL,
    [Ubi_Dataareaid]   NCHAR (10)    NULL,
    [Ubi_Modo]         NCHAR (4)     CONSTRAINT [DF_ubicacion_Ubi_Modo] DEFAULT (N'EVEN') NULL,
    [Ubi_NotEtiVacias] BIT           CONSTRAINT [DF_ubicacion_Ubi_NotEtiVacias] DEFAULT ((1)) NULL,
    [ubi_des_loc]      VARCHAR (255) NULL,
    [TipoEtiqueta]     VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id_ubicacion] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ubicacion])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ubicacion] ([id_ubicacion], [activo], [descripcion], [localizacion], [Ubi_DesLoc], [Ubi_MosPenFac], [Ubi_MsgPenFac], [Ubi_RegTrazAX], [Ubi_Dataareaid], [Ubi_Modo], [Ubi_NotEtiVacias], [TipoEtiqueta])
        SELECT   [id_ubicacion],
                 [activo],
                 [descripcion],
                 [localizacion],
                 [Ubi_DesLoc],
                 [Ubi_MosPenFac],
                 [Ubi_MsgPenFac],
                 [Ubi_RegTrazAX],
                 [Ubi_Dataareaid],
                 [Ubi_Modo],
                 [Ubi_NotEtiVacias],
                 [TipoEtiqueta]
        FROM     [dbo].[ubicacion]
        ORDER BY [id_ubicacion] ASC;
    END

DROP TABLE [dbo].[ubicacion];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ubicacion]', N'ubicacion';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[Borrar]...';


GO
CREATE TABLE [dbo].[Borrar] (
    [xx] NCHAR (10) NULL
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[Historico_Dispositivo]...';


GO
CREATE TABLE [dbo].[Historico_Dispositivo] (
    [id_dispositivo]  VARCHAR (255) NOT NULL,
    [ip]              VARCHAR (255) NULL,
    [ultima_conexion] DATETIME      NULL,
    [version]         VARCHAR (255) NULL,
    [Dis_EnError]     BIT           NULL,
    [Dis_SIM]         NCHAR (100)   NULL,
    [Dis_IPLocal]     NCHAR (100)   NULL,
    [Dis_NotErrMin]   INT           NULL,
    [Dis_NotErrMail]  NCHAR (100)   NULL,
    [Dis_NotErrSta]   INT           NULL,
    [Dis_Tipo]        NCHAR (4)     NULL,
    [Dis_ActAnt0]     BIT           NULL,
    [Dis_ActAnt1]     BIT           NULL,
    [Fecha]           DATE          NULL,
    CONSTRAINT [PK_historico_disposit] PRIMARY KEY CLUSTERED ([id_dispositivo] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_acc_anular_lectura]...';


GO
CREATE TABLE [dbo].[p_acc_anular_lectura] (
    [result] INT           NOT NULL,
    [msg]    VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([result] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_acc_anular_lectura2]...';


GO
CREATE TABLE [dbo].[p_acc_anular_lectura2] (
    [result] INT           NOT NULL,
    [msg]    VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([result] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_acc_reg_lectura]...';


GO
CREATE TABLE [dbo].[p_acc_reg_lectura] (
    [result]       INT           NOT NULL,
    [msg]          VARCHAR (255) NULL,
    [id_circuito]  VARCHAR (255) NULL,
    [circuito]     VARCHAR (255) NULL,
    [mid_circuito] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([result] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_consumos_por_ubicacion]...';


GO
CREATE TABLE [dbo].[p_consumos_por_ubicacion] (
    [id_sububicacion] VARCHAR (255) NOT NULL,
    [referencia_id]   VARCHAR (255) NOT NULL,
    [cantidad]        NUMERIC (19)  NULL,
    [ref_cliente]     VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id_sububicacion] ASC, [referencia_id] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_cta_his_consumidas]...';


GO
CREATE TABLE [dbo].[p_cta_his_consumidas] (
    [lote]          VARCHAR (255) NOT NULL,
    [estado]        VARCHAR (255) NULL,
    [fecha_lectura] DATETIME      NULL,
    [sububicacion]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([lote] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_leer_ficha]...';


GO
CREATE TABLE [dbo].[p_leer_ficha] (
    [artcli]           VARCHAR (255) NOT NULL,
    [art]              VARCHAR (255) NULL,
    [ctdcaja]          INT           NULL,
    [descrip]          VARCHAR (255) NULL,
    [des]              VARCHAR (255) NULL,
    [eng]              VARCHAR (255) NULL,
    [fecprod]          VARCHAR (255) NULL,
    [lin]              VARCHAR (255) NULL,
    [ofab]             VARCHAR (255) NULL,
    [tctdcaja]         VARCHAR (255) NULL,
    [tdes]             VARCHAR (255) NULL,
    [teng]             VARCHAR (255) NULL,
    [articulo_cliente] VARCHAR (255) NOT NULL,
    [articulo]         VARCHAR (255) NULL,
    [deseng]           VARCHAR (255) NULL,
    [fprod]            VARCHAR (255) NULL,
    [linea]            VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([artcli] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_numero_optimo_con_consumos]...';


GO
CREATE TABLE [dbo].[p_numero_optimo_con_consumos] (
    [ubi]    VARCHAR (255) NOT NULL,
    [art]    VARCHAR (255) NOT NULL,
    [etiq]   VARCHAR (255) NOT NULL,
    [cli]    VARCHAR (255) NOT NULL,
    [año]    VARCHAR (255) NULL,
    [color]  VARCHAR (255) NULL,
    [ctdsem] FLOAT (53)    NULL,
    [emp]    VARCHAR (255) NULL,
    [sem]    VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ubi] ASC, [art] ASC, [etiq] ASC, [cli] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[p_pc_ta_app_versiones]...';


GO
CREATE TABLE [dbo].[p_pc_ta_app_versiones] (
    [num_version]    VARCHAR (255) NOT NULL,
    [descripcion]    VARCHAR (255) NULL,
    [fecha_creacion] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([num_version] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_circuito]...';


GO
CREATE TABLE [dbo].[pruebapro_circuito] (
    [cliente_id]              VARCHAR (255) NOT NULL,
    [id_circuito]             VARCHAR (255) NOT NULL,
    [referencia_id]           VARCHAR (255) NOT NULL,
    [activo]                  BIT           NOT NULL,
    [cantidad_piezas]         INT           NOT NULL,
    [cliente_nombre]          VARCHAR (255) NULL,
    [descripcion]             VARCHAR (255) NULL,
    [entrega_dias]            INT           NOT NULL,
    [entrega_horas]           INT           NOT NULL,
    [mail]                    VARCHAR (255) NULL,
    [num_kanban]              INT           NOT NULL,
    [ref_cliente]             VARCHAR (255) NULL,
    [reserva_auto]            BIT           NOT NULL,
    [ubicacion]               VARCHAR (255) NOT NULL,
    [tipo_circuito]           NUMERIC (19)  NOT NULL,
    [tipo_contenedor]         NUMERIC (19)  NOT NULL,
    [Cir_UbiAnt]              VARCHAR (3)   NULL,
    [Cir_LanPedAut]           BIT           NULL,
    [Cir_LunM]                INT           NULL,
    [Cir_LunT]                INT           NULL,
    [Cir_MarM]                INT           NULL,
    [Cir_MarT]                INT           NULL,
    [Cir_MieM]                INT           NULL,
    [Cir_MieT]                INT           NULL,
    [Cir_JueM]                INT           NULL,
    [Cir_JueT]                INT           NULL,
    [Cir_VieM]                INT           NULL,
    [Cir_VieT]                INT           NULL,
    [Proveedor_id]            NCHAR (20)    NULL,
    [Cir_HoraM]               TIME (7)      NULL,
    [Cir_HoraT]               TIME (7)      NULL,
    [Cir_SabM]                INT           NULL,
    [Cir_SabT]                INT           NULL,
    [Cir_DomM]                INT           NULL,
    [Cir_DomT]                INT           NULL,
    [Cir_RAnt]                INT           NULL,
    [Cir_RFec]                DATETIME      NULL,
    [Cir_Consigna]            BIT           NULL,
    [Cir_ConSalesType]        NCHAR (2)     NULL,
    [Cir_ConInventLocationId] NCHAR (25)    NULL,
    [Cir_ConFacturarAut]      INT           NULL,
    [Cir_ConNumFactCli]       NCHAR (50)    NULL,
    [Cir_StdSalesType]        NCHAR (2)     NULL,
    [Cir_StdInventLocationId] NCHAR (25)    NULL,
    [Cir_PedMarco]            NCHAR (20)    NULL,
    [Cir_AlbaranarAut]        INT           NULL,
    [Cir_IntLun]              BIT           NULL,
    [Cir_IntMar]              BIT           NULL,
    [Cir_IntMie]              BIT           NULL,
    [Cir_IntJue]              BIT           NULL,
    [Cir_IntVie]              BIT           NULL,
    [Cir_IntSab]              BIT           NULL,
    [Cir_IntDom]              BIT           NULL,
    [Cir_LanPed5MI]           BIT           NULL,
    [Cir_ConFacturarAgrup]    BIT           NULL,
    [Cir_RegistrarPedCompra]  BIT           NULL,
    [Cir_LecMinParaCons]      INT           NOT NULL,
    [num_kanbanOPT]           INT           NULL,
    [Cir_ReaprovAgrup]        BIT           NULL,
    [Cir_Empresa]             NCHAR (3)     NULL,
    [Cir_DupConsCir]          VARCHAR (255) NULL,
    [Cir_NumPedCli]           NCHAR (50)    NULL,
    [ref_IntegracionERP]      VARCHAR (255) NULL,
    [TransitoActivado]        BIT           NULL,
    CONSTRAINT [PK_pruebapro_circu] PRIMARY KEY CLUSTERED ([cliente_id] ASC, [id_circuito] ASC, [referencia_id] ASC, [ubicacion] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_circuito].[activoTipoCircuito]...';


GO
CREATE NONCLUSTERED INDEX [activoTipoCircuito]
    ON [dbo].[pruebapro_circuito]([tipo_circuito] ASC, [activo] ASC)
    INCLUDE([cliente_id], [id_circuito], [num_kanban], [ref_cliente], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_circuito].[Ubicacion]...';


GO
CREATE NONCLUSTERED INDEX [Ubicacion]
    ON [dbo].[pruebapro_circuito]([ubicacion] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_circuito].[ActivoUbicacion]...';


GO
CREATE NONCLUSTERED INDEX [ActivoUbicacion]
    ON [dbo].[pruebapro_circuito]([ubicacion] ASC, [activo] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_circuito].[IdCircuito]...';


GO
CREATE NONCLUSTERED INDEX [IdCircuito]
    ON [dbo].[pruebapro_circuito]([id_circuito] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_dispositivo]...';


GO
CREATE TABLE [dbo].[pruebapro_dispositivo] (
    [id_dispositivo]      VARCHAR (255)  NOT NULL,
    [activo]              BIT            NOT NULL,
    [imei]                VARCHAR (255)  NULL,
    [num_serie]           VARCHAR (255)  NULL,
    [descripcion]         VARCHAR (255)  NULL,
    [Dis_UltAccFec]       DATETIME       NULL,
    [Dis_UltAccUsu]       NCHAR (50)     NULL,
    [Dis_UltAccVer]       NCHAR (20)     NULL,
    [ultimo_inventario]   DATETIME       NULL,
    [ip]                  VARCHAR (255)  NULL,
    [ultima_conexion]     DATETIME       NULL,
    [version]             VARCHAR (255)  NULL,
    [ant_inventario]      BIT            NULL,
    [realizar_inventario] BIT            NULL,
    [con_sensor]          BIT            NULL,
    [Dis_EnError]         BIT            NULL,
    [Dis_SIM]             NCHAR (100)    NULL,
    [Dis_IPLocal]         NCHAR (100)    NULL,
    [Dis_NotErrMin]       INT            NULL,
    [Dis_NotErrMail]      NCHAR (100)    NULL,
    [Dis_NotErrSta]       INT            NULL,
    [fecha_inicio]        DATETIME       NULL,
    [Dis_Obs]             NVARCHAR (MAX) NULL,
    [Dis_Tipo]            NCHAR (4)      NULL,
    [Dis_ActAnt0]         BIT            NULL,
    [Dis_ActAnt1]         BIT            NULL,
    CONSTRAINT [PK__pruebapro_disposit__FD7B94E53493CFA7] PRIMARY KEY CLUSTERED ([id_dispositivo] ASC) WITH (ALLOW_PAGE_LOCKS = OFF)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_dispositivo].[Id_dispositivo]...';


GO
CREATE NONCLUSTERED INDEX [Id_dispositivo]
    ON [dbo].[pruebapro_dispositivo]([id_dispositivo] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_dispositivo].[Indice_NumSerie]...';


GO
CREATE NONCLUSTERED INDEX [Indice_NumSerie]
    ON [dbo].[pruebapro_dispositivo]([num_serie] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_dispositivo_ubicacion]...';


GO
CREATE TABLE [dbo].[pruebapro_dispositivo_ubicacion] (
    [id]             NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [prioridad]      INT           NOT NULL,
    [id_dispositivo] VARCHAR (255) NOT NULL,
    [id_ubicacion]   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_pruebapro__disposit__3213E83F2BFE89A6] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_sga_usuario]...';


GO
CREATE TABLE [dbo].[pruebapro_sga_usuario] (
    [cod_usuario]                 NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [activo]                      TINYINT       NOT NULL,
    [conectado]                   TINYINT       NOT NULL,
    [nombre]                      VARCHAR (255) NULL,
    [apellidos]                   VARCHAR (255) NULL,
    [username]                    VARCHAR (255) NOT NULL,
    [mail]                        VARCHAR (255) NULL,
    [optimistic_lock]             NUMERIC (19)  NOT NULL,
    [password]                    VARCHAR (255) NULL,
    [puesto]                      NUMERIC (19)  NULL,
    [ptr_rol]                     NUMERIC (19)  NOT NULL,
    [Usu_Mov]                     NCHAR (100)   NULL,
    [Usu_MovSer]                  NCHAR (50)    NULL,
    [Usu_MovFecDesde]             DATE          NULL,
    [Usu_SIM]                     NCHAR (50)    NULL,
    [Usu_Observaciones]           NCHAR (255)   NULL,
    [Usu_Emp]                     INT           NULL,
    [Usu_Idi]                     NCHAR (2)     NULL,
    [conectUltFec]                DATETIME      NULL,
    [Usu_TimeZone]                VARCHAR (35)  NULL,
    [Usu_Token]                   VARCHAR (50)  NULL,
    [Usu_FacConsig]               BIT           NULL,
    [Usu_Dominio]                 NCHAR (30)    NULL,
    [Usu_PasarelaActivada]        BIT           NULL,
    [FTP_Server]                  VARCHAR (255) NULL,
    [FTP_Username]                VARCHAR (255) NULL,
    [FTP_Password]                VARCHAR (255) NULL,
    [Usu_SubirDatosAFTP]          BIT           NULL,
    [TokenAuten_CaducidadMins]    INT           NULL,
    [TokenRefresco_CaducidadMins] INT           NULL,
    [isDBAdmin]                   BIT           NULL,
    CONSTRAINT [PK__pruebapro_sga_usua__EA3C9B1A276] PRIMARY KEY CLUSTERED ([cod_usuario] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_tarjeta]...';


GO
CREATE TABLE [dbo].[pruebapro_tarjeta] (
    [id_lectura]        VARCHAR (255) NOT NULL,
    [antena]            VARCHAR (255) NULL,
    [estado]            VARCHAR (255) NULL,
    [fecha_lectura]     DATETIME      NULL,
    [gestionado]        VARCHAR (255) NULL,
    [movil]             VARCHAR (255) NULL,
    [tipo]              VARCHAR (255) NULL,
    [cliente_id]        VARCHAR (255) NULL,
    [id_circuito]       VARCHAR (255) NULL,
    [referencia_id]     VARCHAR (255) NULL,
    [ubicacion]         VARCHAR (255) NULL,
    [num_lecturas]      INT           NULL,
    [ultima_lectura]    DATETIME      NULL,
    [sentido]           VARCHAR (10)  NOT NULL,
    [kill_tag]          BIT           NULL,
    [kill_enviado]      DATETIME      NULL,
    [Tar_GestionadoUSU] NCHAR (30)    NULL,
    [Tar_GestionadoFEC] DATETIME      NULL,
    [Tar_CTDRealLeida]  VARCHAR (25)  NULL,
    [Tar_IdPedido]      NCHAR (50)    NULL,
    [lote]              VARCHAR (255) NULL,
    [Tar_FecEntSol]     DATETIME      NULL,
    [Tar_FacFec]        DATETIME      NULL,
    [Tar_Facturado]     BIT           NOT NULL,
    [sububicacion]      VARCHAR (255) NULL,
    CONSTRAINT [PK_pruebapro__tarjeta__E9687596114A936A] PRIMARY KEY CLUSTERED ([id_lectura] ASC, [sentido] ASC) WITH (ALLOW_PAGE_LOCKS = OFF)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_tarjeta].[LecturaFecha]...';


GO
CREATE NONCLUSTERED INDEX [LecturaFecha]
    ON [dbo].[pruebapro_tarjeta]([estado] ASC, [gestionado] ASC, [id_circuito] ASC)
    INCLUDE([fecha_lectura], [id_lectura]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_tarjeta].[TarjetaEstado]...';


GO
CREATE NONCLUSTERED INDEX [TarjetaEstado]
    ON [dbo].[pruebapro_tarjeta]([estado] ASC, [id_circuito] ASC, [sentido] ASC, [gestionado] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_tarjeta].[IdCircuito]...';


GO
CREATE NONCLUSTERED INDEX [IdCircuito]
    ON [dbo].[pruebapro_tarjeta]([id_circuito] ASC)
    INCLUDE([estado], [gestionado]) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_tarjeta].[IdLectura]...';


GO
CREATE NONCLUSTERED INDEX [IdLectura]
    ON [dbo].[pruebapro_tarjeta]([id_lectura] ASC, [sentido] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_tarjeta].[tarjetaSentidoEstadoCircuito]...';


GO
CREATE NONCLUSTERED INDEX [tarjetaSentidoEstadoCircuito]
    ON [dbo].[pruebapro_tarjeta]([sentido] ASC, [estado] ASC, [id_circuito] ASC)
    INCLUDE([fecha_lectura], [gestionado], [ultima_lectura]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_TRegistro]...';


GO
CREATE TABLE [dbo].[pruebapro_TRegistro] (
    [Reg_Fec] DATETIME    NULL,
    [Reg_Usu] NCHAR (255) NULL,
    [Reg_Ubi] NCHAR (255) NULL,
    [Reg_opt] NCHAR (255) NULL,
    [Reg_Art] NCHAR (255) NULL,
    [Reg_Cli] NCHAR (255) NULL,
    [Reg_Txt] NCHAR (255) NULL
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_TRegistro].[Ind_Fecha]...';


GO
CREATE NONCLUSTERED INDEX [Ind_Fecha]
    ON [dbo].[pruebapro_TRegistro]([Reg_Fec] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_TRegistroLecturas]...';


GO
CREATE TABLE [dbo].[pruebapro_TRegistroLecturas] (
    [Reg_Fec] DATETIME    NULL,
    [Reg_Dis] NCHAR (100) NULL,
    [Reg_Usu] NCHAR (100) NULL,
    [Reg_Ubi] NCHAR (100) NULL,
    [Reg_Pan] NCHAR (100) NULL,
    [Reg_Txt] NCHAR (255) NULL,
    [Reg_Cir] NCHAR (100) NULL,
    [Reg_Cli] NCHAR (100) NULL,
    [Reg_Art] NCHAR (100) NULL,
    [Reg_Lot] NCHAR (100) NULL,
    [Reg_Ser] NCHAR (100) NULL,
    [Reg_Ant] NCHAR (50)  NULL,
    [Reg_Sen] NCHAR (5)   NULL,
    [Reg_Tip] INT         NULL,
    [Reg_Ctd] NCHAR (50)  NULL,
    [Reg_Emp] NCHAR (10)  NULL
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_TRegistroLecturas].[Fecha]...';


GO
CREATE NONCLUSTERED INDEX [Fecha]
    ON [dbo].[pruebapro_TRegistroLecturas]([Reg_Fec] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_TRegistroLecturas].[Index]...';


GO
CREATE NONCLUSTERED INDEX [Index]
    ON [dbo].[pruebapro_TRegistroLecturas]([Reg_Lot] ASC, [Reg_Ser] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_ubicacion]...';


GO
CREATE TABLE [dbo].[pruebapro_ubicacion] (
    [id_ubicacion]     VARCHAR (255) NOT NULL,
    [activo]           TINYINT       NOT NULL,
    [descripcion]      VARCHAR (255) NULL,
    [localizacion]     VARCHAR (255) NULL,
    [Ubi_DesLoc]       VARCHAR (255) NULL,
    [Ubi_MosPenFac]    BIT           NULL,
    [Ubi_MsgPenFac]    NCHAR (150)   NULL,
    [Ubi_RegTrazAX]    BIT           NULL,
    [Ubi_Dataareaid]   NCHAR (10)    NULL,
    [Ubi_Modo]         NCHAR (4)     NULL,
    [Ubi_NotEtiVacias] BIT           NULL,
    [ubi_des_loc]      VARCHAR (255) NULL,
    [TipoEtiqueta]     VARCHAR (255) NULL,
    CONSTRAINT [PK_pruebapro__ubicacio__81BAA59137A5467C] PRIMARY KEY CLUSTERED ([id_ubicacion] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_usuario_AccReports]...';


GO
CREATE TABLE [dbo].[pruebapro_usuario_AccReports] (
    [id]         NUMERIC (19) IDENTITY (1, 1) NOT NULL,
    [id_usuario] NUMERIC (19) NOT NULL,
    [id_accrep]  NCHAR (9)    NOT NULL,
    [prioridad]  INT          NULL,
    CONSTRAINT [PKaa_usuario_AccReports] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_version_app]...';


GO
CREATE TABLE [dbo].[pruebapro_version_app] (
    [num_version]    VARCHAR (255) NOT NULL,
    [descripcion]    VARCHAR (255) NULL,
    [fecha_creacion] DATETIME      NULL,
    [isAppNueva]     BIT           NULL,
    [URLDescarga]    VARCHAR (255) NULL,
    CONSTRAINT [PK_pruebapro__version_A2FD328247A6A41B] PRIMARY KEY CLUSTERED ([num_version] ASC)
);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PK__disposit__FD7B94E53493CFA7]...';


GO
ALTER INDEX [PK__disposit__FD7B94E53493CFA7]
    ON [dbo].[dispositivo] SET (ALLOW_PAGE_LOCKS = ON);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando restricción sin nombre en [dbo].[dispositivo_ubicacion]...';


GO
ALTER INDEX [PK__disposit__3213E83F2BFE89A6]
    ON [dbo].[dispositivo_ubicacion] SET (ALLOW_PAGE_LOCKS = ON);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[PK_usuario_AccReports]...';


GO
ALTER TABLE [dbo].[usuario_AccReports]
    ADD CONSTRAINT [PK_usuario_AccReports] PRIMARY KEY CLUSTERED ([id] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[tarjeta].[IdCircuito]...';


GO
CREATE NONCLUSTERED INDEX [IdCircuito]
    ON [dbo].[tarjeta]([id_circuito] ASC)
    INCLUDE([estado], [gestionado]) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[tarjeta].[IdLectura]...';


GO
CREATE NONCLUSTERED INDEX [IdLectura]
    ON [dbo].[tarjeta]([id_lectura] ASC, [sentido] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[tarjeta].[LecturaFecha]...';


GO
CREATE NONCLUSTERED INDEX [LecturaFecha]
    ON [dbo].[tarjeta]([estado] ASC, [gestionado] ASC, [id_circuito] ASC)
    INCLUDE([id_lectura], [fecha_lectura]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[tarjeta].[TarjetaEstado]...';


GO
CREATE NONCLUSTERED INDEX [TarjetaEstado]
    ON [dbo].[tarjeta]([estado] ASC, [id_circuito] ASC, [sentido] ASC, [gestionado] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[tarjeta].[tarjetaSentidoEstadoCircuito]...';


GO
CREATE NONCLUSTERED INDEX [tarjetaSentidoEstadoCircuito]
    ON [dbo].[tarjeta]([sentido] ASC, [estado] ASC, [id_circuito] ASC)
    INCLUDE([fecha_lectura], [gestionado], [ultima_lectura]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[TRegistro].[Ind_Fecha]...';


GO
CREATE NONCLUSTERED INDEX [Ind_Fecha]
    ON [dbo].[TRegistro]([Reg_Fec] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[TRegistroLecturas].[Fecha]...';


GO
CREATE NONCLUSTERED INDEX [Fecha]
    ON [dbo].[TRegistroLecturas]([Reg_Fec] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[TRegistroLecturas].[Index]...';


GO
CREATE NONCLUSTERED INDEX [Index]
    ON [dbo].[TRegistroLecturas]([Reg_Lot] ASC, [Reg_Ser] ASC);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[DF_pruebapro_circuito_Cir_LecMinParaCons]...';


GO
ALTER TABLE [dbo].[pruebapro_circuito]
    ADD CONSTRAINT [DF_pruebapro_circuito_Cir_LecMinParaCons] DEFAULT ((1)) FOR [Cir_LecMinParaCons];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[DF_pruebapro_sga_usuario_Usu_Emp]...';


GO
ALTER TABLE [dbo].[pruebapro_sga_usuario]
    ADD CONSTRAINT [DF_pruebapro_sga_usuario_Usu_Emp] DEFAULT ((1)) FOR [Usu_Emp];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[DF_pruebapro_sga_usuario_Usu_FacConsig]...';


GO
ALTER TABLE [dbo].[pruebapro_sga_usuario]
    ADD CONSTRAINT [DF_pruebapro_sga_usuario_Usu_FacConsig] DEFAULT ((0)) FOR [Usu_FacConsig];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[DF_pruebapro_tarjeta_Tar_Facturado]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta]
    ADD CONSTRAINT [DF_pruebapro_tarjeta_Tar_Facturado] DEFAULT ((0)) FOR [Tar_Facturado];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[DF_pruebapro_ubicacion_Ubi_Modo]...';


GO
ALTER TABLE [dbo].[pruebapro_ubicacion]
    ADD CONSTRAINT [DF_pruebapro_ubicacion_Ubi_Modo] DEFAULT (N'EVEN') FOR [Ubi_Modo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[DF_pruebapro_ubicacion_Ubi_NotEtiVacias]...';


GO
ALTER TABLE [dbo].[pruebapro_ubicacion]
    ADD CONSTRAINT [DF_pruebapro_ubicacion_Ubi_NotEtiVacias] DEFAULT ((1)) FOR [Ubi_NotEtiVacias];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_h4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK_h4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_iejhi9irkh7tx1ji2y8t5k47k]...';


GO
ALTER TABLE [dbo].[circuito] WITH NOCHECK
    ADD CONSTRAINT [FK_iejhi9irkh7tx1ji2y8t5k47k] FOREIGN KEY ([ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_3vn6jckmib1of620by1jg35a6]...';


GO
ALTER TABLE [dbo].[circuito] WITH NOCHECK
    ADD CONSTRAINT [FK_3vn6jckmib1of620by1jg35a6] FOREIGN KEY ([tipo_circuito]) REFERENCES [dbo].[tipo_circuito] ([id]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_gpat2w8x7tswwv6tmkaq9u1po]...';


GO
ALTER TABLE [dbo].[circuito] WITH NOCHECK
    ADD CONSTRAINT [FK_gpat2w8x7tswwv6tmkaq9u1po] FOREIGN KEY ([tipo_contenedor]) REFERENCES [dbo].[tipo_contenedor] ([id]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_usuario_cliente_sga_usuario1]...';


GO
ALTER TABLE [dbo].[usuario_cliente] WITH NOCHECK
    ADD CONSTRAINT [FK_usuario_cliente_sga_usuario1] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_usuario_proveedor_sga_usuario]...';


GO
ALTER TABLE [dbo].[usuario_proveedor] WITH NOCHECK
    ADD CONSTRAINT [FK_usuario_proveedor_sga_usuario] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FKDEDB797C40F68DC8]...';


GO
ALTER TABLE [dbo].[sga_usuario] WITH NOCHECK
    ADD CONSTRAINT [FKDEDB797C40F68DC8] FOREIGN KEY ([ptr_rol]) REFERENCES [dbo].[sga_rol] ([cod_rol]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FKDEDB797CDAB65181]...';


GO
ALTER TABLE [dbo].[sga_usuario] WITH NOCHECK
    ADD CONSTRAINT [FKDEDB797CDAB65181] FOREIGN KEY ([puesto]) REFERENCES [dbo].[sga_puesto] ([cod_puesto]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FKEB3B4B51F04FDB86]...';


GO
ALTER TABLE [dbo].[sga_usuario_puesto] WITH NOCHECK
    ADD CONSTRAINT [FKEB3B4B51F04FDB86] FOREIGN KEY ([ptr_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK5577C8CE315BB8B3]...';


GO
ALTER TABLE [dbo].[usuario_ubicacion] WITH NOCHECK
    ADD CONSTRAINT [FK5577C8CE315BB8B3] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_hsx52j13m5icy3spj1hormj1y]...';


GO
ALTER TABLE [dbo].[dispositivo_ubicacion] WITH NOCHECK
    ADD CONSTRAINT [FK_hsx52j13m5icy3spj1hormj1y] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK5577C8CE8CC191D5]...';


GO
ALTER TABLE [dbo].[usuario_ubicacion] WITH NOCHECK
    ADD CONSTRAINT [FK5577C8CE8CC191D5] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_ubicacion_sububicacion_ubicacion]...';


GO
ALTER TABLE [dbo].[ubicacion_sububicacion] WITH NOCHECK
    ADD CONSTRAINT [FK_ubicacion_sububicacion_ubicacion] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_pruebapro_3vn6jckm]...';


GO
ALTER TABLE [dbo].[pruebapro_circuito] WITH NOCHECK
    ADD CONSTRAINT [FK_pruebapro_3vn6jckm] FOREIGN KEY ([tipo_circuito]) REFERENCES [dbo].[tipo_circuito] ([id]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_pruebapro_gpat2w8x7]...';


GO
ALTER TABLE [dbo].[pruebapro_circuito] WITH NOCHECK
    ADD CONSTRAINT [FK_pruebapro_gpat2w8x7] FOREIGN KEY ([tipo_contenedor]) REFERENCES [dbo].[tipo_contenedor] ([id]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_pruebapro_iejhi9irkh7]...';


GO
ALTER TABLE [dbo].[pruebapro_circuito] WITH NOCHECK
    ADD CONSTRAINT [FK_pruebapro_iejhi9irkh7] FOREIGN KEY ([ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_pruebapro_atj79pqko6guiswhr]...';


GO
ALTER TABLE [dbo].[pruebapro_dispositivo_ubicacion] WITH NOCHECK
    ADD CONSTRAINT [FK_pruebapro_atj79pqko6guiswhr] FOREIGN KEY ([id_dispositivo]) REFERENCES [dbo].[pruebapro_dispositivo] ([id_dispositivo]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_pruebapro_hsx52j13m5icy3s]...';


GO
ALTER TABLE [dbo].[pruebapro_dispositivo_ubicacion] WITH NOCHECK
    ADD CONSTRAINT [FK_pruebapro_hsx52j13m5icy3s] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[pruebapro_ubicacion] ([id_ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FKpruebapro_DEDB797C40F68DC8]...';


GO
ALTER TABLE [dbo].[pruebapro_sga_usuario] WITH NOCHECK
    ADD CONSTRAINT [FKpruebapro_DEDB797C40F68DC8] FOREIGN KEY ([ptr_rol]) REFERENCES [dbo].[sga_rol] ([cod_rol]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FKpruebapro_DEDB797CDAB65181]...';


GO
ALTER TABLE [dbo].[pruebapro_sga_usuario] WITH NOCHECK
    ADD CONSTRAINT [FKpruebapro_DEDB797CDAB65181] FOREIGN KEY ([puesto]) REFERENCES [dbo].[sga_puesto] ([cod_puesto]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_pruebapro_h4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK_pruebapro_h4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[pruebapro_circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FKaa_h4a4i8acrq6mun1132lhiru9h_Historico]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FKaa_h4a4i8acrq6mun1132lhiru9h_Historico] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fkaa_Historicosububicacion]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fkaa_Historicosububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fkaa_sububicacion]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fkaa_sububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK2aa_h4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK2aa_h4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK2aa_Historicoh4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK2aa_Historicoh4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fk2aa_Historicosububicacion]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fk2aa_Historicosububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fk2aa_sububicacion]...';


GO
ALTER TABLE [dbo].[pruebapro_tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fk2aa_sububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK_h4a4i8acrq6mun1132lhiru9h_Historico]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK_h4a4i8acrq6mun1132lhiru9h_Historico] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fk_Historicosububicacion]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fk_Historicosububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK2_h4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK2_h4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[FK2_Historicoh4a4i8acrq6mun1132lhiru9h]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [FK2_Historicoh4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fk2_Historicosububicacion]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fk2_Historicosububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[fk2_sububicacion]...';


GO
ALTER TABLE [dbo].[tarjeta] WITH NOCHECK
    ADD CONSTRAINT [fk2_sububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]);


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_ActGesEntradas]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[pruebapro_ActGesEntradas]
   ON  [dbo].[pruebapro_tarjeta]
   AFTER  INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;




	UPDATE [dbo].[pruebapro_tarjeta]
		SET [gestionado] = 'Entrada', estado='E', sentido='E' 
		WHERE 
		sentido='E'  and gestionado is null
		AND ubicacion 
		NOT IN 
        (SELECT        id_ubicacion
        FROM            dbo.ubicacion AS ubicacion_1
        WHERE        (Ubi_Modo = N'DISP'))




	--UPDATE [dbo].[pruebapro_tarjeta]
	--	SET  estado='P', sentido='S' 
	--	WHERE 
	--	sentido='E'  and gestionado is null
	--	AND ubicacion 
	--	IN 
 --       (SELECT        id_ubicacion
 --       FROM            dbo.ubicacion AS ubicacion_1
 --       WHERE        (Ubi_Modo = N'DISP'))










	UPDATE [dbo].[pruebapro_tarjeta]
		SET [gestionado] = 'Inventario', estado='I'
		WHERE sentido='I'  and gestionado is null

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[trg_dispositivo]...';


GO
CREATE TRIGGER [trg_dispositivo] ON [dbo].[dispositivo]
AFTER UPDATE 
 AS 
	INSERT INTO [dbo].[dispositivo_alive] 
			(id_dispositivo, ip, ultima_conexion, version, Dis_EnError, Dis_SIM, Dis_IPLocal, Dis_NotErrMin, 
			Dis_NotErrMail, Dis_NotErrSta, Dis_Tipo, Dis_ActAnt0, Dis_ActAnt1, fecha)
	SELECT d.id_dispositivo, d.ip, d.ultima_conexion, d.version, d.Dis_EnError, d.Dis_SIM, d.Dis_IPLocal, d.Dis_NotErrMin, 
			d.Dis_NotErrMail, d.Dis_NotErrSta, d.Dis_Tipo, d.Dis_ActAnt0, d.Dis_ActAnt1, CURRENT_TIMESTAMP
	FROM [dbo].[dispositivo] d
	inner join inserted i on d.id_dispositivo=i.id_dispositivo
GO
DISABLE TRIGGER [dbo].[trg_dispositivo]
    ON [dbo].[dispositivo];


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[V_LIS_Cir]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[V_LIS_Cir]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VAX_EKANCONSFACT]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VAX_EKANCONSFACT]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VAX_EKANCONSPROG]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VAX_EKANCONSPROG]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[VAX_EKANSTOCKCONSIGPROV]...';


GO
ALTER VIEW dbo.VAX_EKANSTOCKCONSIGPROV
AS
SELECT        TOP (100) PERCENT dbo.sga_usuario.username, dbo.circuito.Proveedor_id, dbo.circuito.ubicacion, dbo.circuito.id_circuito, dbo.circuito.cliente_id, PURCHTABLE_1.ORDERACCOUNT, PURCHLINE_1.ITEMID, 
                         PURCHLINE_1.NAME, PURCHTABLE_1.PURCHID, PURCHLINE_1.LINENUM, PURCHTABLE_1.DELIVERYDATE, PURCHLINE_1.PURCHQTY AS PEDIDO, PURCHLINE_1.INK_ENTRADASCONSIGNA AS ENVIADO, 
                         PURCHLINE_1.PURCHQTY - PURCHLINE_1.INK_ENTRADASCONSIGNA AS PTE, PURCHLINE_1.REMAINPURCHPHYSICAL - (PURCHLINE_1.PURCHQTY - PURCHLINE_1.INK_ENTRADASCONSIGNA) AS STOCKCONSIGNA
FROM            SRVAXSQL.SQLAX.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
                         SRVAXSQL.SQLAX.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
                         dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id INNER JOIN
                         dbo.usuario_proveedor ON dbo.circuito.Proveedor_id = dbo.usuario_proveedor.id_proveedor INNER JOIN
                         dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
WHERE        (PURCHLINE_1.DATAAREAID = N'ork') AND (PURCHLINE_1.PURCHSTATUS = 1) AND (PURCHLINE_1.PURCHASETYPE = 5) AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND (PURCHTABLE_1.VENDORREF LIKE N'%') AND 
                         (PURCHLINE_1.PURCHQTY > 0) AND (PURCHLINE_1.REMAINPURCHPHYSICAL - (PURCHLINE_1.PURCHQTY - PURCHLINE_1.INK_ENTRADASCONSIGNA) > 0) AND (dbo.circuito.activo = 1)
ORDER BY PURCHLINE_1.ITEMID, PURCHTABLE_1.PURCHID, PURCHLINE_1.LINENUM
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VAX_SitTarjetas]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VAX_SitTarjetas]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VAX_STOCKCUSTCONSIG]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VAX_STOCKCUSTCONSIG]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VEKA_PERMISOS]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VEKA_PERMISOS]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VEKA_PERMISOS_PROV]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VEKA_PERMISOS_PROV]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VTarGestionadas]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VTarGestionadas]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[VAX_EKAN_ALBA]...';


GO


ALTER VIEW [dbo].[VAX_EKAN_ALBA]
AS
SELECT        PACKINGSLIPID, INVOICEACCOUNT AS INVOICEACCOUNTkk, ORDERACCOUNT AS INVOICEACCOUNT, ORDERACCOUNT, DELIVERYNAME, DLVTERM, DELIVERYCITY, DELIVERYDATE, LINENUM, ITEMID, 
                         EXTERNALITEMID, ORDERED, QTY, INVENTBATCHID, DATAAREAID, CARRIERCODEREF, TXT, NAME, WEIGHT, INK_TIPOALBARAN, CREATEDDATETIME, PURCHASEORDER 
                         
FROM            SRVAXSQL.SQLAX.dbo.vaux_ekan_ALB AS vaux_ekan_ALB_1
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[VAX_EKAN_OF]...';


GO

ALTER VIEW [dbo].[VAX_EKAN_OF]
AS
SELECT        CUSTACCOUNT, PRODSTATUS, ITEMID, EXTERNALITEMID, PRODID, PRODGROUPID, SCHEDDATE, QTYSCHED, ECTD_CTD, ECTD_FEC, REMAININVENTPHYSICAL, WRKCTRID, DATAAREAID
FROM            preAXSQL.SQLAXbak.dbo.vaux_ekan_of AS vaux_ekan
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[VAX_EKAN_PENORD]...';


GO

ALTER VIEW [dbo].[VAX_EKAN_PENORD]
AS
SELECT        CUSTACCOUNT, SALESID, PURCHORDERFORMNUM, LINENUM, ITEMID, SALESSTATUS, EXTERNALITEMID, QTYORDERED, REMAINSALESPHYSICAL, SHIPPINGDATEREQUESTED, 
                         SHIPPINGDATERECONFIRMED, DATAAREAID, CREATEDDATETIME, SHIPPINGCONFIRMINGDATETIME, SALESTYPE
FROM            PREAXSQL.SQLAXBAK.dbo.vaux_ekan_PENORD AS vaux_ekan_PENORD_1
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[VEnvTraza]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[VEnvTraza]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[A]...';


GO

ALTER FUNCTION [dbo].[A]
(@Circuito as nvarchar(50) )--, @UBI AS NVARCHAR(50)=''
RETURNS int
AS
BEGIN
	DECLARE @A as int
	declare @Art as nvarchar(25)
	DECLARE @PEDPTES AS INT
	DECLARE @CtdKan AS INT
	declare @Tipo as int
	declare @cons as bit
	declare @Dias as int
	declare @Horas as int
	declare @Emp as nvarchar(3)
	
	SET @A=0

	begin
		SELECT @Art=referencia_id,@CtdKan=cantidad_piezas, @Tipo=tipo_circuito, @cons=Cir_Consigna, @Dias=entrega_dias, @Horas=entrega_horas, @Emp=Cir_Empresa
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end

	set @CtdKan=isnull(@CtdKan,1)
	set @Tipo=isnull(@Tipo,1)
	set @cons=isnull(@cons,0)
	set @Dias=isnull(@Dias,0)
	set @Horas=isnull(@Horas,0)


	IF @Tipo=1 --AMARILLAS EN PROVEEDORES
	BEGIN
		if @cons=0  --SI NO ES CONSIGNA, COGEMOS LOS PED COMPRA NORMAL
		BEGIN
			SELECT @PEDPTES=SUM(PURCHLINE_1.REMAINPURCHPHYSICAL) 
			FROM   PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
				   PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
				   dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
			WHERE  (PURCHLINE_1.PURCHSTATUS = 1)  AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND 
				   (dbo.circuito.id_circuito = @Circuito) AND (PURCHLINE_1.DATAAREAID = N'ork') AND  (PURCHTABLE_1.PURCHASETYPE = 3) AND (PURCHTABLE_1.VENDORREF LIKE N'U%') --(PURCHTABLE_1.DOCUMENTSTATUS = 0) AND

			SET @PEDPTES=isnull(@PEDPTES, 0)
			SET @A=@PEDPTES/@CtdKan
		END

		ELSE --SI ES ARTÍCULO DE CONSIGNA
		
		BEGIN
				DECLARE @CTDPED AS INT
				DECLARE @CTDREC AS INT


				SELECT        @CTDPED=SUM(PURCHLINE_1.PURCHQTY),  @CTDREC=SUM(PURCHLINE_1.INK_ENTRADASCONSIGNA) 
				FROM            PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
				PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
				dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
				WHERE        (PURCHLINE_1.PURCHSTATUS = 1) AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND (dbo.circuito.id_circuito = @Circuito) AND (PURCHLINE_1.DATAAREAID = N'ork') AND 
				(PURCHTABLE_1.VENDORREF LIKE N'%') AND (PURCHLINE_1.PURCHASETYPE = 5) AND (PURCHLINE_1.REMAINPURCHPHYSICAL > 0)
				--GROUP BY PURCHLINE_1.REMAINPURCHPHYSICAL
				HAVING        (SUM(PURCHLINE_1.PURCHQTY) > 0)  AND (SUM(PURCHLINE_1.PURCHQTY) - SUM(PURCHLINE_1.INK_ENTRADASCONSIGNA) > 0)
				---ORDER BY SUM(PURCHLINE_1.PURCHQTY) DESC


				set @CTDPED=isnull(@CTDPED, 0)
				set @CTDREC=isnull(@CTDREC, 0)


				SET @PEDPTES=@CTDPED-@CTDREC

				
			set @PEDPTES=isnull(@PEDPTES, 0)
			if @PEDPTES<0 set @PEDPTES=0


			SET @A=@PEDPTES/@CtdKan
		END

	END

	--AMARILLAS EN CLIENTES
	IF @Tipo=3
	BEGIN
		SELECT @PEDPTES=  SUM(dbo.VAX_EKAN_PENORD.REMAINSALESPHYSICAL)
		FROM   dbo.VAX_EKAN_PENORD INNER JOIN
			   dbo.circuito ON dbo.VAX_EKAN_PENORD.CUSTACCOUNT = dbo.circuito.cliente_id AND dbo.VAX_EKAN_PENORD.ITEMID = dbo.circuito.referencia_id AND 
			   (SUBSTRING(dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM, 1, 3) = dbo.circuito.ubicacion OR
			   SUBSTRING(dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM, 1, 2) = dbo.circuito.Cir_UbiAnt OR
			   dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%') AND dbo.VAX_EKAN_PENORD.SALESTYPE = dbo.circuito.Cir_StdSalesType
		GROUP BY dbo.circuito.id_circuito, dbo.circuito.cliente_id, dbo.circuito.referencia_id
		HAVING (dbo.circuito.id_circuito = @Circuito)

		set @PEDPTES=isnull(@PEDPTES, 0)
		SET @A=@PEDPTES/@CtdKan
	END

	--AMARILLAS EN CIRCUITOS INTERNOS  
	IF @Tipo=2
	BEGIN

	--Campo: INK_DIARIOKANBAN

		SELECT        @PEDPTES=SUM(REMAININVENTPHYSICAL)
		FROM           PREAXSQL.SQLAXBAK.dbo.PRODTABLE
		WHERE        (PRODSTATUS = 4 and INK_DIARIOKANBAN=@Circuito)
		GROUP BY ITEMID, PRODSTATUS, DATAAREAID, INK_DIARIOKANBAN
		HAVING        (ITEMID = @Art) AND (DATAAREAID = N'ork')



		SET @PEDPTES=isnull(@PEDPTES, 0)
		SET @A=@PEDPTES/@CtdKan


	END

	--AMARILLAS EN CIRCUITOS EXTERNOS 
	IF  @Tipo=4
	BEGIN
		SELECT      @A=  COUNT(estado)
		FROM            dbo.tarjeta
		WHERE          (estado = 'G' OR  estado = 'A_MODIF')  AND (id_circuito = @Circuito) AND (DATEADD(HH, @Horas, Tar_FecEntSol) > GETDATE())

		set @A=isnull(@A, 0)
	
	END
	
	RETURN @A
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[Estado]...';


GO



ALTER FUNCTION [dbo].[Estado]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SS as int
	DECLARE @Stat as int


	declare @Tipo as int
	--1 proveedor, 3 cliente
	begin
		SELECT @Tipo=tipo_circuito
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end
	set @Tipo=isnull(@Tipo, 1) -- si no estuviera definido, lo consideramos como proveedor


	SET @T=DBO.T(@Circuito)
	SET @R=DBO.R(@Circuito)
	SET @AE=DBO.AE(@Circuito)
	SET @A=DBO.A(@Circuito)
	SET @SS=DBO.SS(@Circuito)
	
	SET @V=@T-@AE-@A-@R+@SS

	set @Stat=1 --verde por defecto

	if @tipo=3 --cliente
	begin
		if ((@R+@A > @T/2) and @SS=0 ) OR @V=0 
			begin
				set @Stat=2 --amarillo
				if @AE=0 or @r>=@t
				BEGIN
				set @Stat=3 -- ROJO
				END
			end
	end
	else
	begin
		--no existe AE por ser proveedor, externos, etc...
		if @V < @T/2
			begin
				set @Stat=2 --amarillo
				if @A=0 
				BEGIN
					set @Stat=3 -- ROJO
				END
			end
	end
	

	RETURN @Stat

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[FAX_SitTarjetas]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[FAX_SitTarjetas]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[SplitString]...';


GO
ALTER FUNCTION SplitString
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_CtaTarOptimas]...';


GO

-- =============================================
-- Author:		IKER
-- Create date: 6-5-2014
-- Description:	Procedimiento para obtener la cantidad optima según circuito
-- =============================================
ALTER PROCEDURE [dbo].[PAX_CtaTarOptimas]
(
@Circuito varchar(255)='', @Usu AS varchar(255)='', @Sal as int=0, @Reg as varchar(50)=' '
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @Tipo as int
	DECLARE @Cliente AS varchar(255)
	DECLARE @Proveedor AS varchar(255)
	DECLARE @Articulo AS varchar(255)
	DECLARE @Ubic AS varchar(255)
	DECLARE @MedDia  as real
	declare @CtdKan as int
	declare @DiaTrans as int
	declare @DiaTrans1 as int
	declare @HoraTrans as int
	declare @DefKan as int
	DECLARE @OPT AS int
	DECLARE @CONSKANDIA AS real
	DECLARE @UbiAnt as varchar(3)=''
	DECLARE @NumEnv as int=0
	

	if @sal=0
	begin
		SELECT        cliente_id AS CLI, ubicacion AS UBI, referencia_id AS ART, num_kanban AS ACT, 1 AS MEDDIA, 1 AS CTDKAN, 1 AS ConsTarDia, 1 AS NumEnvios, 1 AS DemoraDias, 1 AS TRANSD, 1 AS TRANSH, 
		num_kanbanOPT AS OPT, num_kanbanOPT - num_kanban AS DIF
		FROM            dbo.circuito
		WHERE        (id_circuito = @Circuito)

		-- si es 0, que lo muestre en pantalla. situación por defecto. En funcionamiento hasta la fecha. 27/1/2015
		--select  @Cliente as CLI, @Ubic as UBI, @Articulo as ART, @DefKan as ACT, @MedDia as MEDDIA, @CtdKan as CTDKAN, @CONSKANDIA AS ConsTarDia, @NumEnv AS NumEnvios, 5-@NumEnv as DemoraDias, @DiaTrans as TRANSD, @HoraTrans as TRANSH, @OPT as OPT, @OPT-@DefKan as DIF
	end
	else
	begin 


			set @MedDia =0

		begin
			SELECT        @Cliente= cliente_id, @Proveedor=Proveedor_id, @Articulo=referencia_id, @CtdKan=cantidad_piezas, @DiaTrans=entrega_dias, @HoraTrans=entrega_horas, @DefKan=num_kanban, @Ubic=ubicacion,  @Tipo=tipo_circuito, @UbiAnt=Cir_UbiAnt,
							@NumEnv=CASE WHEN Cir_LunM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_MarM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_MieM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_JueM = 0 THEN 1 ELSE 0 END + CASE WHEN
							  Cir_VieM = 0 THEN 1 ELSE 0 END 
			FROM            dbo.circuito
			WHERE        id_circuito = @Circuito
		end
	
			--considerando las ubicaciones y cogiendo los envíos según albarán, en vez de facturas como anteriormente.

		if @tipo=3
		--Significa que es circuito de tipo cliente. 
		begin
			SET @UBIANT=ISNULL(@UBIANT,'')
			SET @UBIANT=LTRIM(RTRIM(@UBIANT))

			SELECT         @MedDia=AVG(CtdSem) / 5 
				FROM            (
	
					SELECT        TOP (40) CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
							THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
					FROM            PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
							PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
							CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
							PREAXSQL.SQLAXBAK.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
							CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
					WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0)) OR
							(SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%') AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0))
					GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
							'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
							CUSTPACKINGSLIPTRANS.DATAAREAID
					HAVING       (SUBSTRING(CUSTPACKINGSLIPJOUR.ORDERACCOUNT,1,6) = substring(@Cliente,1,6)) 
			 AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) AND (DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE) <> DATEPART(week, GETDATE()))
					ORDER BY ART, AÑO, SEM DESC

	/**
			SELECT        TOP (40)  CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, 
									 DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
									 CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
									 THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
			FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
									 SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
									 CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
									 SRVAXSQL.SQLAX.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
									 CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
			WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) OR
									 (SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%')
			GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
									 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
									 CUSTPACKINGSLIPTRANS.DATAAREAID
			HAVING        (CUSTPACKINGSLIPJOUR.ORDERACCOUNT = @Cliente) AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) 
							AND (DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(YEAR, GETDATE()))
       					    AND (DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) <> DATEPART(week, GETDATE()))
			ORDER BY ART, AÑO, SEM DESC
			**/

			) AS ten


			end


		if @tipo=1 --tipo proveedor
		BEGIN
			SELECT         @MedDia=AVG(CtdSem) / 5 
			FROM            (
				SELECT        TOP (100) PERCENT PURCHTABLE_1.INVOICEACCOUNT AS CLI, 1 AS UBI, PURCHLINE_1.ITEMID AS ART, DATEPART(year, PURCHLINE_1.DELIVERYDATE) AS AÑO, DATEPART(week, 
				PURCHLINE_1.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, PURCHLINE_1.DELIVERYDATE))) AS ETIQ, 
				SUM(PURCHLINE_1.PURCHQTY) AS CTDSEM, '1' AS COLOR, PURCHTABLE_1.DATAAREAID AS EMP
				FROM            PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 INNER JOIN
				PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 ON PURCHTABLE_1.PURCHID = PURCHLINE_1.PURCHID
				GROUP BY PURCHTABLE_1.INVOICEACCOUNT, PURCHLINE_1.ITEMID, DATEPART(year, PURCHLINE_1.DELIVERYDATE), DATEPART(week, PURCHLINE_1.DELIVERYDATE), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
				PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, PURCHLINE_1.DELIVERYDATE))), PURCHTABLE_1.DATAAREAID
				HAVING        (PURCHTABLE_1.DATAAREAID = N'ork') AND (PURCHLINE_1.ITEMID = @Articulo) AND (DATEPART(year, PURCHLINE_1.DELIVERYDATE) = DATEPART(YEAR, GETDATE())) AND (DATEPART(week, 
				PURCHLINE_1.DELIVERYDATE) <> 53) AND (PURCHTABLE_1.INVOICEACCOUNT = @Proveedor)
 				) as a
		END


		set @MedDia=isnull(@MedDia,0)

		if @MedDia=0 set @MedDia=1

		SET @CONSKANDIA=@MedDia/@CtdKan

		set @DiaTrans1=@DiaTrans
		if @DiaTrans1=0 set @DiaTrans1=1
		if @DiaTrans1>5 set @DiaTrans1=@DiaTrans1-2

		if @tipo=1 	SET @OPT=@MedDia/@CtdKan*@DiaTrans1*1.2
		if @tipo=3 	SET @OPT=@MedDia/@CtdKan*(5 - @NumEnv + @DiaTrans1) * 1.25

		set @OPT=isnull(@OPT,0)

		if @OPT=0 set @OPT=1


	

		--para visualizar, mientras se ejecuta. 
		select  @Cliente as CLI, @Ubic as UBI, @Articulo as ART, @DefKan as ACT, @MedDia as MEDDIA, @CtdKan as CTDKAN, @CONSKANDIA AS ConsTarDia, @NumEnv AS NumEnvios, 5-@NumEnv as DemoraDias, @DiaTrans as TRANSD, @HoraTrans as TRANSH, @OPT as OPT, @OPT-@DefKan as DIF

		--Actualizamos el campo de la tabla circuito, para acceso rápido
		UPDATE [dbo].[circuito]
		SET [num_kanbanOPT] = @OPT
		WHERE [id_circuito]=@Circuito

		-- En el histórico, ponemos todo a vigente 0
		UPDATE [dbo].[TRegistroOptHis]
		SET [id_Vig] = 0       
		WHERE [id_circuito] =@Circuito
		
		--insertamos un nuevo registro con vigente 1
		INSERT INTO [dbo].[TRegistroOptHis]
           ([id_Reg]
           ,[id_Fec]
           ,[id_Vig]
           ,[id_circuito]
           ,[cliente_id]
           ,[ubicacion]
           ,[referencia_id]
           ,[num_kanbanAct]
           ,[num_kanbanOpt]
           ,[num_kanbanDif]
           ,[MedDia]
           ,[cantidad_piezas]
           ,[MedDiaTarKanban]
           ,[NumEnvios]
           ,[DemoraDias]
           ,[DiasTrans]
           ,[HorasTrans])

		select  
			  @Reg as Registro
			, getdate()
			, 1 as v
			, @Circuito as c
			, @Cliente as CLI
			, @Ubic as UBI
			, @Articulo as ART
			, @DefKan as ACT
			, @OPT as OPT
			, @OPT-@DefKan as DIF
			, @MedDia as MEDDIA
			, @CtdKan as CTDKAN
			, @CONSKANDIA AS ConsTarDia
			, @NumEnv AS NumEnvios
			, 5-@NumEnv as DemoraDias
			, @DiaTrans as TRANSD
			, @HoraTrans as TRANSH

	end



	--EXEC	[dbo].[PRegistro]
	--@Reg_Usu = N'_',
	--@Reg_Ubi = @Ubic,
	--@Reg_opt = N'Cta/Act Optimos',
	--@Reg_Art = @Articulo,
	--@Reg_cli = @Cliente,
	--@Reg_Txt = @OPT


END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAccReports]...';


GO

ALTER PROCEDURE [dbo].[PAccAccReports]
(
@Usuario AS varchar(255)='', @Cliente varchar(255)='',@Ubicacion varchar(255)='', 
@Circuito varchar(255)='', @AccRep varchar(9)='', @Tipo varchar(3)='PDF', 
@Var1 varchar(255)='', @Var2 varchar(255)='', @Var3 varchar(255)=''
)
AS

BEGIN
	DECLARE @Rep varchar(250)
	DECLARE @Ser varchar(25)=''
	DECLARE @ResOK INT=0
	DECLARE @Errmsg varchar(250)=''
	DECLARE @Url varchar(250)=''
	SET NOCOUNT ON;
	
	--REPORTING
	if @Tipo='PDE' OR @Tipo='PDF' OR @Tipo='EXC'
	BEGIN
		SELECT   @ser=AccRep_Ser, @Rep=AccRep_Rep
		FROM     dbo.TAccionesReports
		WHERE   (AccRep_Idd = @AccRep)
	END
	SET @SER=ISNULL(@SER, '')
	SET @Rep=ISNULL(@Rep, '')



	--***********************************************
	/*
	SELECT *
	FROM [dbo].[TAccionesReports]
	where accrep_tip='acc'
	ORDER BY ACCREP_TIP, ACCREP_IDD
	*/

	--ACCIONES
	if @AccRep='ACTMAICON' 
	begin
		UPDATE [dbo].[circuito]
		SET [mail] = @Var1
		WHERE   (id_circuito = @Circuito)
		
		SET @ResOK=1
	end
	if @AccRep='DARCONSUM' SET @ResOK=0
	

	if @AccRep='RESETCIRC' and left(@Circuito,4)='RFID'
	begin
		DELETE FROM [dbo].[tarjeta]
		WHERE        (id_circuito = @Circuito)
		SET @ResOK=1
	end

	

	if @AccRep='UPDMODUBI' --and left(@Circuito,10)='RFIDTEST00'
	begin
		UPDATE [EKANBAN].[dbo].[ubicacion]
		SET [Ubi_Modo] = @Var1
		WHERE [id_ubicacion]=@Ubicacion
		
		SET @ResOK=1
	end



	/*
	exec SRVAXSQL.ORKLB000.dbo.[ReportPDF_Obtener_Path] @Servidor = N'SRVAX',
		@Report = N'900_Adierazleak.rpt',
		@Cliente = N'00000001',
		@Division = N'01',
		@Links = N'1',
		@Empresa = N'ORK', 
		@Formato = N'EXCEL'


	*/


	--RESULTADOS para Acciones, como para Reporting
	SELECT  @Tipo as Tipo, 
			@ResOK as ResOK, 
			@Errmsg as Errmsg,  
			@Url as Url,
			@ser AS servidor,  
			@Rep AS report,  
			@Cliente AS cliente,  
			@Ubicacion AS ubicacion,  
			@Circuito AS circuito,  
			@Usuario AS usuario,  
			'01' AS division,  
			'1' AS links,  
			'9' AS permiso,  
			'ORK' AS empresa,  
			@Tipo AS tipo,  
			@Var1 AS var1,  
			@Var2 AS var2,  
			@Var3 AS var3,  
			'4' AS var4,  
			'5' AS var5,  
			'6' AS var6,  
			'7' AS var7
		
		
		/*
		EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usuario,
		@Reg_Ubi = '',
		@Reg_opt = N'[PCtaAccReports]',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Usuario
		*/

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_GetCircuitoByIntegrationRef]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAX_GetCircuitoByIntegrationRef] 
	-- Add the parameters for the stored procedure here
	@integrationRef varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from circuito where ref_IntegracionERP = @integrationRef;
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_LanzamientoPedidos]...';


GO

-- =============================================
-- Author:		iker
-- Create date: 3-6-2014
-- Description:	Para crear los pedidos AX desde el nuevo sistema eKanban, android e iKtrace
-- =============================================

ALTER PROCEDURE [dbo].[PAX_LanzamientoPedidos]
(
	@CodCircuito varchar(20) = '',
	@NumTarjRojas integer = 0,
	@NumTarjetas integer = 0, 
	@FechEntregaConfirmada datetime,
	@Ubicacion varchar(255) = '',
	@Usuario varchar(255) = ''
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- AITOR. 2014-06-24 
						--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
	
	DECLARE @MsgTxt varchar(255)=''
	DECLARE @numTarjRojasAhora integer=0
	DECLARE @Resul int=0
	DECLARE @Tipo varchar(10)=''
	DECLARE @CodCliPro varchar(100)=''
	DECLARE @Art varchar(100)=''
	DECLARE @Cli varchar(100)
	DECLARE @pro varchar(100)
	DECLARE	@return_value int
	DECLARE @AUT VARCHAR (5)=''
	DECLARE @PedPRO bit=0
	DECLARE @PedCLI bit=0
	DECLARE @IdTar varchar(50)
	DECLARE @FeTar datetime
	DECLARE @Consig as bit=0
	DECLARE @ConTipoVen as varchar(2)
	DECLARE @ConDesde as varchar(25)=''
	DECLARE @StdDesde as varchar(25)=''
	DECLARE @Facturar as int
	DECLARE @SalesTipo varchar(10)=''
	DECLARE @NumPed varchar(50)=''
	DECLARE @CampGest varchar(50)=''
	DECLARE @ReaprovAgrupada as bit=0
	DECLARE @ConFactAgrupada as bit=0
	DECLARE @StatusTrat varchar(1)
	DECLARE @StatusPed int=9
	DECLARE @FechEntregaConfirmada1 AS DATE=GETDATE()
	DECLARE @NumPedCli varchar(50)


	
	--Revisamos si el circuito es de tipo cliente o proveedor y sus cógidos de cliente/proveedor
	SELECT @Tipo=dbo.tipo_circuito.nombre, @Pro=dbo.circuito.Proveedor_id, @Cli=dbo.circuito.cliente_id, @Art=referencia_id, @Consig=Cir_Consigna, @SalesTipo=Cir_StdSalesType, @ConTipoVen=Cir_ConSalesType, @STDDesde=rtrim(ltrim(Cir_StdInventLocationId)), @ConDesde=rtrim(ltrim(Cir_ConInventLocationId)), @Facturar=Cir_ConFacturarAut, @Ubicacion=dbo.circuito.ubicacion, @ConFactAgrupada=dbo.circuito.Cir_ConFacturarAgrup , @ReaprovAgrupada=dbo.circuito.Cir_ReaprovAgrup, @NumPedCli=Cir_NumPedCli
	FROM            dbo.circuito INNER JOIN
				dbo.tipo_circuito ON dbo.circuito.tipo_circuito = dbo.tipo_circuito.id
	WHERE        (dbo.circuito.id_circuito = @CodCircuito)
	
	set @Consig = isnull(@Consig, 0)
	set @ConTipoVen = isnull(@ConTipoVen, '')
	set @STDDesde = isnull(@STDDesde, 'EXP.AUXIL')
	set @ConDesde = isnull(@ConDesde, 'EXP.AUXIL')
	set @Facturar=isnull(@Facturar, 0)
	set @ConFactAgrupada=isnull(@ConFactAgrupada, 0)
	set @SalesTipo=isnull(@SalesTipo, '')
	set @NumPedCli=isnull(@NumPedCli,'')

	if @ConDesde = '' set @ConDesde='EXP.AUXIL'
	if @STDDesde = '' set @STDDesde='EXP.AUXIL'
	


	SET @FechEntregaConfirmada1 = CONVERT(date, @FechEntregaConfirmada) ---******************** IKER 11/7/2017

	SET @StatusPed=9 --por defecto para que si es un equipo no oficial, no vaya a crear pedidos. 
	if CONVERT(sysname, SERVERPROPERTY(N'servername'))= 'SRVDMZ' SET @StatusPed=0  -- estado para que entren los pedidos en crear pedidos.

	IF {fn HOUR(@FechEntregaConfirmada)}=0 AND { fn MINUTE(@FechEntregaConfirmada) }=0
	BEGIN
		SET @FechEntregaConfirmada=DATEADD(MINUTE, { fn MINUTE(GETDATE()) }, DATEADD(HOUR, { fn HOUR(GETDATE()) }, @FechEntregaConfirmada))
	END

	
	SET @StatusTrat='T'
	if @Tipo='CLI' SET @CodCliPro=@CLI
	if @Tipo='PRO' SET @CodCliPro=@PRO

	if @Tipo='INT' OR @Tipo='EXT' 
	BEGIN
		SET @CodCliPro=''
		SET @StatusTrat='G'
	END


		
	--paso de control
	Set @Resul=0
	set @MsgTxt='Sin validar usuario'
	
	--SE MIRA SI EL USUARIO TIENE PERMISO PARA LANZAR LOS PEDIDOS
	--SI ES AUTOMÁTICO, SI, SI ES UN USUARIO, SE MIRA SI CORRESPONDE A UN ROL CON PERMISOS.
	IF @Usuario='AUT'  or  @Usuario='ALB' or @Usuario='5MI'  or @Usuario='iker' 
	  -- HORAS FIJAS           No se utiliza         CADA 5MIN
		begin
			SET @AUT=''--'A'  --LO QUITAMOS PARA QUE TANTO LOS PEDIDOS LANZADOS AUTOMÁTICAMENTE COMO LOS LANZADOS DESDE EL ANDROID SE AGRUPEN. YA QUE TENIENDO A EN AUTOMÁTICOS EL PEDIDO ES DIFERENTE Y SON DOS PEDIDOS APARTE.
			set @pedCLI=1
			set @pedPRO=1
		end
	else
		begin
			SELECT        @PedPRO=dbo.sga_rol.rol_PedPRO, @PedCLI=dbo.sga_rol.rol_PedCLI
			FROM            dbo.sga_rol INNER JOIN
									dbo.sga_usuario ON dbo.sga_rol.cod_rol = dbo.sga_usuario.ptr_rol
			WHERE        (dbo.sga_usuario.username = @Usuario)
		end

	IF @Usuario='demogm' and @Ubicacion='U00' set @pedPRO=1


	--si realmente hay permiso para realizar el pedido...
	IF @pedcli=1 or @pedpro=1
	begin
		-- Si la cantidad de tarjetas rojas actual es diferente a la del panel, no hacemos nada
		SELECT      @numTarjRojasAhora=   [dbo].[R] (@CodCircuito) 
		set @numTarjRojasAhora=isnull(@numTarjRojasAhora,0)
	
		if @numTarjRojasAhora <> @NumTarjRojas
			begin
				set @Resul=18  --Código de resultado para el android. Error al generar pedido.
				set @MsgTxt='Se han dado consumos justo en este instante. Vuelva a intentarlo'
				goto fin
			end
	
		-- Seleccionamos los datos, y los marcamos con el código de circuito en campo de GES. 
		-- significa que estamos en tratamiento.

		set @CampGest='AutAX' + '_' + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113)

		if @NumTarjetas=@NumTarjRojas
			--si hay que gestionar todas...
			begin
			UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada1
				WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito)
			end
		else	
			begin
			if @NumTarjetas<@NumTarjRojas
				begin
					--SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
					--FROM            dbo.tarjeta
					--WHERE        (gestionado IS NULL OR
					--				gestionado = '') AND (id_circuito = @CodCircuito)
					--ORDER BY id_lectura

					SELECT    TOP (@NumTarjetas)  @FeTar=fecha_lectura,   @IdTar =id_lectura
					FROM            dbo.tarjeta
					WHERE        (gestionado IS NULL OR
									gestionado = '') AND (id_circuito = @CodCircuito)
					ORDER BY fecha_lectura, id_lectura



					UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada1
						WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito)
						 and
						 (fecha_lectura < @FeTar or  --todas las tarjetas anteriores a la última seleccionada.
						 (fecha_lectura = @FeTar and id_lectura<=@IdTar)) --+ las que siendo la misma fecha, (si ha habido ajustes), la id lectura sea igual o menor. 
				end
			else
				begin
					UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada1
						WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) AND ESTADO <>'A_MODIF'
					
					--añadir registros A_MODIF en Tarjetas, tantos como @NumTarjetas-@NumTarjRojas
					--ya que hemos insertardo tarjetas nuevas en el circuito al crear el pedido de más cantidad 
					--que la solicitada según las tarjetas rojas. 
					INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
					SELECT        TOP (@NumTarjetas-@NumTarjRojas) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada1
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @CodCircuito)
				end
			end

		
		--inserción de los datos para la creación del pedido de ventas-compras.
		--tipo de pedido según por defecto del cliente



		set @NumPed = @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + @AUT
		

		
		------------------------------------------------------------
		DECLARE @cnt INT = 0;
		DECLARE @nVeces INT = 1;

		if @Tipo='CLI' OR @Tipo='PRO' 
		begin


			IF @ReaprovAgrupada=0 --Significa que no queremos agrupar los consumos, y cada uno tiene que lanzar una línea de pedido, varias tarjetas
				BEGIN 
					set @nVeces=@NumTarjetas
					set @NumTarjetas=1
				END

			if @Tipo='PRO' and @Consig=1 
			begin
				set @NumPed = CONVERT(nvarchar(100), getdate(), 120) --NO AÑADIMOS MÁS Porque la longitud del pedido es de 20 y no entra.
			end

			if @Tipo='PRO' and @Consig<>1 
			begin
				--set @NumPed = @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) --CONVERT(nvarchar(100), getdate(), 103) + ' ' + CONVERT(nvarchar(100), getdate(), 108) --
				--añadir la referencia del artículo o el circuito, para que el número de pedido sea no agrupado
				--*************************
				--iker: goikoa kenduta, para poner articulo y fecha. 17/7/2017
				set @NumPed =left(@Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112)+ @Art,20) --CONVERT(nvarchar(100), getdate(), 103) + ' ' + CONVERT(nvarchar(100), getdate(), 108) --
			end

			if @Tipo='CLI' and @NumPedCli<>''
			BEGIN
			set @NumPed = @NumPedCli   --TODAVÍA ANULADO, HASTA QUE ELI Y JOSU LO VALIDEN. INCIDENICAS EN CREACIÓN DE PEDIDOS??
			END


			WHILE @cnt < @nVeces
				BEGIN
					INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
					(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra)
					SELECT        @StatusPed AS Estado, id_circuito AS Circ, descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NPed, referencia_id AS Ref, 
							cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario, GETDATE() AS Gaur, reserva_auto, @Tipo, @SalesTipo,@StdDesde,0, Cir_PedMarco,'',ISNULL(Cir_AlbaranarAut, 0) , ISNULL(Cir_RegistrarPedCompra, 0) 
					
					--@ConDesde
					
					FROM            dbo.circuito AS TCIRCUITO_1
					WHERE        (id_circuito = @CodCircuito)

					SET @cnt = @cnt + 1;
				END;

		end

		if @Tipo='INT' 
		begin
			set @NumPed = @CodCircuito -- @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + @AUT
		

			IF @ReaprovAgrupada=0 --Significa que no queremos agrupar los consumos, y cada uno tiene que lanzar una línea de pedido, varias tarjetas
				BEGIN 
					set @nVeces=@NumTarjetas
					set @NumTarjetas=1

				END

			WHILE @cnt < @nVeces
				BEGIN
				--@StatusPed    @CodCliPro
					INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
					(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra)
					SELECT       @StatusPed AS Estado, id_circuito AS Circ, descripcion AS Des, '000000' AS CliPro, @NumPed AS NPed, referencia_id AS Ref, 
							cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario, GETDATE() AS Gaur, reserva_auto, @Tipo, @SalesTipo,@StdDesde,0, Cir_PedMarco,'',ISNULL(Cir_AlbaranarAut, 0) , ISNULL(Cir_RegistrarPedCompra, 0) 
				--@ConDesde
					FROM            dbo.circuito AS TCIRCUITO_1
					WHERE        (id_circuito = @CodCircuito)

			

					SET @cnt = @cnt + 1;
				END;

		end
		------------------------------------------------------------

		if @Consig=1 and @Tipo='CLI' --en proveedores, no lanzamos la parte de pedido venta/compra, ya que se debe lanzar a final de día. Para que no lance en GARAY
		begin

			if @ConFactAgrupada=0 
			begin
				DECLARE @LectID VARCHAR(255)
				DECLARE @ALB VARCHAR(255)
				DECLARE @ALBLIN VARCHAR(255)
				DECLARE @NBase_Cursor varchar(255)

				DECLARE  CDatos1 CURSOR FOR
					SELECT        id_lectura
					FROM            dbo.tarjeta
					WHERE        (gestionado = @CampGest)

				OPEN CDatos1 

				FETCH NEXT FROM CDatos1
				INTO @NBase_Cursor

																														
				WHILE @@fetch_status = 0 
				BEGIN
				SET @LectID = @NBase_Cursor


				SET @ALB=''
				SET @ALBLIN=''
				
				IF SUBSTRING(@LECTID, 7, 3)='_AL'
				BEGIN
						SET @ALB=SUBSTRING(@LECTID, 8, 10)
						SET @ALBLIN =SUBSTRING(@LECTID, 19, 1)

						--PARA MARCAR LA LÍNEA DEL ALBARÁN COMO FACTURADO EN AX
						UPDATE preaxsql.SQLAXBAK.dbo.CustPackingSlipTrans
						SET  INK_FACTURADOCONSIGNA = 1
						WHERE   (DATAAREAID = N'ORK') AND (PACKINGSLIPID = @ALB) AND (LINENUM = @ALBLIN)


				END

				--******************************
				--en los de consigna, coge el número de albarán para copreci. hay que coger el que aparezca en campo IDPEDIDO, pero cuanto HECTOR lo actualice.
				--como va a afectar a los pedidos de compra?? hay que coger también la cantidad real.
				SELECT        @NumPed=SUBSTRING(id_lectura, 8, 10) 
				FROM            dbo.tarjeta
				WHERE        (id_lectura = @LectID)



				set @NumPed=isnull(@NumPed, @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + @AUT )

				INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
					(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra)
					SELECT        @StatusPed AS Estado, TCIRCUITO_1.id_circuito AS Circ, TCIRCUITO_1.descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NumPed, TCIRCUITO_1.referencia_id AS Ref, 
								 TCIRCUITO_1.cantidad_piezas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario AS Expr1, GETDATE() AS Gaur, TCIRCUITO_1.reserva_auto, @Tipo AS Expr2, @ConTipoVen AS Expr3, 
								 @ConDesde AS Expr4, @Facturar AS Expr5, TCIRCUITO_1.Cir_PedMarco, TCIRCUITO_1.Cir_ConNumFactCli, 0 AS Expr6, ISNULL(Cir_RegistrarPedCompra, 0) 
						FROM            dbo.circuito AS TCIRCUITO_1 INNER JOIN
												 dbo.tarjeta ON TCIRCUITO_1.id_circuito = dbo.tarjeta.id_circuito
						WHERE        (TCIRCUITO_1.id_circuito = @CodCircuito) AND (dbo.tarjeta.gestionado = @CampGest) AND (dbo.tarjeta.id_lectura = @LectID)
			
			
					/**SELECT       0 AS Estado, id_circuito AS Circ, descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NumPed, referencia_id AS Ref, 
							cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario, GETDATE() AS Gaur, reserva_auto, @Tipo, @ConTipoVen, @ConDesde, @Facturar, Cir_PedMarco,Cir_ConNumFactCli,0
					FROM            dbo.circuito AS TCIRCUITO_1
					WHERE        (id_circuito = @CodCircuito)
					**/

					FETCH NEXT FROM CDatos1
					INTO @NBase_Cursor
			END
				
				close CDatos1
				deallocate CDatos1
			end
			else
			begin

				-- entran los circuitos de cosigna que hay que facturar de forma agrupada

				set @NumPed=@Ubicacion + CONVERT(nvarchar(100), getdate(), 112) + @AUT


				INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
						(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut)
						SELECT        @StatusPed AS Estado, TCIRCUITO_1.id_circuito AS Circ, TCIRCUITO_1.descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NumPed, TCIRCUITO_1.referencia_id AS Ref, 
									 TCIRCUITO_1.cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario AS Expr1, GETDATE() AS Gaur, TCIRCUITO_1.reserva_auto, @Tipo AS Expr2, @ConTipoVen AS Expr3, 
									 @ConDesde AS Expr4, @Facturar AS Expr5, TCIRCUITO_1.Cir_PedMarco, TCIRCUITO_1.Cir_ConNumFactCli, 0 AS Expr6
							FROM            dbo.circuito AS TCIRCUITO_1 
							WHERE        (TCIRCUITO_1.id_circuito = @CodCircuito) 
		
			end

		end	
		
		
		
		
		
		Set @Resul=0
		set @MsgTxt='se han insertado los datos'

		SET XACT_ABORT OFF;  --- AITOR. 2014-06-24 
		--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
	end

fin:
select  @Resul as Result, @MsgTxt as Msg, @IdTar

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_ProcesarPedido]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAX_ProcesarPedido]
	-- Add the parameters for the stored procedure here
	@circuito varchar(255), 
	@referencia varchar(255),
	@cantidadPedido numeric(18,5),
	@cantidadPiezasPorTarjeta numeric(18,5), 
	@cantidadTarjetasRojas int, 
	@usuario varchar(255), 
	@FechaEntregaConfirmada datetime,
	@pedido varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
	declare @cantidadTarjetas numeric(18,5);
	declare @CampGest varchar(255);
	declare @StatusTrat varchar(1);
	DECLARE @IdTar varchar(50);
	DECLARE @FeTar datetime;
	declare @cantTarjetasInt int;
	
	set @cantidadTarjetas = @cantidadPedido / @cantidadPiezasPorTarjeta;
	SELECT @cantidadTarjetas = CONVERT(INT, ROUND(@cantidadTarjetas, 0, 0), 0);
	set @cantTarjetasInt = CAST(@cantidadTarjetas as int);
	set @CampGest='AutWS' + '_' + CONVERT(nvarchar(100), @FechaEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113);
	SET @StatusTrat='G';
		
	if @cantidadTarjetas > @cantidadTarjetasRojas
	--actualizar todas las tarjetas
	begin
		print 'actualizar todas + generar nuevas tarjetas ';
		UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechaEntregaConfirmada,
				Tar_IdPedido = @pedido
				WHERE (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @circuito) and (estado='P' OR estado='A') AND sentido = 'S'
		
		INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol, Tar_IdPedido)
					SELECT        TOP (convert(int, @cantidadTarjetas-@cantidadTarjetasRojas)) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechaEntregaConfirmada, @pedido
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @circuito)
	end
	else
	begin
		print 'Actualizar algunas tarjetas:' + cast (@cantTarjetasInt as varchar(255));
		--hay que actualizar parte de las tarjetas
		SELECT    TOP (@cantTarjetasInt)  @FeTar=fecha_lectura,   @IdTar =id_lectura
					FROM            dbo.tarjeta
					WHERE        (gestionado IS NULL OR
									gestionado = '') AND (id_circuito = @circuito)
					ORDER BY fecha_lectura, id_lectura
		print @IdTar;
		UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechaEntregaConfirmada,
						Tar_IdPedido = @pedido
						WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @circuito)
						 and
						 (fecha_lectura < @FeTar or  --todas las tarjetas anteriores a la última seleccionada.
						 fecha_lectura = @FeTar) and id_lectura<=@IdTar --+ las que siendo la misma fecha, (si ha habido ajustes), la id lectura sea igual o menor.  
	end
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_SitTarjetas]...';


GO

-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
ALTER PROCEDURE [dbo].[PAX_SitTarjetas]
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
	DECLARE @Cliente AS varchar(255)
	DECLARE @Articulo AS varchar(255)
	DECLARE @CtdKan AS INT
	DECLARE @ArtCli as varchar(255)
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SobreStock as int
	DECLARE @PEDPTES AS INT
	DECLARE	@return_value int
	DECLARE	@stat int
	DECLARE @UltLec as datetime
	DECLARE @TipCon as integer
	DECLARE @TipCir	as integer
	DECLARE @num_kanbanOPT as int = 0
	DECLARE @Ubi as varchar(50)
	declare @transitoActivado as bit;

	
	SET @T =0
	SET @V=0
	SET @AE=0
	SET @A=0
	SET @R=0
	SET @SobreStock=0
	SET @PEDPTES=0
	SET @stat=0
	set @UltLec=getdate()
	set @TipCon=0
	set @TipCir=0

	begin
		SELECT        @Cliente= cliente_id, @Articulo=referencia_id, @T=num_kanban, @CtdKan=cantidad_piezas, @ArtCli=ref_cliente, @TipCon=tipo_contenedor, @TipCir=tipo_circuito, @num_kanbanOPT=num_kanbanOPT, @Ubi=ubicacion, @transitoActivado= TransitoActivado
		FROM            dbo.circuito
		WHERE        id_circuito = @Circuito
	end

	set @ArtCli=isnull(@ArtCli, '')
	set @transitoActivado = ISNULL(@transitoActivado, 0);
	
	SET @SobreStock=dbo.SS(@Circuito)

	SET @R=dbo.R(@Circuito)

	SET @A=dbo.A(@Circuito)--, @ubi
	
	if @transitoActivado = 1
	begin
		SET @AE=dbo.AE(@Circuito)
	end
	else
	begin
		SET @AE=0;
	end
	


	--************************************************

	declare @Modo as varchar(30)
	--SELECT @Modo=dbo.ubicacion.Ubi_Modo
	--FROM   dbo.circuito INNER JOIN
	--dbo.ubicacion ON dbo.circuito.ubicacion = dbo.ubicacion.id_ubicacion
	--WHERE (dbo.circuito.id_circuito = @Circuito)
	
	SELECT @Modo=Ubi_Modo
	FROM ubicacion
	WHERE id_ubicacion =@Ubi;

	set @Modo=isnull(@Modo, 0)

	if @Modo<>'DISP'
	BEGIN 
		--MODO EVENTOS DE CONSUMO
		SET @V=@T-@AE-@A-@R
		SET @V=@V+@SobreStock 	--todo sobrestock se suma como si fuese verde.
		IF @V<0 SET @V=0
	
	END
	ELSE
	BEGIN 
	--MODO DISPNIBILIAD
		SET @V=@R --ES EL RESULTADO QUE CONSEGUIMOS DE LAS ENTRADAS

		SET @R=@T-@AE-@A-@V
		--SET @V=@V+@SobreStock 	--todo sobrestock se suma como si fuese verde.
		IF @R<0 SET @R=0
		--XXXXXXXXXXXXXXXXXXXXXX
	END
	--************************************************
	
	
	
	
	
	
	--Consulta de situación del circuito
	SET @stat=dbo.Estado(@Circuito);
	SET @UltLec=dbo.UltLec(@Circuito);
	--RESULTADOS PARA MOSTRAR
	SELECT @Articulo AS ART, @ArtCli as ARTCLI, @T AS T,  @R AS R, @A AS A, @AE AS AE, @V AS V, @SobreStock as SS, @stat as Estado,  @Circuito as Circuito, @UltLec as UltLec, @TipCon as Cont, @TipCir as Circ, @num_kanbanOPT as OPT, @transitoActivado as Transito_Activado, @CtdKan as ctd_Kan

	
	--REGISTRAR DATOS, CONTROL DE USO
	set @Usu=isnull(@Usu,' ')

	--EXEC	[dbo].[PRegistro]
	--	@Reg_Usu = @Usu,
	--	@Reg_Ubi = '',
	--	@Reg_opt = N'SitTarjetas',
	--	@Reg_Art = @Articulo,
	--	@Reg_cli = @Cliente,
	--	@Reg_Txt = ''

	COMMIT;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaEnPrep]...';


GO



ALTER PROCEDURE [dbo].[PCtaEnPrep] 
	(@Cir varchar(255), @USU varchar(255)='', @Dis int=0)
AS
BEGIN
--PEDIDO FECHA CANTIDAD
	--DECLARE @A as int
	--DECLARE @PEDPTES AS INT
	--DECLARE @CtdKan AS INT
	declare @Tipo as int
	declare @cons as bit
	declare @Dias as int
	declare @Horas as int
	declare @Art as char(50)


	begin
		SELECT @Tipo=tipo_circuito, @cons=Cir_Consigna,  @Dias=entrega_dias, @Horas=entrega_horas, @Art=referencia_id
		FROM dbo.circuito
		WHERE id_circuito = @Cir
	end

	--set @CtdKan=isnull(@CtdKan,1)
	set @Tipo=isnull(@Tipo,1)
	set @cons=isnull(@cons,0)
	set @Dias=isnull(@Dias,0)
	set @Horas=isnull(@Horas,0)

	IF @Tipo=1 --AMARILLAS EN PROVEEDORES
	BEGIN
		if @cons=0  --SI NO ES CONSIGNA, COGEMOS LOS PED COMPRA NORMAL
			BEGIN
				SELECT        PURCHLINE_1.PURCHID AS PED, PURCHLINE_1.LINENUM AS LIN, PURCHLINE_1.ConfirmedDlv AS FEC, PURCHLINE_1.REMAINPURCHPHYSICAL AS CTD, @Tipo as TIPO   
				FROM            PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
				PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
				dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
				WHERE        (PURCHLINE_1.PURCHSTATUS = 1) AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND (dbo.circuito.id_circuito = @Cir) AND (PURCHLINE_1.DATAAREAID = N'ork') AND 
				(PURCHTABLE_1.PURCHASETYPE = 3) AND (PURCHTABLE_1.VENDORREF LIKE N'U%')
			END
		ELSE --SI ES ARTÍCULO DE CONSIGNA
			BEGIN
				SELECT        PURCHLINE_1.PURCHID AS PED, PURCHLINE_1.LINENUM AS LIN, PURCHLINE_1.ConfirmedDlv AS FEC, SUM(PURCHLINE_1.PURCHQTY) - SUM(PURCHLINE_1.INK_ENTRADASCONSIGNA) AS CTD, 
				@Tipo  AS TIPO
				FROM            PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
				PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
				dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
				WHERE        (PURCHLINE_1.PURCHSTATUS = 1) AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND (dbo.circuito.id_circuito = @Cir) AND (PURCHLINE_1.DATAAREAID = N'ork') AND 
				(PURCHTABLE_1.VENDORREF LIKE N'%') AND (PURCHLINE_1.PURCHASETYPE = 5)
				GROUP BY PURCHLINE_1.PURCHID, PURCHLINE_1.LINENUM, PURCHLINE_1.ConfirmedDlv, PURCHLINE_1.REMAINPURCHPHYSICAL
				HAVING        (SUM(PURCHLINE_1.PURCHQTY) > 0) AND (PURCHLINE_1.REMAINPURCHPHYSICAL > 0) AND (SUM(PURCHLINE_1.PURCHQTY) - SUM(PURCHLINE_1.INK_ENTRADASCONSIGNA) > 0)
	
			END
	END
	
	--AMARILLAS EN CIRCUITOS INTERNOS 
	IF @Tipo=2 
	BEGIN
		SELECT        DATAAREAID, PRODSTATUS, PRODID AS PED, 1 AS LIN, DLVDATE AS FEC, REMAININVENTPHYSICAL AS CTD, @Tipo as TIPO
		FROM            PREAXSQL.SQLAXBAK.dbo.PRODTABLE AS PRODTABLE_1
		WHERE        (PRODSTATUS = 4) AND (ITEMID = @Art) AND (DATAAREAID = N'ork') and 		INK_DIARIOKANBAN=@Cir






	END
	


	--AMARILLAS EN CLIENTES
	IF @Tipo=3
	BEGIN
		SELECT        dbo.VAX_EKAN_PENORD.SALESID AS PED, dbo.VAX_EKAN_PENORD.LINENUM AS LIN, dbo.VAX_EKAN_PENORD.SHIPPINGDATERECONFIRMED AS FEC, 
		dbo.VAX_EKAN_PENORD.REMAINSALESPHYSICAL AS CTD, @Tipo as TIPO
		FROM            dbo.VAX_EKAN_PENORD INNER JOIN
		dbo.circuito ON dbo.VAX_EKAN_PENORD.CUSTACCOUNT = dbo.circuito.cliente_id AND dbo.VAX_EKAN_PENORD.ITEMID = dbo.circuito.referencia_id AND 
		(SUBSTRING(dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM, 1, 3) = dbo.circuito.ubicacion OR
		SUBSTRING(dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM, 1, 2) = dbo.circuito.Cir_UbiAnt OR
		dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%') AND dbo.VAX_EKAN_PENORD.SALESTYPE = dbo.circuito.Cir_StdSalesType
		WHERE        (dbo.circuito.id_circuito = @Cir)

	END


	--AMARILLAS EN CIRCUITOS EXTERNOS. 
	IF @Tipo=4
	BEGIN
		SELECT        dbo.tarjeta.id_lectura AS PED, 1 AS LIN, dbo.tarjeta.Tar_FecEntSol AS FEC, dbo.circuito.cantidad_piezas AS CTD, @Tipo as TIPO
		FROM            dbo.tarjeta INNER JOIN
		dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito
		WHERE       
		      (estado = 'G' OR  estado = 'A_MODIF') AND (dbo.tarjeta.id_circuito = @Cir) AND (DATEADD(HH, @Horas, Tar_FecEntSol) > GETDATE())
	END
	

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaEnStockCons]...';


GO





ALTER PROCEDURE [dbo].[PCtaEnStockCons] 
	(@Cir varchar(255), @USU varchar(255)='', @Dis int=0)
AS
BEGIN
	
	declare @Tipo as int


	begin
		SELECT        @Tipo=tipo_circuito
		FROM            dbo.circuito
		WHERE        id_circuito = @Cir
	end

	set @Tipo=isnull(@Tipo,1)


	--EN PROVEEDORES NO GESTIONAMOS LA CTD EN TRANSITO, O CONSUMIDO, O CONFIRMADO O EN ALMACÉN
	if @Tipo=1
	begin
		SELECT '000000' AS ALB, GETDATE() AS FEC, GETDATE() AS ENT, 0 AS CTD, 'ABC' AS LOT, 0 as RECID, 0 as LINENUM, 1 as TIP, '1' AS LINK, '1' AS COLOR, 'Confirmar?' AS TXTVAL
	end

	--TRANSITO EN CLIENTES
	if @Tipo=3
	begin
		SELECT        dbo.VAX_EKAN_ALBA.PACKINGSLIPID AS ALB,  dbo.VAX_EKAN_ALBA.CREATEDDATETIME AS FEC,  

		DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME))

		AS ENT, 
		dbo.VAX_EKAN_ALBA.QTY AS CTD, dbo.VAX_EKAN_ALBA.INVENTBATCHID AS LOT, dbo.VAX_EKAN_ALBA.LINENUM, '1' AS TIP, '1' AS LINK, '1' AS COLOR, 'Confirmar?' AS TXTVAL
 ,0 AS RECID


		FROM            dbo.circuito INNER JOIN
		dbo.VAX_EKAN_ALBA ON dbo.circuito.cliente_id = dbo.VAX_EKAN_ALBA.ORDERACCOUNT AND dbo.circuito.referencia_id = dbo.VAX_EKAN_ALBA.ITEMID AND 
		(dbo.circuito.ubicacion = SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 3) OR
		SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 2) = dbo.circuito.Cir_UbiAnt OR
		dbo.VAX_EKAN_ALBA.PURCHASEORDER LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%')
		WHERE        (dbo.VAX_EKAN_ALBA.QTY > 0) AND (dbo.circuito.id_circuito = @Cir)  and
		DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME))<getdate()
 --aND (dbo.VAX_EKAN_ALBA.INK_FACTURADOCONSIGNA = 0)
		order by dbo.VAX_EKAN_ALBA.CREATEDDATETIME
	END




END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetAllCircuitos]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetAllCircuitos 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from circuito
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetAllConsumptionsByUser]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PCtaGetAllConsumptionsByUser] 
	-- Add the parameters for the stored procedure here
	@usuario varchar(255), @pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT t.id_lectura, t.fecha_lectura, t.num_lecturas, t.sentido, c.id_circuito, c.cliente_id, c.ubicacion, c.referencia_id as referencia_id, c.ref_cliente
		, tr.Reg_Ctd as ctdRegistroLecturas
	FROM ((SELECT DISTINCT UU.id_ubicacion FROM  sga_usuario U INNER JOIN usuario_ubicacion UU ON U.cod_usuario = UU.id_usuario 
		WHERE U.username = @usuario and U.password =@pwd
	) AS T1
		INNER JOIN circuito C ON C.ubicacion = T1.id_ubicacion) INNER JOIN tarjeta T ON C.id_circuito = T.id_circuito 
		 inner join ubicacion ub on T1.id_ubicacion = ub.id_ubicacion
		 left outer join TRegistroLecturas tr on  Ltrim(RTrim(tr.Reg_Lot)) + '_' +  rtrim(Ltrim(tr.Reg_Ser)) = t.id_lectura
	WHERE C.activo = 1 AND (T.estado = 'A' OR T.estado = 'P') AND (T.gestionado IS NULL OR T.gestionado ='')
		and c.descripcion <> 'TRAZABILIDAD'
		and t.sentido ='S' 
		and ub.activo = 1
	ORDER BY t.ubicacion, t.referencia_id
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetCircuitoInfo]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetCircuitoInfo 
	-- Add the parameters for the stored procedure here
	@idCircuito varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from circuito where id_circuito= @idCircuito
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetOrdersInformation]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetOrdersInformation 
	-- Add the parameters for the stored procedure here
	@usuario varchar(255), @pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT t.id_lectura, t.fecha_lectura, t.num_lecturas, t.sentido, c.id_circuito, c.cliente_id, c.ubicacion, c.referencia_id as referencia_id, c.ref_cliente,
			t.Tar_FecEntSol
	FROM ((SELECT DISTINCT UU.id_ubicacion FROM  sga_usuario U INNER JOIN usuario_ubicacion UU ON U.cod_usuario = UU.id_usuario 
		WHERE U.username = @usuario and U.password =@pwd
	) AS T1
		INNER JOIN circuito C ON C.ubicacion = T1.id_ubicacion) INNER JOIN tarjeta T ON C.id_circuito = T.id_circuito 
		 inner join ubicacion ub on T1.id_ubicacion = ub.id_ubicacion
	WHERE C.activo = 1 AND (t.estado = 'G' OR  t.estado = 'A_MODIF') AND (DATEADD(HH, c.entrega_horas, Tar_FecEntSol) > GETDATE())
		and ub.activo = 1
	ORDER BY t.ubicacion, t.referencia_id
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaHisConsumidas]...';


GO





ALTER PROCEDURE [dbo].[PCtaHisConsumidas] 
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	SET NOCOUNT ON;

SELECT   TOP (100)  dbo.tarjeta.sububicacion, dbo.tarjeta.estado, dbo.tarjeta.id_lectura , dbo.tarjeta.fecha_lectura,
				CAST( CASE 
                  WHEN gestionado IS NULL or gestionado = '' 
                     THEN 0
                  ELSE 1
             END AS bit) as gestionado
FROM            dbo.circuito INNER JOIN
				dbo.tarjeta ON dbo.circuito.id_circuito = dbo.tarjeta.id_circuito
	WHERE      (dbo.tarjeta.sentido = 'S') AND (dbo.circuito.id_circuito = @Circuito) 
ORDER BY dbo.tarjeta.fecha_lectura DESC

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PValidarLecPorticos]...';


GO


ALTER PROCEDURE [dbo].[PValidarLecPorticos] 
--DE MOMENTO SE LANZA CADA VEZ QUE HAY ACTUALIZACIÓN DE DISPOSITIVOS, COMO TRIGUER DE SEGUIMINETO DE ERRORES, AL FINAL DEL CODIGO
--ES DEMASIADO, Y SE PODRÍA PONER EN ACTUALIZACIÓN DE TARJETA, PERO NO GARANTIZARÍAMOS QUE SE BORRE AL TIEMPO, SI ES QUE NO HAY MÁS LECTURAS. 

AS

BEGIN

	--DECLARE	@return_value int
	DECLARE @Resul as varchar(100)
	DECLARE @Emaitza as int
	DECLARE @TConsLecPor as int
	DECLARE @NMinLecPor as int
	declare @Lecturas as int
	DECLARE @HayLecMin as varchar(100)
	DECLARE @Cli as varchar(100)
	DECLARE @Art as varchar(100)
	DECLARE @Ubi as varchar(100)

	set @Emaitza=1  --por defecto como error
	set @HayLecMin=''

	--Recuperamos los parámetros para considerar o no válido las lecturas de palet por el portico
	SELECT        @TConsLecPor=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'TMaxEntreLecturasPor')

	SELECT        @NMinLecPor=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'NMinLecturasPor')

	set @TConsLecPor=isnull(@TConsLecPor,0)
	set @NMinLecPor=isnull(@NMinLecPor,0)

	--REVISAMOS QUE TENEMOS CONEXIÓN DURANTE LOS ÚLTIMOS X SEGUNDOS, PARA RECIBIR LECTURAS, SI ES QUE NO. YA PROBAREMOS MÁS ADELANTE.
	--DE MOMENTO SIN IMPLANTAR, YA QUE NO SABEMOS SI VA A SER DE UN ARCO O DE OTRO. CONSIDERAMOS QUE SI HA LLEGADO LECTURA INICIAL, DEBERÍA DE 
	--HABER TENIDO CONNEXIÓN. SI VUELVE A ENVIAR, YA REACTIVAREMOS. 
	
	--PARA ACTUALIZAR A GESTIONADO, AQUELLAS LECTURAS DE CIRCUITOS A NIVEL DE PALET, QUE TENGAN MENOS LECTURAS AL DEFINIDO Y QUE HAYA PASADO YA EL TIEMPO MAXIMO
	--ENTRE LECTURAS.


	select @HayLecMin=id_lectura, @Lecturas=dbo.tarjeta.num_lecturas, @cli=dbo.circuito.cliente_nombre, @Art=dbo.circuito.referencia_id, @NMinLecPor=dbo.circuito.Cir_LecMinParaCons, @Ubi=dbo.circuito.ubicacion
	from 	dbo.tarjeta INNER JOIN  dbo.circuito 
	ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito AND dbo.tarjeta.num_lecturas < dbo.circuito.Cir_LecMinParaCons
	WHERE        (dbo.tarjeta.tipo = 'P') AND (dbo.circuito.tipo_contenedor = 2) AND (dbo.tarjeta.estado = 'P') AND (dbo.tarjeta.gestionado IS NULL OR
                         dbo.tarjeta.gestionado = '') AND (DATEADD(ss, @TConsLecPor, dbo.tarjeta.ultima_lectura) < GETDATE())
	

	set @HayLecMin= isnull(@HayLecMin,'')
	set @Lecturas= isnull(@Lecturas,0)
	set @NMinLecPor=isnull(@NMinLecPor,1)


	declare @cad varchar(200)
	set @cad = 'Se han realizado ' + CONVERT(CHAR(3),@Lecturas) + 'lecturas de '+ @HayLecMin + ' en ' + @cli + ' (' + @Ubi + '), y el mínimo debe ser de ' + CONVERT(CHAR(3),@NMinLecPor)  + '. '
	SET @cad= @cad + ' Revisar si realmente se debe considerar o no la lectura. Artículo ' + @Art + '.'

	if @HayLecMin<>''  
	begin 
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'mmujika@orkli.es; dallodoli@orkli.it; alert@ekanban.es', 
		@Tem = N'Faltan Lecturas Mínimas',
		@Cue = @cad
	end 



	--PARA ANULAR LECTURAS QUE NO HAYA CTD MÍNIMA EN TIEMPO MÁXIMO
	update   dbo.tarjeta 
	SET gestionado = 'GesAUT_NumLecMin', estado='T'
	from
	dbo.tarjeta INNER JOIN  dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito
	WHERE      id_lectura=@HayLecMin AND sentido='S'
	


	--PARA RECUPERAR LECTURAS VIEJAS, QUE SE ANULARON PORQUE HAYA LECTURA NUEVA
	update   dbo.tarjeta 
	SET gestionado = '', estado='P', dbo.tarjeta.num_lecturas=1
	from
	dbo.tarjeta
	WHERE        (gestionado = 'GesAUT_NumLecMin') AND (DATEADD(ss, @TConsLecPor, ultima_lectura) > GETDATE()) -- (estado = 'T') AND
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_UserIsDBAdmin]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PAX_UserIsDBAdmin
	-- Add the parameters for the stored procedure here
	@username varchar(255),
	@pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from sga_usuario where username = @username and password = @pwd and isDBAdmin = 1
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaMenuClientes]...';


GO



ALTER PROCEDURE [dbo].[PCtaMenuClientes] 
(@USU varchar(255)='', @Dis int=0)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT        derivedtbl_1.localizacion as Cod, custtable_1.NAME as Nom, DATAAREAID as CodEmpresa, LanguageId as Idioma
FROM            (SELECT DISTINCT dbo.ubicacion.localizacion
                          FROM            dbo.usuario_ubicacion INNER JOIN
                                                    dbo.sga_usuario ON dbo.usuario_ubicacion.id_usuario = dbo.sga_usuario.cod_usuario INNER JOIN
                                                    dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion
                          WHERE        (dbo.sga_usuario.username = @USU) AND (dbo.ubicacion.activo = 1) AND (dbo.sga_usuario.activo = 1) --AND (dbo.ubicacion.localizacion <> '00000000')
                          UNION
                          SELECT DISTINCT dbo.usuario_cliente.id_cliente
                          FROM            dbo.usuario_cliente INNER JOIN
                                                   dbo.sga_usuario AS sga_usuario_1 ON dbo.usuario_cliente.id_usuario = sga_usuario_1.cod_usuario
                          WHERE        (sga_usuario_1.username = @USU)) AS derivedtbl_1 INNER JOIN
                         PREAXSQL.SQLAXBAK.dbo.custtable AS custtable_1 ON derivedtbl_1.localizacion = custtable_1.ACCOUNTNUM
WHERE        (custtable_1.DATAAREAID = N'ork')



END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PGetUserInfo]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PGetUserInfo 
	-- Add the parameters for the stored procedure here
	@username varchar(255),
	@password varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select *
	from sga_usuario
	where username = @username and [password] = @password
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PRegistro]...';


GO

ALTER PROCEDURE [dbo].[PRegistro]
	-- Add the parameters for the stored procedure here
@Reg_Usu varchar(255)='',
@Reg_Ubi varchar(255)='',
@Reg_opt varchar(255)='',
@Reg_Art varchar(255)='',
@Reg_Txt varchar(255)='', 
@Reg_Cli varchar(255)=''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


--INSERT INTO [dbo].[TRegistro]  ([Reg_Fec] ,[Reg_Usu],[Reg_Ubi],[Reg_opt],[Reg_Art],[Reg_Txt],[Reg_Cli])
--     VALUES
--           (getdate(),@Reg_Usu, @Reg_Ubi,@Reg_opt,@Reg_Art,@Reg_Txt, @Reg_Cli)

		   

	--UPDATE [dbo].[sga_usuario]
	--SET [conectado] = 1
	--,[conectUltFec] = getdate()
	--WHERE [username]=@Reg_Usu

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAlbEnv]...';


GO


ALTER PROCEDURE [dbo].[PAccAlbEnv]
(
@Usuario AS varchar(255)='', @Cliente varchar(255)='', @Recid varchar(50)='', 
@Alb varchar(255)='', @Lin varchar(5)='', @Art varchar(100)=''
)
AS

BEGIN
	DECLARE @Rep varchar(250)
	DECLARE @Ser varchar(25)=''
	DECLARE @Resul INT=1
	DECLARE @Msg varchar(250)=''
	DECLARE @Url varchar(250)=''
	SET NOCOUNT ON;
	
	SET XACT_ABORT ON;


	SET @Resul=0  --1 es malo, 0 es ok
	set @Msg='Error'


	--ACCIONES
	if @Recid<>'' and @Cliente='01900200'
	begin

		--LANZAMOS EL PEDIDO PARA FACTURACIÓN Y REPOSICIÓN, DANDO CONSUMO AL ALBARÁN, LINEA Y ARTÍCULO Y MARCAMOS COMO FACTURADO. 
		--************************* PENDIENTE
		--
		begin distributed transaction
		
		
		UPDATE  PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPTRANS
		SET INK_FACTURADOCONSIGNA = 1
		WHERE RECID=@Recid
	
		SET @Resul=0
		set @Msg='OK'
		
		commit transaction;
	end




	--RESULTADOS para Acciones, como para Reporting
	SELECT  'PAccAlbEnv' as Tipo, 
			@Resul as Resul, 
			@Msg as Msg  
			

	
		--EXEC	[dbo].[PRegistro]
		--@Reg_Usu = @Usuario,
		--@Reg_Ubi = @Recid,
		--@Reg_opt = N'[PCtaAccReports]',
		--@Reg_Art = @Art,
		--@Reg_cli = @Cliente,
		--@Reg_Txt = @Usuario
		

			

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_ActualizarPermisosReport]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PAX_ActualizarPermisosReport 
	-- Add the parameters for the stored procedure here
	@idReport varchar(255),
	@usuarios varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from usuario_AccReports where id_accrep = @idReport;
	
	DECLARE @start INT, @end INT 
	declare @usuario varchar(255)
    SELECT @start = 1, @end = CHARINDEX('|', @usuarios) 
    declare @index int = 1;
    WHILE @start < LEN(@usuarios) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@usuarios) + 1
       
        set @usuario= SUBSTRING(@usuarios, @start, @end - @start) ;
        insert into usuario_AccReports (id_usuario, id_accrep) values (@usuario, @idReport);
        SET @start = @end + 1 
        SET @end = CHARINDEX('|', @usuarios, @start)
        
    END 
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_IntegrarPedidos]...';


GO

ALTER PROCEDURE [dbo].[PAX_IntegrarPedidos] 

AS

BEGIN
SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- iker. 20151020
						--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
	--DECLARE	@return_value int
	DECLARE @Resul as varchar(100)
	DECLARE @Emaitza as int
	DECLARE @EmaitzaErr as int
	DECLARE @Des as varchar(300)
	DECLARE @JournalNumMIN as varchar(100)
	DECLARE @ErrIntegracion as varchar(100)

	set @Emaitza=1  --por defecto como error
	set @EmaitzaErr=1

	SELECT        @JournalNumMIN= MIN(JournalNum) 
	FROM            PREAXSQL.ORKLB000.dbo.TAUX_CREAR_PEDIDO AS TAUX_CREAR_PEDIDO_1
	WHERE        (Status = 0)

	--si no hay registro en 0
	if ISNULL(@JournalNumMIN,'')='' set @JournalNumMIN='99999'
		

	--******************************************************************************************************
		EXEC	PREAXSQL.[ORKLB000].[dbo].[PAX_IntegrarPedKanban]
		--ws alojado en wspro1, y realizado por iniker. Cyc no ha participado
	--******************************************************************************************************
		



	--Para ver si ha subido ok o no, vamos a mirar si hay algún registro todavía en 0. No debería de estar. 
	SELECT     @Resul=JournalNum
	FROM       PREAXSQL.ORKLB000.dbo.TAUX_CREAR_PEDIDO AS TAUX_CREAR_PEDIDO_1
	WHERE      (Status = 0)

	if ISNULL(@Resul,'')='' set @Emaitza=0
	
	if @Emaitza=1
	begin
		EXEC [dbo].[PEnvCorreo]
		@Des = N'alert@ekanban.es',--; loli@orkli.es; aarrondo@orkli.es',
		@Tem = N'Fallo en la Integración de Pedidos al AX',
		@Cue = N'No se han integrado los pedidos en el AX. Se debe de revisar el proceso de integración. En caso de tener problemas, ejecutar la integarción manual desde el AX: Menú Clientes, Periódico : INTEGRACIÓN PEDIDOS KANBAN'
	end
		
	SELECT @Emaitza AS RESULT
	-- SELECT 1 AS RESULT en caso de que esté mal. lo considera el websercice como error
	-- de momento como en el AX no tenemos feedback, dejamos como fijo. Como si fuera ok. IES, 10-7-2014



	--PARA MIRAR SI EN LA INTEGRACIÓN HAY ALGÚN REGISTRO CON ERROR DE INTEGRACIÓN DE PEDIDOS

	
	SELECT       @ErrIntegracion=JournalNum
	FROM            PREAXSQL.ORKLB000.dbo.TAUX_CREAR_PEDIDO AS TAUX_CREAR_PEDIDO_1
	WHERE        (Status = 2) AND (JournalNum >= @JournalNumMIN)

	if ISNULL(@ErrIntegracion,'')='' set @EmaitzaErr=0
	


	if @EmaitzaErr=1
	begin
		EXEC [dbo].[PEnvCorreo]
		@Des = N'alert@ekanban.es',--; loli@orkli.es',
		@Tem = N'Error en Integrar un Pedido al AX',
		@Cue = N'No se han integrado todos los pedidos en el AX. Revisar el listado de Crear Pedidos y mirar el tipo de error'
	end

	SET XACT_ABORT OFF;  --- iker. 20151020
		--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCta_GetRespuestaBytes]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCta_GetRespuestaBytes
	-- Add the parameters for the stored procedure here
	@idMatricula int,
	@IdPregunta varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select respuestaBytes from MatriculaRespuesta where IdMatricula = @idMatricula and IdPregunta = @IdPregunta;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetAllRoles]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetAllRoles 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from sga_rol where activo  = 1
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetEmpresas]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetEmpresas
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM TEmpresa;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetRegistroLectura]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PCtaGetRegistroLectura] 
	-- Add the parameters for the stored procedure here
	@idCircuito varchar(255) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        TOP (1) *
		FROM            dbo.TRegistroLecturas
		WHERE        Ltrim(RTrim(Reg_Lot)) + '_' +  rtrim(Ltrim(Reg_Ser)) = @idCircuito
		ORDER BY Reg_Fec
		
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetTiposCircuito]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetTiposCircuito 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from tipo_circuito
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaGetTiposContenedor]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE PCtaGetTiposContenedor 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from tipo_contenedor 
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PEnvSitArcRFID]...';


GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PEnvSitArcRFID] 
	-- Add the parameters for the stored procedure here
	--@Des as varchar(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	 DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>eKanban RFID Arkuak</H1>' +
    N'<table border="1">' +
    N'<tr><th>Arkua</th>' +
	N'<th>Izena</th>' +
    N'<th>Egoera</th>' +
	N'<th>Azken Konexioa</th>' +
	N'<th>IP</th>' +
    N'<th>Bertxioa</th>' +
	N'<th>Azken piztuera</th></tr>' +
    CAST ( ( SELECT "td/@align" =  'center', td = [id_dispositivo],   ''
      ,td = [descripcion],   ''
      ,"td/@align" =  'center',  "td/@bgcolor" =  case when [Dis_EnError]=0 then 'green' ELSE 'red' END, td = case when [Dis_EnError]=0 then 'OK' ELSE 'ERR' END,  ''
      ,td = convert(varchar(50), [ultima_conexion], 120),   ''
	  ,td = 'https://' + [ip] + ':10000',   ''
      ,"td/@align" =  'center', td = [version], '',td=convert(varchar(50), [fecha_inicio], 120)
	  FROM [dbo].[dispositivo] where activo=1
	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;


	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='jsolis@orkli.es; eunanue@orkli.es; iker@orkli.es',
	  @subject='eKanban RFID Arkuak',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'





	  /**



	  para ver la última lectura de cada portico. iker.
	  (SELECT        MAX(fecha_lectura) AS Expr1
                               FROM            dbo.tarjeta
                               GROUP BY ubicacion, tipo
                               HAVING         (ubicacion = dbo.dispositivo_ubicacion.id_ubicacion) AND (tipo = 'P')) AS UltLec




	  DECLARE @tableHTML  NVARCHAR(MAX) ;

SET @tableHTML =
    N'<H1>Work Order Report</H1>' +
    N'<table border="1">' +
    N'<tr><th>Work Order ID</th><th>Product ID</th>' +
    N'<th>Name</th><th>Order Qty</th><th>Due Date</th>' +
    N'<th>Expected Revenue</th></tr>' +
    CAST ( ( SELECT td = [id_dispositivo],   ''
      ,td = [activo],   ''
      ,td = [imei],   ''
      ,td = [num_serie],   ''
      ,td = [descripcion],   ''
      ,td = [Dis_UltAccFec]
  FROM [dbo].[dispositivo]
  FOR XML PATH('tr'), TYPE 
    ) AS NVARCHAR(MAX) ) +
    N'</table>' ;

EXEC msdb.dbo.sp_send_dbmail 
	@profile_name='posta',
	@recipients='iker@orkli.es',
    @subject = 'Work Order List',
    @body = @tableHTML,
    @body_format = 'HTML'











	  SELECT        TOP (100) PERCENT dbo.dispositivo.id_dispositivo, dbo.dispositivo.descripcion, dbo.dispositivo.ultima_conexion, dbo.dispositivo.Dis_EnError, dbo.dispositivo.ip, dbo.dispositivo.version, 
                         dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has, dbo.TRegistroEstPort.RegPor_Err, DATEDIFF(mi, dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has) AS Expr1
FROM            dbo.dispositivo INNER JOIN
                         dbo.TRegistroEstPort ON dbo.dispositivo.id_dispositivo = dbo.TRegistroEstPort.RegPor_Dis
WHERE        (dbo.dispositivo.id_dispositivo = '4') AND (dbo.TRegistroEstPort.RegPor_Err = 1) AND (DATEDIFF(mi, dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has) > 10)
ORDER BY dbo.TRegistroEstPort.RegPor_Has DESC
	  **/



END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PRegistroWeb]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PRegistroWeb] 
	-- Add the parameters for the stored procedure here
	(@Name varchar(200) = '',
	@Company varchar(200) = '',
	@Website varchar(200) = '',
	@Tel varchar(200) = '',
	@Email varchar(200) = ''
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Resul INT=1
	DECLARE @Msg varchar(250)=''


	INSERT INTO [dbo].[TRegistroWeb]
           ([RegWeb_Date]
           ,[RegWeb_Name]
           ,[RegWeb_Company]
           ,[RegWeb_Website]
           ,[RegWeb_Tel]
           ,[RegWeb_Email])
      
     VALUES
           (GETDATE()
           ,@Name
           ,@Company
           ,@Website
           ,@Tel
           ,@Email)

	Set @Resul=0
	set @Msg='se han insertado los datos'

	--RESULTADOS para Acciones, como para Reporting
	SELECT  'PAccAlbEnv' as Tipo, 
			@Resul as Resul, 
			@Msg as Msg  


	DECLARE	@return_value int
	Declare @Texto varchar(400)

	set @Texto = 'Se ha registrado un nuevo contacto en la Web ekanban:' + CHAR(13) + CHAR(13)
	set @Texto = @Texto + 'Nombre: ' + @Name + CHAR(13)
	set @Texto = @Texto + 'Empresa: ' + @Company + CHAR(13)
	set @Texto = @Texto + 'Web: ' + @Website + CHAR(13)
	set @Texto = @Texto + 'Tel: ' + @Tel + CHAR(13)
	set @Texto = @Texto + 'eMail: ' + @Email + CHAR(13)
	

	EXEC	@return_value = [dbo].[PEnvCorreo]
			@Des = N'info@ekanban.es; iker@orkli.es',
			@Tem = N'Nuevo Registro Web en eKanban',
			@Cue = @Texto


END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[PAX_HistoricoVerdes]...';


GO

-- =============================================
-- Author:		IMR
-- Create date: 4-10-2018
-- Description:	Procedimiento para actualizar el histórico con los valores de las tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_HistoricoVerdes]
(
@Circuito varchar(255)='', @Origen AS varchar(255)='', @Usuario AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
		DECLARE @V as int 
		DECLARE @AE as int
		DECLARE @A as int
		DECLARE @R as int
		DECLARE @SobreStock as int
		DECLARE @UltLec as datetime
		DECLARE @OrigenActualizacion as varchar(255)

		SET @V = dbo.V(@Circuito)
		SET @A = dbo.A(@Circuito)
		SET @R = dbo.R(@Circuito)
		SET @AE = dbo.AE(@Circuito)
		SET @SobreStock=dbo.SS(@Circuito)
		SET @OrigenActualizacion = @Origen
				
		PRINT @Circuito
		
		IF NOT EXISTS (
					   /*SELECT IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito AND CtdVerdes = @V AND CtdRojas = @R AND 
		               CtdAmarillasVerdes = @AE AND CtdSobreStock = @SobreStock */
		               SELECT MAX(fecha) as fecha, IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito AND CtdVerdes = @V AND CtdRojas = @R AND 
								CtdAmarillasVerdes = @AE AND CtdSobreStock = @SobreStock AND
								fecha in (select max(fecha) as fecha from THistoricoVerdes where IdCircuito = @Circuito)
		               GROUP BY IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               )
			BEGIN
				INSERT INTO ekanban.dbo.THistoricoVerdes (IdCircuito, Fecha, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
															CtdSobreStock,tag, origen, Usuario)
				values (@Circuito, getdate(), @V,@A,@R, @AE, @SobreStock, null, @OrigenActualizacion, @Usuario)
			END
		
	COMMIT TRANSACTION;
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[PAX_IRA_PRUEBAS]...';


GO

-- =============================================
-- Author:		IMR
-- Create date: 4-10-2018
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_IRA_PRUEBAS]
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
		DECLARE @V as int 
		DECLARE @AE as int
		DECLARE @A as int
		DECLARE @R as int
		DECLARE @SobreStock as int
		DECLARE @UltLec as datetime
		--DECLARE @transitoActivado as bit;

		SET @V = dbo.V(@Circuito)
		SET @A = dbo.A(@Circuito)
		SET @R = dbo.R(@Circuito)
		SET @AE = dbo.AE(@Circuito)
		SET @SobreStock=dbo.SS(@Circuito)
		
		PRINT @Circuito
		
		INSERT INTO dbo.THistoricoVerdes (IdCircuito, Fecha, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
			CtdSobreStock,tag) values (@Circuito, getdate(), @V,@A,@R, @AE, @SobreStock, null)
	COMMIT TRANSACTION;
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[PAX_PatronLec_BAK]...';


GO


-- =============================================
-- Author:		IKER
-- Create date: 13/5/2014
-- Description:	Procedimiento para conseguir datos, según patrón de lectura del código de barras

-- Este procedimiento se ejecuta siempre que hay una lectura de un codigo de barras en los dispositivos ANDROID (Consumo y Consulta)
-- y cuando hay una lectura del portico. 
-- El objetivo es conseguir la OF y la SERIE de la información de la etiqueta/QR/RFID, etc. 
-- El resultado da, OF, SERIE y GRAB. El campo grab es la que se utiliza para garantizar que no se repite la lectura de la misma etiqueta. 
-- Si el resultado tiene OF no válido (Que no sea null, vacio o 000000) el proceso siguiente, coge el Articulo y cliente desde el resultado del procedimiento. 
-- Si el resultado tiene OF válido, va a VAX_EKAN_OF y coge como resultado el artículo y cliente. 

-- una vez que tiene, ARTICULO, CLIENTE Y USUARIO (el de la aplicación), consulta en la tabla de circuitos y obtiene el código circuito para seguir el proceso. 
-- =============================================
CREATE PROCEDURE [dbo].[PAX_PatronLec_BAK]
(
@Txt varchar(255)='', @Usu AS varchar(255)='', @Ubi AS varchar(255)='', @Pant AS int=0 , @Disp as varchar(255)='', @Ant as varchar(50)='', @Sen as varchar(5)=''
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TP AS INT=0  --TIPO DE PATRON APLICADO. SI ES 0 EN RESULTADO, NO HA ENTRADO EN NINGUNO DEFINIDO. 
	DECLARE @L AS VARCHAR(25)=''
	DECLARE @L1 AS INT
	DECLARE @L2 AS INT
	DECLARE @S AS VARCHAR(100)=''
	DECLARE @S1 AS INT
	DECLARE @S2 AS INT
	DECLARE @A AS VARCHAR(25)=''
	DECLARE @A1 AS INT
	DECLARE @A2 AS INT
	DECLARE @AR AS VARCHAR(25)=''
	DECLARE @Cli AS VARCHAR(25)=''
	DECLARE @UbiTip AS VARCHAR(25)=''
	DECLARE @V AS VARCHAR(25)=''
	DECLARE @CliOF AS VARCHAR(25)=''
	DECLARE @CliUBI AS VARCHAR(25)=''
	DECLARE @I AS INT
	DECLARE @CIR AS VARCHAR(100)=''
	DECLARE @CTD AS VARCHAR(100)=''
	DECLARE @PED AS VARCHAR(100)=''
	DECLARE @NIVACC AS INT
	DECLARE @ACCION AS VARCHAR(10)=''
	DECLARE @CTDACCCION AS INT
	DECLARE @TipCont AS INT
	DECLARE @position int
	DECLARE @string char(500)
	DECLARE @1P AS VARCHAR(100)=''
	DECLARE @16K AS VARCHAR(100)=''
	DECLARE @2L AS VARCHAR(100)=''
	DECLARE @1J AS VARCHAR(100)=''
	DECLARE @5J AS VARCHAR(100)=''
	DECLARE @6J AS VARCHAR(100)=''
	DECLARE @7Q58 AS VARCHAR(100)=''
	DECLARE @7QGT AS VARCHAR(100)=''
	DECLARE @16D AS VARCHAR(100)=''
	DECLARE @2P AS VARCHAR(100)=''
	DECLARE @ALB AS VARCHAR(100)=''
	DECLARE @ALBLIN AS VARCHAR(100)=''
	DECLARE @UbiCap AS VARCHAR(25)=''
	DECLARE @LotCap AS VARCHAR(50)=''
	DECLARE @Emp AS VARCHAR(3)=''
	DECLARE @URLPlano AS VARCHAR(50)=''
	--SI LA UBICACIÓN ACTUAL, TIENE CHECK DE REGISTRO DE TRAZABILIDAD. 
	DECLARE @RegTraz bit=0
	DECLARE @EMPR NCHAR(3)=''
	declare @cade varchar(200)
	declare @TIPOETIQUETA VARCHAR(50) = NULL;
	DECLARE @TIPO AS VARCHAR(50) = NULL;
	DECLARE @INDICE AS INT = 0;
	DECLARE @QRDATA VARCHAR(255) = NULL;

	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT
	begin transaction
	--*************************************************************************************************************************************************
	--Si la ubicación tiene predefinida un tipo concreto de etiqueta tratamos esa
	 SELECT @TIPOETIQUETA = TipoEtiqueta FROM ubicacion WHERE id_ubicacion = @UBI;
	 PRINT @TIPOETIQUETA;
	 IF @TIPOETIQUETA <> ''
	 BEGIN
		PRINT 'TIPO ETIQUETA PERSONALIZADA';
		DECLARE TIPO CURSOR FOR select * from dbo.fnSplitString (@TIPOETIQUETA,':');
		OPEN TIPO
		FETCH NEXT FROM TIPO INTO @TIPO
		WHILE @@fetch_status = 0
		BEGIN
			IF @INDICE = 0 SET @TP = @TIPO;
			else set @qrData = @tipo;
			SET @INDICE = @INDICE + 1;
			FETCH NEXT FROM TIPO INTO @TIPO
		END
		print @tp;
		DECLARE @VARIABLE VARCHAR(255);
		DECLARE @VALOR VARCHAR(255);
		declare @var varchar(255);
		declare @posVariable int = -1;
		DECLARE DATA CURSOR FOR select * from dbo.fnSplitString (@QRDATA,'|');
		OPEN DATA
		FETCH NEXT FROM DATA INTO @var
		WHILE @@fetch_status = 0
		BEGIN
			select @posVariable=  CHARINDEX('=', @var); 
			set @VARIABLE = SUBSTRING(@var, 0, @posVariable);
			set @VALOR = SUBSTRING(@var, @posVariable + 1, len(@var));
			print @variable +  @valor;
			IF @VARIABLE='CTD'  BEGIN PRINT @VALOR;SET @CTD = DBO.GS1Data(@TXT, @VALOR); END
			IF @VARIABLE='LOTE' SET @L = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='REF' SET @A = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='PROV' SET @V = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='BULTO' SET @S = DBO.GS1Data(@TXT, @VALOR);
			FETCH NEXT FROM DATA INTO @VAR
		END
		print 'lote';
		print @L;
		PRINT 'ERREFERENTZIA=';
		PRINT @A;
		deallocate  tipo;
		deallocate  data;
		
		set @CIR=''

		--SELECT @S=DBO.GS1Data(@txt,'S')
		set @L=isnull(@L,'')
		IF @L='' SET @L='0' 
		PRINT @CTD;
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		PRINT 'CANTIDAD OK';
		--revisamos a qué empresa corresponde la ubicación
		SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)

		--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
		--Cogemos el cod cliente de la ubiación
		SET @A=ISNULL(@A,'')
		SET @CIR=''
		--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 
		PRINT 'REFERENCIA_ID=';
		PRINT @A;
		SELECT        @CIR=id_circuito, @Cli=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1

		set @ant=@V

		SET @CIR=ISNULL(@CIR,'')

		--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
		IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
		--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

		BEGIN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
		END


		SET @S=RTRIM(@S) + '_' + @L
		SET @L='000000'
		
		if (not exists(select * from tarjetaMetadata where id_lectura=@L + '_' + @S and sentido=@Sen))
			insert into tarjetaMetadata (id_lectura, sentido, txtLectura, tipo,fechaPrimeraLectura ,fechaUltimaLectura, PatronLectura) values (@L + '_' + @S, @Sen, @Txt, @tp, GETDATE(), GETDATE(), @TIPOETIQUETA);
		else update tarjetaMetadata set fechaUltimaLectura = GETDATE(), PatronLectura=@TIPOETIQUETA where id_lectura = @L + '_' + @S and sentido = @Sen
		GOTO REGISTROLECTURAS;
	 END
	
	
	--*************************************************************************************************************************************************
	-- Cuando no se inserta ningún dato manualmente en la referencia
	if @txt='P'
	BEGIN
		SET @TP=1
		SET @L='000000'
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación y que sean activos
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi AND (activo = 1) ORDER BY num_kanban
	END

	--*************************************************************************************************************************************************
	--Si es un artículo el que se lee. Automaticamente le añade una P al inicio
	if @txt<>'P' AND left(@txt,1)='P' AND left(@txt,2)<>'P*' AND left(@txt,4)<>'PROV' and  (CHARINDEX('_', @txt, 1) = 0)
	begin
		--P08296603
		SET @TP=2
		SET @L ='000000'
		SET @Cli ='00000000'

		SET @AR =SUBSTRING(@txt, 2, LEN(@TXT))

		SELECT      @Cli= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		--en vez de esta consulta, mejor la de lista de precios, o ref cruzadas, para garantizar que es una del cliente.
		SELECT   @A = ITEMID
		FROM     PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE
		WHERE    (DATAAREAID = N'ORK') AND (ITEMID = @AR)

		IF @A = ''
		BEGIN
			SELECT     @A= CUSTVENDEXTERNALITEM.ITEMID
			FROM       PREAXSQL.SQLAXBAK.dbo.CUSTVENDEXTERNALITEM AS CUSTVENDEXTERNALITEM INNER JOIN
					   PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE ON CUSTVENDEXTERNALITEM.DATAAREAID = INVENTTABLE.DATAAREAID AND CUSTVENDEXTERNALITEM.ITEMID = INVENTTABLE.ITEMID
			WHERE        (CUSTVENDEXTERNALITEM.DATAAREAID = N'ork') AND (CUSTVENDEXTERNALITEM.EXTERNALITEMID = @AR) AND (CUSTVENDEXTERNALITEM.CUSTVENDRELATION = @Cli)
			ORDER BY CUSTVENDEXTERNALITEM.EXTERNALITEMID
		END
	end 



	--*************************************************************************************************************************************************
	-- cuando se lee una consulta con * para realizar operaciones. 
	--  *A2-609:T12
	--  *A2-609:N5
	if left(@txt,2)='P*' and left(@txt,3)<>'P*U'
	begin
		SET @TP=3
		IF CHARINDEX(':', @TXT,1) > 0
			BEGIN
				SET @L='000000'	
				SET @A1 = 3
				SET @A2 =CHARINDEX(':', @TXT,1)
				IF @A2>0 
				begin
					SET @A =SUBSTRING(@txt, @A1, @A2-@A1)
					sET @ACCION =SUBSTRING(@txt, @A2+1, LEN(@TXT)-(@A2))
					set @CTDACCCION= SUBSTRING(@ACCION, 2, len(@ACCION)-1)
					set @ACCION=SUBSTRING(@ACCION, 1, 1)
				end
		
				--CIRCUITO? SI EXISTE
				SELECT       @CIR= id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        ubicacion = @Ubi AND referencia_id = @A
		
				--si no existe, cogemos el cliente de la ubicación
				IF @CIR=''
					BEGIN
						SELECT        @Cli=localizacion
						FROM            dbo.ubicacion
						WHERE        id_ubicacion = @Ubi
					END
		
				--Cogemos el rol del usuario
				SELECT        @NIVACC= ptr_rol
				FROM            dbo.sga_usuario
				WHERE        USERNAME = @Usu
		
				-- Con permiso, para crear y no hay circuito
				IF @NIVACC=7 AND @ACCION='N' AND @CIR=''
				BEGIN
		
					-- CREAR CIRCUITO
					INSERT INTO [dbo].[circuito]
					   ([cliente_id],[id_circuito],[referencia_id],[activo],[cantidad_piezas],[cliente_nombre],[descripcion],[entrega_dias]
					   ,[entrega_horas],[mail],[num_kanban],[ref_cliente],[reserva_auto],[ubicacion],[tipo_circuito],[tipo_contenedor])
					VALUES
					   (@Cli, @Ubi+@A, @A, 1, 1, '', '', 0,0,'', @CTDACCCION, '', 1, @Ubi, 1,1)

					   SET @S='NuevoCir'
				END
	
				-- Con permiso, para modif y  hay circuito
				IF @NIVACC=7 AND @ACCION='T' AND @CIR<>''
				BEGIN
					SET @S='ActNumKan'
					UPDATE [dbo].[circuito]
					SET [num_kanban] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END

				IF @NIVACC=7 AND @ACCION='Q' AND @CIR<>''
				BEGIN
					SET @S='ActQPzas'
					UPDATE [dbo].[circuito]
					SET [cantidad_piezas] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END
			END
			else
			begin
				DECLARE	@return_value int
				if left(@txt,4)='P*er' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvUsers]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*rf' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitArcRFID]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*eg' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitPanel]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
				if left(@txt,4)='P*01' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0001'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				if left(@txt,4)='P*02' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0002'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*03' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0003'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
			end

	end


	----*************************************************************************************************************************************************
	----PARA PROBAR EL EKANBAN, BORRANDO LAS ENTRADAS AL CIRCUITO
	----/**
	--if left(@txt,2)='P*' and (left(@txt,5)='P*U00') --OR left(@txt,5)='P*U07'
	--begin
	--	begin
	--		DELETE FROM [dbo].[tarjeta]
	--		WHERE        (ubicacion = SUBSTRING(@TXT,3,3))

	--	end
	--	SET @L='000000'	
	--	--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
	--	SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
	--end
	----**/
	----*************************************************************************************************************************************************



	-- si es tipo de SMSTO
	if left(@txt,5)='SMSTO'
	begin
		SET @TP=4
		--LOS MENSAJES NORMALES:     (QUE NO SEAN DE FORMACIÓN NI DE TIPO ORK. QUE NO EXISTA UORK O FORMACIÓN)
		IF CHARINDEX('UORK_', @TXT,1)=0 and CHARINDEX('FORMACION', @TXT,1)=0 
		BEGIN
			--SMSTO:+34696469617:H123456_179993_20900-31
			SET @L1 =CHARINDEX('H', @TXT,15)+1
			SET @L2 =CHARINDEX('_', @TXT,@L1)
			SET @L =SUBSTRING(@txt, @L1, @L2-@L1)

			SET @S1 =CHARINDEX('_', @TXT,@L2)+1
			SET @S2 =CHARINDEX('_', @TXT,@S1)
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)

			SET @A1 =CHARINDEX('_', @TXT,@S2)+1
			SET @A2 =LEN(@TXT)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1+1)

			IF @L ='000000' SET @Cli ='00000000'

 
			-- para el error de reetiquetados de COPRECI
			--SI MIRAMOS 

			
			SELECT       @CliOF=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END

		END
		
		--IF CHARINDEX('FORMACION', @TXT,1)>0 
		--BEGIN
		----SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		----SMSTO:+34696469617:U0PFORMACION4K00
		--	SET @A ='20900-1'
		--	SET @L ='000000'
		--	SET @Cli ='00000000'
		--	SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		--END

			
		IF CHARINDEX('UORK_', @TXT,1)>0 
		BEGIN
			--Etiquetas internas de SKINTER
			--SMSTO:+34629552542:UORK_A2-609_S69901
			SET @A1 =CHARINDEX('UORK_', @TXT,15)+5
			SET @A2 =CHARINDEX('_', @TXT,@A1)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1)

			SET @S1 =CHARINDEX('_S', @TXT,@A2)+2
			SET @S2 =LEN(@TXT)+1
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)
			SET @L ='000000'
			SET @Cli ='00000000'
		END
	
	END

--*************************************************************************************************************************************************

-- si es tipo de http://www.ekanban.es/qr/157238252841
	-- si es tipo de http		
	if left(@txt,5)='http:' --and @Ubi<>'U41'
	begin
		SET @TP=5
		SET @Emp='ORK'
		--http://www.orkli.com/qr/123456088082
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @L =SUBSTRING(@txt, @L1, 6)
		SET @S =SUBSTRING(@txt, @L1+6, 6)
		SET @Emp=SUBSTRING(@txt, @L1+12, 3)-- no siempre lo tiene. 

		--Si no dispone de dato en empresa, cogemos ORK por defecto.
		IF @Emp<>'BRA' SET @Emp='ORK'
		
		--SET @Emp='BRA'


		SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L) AND (DATAAREAID = @Emp)

		SELECT      @CliUBI= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		IF (@CliUBI<>@CliOF) or (@Emp='BRA')   -- EN CASO DE QUE SEA LA EMPRESA BRASIL, COMO EL APP, NO REVISA LAS OF, SEGÚN EMPRESA, LE PASAMOS EL CLIENTE CORRESPONDIENTE Y LOTE 000000, PARA QUE NO COJA EN BASE 
			BEGIN
				SET @Cli =@CliUBI
				SET @S= @L + @S + @Emp  -- @Emp jarria el 25/5/2017, para evitar de que consumos de defendi, diga que existen por haber consumido apis
				SET @L ='000000'

				
				SET @AR=''
				--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
				--1230/1851-1200
				SELECT        @AR=referencia_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

				SET @AR=ISNULL(@AR,'')

				IF @AR=''
				BEGIN
					set @A=SUBSTRING (@A, LEN(@A)-3, 4)
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES
				END

			END
	END


	
-- si es tipo de http://www.ekanban.es/qr/157238252841   para mueller
	-- si es tipo de http		, long 37
	if left(@txt,5)='http:'  and @Ubi='U41' and CHARINDEX('BRA', @TXT,1)=0 
	begin
		SET @TP=55
		--http://www.orkli.com/qr/123456088082
		--http://www.ekanban.es/ekanban/qr/196020BR123018511201 
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @S =SUBSTRING(@txt, @L1, 6)
		SET @A =SUBSTRING(@txt, @L1+6, 50)
		set @L='000000'


			--SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
			--FROM            dbo.VAX_EKAN_OF
			--WHERE        (PRODID = @L)


			SET @AR=''
			--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
			--1230/1851-1200
			SELECT        @AR=referencia_id
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

			SET @AR=ISNULL(@AR,'')

			IF @AR=''
			BEGIN
				set @A=SUBSTRING (@A, LEN(@A)-3, 4)
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES

			END




			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END
	end 


-------------------------------------------------------------------------------------------
	
	--borrado el 30/11/2015. quitar el 31/12/2015 del todo
	-------------------------------------------------------------------------------------------
	
	--ETIQUETA "ESTAMPA" CON SOLO PROVEEDOR Y ARTÍCULO (opcional la cantidad)
		--PROV_000388_20100374_151.1

		--SI ES EL TIPO VIEJO DE ETIQUETAS DE ESTAMPA. A BORRAR ESTA PARTE.
	if (SUBSTRING(@txt, 1, 4)='PROV' AND CHARINDEX('PROVV', @txt, 1) = 0)				--PROV_V
	begin
		SET @TP=6
		IF CHARINDEX('_', @txt, 13)>0
			BEGIN
			SET @A=SUBSTRING(@txt, 13, CHARINDEX('_', @txt, 13)-13)
			SET @CTD=SUBSTRING(@txt, CHARINDEX('_', @txt, 13)+1, LEN(@TXT)-CHARINDEX('_', @txt, 13))
			END
		ELSE
			BEGIN
			SET @A=SUBSTRING(@txt, 13, LEN(@TXT))
			SET @CTD='0'
			END

		SET @L ='000000'
		SET @Cli ='00000000'
		
		--SET @S = right(@A,3) + '_' + @CTD + '_' + CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		SET @S = SUBSTRING(@txt, 6, LEN(@TXT))+ CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		
	end 
	--	A BORRAR HASTA ESTA PARTE

	
--*************************************************************************************************************************************************

	-- ETIQUETAS DE TRAZABILIDAD INTERNA, SIN CONSIDERAR EL FORMATO PROV_ QUE ES EL TIPO 6 E INICIO CON INTE O PROV Y SEPARADOR ;
	if ((SUBSTRING(@txt, 1, 4)='PROV' or SUBSTRING(@txt, 1, 4)='INTE') AND 
		SUBSTRING(@txt, 1, 5)<>'PROV_' AND CHARINDEX(';', @txt, 1) > 0 )
	
		begin
		SET @TP=7
		
		--PROV;V000388;P20100301;Q1010;TT032580351;S14288      se utiliza en estampa
		--INTE;SORKA77013  se utiliza para etiquetado interior decoletajes, lapeados, etc. 

		DECLARE @N AS INT=0 
		DECLARE @N1 AS INT=0
		DECLARE @C AS char(40)=''
		DECLARE @ID AS Char(30)=''
		DECLARE @P AS Char(30)=''
		DECLARE @VCAP AS Char(30)=''

		SET @CTD='0'
		SET @L ='000000'
		SET @Cli ='00000000'
		
		SET @N=CHARINDEX(';', @txt, 1)+1
		
		SET @TXT=REPLACE(@TXT,CHAR(4),'') --EOT


		SET @EMPR='ORK'
		
		--QUE EMPRESA ES ESTA UBICACIÓN?
		SELECT    @EMPR=Ubi_Dataareaid 
		FROM     dbo.ubicacion
		WHERE   (id_ubicacion = @Ubi)

		--PROV;V000388;P20100301;Q1010;TT032580351;S14288   
		WHILE @N<LEN(@TXT)
			BEGIN
				SET @ID=''
				SET @N1=CHARINDEX(';', @txt, @N)
				IF @N>0 AND @N1=0 SET @N1=LEN(@TXT)+1
				SET @C =SUBSTRING(@TXT, @N, @N1-@N)

				SET @ID=SUBSTRING(@C, 1, 1)
			
				if @ID='P' set @A=SUBSTRING(@C, 2, len(@c)) 
				if @ID='Q' set @CTD=SUBSTRING(@C, 2, len(@c)) 
				if @ID='H' OR @ID='T' set @L=SUBSTRING(@C, 2, len(@c)) 
				if @ID='S' set @S=SUBSTRING(@C, 2, len(@c))						-- PARA LAS ETIQUETAS INTERNAS, COGEMOS LA SERIE
				if @ID='V' set @V=SUBSTRING(@C, 2, len(@c)) 

				SET @N=@N1+1
			END

		--INTE;SORKA77013  ETIQUETADO INTERIOR DE DECOLETAJE, LAPEADO
		IF SUBSTRING(@txt, 1, 4)='INTE' 
			BEGIN
				--COGEMOS LOS DATOS REGISTRADOS AL IMPRIMIR LA ETIQUETA
				SELECT TOP 1 @A=Referencia, @L=NLote, @CTD=Cantidad, @V=Proveedor         --CAMPOS DISPONIBLES Referencia, NSerie, NLote, Cantidad, Proveedor, Fecha, Tipo
				from [SRVSPS2007\SQL2005x32].[ORKLB000].dbo.VEtiquetas 
				where nserie = @S
				ORDER BY FECHA DESC
			END
		
		
		set @LotCap=@S
		-- SI ES DE CAPTOR
		IF SUBSTRING(@txt, 1, 9)='INTE;SCAP' 
		BEGIN
				SELECT   @Usu=SUBSTRING(descripcion,1,10)--limitamos a 10 la descripcion, bestela error
				FROM     dbo.ubicacion
				WHERE   (id_ubicacion = @Ubi)

				--EL USUARIO DEBE SER EL NOMBRE DEL RECURSO DEL AX
				-- 9/3/2017 no estamos insertando THI1 como recurso, pq el apartado anterior estaba anulado. lo activo otra vez. iker.  INTE;SCAP115049  
				--iñigo y blanca han venido. hay foto etiquta en mov ies



				set @S=SUBSTRING(@S, 4, LEN(@S))
				
				SELECT        @L=Lote, @A=Referencia, @CTD=Cantidad--, Compañia
				FROM            SRVCAPTORDB.Captor3.dbo.VCAPTOR_Lotes AS A
				WHERE        ( A.Compañia = '01' AND A.Lote=@S)

				set @LotCap=@S
				set @S=@ubi

				SET @V='000000' -- CONSIDERAMOS QUE SI ES INTERNA Y DE CAPTOR EL PROVEEDOR ES ORKLI
		END

		

		set @A=isnull(@A,'')
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		set @Cli='00000000'
		SET @V=ISNULL(@V,'000000')
		IF @V='' SET @V='000000'

		EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
				@empresa = @EMPR,
				@linea = @Usu, -- @Ubi, 
				@P = @A,
				@Q = @CTD ,
				@V = @V,
				@serie = @S,
				@H = @L
		
		
			SET @VCAP='PROV:' + @V -- + ';TRAT:16' + ';POS:8'    --CUIDADO EN APLICAR ESTO, YA QUE LUEGO NO ENCUENTRA EL CIRCUITO DE ESTE PROVEEDOR. CAMBIAR NOMBRE DE VARIALBE.


		EXEC	@return_value = PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			@Empresa = N'01',
			@Maquina = @Usu,
			@NLote =@LotCap,
			@Referencia = @A,
			@Cantidad = @CTD,
			@DatosAdicionales = N''		--DEBERÍA DE SER EL SIGUIENTE PARA AÑADIR EL PROVEEDOR. DE MOMENTO NADA. @DatosAdicionales = @VCAP

		
		
		--MIRAMOS SI LA REF Y PROVEEDOR DISPONE DE CIRCUITO O NO 
		SET @CIR=''
		
		SELECT @CIR= id_circuito
		FROM  dbo.circuito
		WHERE (referencia_id = @A) AND (Proveedor_id = @V) AND (ubicacion = @Ubi) AND    (activo = 1)

		SET @CIR=isnull(@CIR,'')	--SI NO EXISTE MANTENEMOS EN ''

		IF @CIR=''
		BEGIN
			SET @A=''  --ANULAMOS LA REFERENCIA PARA EVITAR QUE SE PUEDA DAR CONSUMO DE UNA REFERENCIA QUE SEA DE OTRO PROVEEDOR QUE TENGA KANBAN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			set @A=isnull(@A,'')
		END


		SET @S=@L + '/' + @S +@CIR
		SET @L ='000000'

	end

	
--*************************************************************************************************************************************************

	if left(@txt,5)='[)>{R'
	begin
		SET @TP=8
		SET @txt='[)>{RS}06{GS}2L1{GS}V001700{GS}1PXXX{GS}16K004330{GS}Q1{GS}PF-4002-4{GS}1JUNXXXXXXXXXXXX{GS}7Q1058{GS}7Q16GT{GS}T147/99{GS}6DD10/9/99{GS}S2{GS}2P4{RS}{EOT}'



		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A='Prueba GTL'
	end


--*************************************************************************************************************************************************


	--ETIQUETA "MALA" CON SOLO ARTÍCULO DE SKINTER
	--A2-609
	if SUBSTRING(@txt, 1, 1)='A' AND CHARINDEX('-', @txt, 1) =3
	begin
		SET @TP=9
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************


	--ETIQUETA EAN 13
	--8477637477
	if LEN(@TXT)=13 AND (SUBSTRING(@txt, 1, 2)='84' OR SUBSTRING(@txt, 1, 2)='44')
	begin
		SET @TP=10
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************

	--LECTURAS RFID DE LOS PORTICOS
	--114208092289
	if LEN(@TXT)=12 AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID'
	begin
		SET @TP=11

		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)


		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 


		set @TipCont=isnull(@TipCont,0)
		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 
			IF @Ubi='UWOR'
				BEGIN
				--si es en la ubicación BOSCH, LE AÑADIMOS EL NÚMERO DE PALET AL SERIE
			
				set @S=@L + @S --SUBSTRING(@S,1,2) + '0000'--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

				END
			ELSE
				BEGIN 
				--LECTURAS DE SABAF
			
				set @S=@L --MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO


				END
		
		END
		ELSE
		BEGIN
		IF @TipCont=1 
			begin

				set @S=@L + @S--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
					set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

			end


		END
				
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			DECLARE @ActAnt0 bit
			DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
				set @S='000000'
				set @L='000000'
			end
			--*****************************
			
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END

			

	end

	
	--LECTURAS RFID DE LOS PORTICOS CON CÓDIGO HEXADECIMAL
	--114208092289
	if (LEN(@TXT)=22 OR LEN(@TXT)=24 ) AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID' AND LEFT(@TXT,4)<>'PROV'
	begin
		SET @TP=110



		declare @sql nvarchar(MAX), @ch varchar(200)
		--select @v = '313931323434323637393338'
		select @sql = 'SELECT @ch = convert(varchar, 0x' + @TXT + ')'
		EXEC sp_executesql @sql, N'@ch varchar(30) OUTPUT', @ch OUTPUT
		--SELECT @ch AS ConvertedToASCII
		SET @txt=@ch
		
		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)

		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 



		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END
		IF @TipCont<>2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			--set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END

					
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END


	end

	
--*************************************************************************************************************************************************
	
	--control de stocks DISPONIBILIDAD
	if LEFT(@TXT,3)='CAE' --AND LEN(@TXT)=12 left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND 
	BEGIN
		SET @TP=12
		begin 
			set @A=substring(@TXT,1,6)
			
			
			SELECT      @UbiTip=Ubi_Modo   --EVEN  DISP
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi


			SET @L='000000'
			SET @Cli ='00000000'
			SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
			SET @S = @Ubi + '_' + @TXT 


			IF @Sen='E' AND @UbiTip='DISP'
			BEGIN
				DELETE FROM [dbo].[tarjeta]
				WHERE
				id_lectura=@L + '_' + @S AND SENTIDO='S'
			END





		end
	END

	
--*************************************************************************************************************************************************

	--LECTURA DE ALBARANES (CONSIGNA)
	--AL00109472_1_134340    
	if  LEFT(@TXT,2)='AL'
	begin
			DECLARE @CLI1 AS VARCHAR(25)=''
			

			SET @TP=13
			
			SET @ALB = SUBSTRING(@txt, 1, CHARINDEX('_', @txt, 1)-1)
			SET @ALBLIN = SUBSTRING(@txt,  CHARINDEX('_', @txt, 1)+1,1)
			SET @L = SUBSTRING(@txt,  CHARINDEX('_', @txt,  CHARINDEX('_', @txt, 1)+1)+1, len(@txt))
			
			--000500 ES UN LOTE FICTICIO DEL AX
			SET @L=ISNULL(@L,'000500')
			IF @L='' SET @L='000500'


			SET @S = @txt 

			--Cogemos el artículo según el lote de fabricación. 
			SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			--CANTIDADES SEGÚN EL ALBARAN??????????????????????????????????????????????????????????


			--Cogemos el cliente, según el artículo y ubicación donde estamos leyendo.
			SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor, @CLI1=cliente_id 
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) AND   (activo = 1)

			IF @CLI<>@CLI1
			begin
				set @CLI=@CLI1
				SET @L='000000'
			end
			



	end

	--Si es sólo lote de la etiqueta, cogemos el lote y apartir de este dato, consultará en vista de OFs
	if left(@txt,1)='H' AND LEN(@TXT)=7
	begin
		--H113688
		SET @TP=15
		SET @L =SUBSTRING(@txt, 2, 6)
		SET @S ='000000'
	end 
	
--*************************************************************************************************************************************************

	IF  LEFT(@TXT,4)='RFID'
	BEGIN
		SET @TP=16
		SET @L ='000000'--SUBSTRING(@txt, 1, 8)
		SET @S =SUBSTRING(@txt, 1, 8)
		set @cade = @TXT + ' en Ubi: ' + @Ubi + ' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '
		declare @cade1 as varchar(50)

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=REFERENCIA_ID, @Cli=cliente_id FROM dbo.circuito WHERE id_circuito = @TXT
		SET @L='000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 



		
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			--DECLARE @ActAnt0 bit
			--DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
			end
			--*****************************
			




		SELECT        @cade1=mail
		FROM            dbo.circuito
		WHERE        (id_circuito = @TXT)


		set @cade1=isnull(@cade1,'')

		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		IF @cade1<>''
		BEGIN
			EXEC	[dbo].[PEnvCorreo]
			@Des = @cade1,
			@Tem = N'Lectura de Etiqueta TEST RFID',
			@Cue =@cade
		END

	END

	
--*************************************************************************************************************************************************

	--ELABORACIÓN, SEGUIMINETO TRAZABILIDAD THIELENHAUS....
	
	IF  LEFT(@TXT,2)='SO' AND @TP=0 --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	BEGIN
		SET @TP=17

			SET @S=SUBSTRING(@TXT, 2, LEN(@TXT)+1)
		EXEC	PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazab]
		@serie =@S,--'ORKA50136',--
		@linea =@Ubi


		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S=@L + '/' + @S
		SET @L ='000000'
	END


	--V001857PE-20262Q4093H126511K154572   la etiqueta de vilardell
	--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	--PTE


	--ETIQUETA GS1, SEGÚN NUEVA NORMA. ENERO 2016
	--[)>06V000782PA5-437Q450H2/5357S
	
	if left(@txt,3)='[)>'
		BEGIN
		SET @TP=18
			print @txt;
			SET @L=''
			SET @A=''
			set @CIR=''

			SELECT @A=DBO.GS1Data(@txt,'P')
			SELECT @CTD=DBO.GS1Data(@txt,'Q')
			SELECT @L=DBO.GS1Data(@txt,'H')
			IF @L='' SELECT @L=DBO.GS1Data(@txt,'T')
			SELECT @S=DBO.GS1Data(@txt,'S')
			SELECT @V=DBO.GS1Data(@txt,'V')

			--SELECT @16K=DBO.GS1Data(@txt,'16K')
			--SELECT @2L=DBO.GS1Data(@txt,'2L')
			--SELECT @1J=DBO.GS1Data(@txt,'1J')
			--SELECT @5J=DBO.GS1Data(@txt,'5J')
			--SELECT @6J=DBO.GS1Data(@txt,'6J')
			--SELECT @7Q58=DBO.GS1Data(@txt,'7Q')
			--SELECT @16D=DBO.GS1Data(@txt,'16D')
			--SELECT @2P=DBO.GS1Data(@txt,'2P')
			print @L;
			set @L=isnull(@L,'')
			IF @L='' SET @L='0' 
			


			--SET @CTD=REPLACE(@CTD,'.','') gs-n jarriak
			--SET @CTD=REPLACE(@CTD,',','.')gs-n jarriak
			SET @CTD= CONVERT(NUMERIC(9,1),@CTD)

			--revisamos a qué empresa corresponde la ubicación
			SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
			FROM            dbo.ubicacion
			WHERE        (id_ubicacion = @Ubi)
			
			--if @RegTraz=1
			--begin

			--	--REGISTRAMOS DATOS EN EL AX A TRAVÉS DEL PROCEDMINETO DEL SERVIDOR DEL AX.
			--	EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
			--		@empresa = @EMPR,
			--		@linea = @Usu, -- @Ubi,
			--		@P = @A,
			--		@Q = @CTD ,
			--		@V = @V,
			--		@serie = @S,
			--		@H = @L
		


			--	----REGISTRAMOS LOS DATOS EN CAPTOR
			--	--como da error si el lote no existe...


			--	SET @V='PROV:' + @V
			--	set @LotCap=@L + '_' + @S

			--	EXEC	@return_value =PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			--			@Empresa = N'01',
			--			@Maquina = @Usu,
			--			@NLote =@LotCap,
			--			@Referencia = @A,
			--			@Cantidad =@CTD,
			--			@DatosAdicionales = @V

			--END

			--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
			--Cogemos el cod cliente de la ubiación
			SET @A=ISNULL(@A,'')
			SET @CIR=''
			--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 


			if  @ubi='U45' OR @ubi='U49' OR @ubi='U51' OR @ubi='U52' OR @ubi='U53' 

				BEGIN 
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi)  AND activo=1 and Proveedor_id=@V
								
				END
			ELSE
				BEGIN
				
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1
				END
			--PRINT 'CLIENTE = ' + @CLI;
			--PRINT 'CIRCUITO =' + @CIR;

			set @ant=@V




			SET @CIR=ISNULL(@CIR,'')

			--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
			IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
			--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

			BEGIN
				SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
				WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			END


			SET @S=RTRIM(@S) + '_' + @L
			SET @L='000000'

		END
		---------------*******************************************************************

	IF  LEFT(@TXT,3)='UTI' AND @TP=0 and (@Ubi='UUTI' or @Ubi='UUTIDEK') --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--UTI_P1210-1115-1-10_Q4_V000535
	BEGIN
		SET @TP=19
		
		DECLARE @Usuario nvarchar(50),
		@Referencia nvarchar(20),
		@ReferenciaDescripcion nvarchar(60),
		@Proveedor nvarchar(20),
		@Cantidad int,
		@Ubicacion nvarchar(600),
		@FechaSolicitud nvarchar(10),
		@FechaPrevista nvarchar(10),
		@Precio decimal(14,6),
		@Url nvarchar(1),
		@Resultado int
		
		DECLARE @SelectedValue int=0
		DECLARE @SelectedValueTXT VARCHAR(50)



		SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,CHARINDEX('_U', @txt, 1)-CHARINDEX('_V', @txt, 1)-2)
		SET @Ubicacion = SUBSTRING(@txt, CHARINDEX('_U', @txt, 1)+2,LEN(@TXT))

		SET @Cli =isnull(@Cli,'')
		SET @Ubicacion =isnull(@Ubicacion,'')

		SET @Cli = SUBSTRIng(@Cli ,1,6)


		set @URLPlano='1'--'http://srvsps2007/Procesos/UtilesPDF/' + @A + '.pdf'

		SET XACT_ABORT ON  
	   
		SELECT @Usuario=[Usu_Dominio]
		FROM [dbo].[sga_usuario]
		where [username]=@Usu

		SET @Usuario =isnull(@Usuario,'orkliord\gm')
		
		DECLARE @UbicacionDes nchar(100)
		
		SELECT @UbicacionDes=[descripcion]
		FROM [EKANBAN].[dbo].[ubicacion]
		WHERE   [id_ubicacion]=@Ubi
		
		SET @UbicacionDes =isnull(@UbicacionDes,'')


		SET @Referencia = @A  --N'1331-0111-0-11'
		SET @ReferenciaDescripcion = ''
		SET @Proveedor = @Cli
		SET @Cantidad = @CTD
		SET @Ubicacion = 'Sekzioa: ' + rtrim(LTRIM( @UbicacionDes)) + ' (' + @Ubicacion + ') Erreferentziak LASER bidez markatu.' 
		SET @FechaSolicitud = null --para que coja el de hoy
		SET @FechaPrevista = null --si es nulo, añade 21 días. 
		SET @Precio = 1 --no se utiliza para nada
		SET @Url = 1 --1 edo 0





		SELECT        @Referencia= ITEMID
		FROM             [PREaxsql].[SQLAXBAK].dbo.INVENTTABLE
		WHERE        (DATAAREAID = N'ork') AND (ITEMID = @A)

		set @Referencia=isnull(@Referencia,'') 

		if @Referencia=''
		begin
			SELECT     @Referencia=   ITEMID
			FROM             [PREaxsql].[SQLAXBAK].dbo.CUSTVENDEXTERNALITEM
			WHERE        (DATAAREAID = N'ORK') AND (EXTERNALITEMID = @A) AND (CUSTVENDRELATION = @Proveedor)
		end

		set @Referencia=isnull(@Referencia,'') 


		


		--EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
		--	'orkliord\aauzmendi',
		--	'1331-0111-0-11',
		--	'',
		--	'000535 MEKALAN',
		--	1,
		--	'UUTI',
		--	null,
		--	null,
		--	1,
		--	1,
		--   @Resultado OUTPUT

		EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
			@Usuario,
			@Referencia,
			@ReferenciaDescripcion,
			@Proveedor,
			@Cantidad,
			@Ubicacion,
			@FechaSolicitud,
			@FechaPrevista,
			@Precio,
			@Url,
		   @Resultado OUTPUT

		 SET @SelectedValueTXT=CASE @Resultado 
					WHEN 0 THEN 'Error dentro del servicio. Tendréis que comunicármelo para que revise que a poder pasar.'
					WHEN 1 THEN 'El pedido se ha generado. Otra cosa que se haya quedado parado, se haya un producido un problema al volcar al AX o haya todo correctamente. Estas situaciones ya se revisaran dentro de la aplicación.'
					WHEN 2 THEN 'Fecha solicitud incorrecta.  La fecha tiene un formato incorrecto o es anterior al día de hoy.'
					WHEN 3 THEN 'Fecha prevista incorrecta.  La fecha tiene un formato incorrecto'
					WHEN 4 THEN 'Fecha Prevista menor que la Fecha Solicitud.'
					WHEN 5 THEN 'Usuario incorrecto. El usuario que se pasa (usuario de red) es incorrecto o no tiene permisos para Compras.'
					WHEN 6 THEN 'Empresa incorrecta. La empresa del usuario no es correcta (No debería pasar que se valida por si acaso).'
					WHEN 7 THEN 'Sección incorrecta. Se valida que la sección del usuario es correcta (No debería pasar que se valida por si acaso).'
					WHEN 8 THEN 'Almacén incorrecto. Se valida que el almacén que se obtiene en base a los datos de la sección del usuario es correcto en base a la empresa en que estamos tratando (No debería pasar que se valida por si acaso).'
					WHEN 9 THEN 'Referencia incorrecta. El código de la referencia no existe en la empresa que estamos tratando.'
					WHEN 10 THEN 'Proveedor incorrecto. El código de proveedor introducido no existe para la empresa que estamos tratando.'
					WHEN 11 THEN 'Precio no encontrado. No hay en el AX un precio definido para ese artículo y proveedor.'
				END
		


		--REGISTRAMOS EL PEDIDO EN CREAR PEDIDOS, PERO YA TRATADO CON ESTADO 99. PARA SEGUIMIENTO
		INSERT INTO [PREaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
		(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra, DocumentoAdjunto,  Observaciones)
		VALUES
		('99',    @Referencia,        'UTILES', substring(@Cli,1,6),  @Usu, @A,   @CTD,      GETDATE()+7,           @Usu,   GETDATE(),   '0',    'PRO', '3',                       '', 0, '','', 0,0,@SelectedValue,  @Ubicacion + @SelectedValueTXT)
		
		IF @Resultado=1--1
		BEGIN 
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1)
			ORDER BY num_kanban
		END

		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END

	---------------*******************************************************************

	IF   @Ubi='URFIdD' --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--pruebas de EMBEBLUE
	BEGIN
		SET @TP=20

		--SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		--SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		--SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,LEN(@TXT))
			
		
		SET @cade=@Usu + ': ' + @Txt
		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		--IF @cade1<>''
		--BEGIN
		--	EXEC	[dbo].[PEnvCorreo]
		--	@Des = 'test_910@embeblue.com; auxiliar@orkli.es; iker@orkli.es',
		--	@Tem = N'Lectura de Etiqueta RFID EMBEBLUE',
		--	@Cue =@cade
		--END

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END
	
		
--*************************************************************************************************************************************************


	if @txt like '%@%' 
	begin
		SET @TP=14
		DECLARE @Notif bit=0

		SELECT       @Notif= Ubi_NotEtiVacias
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)



		set @cade = 'Ubi: ' + @Ubi + ', Texto: ' + @txt +' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '

		if @Notif=1 --@Ubi<>'UCAE'
			begin
			EXEC	[dbo].[PEnvCorreo]
					@Des = N'alert@ekanban.es;',
					@Tem = N'Lectura de Etiquetas Vacías por RFID',
					@Cue = @cade
			end
	end



	
--*************************************************************************************************************************************************
REGISTROLECTURAS:

print 'tregistroLecturas';

INSERT INTO [dbo].[TRegistroLecturas] 
           ([Reg_Fec]
           ,[Reg_Dis]
           ,[Reg_Usu]
           ,[Reg_Ubi]
           ,[Reg_Pan]
           ,[Reg_Txt]
           ,[Reg_Cir]
           ,[Reg_Cli]
           ,[Reg_Art]
           ,[Reg_Lot]
           ,[Reg_Ser]
		   ,[Reg_Ant]
		   ,[Reg_Sen]
		   ,[Reg_Tip]
		   ,[Reg_Ctd]) 
     VALUES
           (getdate()
           ,@Disp
           ,@Usu
		   ,@Ubi
           ,@Pant
           ,@Txt
           ,@CIR
           ,@Cli
           ,@A
           ,@L
           ,@s
		   ,@Ant
		   ,@Sen
		   ,@TP
		   ,@CTD)


	
	--select @L1,  @L2, @L,  @S1,  @S2, @S, @A1,  @A2, @A
	select @L AS LOT,  @S AS SER, @A AS ART, 0 as CTD, '000000' AS PRO, @Cli as CLI,  @L + '_' + @S AS GRAB, @CTD AS CTDLEIDA, @PED AS IDPED

	commit;
	print getDate();
	SET XACT_ABORT OFF;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
		



END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[pruebapro_ActErrores]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[pruebapro_ActErrores]
   ON  [dbo].[pruebapro_dispositivo]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	DECLARE @NBase_Cursor varchar(255)
	DECLARE @Dis as varchar(255)
	DECLARE @EnError as bit
	DECLARE @EnErrorAnt as bit
	DECLARE @FecEstAnt as datetime
	DECLARE @FecAct as datetime 
	DECLARE @IP as varchar(20)
	DECLARE @Ver as varchar(20)
	DECLARE @IPAnt as varchar(20)
	DECLARE @VerAnt as varchar(20)
	declare @TipInc as int
	DECLARE @NotMin as int
	DECLARE @NotMail as varchar (100)
	DECLARE @NotSt as int
	DECLARE @UltCon as datetime
	declare @SegErr as int
	DECLARE @UltIni as datetime
	DECLARE @AntIni as datetime


	set @FecAct=getdate()
	
	--Cogemos los segundos según los parámetros. A partir de no tener una señal del portico en estos segundos, se considera que no responde. Cambio IP?
	SELECT      @SegErr=valor
	FROM        dbo.sga_parametros
	WHERE       (nombre = 'SegundosErrorArco')

	set @SegErr=isnull(@SegErr,60)

	--actualiza a error, aquellos dispositivos que no han mostrado estar alive durante los últimos x segundos.
	UPDATE [dbo].[pruebapro_dispositivo]
	SET Dis_EnError = CASE WHEN DATEDIFF(second, ultima_conexion, @FecAct) > @SegErr THEN 1 ELSE 0 END

	CREATE TABLE #TempErr (Disp VARCHAR(50), Ult datetime, EnErr bit)

	DECLARE  CDatos CURSOR FOR
		SELECT [id_dispositivo]
		FROM [dbo].[pruebapro_dispositivo]
   
	OPEN CDatos

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
		SET @Dis = @NBase_Cursor
		set @TipInc=0


		--situación actual?
		SELECT       @EnError=Dis_EnError, @IP=ip, @Ver=version, @NotMin=Dis_NotErrMin, @NotMail=Dis_NotErrMail, @NotSt=Dis_NotErrSta, @UltCon=ultima_conexion, @UltIni=fecha_inicio
		FROM            dbo.pruebapro_dispositivo
		WHERE        (id_dispositivo = @Dis)

		Set @ip=isnull(@ip,'')
		set @Ver=isnull(@Ver,'')
		set @NotMin=isnull(@NotMin,5)
		set @NotMail=isnull(@NotMail,'alert@ekanban.es')
		set @NotSt=isnull(@NotSt,0)
		set @UltIni=isnull(@UltIni,getdate())
		

		--situación anterior?
		SELECT     @FecEstAnt= RegPor_Fec, @EnErrorAnt=RegPor_Err, @IPAnt=RegPor_IP, @VerAnt=RegPor_Ver, @AntIni=RegPor_UltIni
		FROM            dbo.TRegistroEstPort
		WHERE        RegPor_Dis = @Dis
		ORDER BY RegPor_Fec ASC

		set @FecEstAnt=isnull(@FecEstAnt,getdate())
		set @EnErrorAnt=isnull(@EnErrorAnt,0)
		set @EnError=isnull(@EnError,0)
		set @AntIni=isnull(@AntIni,getdate())
		
		
		-- PARA REGISTRAR EL FICHERO DE ESTADO DE PORTICOS
		if @EnError<>@EnErrorAnt or @ip<>@IPAnt or @ver<>@VerAnt or @AntIni<>@UltIni
		begin		
			UPDATE [dbo].[TRegistroEstPort]
			SET [RegPor_Has] = @FecAct
			WHERE [RegPor_Dis]=@Dis and RegPor_Fec=@FecEstAnt

			if @EnError<>@EnErrorAnt  set @TipInc=1
			if @ip<>@IPAnt  set @TipInc=2
			if @ver<>@VerAnt  set @TipInc=3
		
			INSERT INTO [dbo].[TRegistroEstPort]
					   ([RegPor_Fec]
					   ,[RegPor_Dis]
					   ,[RegPor_Des]
					   ,[RegPor_Err]
					   ,[RegPor_IP]
					   ,[RegPor_Ver]
					   ,[RegPor_Est]
					   ,[RegPor_UltIni])
				 VALUES
					   (@FecAct
					   ,@Dis
					   ,@FecAct
					   ,@EnError
					   ,@ip
					   ,@Ver
					   ,@TipInc
					   ,@UltIni)
		end
	
		--EN ERROR
		-- PARA REGISTRAR SI ES UN ERROR DE TENER QUE NOTIFICAR POR CORREO O NO
		if @EnError=1 and @NotSt=0 and DATEDIFF(minute, @UltCon, @FecAct) >= @NotMin
		begin
			UPDATE [dbo].[pruebapro_dispositivo]
			SET [Dis_NotErrSta] = 1
			WHERE [id_dispositivo]=@Dis
		end

		--SIN ERROR
		--TODAVÍA SIN HABER NOTIFIFICADO
		if @EnError=0 and (@NotSt=1) --es que no hemos llegado a notificar por correo pero ha recuperado la conexión
		begin
			UPDATE [dbo].[pruebapro_dispositivo]
			SET [Dis_NotErrSta] = 0
			WHERE [id_dispositivo]=@Dis
		end

		
		--SIN ERROR
		--YA NOTIFIFICADO, HAY QUE AVISAR QUE SE HA RECUPERADO
		if @EnError=0 and (@NotSt=2) --@NotSt=1 or   (LE DEJAMOS QUE LA OTRA INSTANCIA LO NOTIFIQUE POR CORREO)
		begin
			UPDATE [dbo].[pruebapro_dispositivo]
			SET [Dis_NotErrSta] = -1
			WHERE [id_dispositivo]=@Dis
		end

		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor
	END
close CDatos
deallocate CDatos

DROP TABLE #TempErr


EXEC	[dbo].[PValidarLecPorticos]


END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_SitPanelUbi]...';


GO


-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
ALTER PROCEDURE [dbo].[PAX_SitPanelUbi]
(
@Ubi varchar(255)='', @Usu AS varchar(255)='', @ST as bit=1
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
	DECLARE @Clis integer
	DECLARE @Pros integer

	set @st=isnull(@st,0)
	

	SELECT     @Clis=COUNT(DISTINCT cliente_id) , @Pros=COUNT(DISTINCT Proveedor_id) 
	FROM        dbo.circuito
	WHERE     (activo = 1) AND (ubicacion = @Ubi)

	set @Clis=isnull(@Clis,0)
	set @Pros=isnull(@Pros,0)

	if @Pros<=1
	begin

		if @ST=0 
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, 0 AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi)
		end
		else
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, dbo.Estado(id_circuito) AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi)
		end
	end
	else
	begin
		
		if @ST=0
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, 0 AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi) 
			and Proveedor_id in (
				SELECT     dbo.usuario_proveedor.id_proveedor
				FROM        dbo.usuario_proveedor INNER JOIN
				dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
				WHERE     (dbo.sga_usuario.username = @Usu)
			)
		end
		else
		begin
			SELECT id_circuito, referencia_id, isnull(ref_cliente,'') as ref_cliente, dbo.Estado(id_circuito) AS status
			FROM   dbo.circuito
			WHERE (activo = 1) AND (ubicacion = @Ubi) 
			and Proveedor_id in (
				SELECT     dbo.usuario_proveedor.id_proveedor
				FROM        dbo.usuario_proveedor INNER JOIN
				dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
				WHERE     (dbo.sga_usuario.username = @Usu)
			)
		end

	end
	--status
	--0 nada
	--1 ok
	--2 alerta
	--3 crítico

	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = @Ubi,
		@Reg_opt = N'SitPanelUbi',
		@Reg_Art = @st,
		@Reg_cli = @Clis,
		@Reg_Txt = @Pros
	COMMIT;	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAjustar]...';


GO


ALTER PROCEDURE [dbo].[PAccAjustar]
(
	@CodCircuito varchar(20) = '',
	@NumTarjVerdes integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)=''
)
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NumTarjRojas as int
	DECLARE @NumTarjetas AS INT
	DECLARE @Verdes as int
	DECLARE @SobreStock as int

	DECLARE @CampGest varchar(50)=''

	DECLARE @Tipo varchar(10)=''
	DECLARE @StatusTrat varchar(1)
	DECLARE @IdTar varchar(50)
	DECLARE @Usuario varchar(255) = ''
	DECLARE @FechEntregaConfirmada AS DATETIME

	DECLARE @MsgTxt varchar(255)=''
	DECLARE @Resul int=0

	SET @Usuario=@Usu

	SELECT @Tipo=dbo.tipo_circuito.nombre
	FROM            dbo.circuito INNER JOIN
				dbo.tipo_circuito ON dbo.circuito.tipo_circuito = dbo.tipo_circuito.id
	WHERE        (dbo.circuito.id_circuito = @CodCircuito)

	SET @StatusTrat='T'
	if @Tipo='INT' OR @Tipo='EXT' 
	BEGIN
		SET @StatusTrat='G'
	END

	set @NumTarjRojas=dbo.r(@CodCircuito)
	set @Verdes=dbo.v(@CodCircuito)
	set @SobreStock=dbo.SS(@CodCircuito)
	SET @FechEntregaConfirmada=GETDATE()
	
	if @NumTarjVerdes<0 set @NumTarjVerdes=0
	
	SET @NumTarjetas=@NumTarjVerdes-@Verdes


	set @CampGest='AutAX' + '_' + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113)


	IF @NumTarjetas>0 --Hay más stock real, verdes que los que indica el circuito. Si hay rojas, quitar. sino crear amodif
	BEGIN
		print 'numTarjetas > 0 ';
		IF @NumTarjRojas>=@NumTarjetas--=@NumTarjetas
		BEGIN
			SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
					FROM            dbo.tarjeta
					WHERE        (gestionado IS NULL OR
									gestionado = '') AND (id_circuito = @CodCircuito)
					ORDER BY id_lectura


			UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
				, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
				WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) and id_lectura<=@IdTar
		END
		ELSE
		BEGIN
					UPDATE [EKANBAN].[dbo].[tarjeta]
					SET gestionado =  @CampGest
					, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
					, Tar_GestionadoUSU = @Usuario
					, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
					WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) AND ESTADO <>'A_MODIF'
					

					INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
					SELECT        TOP (@NumTarjetas-@NumTarjRojas) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @CodCircuito) ORDER BY id_circuito
		END
	END
	ELSE
	--HAY QUE AÑADIR ROJAS (PRIMERO QUITAR AMODIF)
	BEGIN
		print ' opcion 2';
		set @NumTarjetas=-@NumTarjetas

		IF @SobreStock>=@NumTarjetas
		BEGIN

			SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
			FROM            dbo.tarjeta
			WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
			gestionado = '') AND (id_circuito = @CodCircuito)
			ORDER BY id_lectura


			UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
				, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
				WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
				gestionado = '') AND (id_circuito = @CodCircuito) and id_lectura<=@IdTar
		END

		ELSE
		BEGIN

					UPDATE [EKANBAN].[dbo].[tarjeta]
					SET gestionado =  @CampGest
					, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G, 
				    , Tar_GestionadoUSU = @Usuario
					, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
					WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
					gestionado = '') AND (id_circuito = @CodCircuito)
					

					INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
					SELECT        TOP (@NumTarjetas-@SobreStock) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @CodCircuito) ORDER BY id_circuito
		END




	END

	--Set @Resul=1
	--set @MsgTxt='Sin validar usuario'
	
	--Set @Resul=0
	--set @MsgTxt='se han insertado los datos'

	--set @Resul=18  --Código de resultado para el android. Error al generar pedido.
	--set @MsgTxt='Se han dado consumos justo en este instante. Vuelva a intentarlo'

	Set @Resul=0
	set @MsgTxt='se han insertado los datos'

	select  @Resul as Result, @MsgTxt as Msg
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @CodCircuito, @Origen = 'AjusteStock', @Usuario = @Usu
	-----20181008 FIN INCLUSION DE TABLA HISTORICOVERDES
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_ConsumosHtoricos]...';


GO


-- =============================================
-- Author:		IKER
-- Create date: 6-5-2014
-- Description:	Procedimiento para obtener consumos historicos semanales
-- =============================================
ALTER PROCEDURE [dbo].[PAX_ConsumosHtoricos]
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Cliente AS varchar(255)
	DECLARE @Proveedor AS varchar(255)
	DECLARE @Tipo as int
	DECLARE @Articulo AS varchar(255)
	declare @Ubic as varchar(255)
	DECLARE @MedDia AS int
	declare @CtdKan as int
	declare @DiaTrans as int
	declare @DefKan as int
	DECLARE @UbiAnt as varchar(3)=''
	DECLARE @UBIANTERIOR AS VARCHAR(5)=''
	
	set @MedDia =0

	SET DATEFIRST 1;

	begin
		SELECT        @Cliente= cliente_id, @Proveedor=proveedor_id, @Articulo=referencia_id, @CtdKan=cantidad_piezas, @DiaTrans=entrega_dias, @DefKan=num_kanban, @Ubic=ubicacion, @Tipo=tipo_circuito, @UbiAnt=Cir_UbiAnt
		FROM            dbo.circuito
		WHERE        id_circuito = @Circuito
	end
	
	 SET @UBIANT=ISNULL(@UBIANT,'')
	 SET @UBIANT=LTRIM(RTRIM(@UBIANT))

	if @tipo=1
	--Significa que es circuito de tipo proveedor. 
	begin
		--consulta de compras durante las últimas semanas. 
			SELECT        TOP (100) PERCENT PURCHTABLE_1.INVOICEACCOUNT AS CLI, 1 AS UBI, PURCHLINE_1.ITEMID AS ART, DATEPART(year, PURCHLINE_1.DELIVERYDATE) AS AÑO, DATEPART(ISO_WEEK, 
			PURCHLINE_1.DELIVERYDATE)-1 AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, PURCHLINE_1.DELIVERYDATE))) AS ETIQ, 
			SUM(PURCHLINE_1.PURCHQTY) AS CTDSEM, '1' AS COLOR, PURCHTABLE_1.DATAAREAID AS EMP
			FROM            srvaxsql.sqlax.dbo.PURCHTABLE AS PURCHTABLE_1 INNER JOIN
			srvaxsql.sqlax.dbo.PURCHLINE AS PURCHLINE_1 ON PURCHTABLE_1.PURCHID = PURCHLINE_1.PURCHID
			GROUP BY PURCHTABLE_1.INVOICEACCOUNT, PURCHLINE_1.ITEMID, DATEPART(year, PURCHLINE_1.DELIVERYDATE), DATEPART(ISO_WEEK, PURCHLINE_1.DELIVERYDATE), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
			PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, PURCHLINE_1.DELIVERYDATE))), PURCHTABLE_1.DATAAREAID, PURCHTABLE_1.PURCHASETYPE
			HAVING        (PURCHTABLE_1.DATAAREAID = N'ork') AND (PURCHLINE_1.ITEMID = @Articulo) AND (DATEPART(year, PURCHLINE_1.DELIVERYDATE) = DATEPART(YEAR, GETDATE())) AND (DATEPART(ISO_WEEK, 
			PURCHLINE_1.DELIVERYDATE) <> 53) AND (PURCHTABLE_1.PURCHASETYPE = 3) AND (PURCHTABLE_1.INVOICEACCOUNT = @Proveedor)

		--UNION

			----Añadimos dos meses de previsiones en color rojo
			--SELECT        VENDID AS CLI, 3 AS UBI, ITEMID AS ART, DATEPART(year, REQDATE) AS AÑO, DATEPART(MONTH, REQDATE) AS MES, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, REQDATE)))) 
			--						 + ' M' + LTRIM(STR(DATEPART(month, REQDATE))) AS ETIQ, SUM(QTY) AS CTDSEM, '0' AS COLOR, DATAAREAID
			--FROM            SRVAXSQL.SQLAX.dbo.REQPO AS REQPO_1
			--WHERE        (REQPLANID = N'PLANI_PREV')
			--GROUP BY VENDID, ITEMID, DATAAREAID, DATEPART(year, REQDATE), DATEPART(MONTH, REQDATE), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, REQDATE)))) + ' M' + LTRIM(STR(DATEPART(month, REQDATE)))
			--HAVING        (ITEMID = @Articulo) AND (DATAAREAID = N'ORK')
			ORDER BY ART, AÑO, SEM
	end


	if @tipo=2
	begin
		SELECT        dbo.circuito.cliente_id AS CLI, dbo.circuito.ubicacion AS UBI, dbo.circuito.referencia_id AS ART, DATEPART(YEAR, dbo.tarjeta.fecha_lectura) AS AÑO, DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura) AS SEM, 
		'Y' + LTRIM(RTRIM(STR(DATEPART(year, dbo.tarjeta.fecha_lectura)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura))) AS ETIQ, SUM(dbo.circuito.cantidad_piezas) AS CTDSEM, 1 AS COLOR, 
		'ORK' AS EMP
		FROM            dbo.tarjeta INNER JOIN
		dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito
		WHERE        (dbo.circuito.id_circuito = @Circuito)
		GROUP BY dbo.circuito.cliente_id, dbo.circuito.ubicacion, dbo.circuito.referencia_id, DATEPART(YEAR, dbo.tarjeta.fecha_lectura), DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
		dbo.tarjeta.fecha_lectura)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura))), dbo.circuito.tipo_circuito, dbo.circuito.id_circuito, dbo.tarjeta.estado
		HAVING        (dbo.circuito.tipo_circuito = 2) AND (dbo.tarjeta.estado = 'G') OR
		(dbo.circuito.tipo_circuito = 4) AND (dbo.tarjeta.estado = 'G')
	end




	if @tipo=3
	--Significa que es circuito de tipo cliente. 
	begin
		

		SET @UBIANT=ISNULL(@UBIANT,'')
		SET @UBIANT=LTRIM(RTRIM(@UBIANT))

		SELECT        TOP (100) PERCENT SUBSTRING(CUSTPACKINGSLIPJOUR.ORDERACCOUNT, 1, 6) + '00' AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, 
                         CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, DATEPART(ISO_WEEK, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
                         CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, 
                         CASE WHEN DATEPART(ISO_WEEK, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(ISO_WEEK, getdate()) THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
		FROM            srvaxsql.sqlax.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
								 srvaxsql.sqlax.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
								 CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
								 srvaxsql.sqlax.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
								 CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
		WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0)) OR
								 (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0)) AND (SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%')
		GROUP BY SUBSTRING(CUSTPACKINGSLIPJOUR.ORDERACCOUNT, 1, 6), CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(ISO_WEEK, 
								 CUSTPACKINGSLIPTRANS.DELIVERYDATE), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, 
								 CUSTPACKINGSLIPTRANS.DELIVERYDATE))), CUSTPACKINGSLIPTRANS.DATAAREAID
		HAVING        (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) AND (SUBSTRING(CUSTPACKINGSLIPJOUR.ORDERACCOUNT, 1, 6) + '00' = SUBSTRING(@Cliente, 1, 6) + '00')
		ORDER BY ART, AÑO, SEM

		/**
		SELECT        TOP (100) PERCENT CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, 
								 DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
								 CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
								 THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
		FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
								 SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
								 CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
								 SRVAXSQL.SQLAX.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
								 CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
		WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) OR
								 (SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%')
		GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
								 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
								 CUSTPACKINGSLIPTRANS.DATAAREAID
		HAVING        (CUSTPACKINGSLIPJOUR.ORDERACCOUNT = @Cliente) AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) AND (DATEPART(year, 
								 CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(YEAR, GETDATE()))
		ORDER BY ART, AÑO, SEM
		**/

	end
	
	if @tipo=4
	begin
		SELECT        dbo.circuito.cliente_id AS CLI, dbo.circuito.ubicacion AS UBI, dbo.circuito.referencia_id AS ART, DATEPART(YEAR, dbo.tarjeta.fecha_lectura) AS AÑO, DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura) AS SEM, 
		'Y' + LTRIM(RTRIM(STR(DATEPART(year, dbo.tarjeta.fecha_lectura)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura))) AS ETIQ, SUM(dbo.circuito.cantidad_piezas) AS CTDSEM, 1 AS COLOR, 
		'ORK' AS EMP
		FROM            dbo.tarjeta INNER JOIN
		dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito
		WHERE        (dbo.circuito.id_circuito = @Circuito)
		GROUP BY dbo.circuito.cliente_id, dbo.circuito.ubicacion, dbo.circuito.referencia_id, DATEPART(YEAR, dbo.tarjeta.fecha_lectura), DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
		dbo.tarjeta.fecha_lectura)))) + ' W' + LTRIM(STR(DATEPART(ISO_WEEK, dbo.tarjeta.fecha_lectura))), dbo.circuito.tipo_circuito, dbo.circuito.id_circuito, dbo.tarjeta.estado
		HAVING        (dbo.circuito.tipo_circuito = 2) AND (dbo.tarjeta.estado = 'G') OR
		(dbo.circuito.tipo_circuito = 4) AND (dbo.tarjeta.estado = 'G')
	end




		EXEC	[dbo].[PRegistro]
		@Reg_Usu = '',
		@Reg_Ubi = @Ubic,
		@Reg_opt = N'ConsumosHistoric',
		@Reg_Art = @Articulo,
		@Reg_cli = @Cliente,
		@Reg_Txt = ''

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_CtaEspecifaciones]...';


GO

-- =============================================
-- Author:		IKER
-- Create date: 6-5-2014
-- Description:	Procedimiento para obtener datos del artículo
-- =============================================
ALTER PROCEDURE [dbo].[PAX_CtaEspecifaciones]
(
@OF varchar(255)='', @Articulo AS varchar(255)='',  @Usur AS varchar(255)='',  @Ubi AS varchar(255)=''
)
AS
BEGIN
	SET NOCOUNT ON;
	declare @Fam as varchar(25)=''
	declare @ArtCli as Varchar (100)='Artcli'
	DECLARE @Descri as varchar (255)
	DECLARE @Cliente AS varchar(25)--='00000000'
	declare @FecProd as date =''
	Declare @Lin as varchar (25)=''
	declare @Eng as varchar (25)=''
	declare @Des as varchar (25)=''
	declare @CtdCaja as varchar (25)=''
	declare @TEng as varchar(25)=''
	declare @TDes as varchar(25)=''
	declare @TCtdCaja as varchar(25)='Qty/Box'
	declare @Dispos as varchar(25)=''
	DECLARE @UltCon as datetime
	DECLARE @IP as varchar(49)=''
	DECLARE @Err as bit
		

	-- SI DISPONEMOS DE LA OF. CONSULTAMOS EN LA TABLA DE OFs EL ARTÍCULO Y CLIENTE.
	
	if @OF>'' 
	
		begin
			SET @Articulo=''
			SELECT @Articulo=ITEMID, @Cliente=CUSTACCOUNT, @FecProd=ECTD_FEC, @Lin=WRKCTRID
				FROM dbo.VAX_EKAN_OF
				WHERE PRODID = @OF

			SET @Articulo=isnull(@Articulo,'')

			SELECT @ArtCli=CASE WHEN EXTERNALITEMID = '' THEN INVENTTABLE.ITEMID ELSE EXTERNALITEMID END, 
				@Descri=INVENTTABLE.ITEMNAME, @Fam=INVENTTABLE.ItemGroupId, @CtdCaja=CONVERT(int, INVENTTABLE.TAXPACKAGINGQTY)
			FROM PREAXSQL.SQLAXBAK.dbo.CUSTVENDEXTERNALITEM AS CUSTVENDEXTERNALITEM RIGHT OUTER JOIN
				PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE ON CUSTVENDEXTERNALITEM.DATAAREAID = INVENTTABLE.DATAAREAID AND CUSTVENDEXTERNALITEM.ITEMID = INVENTTABLE.ITEMID
			WHERE CUSTVENDEXTERNALITEM.CUSTVENDRELATION = @Cliente AND INVENTTABLE.DATAAREAID = N'ORK' AND INVENTTABLE.ITEMID = @Articulo
		end 
	
	else
		--	NO disponemos de OF sino sólo del artículo a consultar.
		begin		
				SELECT @Descri=ITEMNAME, @CtdCaja=CONVERT(int, TAXPACKAGINGQTY), @Fam=ItemGroupId 
				FROM  PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE
				WHERE ITEMID = @Articulo AND DATAAREAID = N'ORK'
		
				SELECT        @ArtCli=ref_cliente
				FROM            dbo.circuito
				WHERE        (referencia_id = @Articulo) AND (ubicacion = @Ubi)
				
				SELECT        @Dispos=dbo.dispositivo_ubicacion.id_dispositivo, @UltCon=dbo.dispositivo.ultima_conexion, @ip=dbo.dispositivo.ip, @err=dbo.dispositivo.Dis_EnError
				FROM            dbo.dispositivo_ubicacion INNER JOIN
							dbo.dispositivo ON dbo.dispositivo_ubicacion.id_dispositivo = dbo.dispositivo.id_dispositivo
				WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

				set @Dispos=isnull(@dispos,'')

				SET @OF=@err---??????????????
				SET @FecProd=@UltCon
				SET @Lin=@Dispos
		end

		

	if SUBSTRING(@fam,1,3)='102'
		begin
			-- datos reales de eng y des de la IDS en SRVSQL cabeceras y ensayos. PTE.
			set @TEng='I. Eng'
			SELECT        @Eng= ATTRIBUTEVALUE
			FROM            PREAXSQL.SQLAXBAK.dbo.AOITEMATTRIBUTES AS AOITEMATTRIBUTES_1
			WHERE        ITEMID = @Articulo AND DATAAREAID = N'ork' AND ATTRIBUTEID = N'ATR0200001'
			
			set @TDes='I. Des'
			SELECT        @Des= ATTRIBUTEVALUE
			FROM            PREAXSQL.SQLAXBAK.dbo.AOITEMATTRIBUTES AS AOITEMATTRIBUTES_1
			WHERE        ITEMID = @Articulo AND DATAAREAID = N'ork' AND ATTRIBUTEID = N'ATR0200003'
	
			--if @eng='80' set @eng='110'
			if convert(int, @eng)>60 and convert(int, @eng)<110 set @eng='110'
			if convert(int, @eng)  <60 set @eng='60'
		end
		ELSE
		begin
			set @eng='0'--necesarios, sino da error el android
			set @DES='0' --necesarios, sino da error el android
		end


		
	--IF @Dispos<>''
		--BEGIN
		--	set @eng='2'--@IP
		--	set @DES='0'
			--set @CtdCaja='A'
		--	set @TCtdCaja='C'
	--	END
/**


		--El programa no acepta todavía, valores en eng, des y ctd caja que sean no numéricos. '' falla, ' '  también falla y hay que poner sólo numeros. 
		--pte de la modificación del programa por parte de KOldo. 
		if @CtdCaja='1'
		begin
			set @CtdCaja=''
			set @TCtdCaja=''
		end
		**/
		--set @OF=ISNULL(@OF, '000000')
		--set @Lin=isnull(@Lin,'0')
		--set @Descri=isnull(@Descri, 'xx')
		--set @FecProd=isnull(@FecProd,getdate())
		if @CtdCaja='' set @CtdCaja='0'
		if @TEng='' set @TEng=' ' --beharrezkoa bestela errorea ematen du
		if @TDes='' set @TDes=' ' --beharrezkoa bestela errorea ematen du
		set @Descri=isnull(@Descri,'')
		set @FecProd=isnull(@FecProd,CAST('00 AM' AS datetime))

		select @OF as 'OF', @Articulo AS ART, @ArtCli AS ARTCLI, @Descri AS DESCRIP, @FecProd AS FECPROD, @Lin AS LIN, @Eng as ENG, @Des as DES, @CtdCaja AS CTDCAJA, @TEng as TENG, @TDes as TDES, @TCtdCaja as TCTDCAJA

		EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usur,
		@Reg_Ubi = @Ubi,
		@Reg_opt = N'Cta Especific',
		@Reg_Art = @Articulo,
		@Reg_cli = @Cliente,
		@Reg_Txt = @OF


END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_FactConsProv]...';


GO

ALTER PROCEDURE [dbo].[PAX_FactConsProv]
AS
BEGIN

	/*
	UPDATE     tarjeta
	SET                Tar_Facturado = 0
	WHERE        (fecha_lectura > CONVERT(DATETIME, '2015-11-25 06:00:00', 102)) AND (id_circuito LIKE 'EST%')
	*/
	SET NOCOUNT ON;
	SET XACT_ABORT ON;  

	
	DECLARE @NumPed AS VARCHAR(50)
	--DECLARE @LineasPed AS int

	DECLARE @FactFEC as datetime
	set @FactFEC=getdate()

	DECLARE @NBase_Cursor varchar(50)
	DECLARE	@return_value int
	
	DECLARE  CDatos CURSOR FOR
	SELECT  id_circuito 
	FROM            dbo.circuito
	WHERE        (activo = 1) AND (tipo_circuito = 1) AND (Cir_Consigna = 1) AND (Cir_ConFacturarAut = 1) --AND id_circuito='EST'


	OPEN CDatos  -- dispone de todos los códigos de circuito .

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
		
		set @NumPed=@NBase_Cursor + '_' + CONVERT(nvarchar(100), getdate(), 112) 
		
		UPDATE [dbo].[tarjeta]
		SET [Tar_FacFec] = @FactFEC
		,[Tar_Facturado] = 1
		where
		(dbo.tarjeta.id_circuito = @NBase_Cursor) 
		--AND (NOT (dbo.TRegistroLecturas.Reg_Ctd IS NULL)) 
		AND (dbo.tarjeta.Tar_Facturado = 0) 
		AND (estado = 'P'  OR estado = 'T' OR estado = 'A') 

		INSERT INTO [srvaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
		(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, 
		InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra, FechaConsumo)
			
			SELECT      0 AS STA, dbo.tarjeta.id_circuito AS CIR, SUBSTRING(dbo.tarjeta.id_lectura, 8, 5100) AS FRO,
			 dbo.circuito.Proveedor_id AS PRO, 
			 SUBSTRING(dbo.tarjeta.id_lectura, 8, 18) AS PED, --Failoa ematne zuena elaborazioan, 5100 luzera jartzean, -qtxx hartzen duelako, y es por kenketa de txt. 
			 dbo.circuito.referencia_id AS REF,
			
			CONVERT(NUMERIC(9,3),REPLACE(
			isnull(
			(SELECT        TOP (1) Reg_Ctd
			FROM            dbo.TRegistroLecturas
			WHERE        (Reg_Ser = SUBSTRING(dbo.tarjeta.id_lectura, 8, 5100))
			ORDER BY Reg_Fec),0)
			,',','.'))
			AS CTD,

			GETDATE() AS HOY, 'iker' AS USU, @FactFEC AS FEC, dbo.circuito.reserva_auto AS RES, 'PRO' AS TIP, 3 AS ID, 'MP.CH' AS ALM, dbo.circuito.Cir_ConFacturarAut AS FAC, 
			dbo.circuito.Cir_PedMarco AS MAR, dbo.circuito.Cir_ConFacturarAgrup AS FACT, 1 AS EDD, ISNULL(dbo.circuito.Cir_RegistrarPedCompra, 0) AS REGIS,
			CONVERT(VARCHAR(200), dbo.tarjeta.fecha_lectura, 120) as FECLEC

			FROM            dbo.tarjeta INNER JOIN
			dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito

			WHERE        (dbo.tarjeta.id_circuito = @NBase_Cursor) AND  [Tar_FacFec] >= @FactFEC


		-- Para mostrar en pantalla si lo hacemos manual. NO FUNCIONAL
		select * from  [dbo].[tarjeta]
		where  [Tar_FacFec] = @FactFEC AND dbo.tarjeta.id_circuito = @NBase_Cursor

		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor

	END

	--set @LineasPed= @@CURSOR_ROWS;

	close CDatos
	deallocate CDatos
	SET XACT_ABORT OFF;

	--****************************************************
	EXEC	[dbo].[PAX_IntegrarPedidos]
	--****************************************************



	DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>AXean Sortu diren Eskaerak</H1>' +
    N'<table border="1">' +
	N'<tr><th>Grabaketa Eguna</th>' +
	N'<th>Ubik.</th>' +
	N'<th>Kodea</th>' +
    N'<th>Izena</th>' +
	N'<th>Izena 2</th>' +
	N'<th>Erabilitako Eguna</th>' +
	N'<th>Txartela</th>' +
	N'<th>Kgs</th>' +
	N'<th>Eskaera AX</th>' +
	N'<th>Linea AX</th>' +
	N'<th>Msg</th>' +
	N'<th>Albaran</th>' +
	N'<th>Linea</th>' +

    --N'<th>Artikulua</th>' +
	--N'<th>Bezeroaren Erref</th>' +
	--N'<th>Txartelak</th>' +
	--N'<th>OPTIMOAK</th>' +
	--N'<th>Diferentzia</th>' +
	--	N'<th>Pzak/Kanban</th>' +
	N'</tr>' +
    CAST ( ( SELECT DISTINCT "td/@align" =  'center', td =convert(varchar(50),  dbo.VTAUX_CREAR_PEDIDO.FechaVolc , 120) ,   ''--convert(varchar(50),  dbo.TRegistroOptHis.id_Fec , 120)
		,td = dbo.circuito.ubicacion,   ''
		,td = dbo.VTAUX_CREAR_PEDIDO.ITEMID,   ''
		,td =   dbo.circuito.descripcion,   ''
		,td =   dbo.circuito.ref_cliente,   ''
		,td =  dbo.VTAUX_CREAR_PEDIDO.FechaConsumo,   ''
		,td =  dbo.VTAUX_CREAR_PEDIDO.Name,   ''
		,td =  cast(dbo.VTAUX_CREAR_PEDIDO.SALESQTY as int),   ''
		,td =  isnull(dbo.VTAUX_CREAR_PEDIDO.SALESID,0),   ''
		,td =  CONVERT(NUMERIC(2,0),isnull(dbo.VTAUX_CREAR_PEDIDO.LINENUM,0)),   ''
		,td =  isnull(dbo.VTAUX_CREAR_PEDIDO.ERRTXT,0),   ''
		,td =  isnull(dbo.VTAUX_CREAR_PEDIDO.PackingSlipId,0),   ''
		,td =  CONVERT(NUMERIC(2,0),isnull(dbo.VTAUX_CREAR_PEDIDO.PackingSlipLINENUM,0)),   ''
		--,td =  dbo.circuito.referencia_id,   ''
		--,td =    dbo.circuito.ref_cliente,   ''
		--,"td/@align" =  'center', td =   dbo.TRegistroOptHis.num_kanbanAct,   ''
		--,"td/@align" =  'center', td =   dbo.TRegistroOptHis.num_kanbanOpt,   ''
		--,"td/@align" =  'center',  "td/@bgcolor" =  case when ((dbo.TRegistroOptHis.num_kanbanOpt*100/dbo.TRegistroOptHis.num_kanbanAct)>50 and (dbo.TRegistroOptHis.num_kanbanOpt*100/dbo.TRegistroOptHis.num_kanbanAct)<120) then 'green' ELSE 'yellow' END, td = dbo.TRegistroOptHis.num_kanbanDif,  '' --case when [Dis_EnError]=0 then 'OK' ELSE 'ERR' END--   
		--,"td/@align" =  'right', td =   dbo.TRegistroOptHis.cantidad_piezas,   ''
		--  ,td = convert(varchar(50), [ultima_conexion], 120),   ''
		--  ,td = 'https://' + [ip] + ':10000',   ''
		--  ,"td/@align" =  'center', td = [version], '',td=convert(varchar(50), [fecha_inicio], 120)
		FROM            dbo.VTAUX_CREAR_PEDIDO INNER JOIN
								 dbo.circuito ON dbo.VTAUX_CREAR_PEDIDO.Journalname = dbo.circuito.id_circuito
		WHERE     (dbo.VTAUX_CREAR_PEDIDO.FechaVolc = @FactFEC)
		order by dbo.circuito.ubicacion, dbo.VTAUX_CREAR_PEDIDO.ITEMID, dbo.VTAUX_CREAR_PEDIDO.FechaConsumo
		--, CONVERT(NUMERIC(2,0),isnull(dbo.VTAUX_CREAR_PEDIDO.LINENUM,0)),   dbo.VTAUX_CREAR_PEDIDO.Name
		--isnull(dbo.VTAUX_CREAR_PEDIDO.SALESID,0)



	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;


	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='iker@orkli.es; jestensoro@orkli.es; jolasagasti@orkli.es; eunanue@orkli.es; ptejeira@orkli.es; mgonzalez@orkli.es; iferrer@orkli.es; egoitze@orkli.es; nantxia@orkli.es',
	  @subject='eKanban: Fakturatzeko AXean Grabaturiko Txartelak',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'



END


	
	/*



	

	*/
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_PatronLec]...';


GO


-- =============================================
-- Author:		IKER
-- Create date: 13/5/2014
-- Description:	Procedimiento para conseguir datos, según patrón de lectura del código de barras

-- Este procedimiento se ejecuta siempre que hay una lectura de un codigo de barras en los dispositivos ANDROID (Consumo y Consulta)
-- y cuando hay una lectura del portico. 
-- El objetivo es conseguir la OF y la SERIE de la información de la etiqueta/QR/RFID, etc. 
-- El resultado da, OF, SERIE y GRAB. El campo grab es la que se utiliza para garantizar que no se repite la lectura de la misma etiqueta. 
-- Si el resultado tiene OF no válido (Que no sea null, vacio o 000000) el proceso siguiente, coge el Articulo y cliente desde el resultado del procedimiento. 
-- Si el resultado tiene OF válido, va a VAX_EKAN_OF y coge como resultado el artículo y cliente. 

-- una vez que tiene, ARTICULO, CLIENTE Y USUARIO (el de la aplicación), consulta en la tabla de circuitos y obtiene el código circuito para seguir el proceso. 
-- =============================================
ALTER PROCEDURE [dbo].[PAX_PatronLec]
(
@Txt varchar(255)='', @Usu AS varchar(255)='', @Ubi AS varchar(255)='', @Pant AS int=0 , @Disp as varchar(255)='', @Ant as varchar(50)='', @Sen as varchar(5)='', @OrigenLectura as varchar(25)=''
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TP AS INT=0  --TIPO DE PATRON APLICADO. SI ES 0 EN RESULTADO, NO HA ENTRADO EN NINGUNO DEFINIDO. 
	DECLARE @L AS VARCHAR(25)=''
	DECLARE @L1 AS INT
	DECLARE @L2 AS INT
	DECLARE @S AS VARCHAR(100)=''
	DECLARE @S1 AS INT
	DECLARE @S2 AS INT
	DECLARE @A AS VARCHAR(25)=''
	DECLARE @A1 AS INT
	DECLARE @A2 AS INT
	DECLARE @AR AS VARCHAR(25)=''
	DECLARE @Cli AS VARCHAR(25)=''
	DECLARE @UbiTip AS VARCHAR(25)=''
	DECLARE @V AS VARCHAR(25)=''
	DECLARE @CliOF AS VARCHAR(25)=''
	DECLARE @CliUBI AS VARCHAR(25)=''
	DECLARE @I AS INT
	DECLARE @CIR AS VARCHAR(100)=''
	DECLARE @CTD AS VARCHAR(100)=''
	DECLARE @PED AS VARCHAR(100)=''
	DECLARE @NIVACC AS INT
	DECLARE @ACCION AS VARCHAR(10)=''
	DECLARE @CTDACCCION AS INT
	DECLARE @TipCont AS INT
	DECLARE @position int
	DECLARE @string char(500)
	DECLARE @1P AS VARCHAR(100)=''
	DECLARE @16K AS VARCHAR(100)=''
	DECLARE @2L AS VARCHAR(100)=''
	DECLARE @1J AS VARCHAR(100)=''
	DECLARE @5J AS VARCHAR(100)=''
	DECLARE @6J AS VARCHAR(100)=''
	DECLARE @7Q58 AS VARCHAR(100)=''
	DECLARE @7QGT AS VARCHAR(100)=''
	DECLARE @16D AS VARCHAR(100)=''
	DECLARE @2P AS VARCHAR(100)=''
	DECLARE @ALB AS VARCHAR(100)=''
	DECLARE @ALBLIN AS VARCHAR(100)=''
	DECLARE @UbiCap AS VARCHAR(25)=''
	DECLARE @LotCap AS VARCHAR(50)=''
	DECLARE @Emp AS VARCHAR(3)=''
	DECLARE @URLPlano AS VARCHAR(50)=''
	--SI LA UBICACIÓN ACTUAL, TIENE CHECK DE REGISTRO DE TRAZABILIDAD. 
	DECLARE @RegTraz bit=0
	DECLARE @EMPR NCHAR(3)=''
	declare @cade varchar(200)
	declare @TIPOETIQUETA VARCHAR(50) = NULL;
	DECLARE @TIPO AS VARCHAR(50) = NULL;
	DECLARE @INDICE AS INT = 0;
	DECLARE @QRDATA VARCHAR(255) = NULL;

	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT
	begin transaction
	--*************************************************************************************************************************************************
	--Si la ubicación tiene predefinida un tipo concreto de etiqueta tratamos esa
	 SELECT @TIPOETIQUETA = TipoEtiqueta FROM ubicacion WHERE id_ubicacion = @UBI;
	 PRINT @TIPOETIQUETA;
	 IF @TIPOETIQUETA <> ''
	 BEGIN
		PRINT 'TIPO ETIQUETA PERSONALIZADA';
		DECLARE TIPO CURSOR FOR select * from dbo.fnSplitString (@TIPOETIQUETA,':');
		OPEN TIPO
		FETCH NEXT FROM TIPO INTO @TIPO
		WHILE @@fetch_status = 0
		BEGIN
			IF @INDICE = 0 SET @TP = @TIPO;
			else set @qrData = @tipo;
			SET @INDICE = @INDICE + 1;
			FETCH NEXT FROM TIPO INTO @TIPO
		END
		print @tp;
		DECLARE @VARIABLE VARCHAR(255);
		DECLARE @VALOR VARCHAR(255);
		declare @var varchar(255);
		declare @posVariable int = -1;
		DECLARE DATA CURSOR FOR select * from dbo.fnSplitString (@QRDATA,'|');
		OPEN DATA
		FETCH NEXT FROM DATA INTO @var
		WHILE @@fetch_status = 0
		BEGIN
			select @posVariable=  CHARINDEX('=', @var); 
			set @VARIABLE = SUBSTRING(@var, 0, @posVariable);
			set @VALOR = SUBSTRING(@var, @posVariable + 1, len(@var));
			print @variable +  @valor;
			IF @VARIABLE='CTD'  BEGIN PRINT @VALOR;SET @CTD = DBO.GS1Data(@TXT, @VALOR); END
			IF @VARIABLE='LOTE' SET @L = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='REF' SET @A = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='PROV' SET @V = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='BULTO' SET @S = DBO.GS1Data(@TXT, @VALOR);
			FETCH NEXT FROM DATA INTO @VAR
		END
		print 'lote';
		print @L;
		PRINT 'ERREFERENTZIA=';
		PRINT @A;
		deallocate  tipo;
		deallocate  data;
		
		set @CIR=''

		--SELECT @S=DBO.GS1Data(@txt,'S')
		set @L=isnull(@L,'')
		IF @L='' SET @L='0' 
		PRINT @CTD;
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		PRINT 'CANTIDAD OK';
		--revisamos a qué empresa corresponde la ubicación
		SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)

		--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
		--Cogemos el cod cliente de la ubiación
		SET @A=ISNULL(@A,'')
		SET @CIR=''
		--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 
		PRINT 'REFERENCIA_ID=';
		PRINT @A;
		SELECT        @CIR=id_circuito, @Cli=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1

		set @ant=@V

		SET @CIR=ISNULL(@CIR,'')

		--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
		IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
		--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

		BEGIN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
		END


		SET @S=RTRIM(@S) + '_' + @L
		SET @L='000000'
		
		if (not exists(select * from tarjetaMetadata where id_lectura=@L + '_' + @S and sentido=@Sen))
			insert into tarjetaMetadata (id_lectura, sentido, txtLectura, tipo,fechaPrimeraLectura ,fechaUltimaLectura, PatronLectura) values (@L + '_' + @S, @Sen, @Txt, @tp, GETDATE(), GETDATE(), @TIPOETIQUETA);
		else update tarjetaMetadata set fechaUltimaLectura = GETDATE(), PatronLectura=@TIPOETIQUETA where id_lectura = @L + '_' + @S and sentido = @Sen
		GOTO REGISTROLECTURAS;
	 END
	
	
	--*************************************************************************************************************************************************
	-- Cuando no se inserta ningún dato manualmente en la referencia
	if @txt='P'
	BEGIN
		SET @TP=1
		SET @L='000000'
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación y que sean activos
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi AND (activo = 1) ORDER BY num_kanban
	END

	--*************************************************************************************************************************************************
	--Si es un artículo el que se lee. Automaticamente le añade una P al inicio
	if @txt<>'P' AND left(@txt,1)='P' AND left(@txt,2)<>'P*' AND left(@txt,4)<>'PROV' and  (CHARINDEX('_', @txt, 1) = 0)
	begin
		--P08296603
		SET @TP=2
		SET @L ='000000'
		SET @Cli ='00000000'

		SET @AR =SUBSTRING(@txt, 2, LEN(@TXT))

		SELECT      @Cli= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		--en vez de esta consulta, mejor la de lista de precios, o ref cruzadas, para garantizar que es una del cliente.
		SELECT   @A = ITEMID
		FROM     PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE
		WHERE    (DATAAREAID = N'ORK') AND (ITEMID = @AR)

		IF @A = ''
		BEGIN
			SELECT     @A= CUSTVENDEXTERNALITEM.ITEMID
			FROM       PREAXSQL.SQLAXBAK.dbo.CUSTVENDEXTERNALITEM AS CUSTVENDEXTERNALITEM INNER JOIN
					   PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE ON CUSTVENDEXTERNALITEM.DATAAREAID = INVENTTABLE.DATAAREAID AND CUSTVENDEXTERNALITEM.ITEMID = INVENTTABLE.ITEMID
			WHERE        (CUSTVENDEXTERNALITEM.DATAAREAID = N'ork') AND (CUSTVENDEXTERNALITEM.EXTERNALITEMID = @AR) AND (CUSTVENDEXTERNALITEM.CUSTVENDRELATION = @Cli)
			ORDER BY CUSTVENDEXTERNALITEM.EXTERNALITEMID
		END
	end 



	--*************************************************************************************************************************************************
	-- cuando se lee una consulta con * para realizar operaciones. 
	--  *A2-609:T12
	--  *A2-609:N5
	if left(@txt,2)='P*' and left(@txt,3)<>'P*U'
	begin
		SET @TP=3
		IF CHARINDEX(':', @TXT,1) > 0
			BEGIN
				SET @L='000000'	
				SET @A1 = 3
				SET @A2 =CHARINDEX(':', @TXT,1)
				IF @A2>0 
				begin
					SET @A =SUBSTRING(@txt, @A1, @A2-@A1)
					sET @ACCION =SUBSTRING(@txt, @A2+1, LEN(@TXT)-(@A2))
					set @CTDACCCION= SUBSTRING(@ACCION, 2, len(@ACCION)-1)
					set @ACCION=SUBSTRING(@ACCION, 1, 1)
				end
		
				--CIRCUITO? SI EXISTE
				SELECT       @CIR= id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        ubicacion = @Ubi AND referencia_id = @A
		
				--si no existe, cogemos el cliente de la ubicación
				IF @CIR=''
					BEGIN
						SELECT        @Cli=localizacion
						FROM            dbo.ubicacion
						WHERE        id_ubicacion = @Ubi
					END
		
				--Cogemos el rol del usuario
				SELECT        @NIVACC= ptr_rol
				FROM            dbo.sga_usuario
				WHERE        USERNAME = @Usu
		
				-- Con permiso, para crear y no hay circuito
				IF @NIVACC=7 AND @ACCION='N' AND @CIR=''
				BEGIN
		
					-- CREAR CIRCUITO
					INSERT INTO [dbo].[circuito]
					   ([cliente_id],[id_circuito],[referencia_id],[activo],[cantidad_piezas],[cliente_nombre],[descripcion],[entrega_dias]
					   ,[entrega_horas],[mail],[num_kanban],[ref_cliente],[reserva_auto],[ubicacion],[tipo_circuito],[tipo_contenedor])
					VALUES
					   (@Cli, @Ubi+@A, @A, 1, 1, '', '', 0,0,'', @CTDACCCION, '', 1, @Ubi, 1,1)

					   SET @S='NuevoCir'
				END
	
				-- Con permiso, para modif y  hay circuito
				IF @NIVACC=7 AND @ACCION='T' AND @CIR<>''
				BEGIN
					SET @S='ActNumKan'
					UPDATE [dbo].[circuito]
					SET [num_kanban] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END

				IF @NIVACC=7 AND @ACCION='Q' AND @CIR<>''
				BEGIN
					SET @S='ActQPzas'
					UPDATE [dbo].[circuito]
					SET [cantidad_piezas] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END
			END
			else
			begin
				DECLARE	@return_value int
				if left(@txt,4)='P*er' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvUsers]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*rf' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitArcRFID]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*eg' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitPanel]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
				if left(@txt,4)='P*01' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0001'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				if left(@txt,4)='P*02' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0002'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*03' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0003'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
			end

	end


	----*************************************************************************************************************************************************
	----PARA PROBAR EL EKANBAN, BORRANDO LAS ENTRADAS AL CIRCUITO
	----/**
	--if left(@txt,2)='P*' and (left(@txt,5)='P*U00') --OR left(@txt,5)='P*U07'
	--begin
	--	begin
	--		DELETE FROM [dbo].[tarjeta]
	--		WHERE        (ubicacion = SUBSTRING(@TXT,3,3))

	--	end
	--	SET @L='000000'	
	--	--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
	--	SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
	--end
	----**/
	----*************************************************************************************************************************************************



	-- si es tipo de SMSTO
	if left(@txt,5)='SMSTO'
	begin
		SET @TP=4
		--LOS MENSAJES NORMALES:     (QUE NO SEAN DE FORMACIÓN NI DE TIPO ORK. QUE NO EXISTA UORK O FORMACIÓN)
		IF CHARINDEX('UORK_', @TXT,1)=0 and CHARINDEX('FORMACION', @TXT,1)=0 
		BEGIN
			--SMSTO:+34696469617:H123456_179993_20900-31
			SET @L1 =CHARINDEX('H', @TXT,15)+1
			SET @L2 =CHARINDEX('_', @TXT,@L1)
			SET @L =SUBSTRING(@txt, @L1, @L2-@L1)

			SET @S1 =CHARINDEX('_', @TXT,@L2)+1
			SET @S2 =CHARINDEX('_', @TXT,@S1)
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)

			SET @A1 =CHARINDEX('_', @TXT,@S2)+1
			SET @A2 =LEN(@TXT)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1+1)

			IF @L ='000000' SET @Cli ='00000000'

 
			-- para el error de reetiquetados de COPRECI
			--SI MIRAMOS 

			
			SELECT       @CliOF=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END

		END
		
		--IF CHARINDEX('FORMACION', @TXT,1)>0 
		--BEGIN
		----SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		----SMSTO:+34696469617:U0PFORMACION4K00
		--	SET @A ='20900-1'
		--	SET @L ='000000'
		--	SET @Cli ='00000000'
		--	SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		--END

			
		IF CHARINDEX('UORK_', @TXT,1)>0 
		BEGIN
			--Etiquetas internas de SKINTER
			--SMSTO:+34629552542:UORK_A2-609_S69901
			SET @A1 =CHARINDEX('UORK_', @TXT,15)+5
			SET @A2 =CHARINDEX('_', @TXT,@A1)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1)

			SET @S1 =CHARINDEX('_S', @TXT,@A2)+2
			SET @S2 =LEN(@TXT)+1
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)
			SET @L ='000000'
			SET @Cli ='00000000'
		END
	
	END

--*************************************************************************************************************************************************

-- si es tipo de http://www.ekanban.es/qr/157238252841
	-- si es tipo de http		
	if left(@txt,5)='http:' --and @Ubi<>'U41'
	begin
		SET @TP=5
		SET @Emp='ORK'
		--http://www.orkli.com/qr/123456088082
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @L =SUBSTRING(@txt, @L1, 6)
		SET @S =SUBSTRING(@txt, @L1+6, 6)
		SET @Emp=SUBSTRING(@txt, @L1+12, 3)-- no siempre lo tiene. 

		--Si no dispone de dato en empresa, cogemos ORK por defecto.
		IF @Emp<>'BRA' SET @Emp='ORK'
		
		--SET @Emp='BRA'


		SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L) AND (DATAAREAID = @Emp)

		SELECT      @CliUBI= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		IF (@CliUBI<>@CliOF) or (@Emp='BRA')   -- EN CASO DE QUE SEA LA EMPRESA BRASIL, COMO EL APP, NO REVISA LAS OF, SEGÚN EMPRESA, LE PASAMOS EL CLIENTE CORRESPONDIENTE Y LOTE 000000, PARA QUE NO COJA EN BASE 
			BEGIN
				SET @Cli =@CliUBI
				SET @S= @L + @S + @Emp  -- @Emp jarria el 25/5/2017, para evitar de que consumos de defendi, diga que existen por haber consumido apis
				SET @L ='000000'

				
				SET @AR=''
				--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
				--1230/1851-1200
				SELECT        @AR=referencia_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

				SET @AR=ISNULL(@AR,'')

				IF @AR=''
				BEGIN
					set @A=SUBSTRING (@A, LEN(@A)-3, 4)
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES
				END

			END
	END


	
-- si es tipo de http://www.ekanban.es/qr/157238252841   para mueller
	-- si es tipo de http		, long 37
	if left(@txt,5)='http:'  and @Ubi='U41' and CHARINDEX('BRA', @TXT,1)=0 
	begin
		SET @TP=55
		--http://www.orkli.com/qr/123456088082
		--http://www.ekanban.es/ekanban/qr/196020BR123018511201 
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @S =SUBSTRING(@txt, @L1, 6)
		SET @A =SUBSTRING(@txt, @L1+6, 50)
		set @L='000000'


			--SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
			--FROM            dbo.VAX_EKAN_OF
			--WHERE        (PRODID = @L)


			SET @AR=''
			--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
			--1230/1851-1200
			SELECT        @AR=referencia_id
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

			SET @AR=ISNULL(@AR,'')

			IF @AR=''
			BEGIN
				set @A=SUBSTRING (@A, LEN(@A)-3, 4)
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES

			END




			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END
	end 


-------------------------------------------------------------------------------------------
	
	--borrado el 30/11/2015. quitar el 31/12/2015 del todo
	-------------------------------------------------------------------------------------------
	
	--ETIQUETA "ESTAMPA" CON SOLO PROVEEDOR Y ARTÍCULO (opcional la cantidad)
		--PROV_000388_20100374_151.1

		--SI ES EL TIPO VIEJO DE ETIQUETAS DE ESTAMPA. A BORRAR ESTA PARTE.
	if (SUBSTRING(@txt, 1, 4)='PROV' AND CHARINDEX('PROVV', @txt, 1) = 0)				--PROV_V
	begin
		SET @TP=6
		IF CHARINDEX('_', @txt, 13)>0
			BEGIN
			SET @A=SUBSTRING(@txt, 13, CHARINDEX('_', @txt, 13)-13)
			SET @CTD=SUBSTRING(@txt, CHARINDEX('_', @txt, 13)+1, LEN(@TXT)-CHARINDEX('_', @txt, 13))
			END
		ELSE
			BEGIN
			SET @A=SUBSTRING(@txt, 13, LEN(@TXT))
			SET @CTD='0'
			END

		SET @L ='000000'
		SET @Cli ='00000000'
		
		--SET @S = right(@A,3) + '_' + @CTD + '_' + CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		SET @S = SUBSTRING(@txt, 6, LEN(@TXT))+ CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		
	end 
	--	A BORRAR HASTA ESTA PARTE

	
--*************************************************************************************************************************************************

	-- ETIQUETAS DE TRAZABILIDAD INTERNA, SIN CONSIDERAR EL FORMATO PROV_ QUE ES EL TIPO 6 E INICIO CON INTE O PROV Y SEPARADOR ;
	if ((SUBSTRING(@txt, 1, 4)='PROV' or SUBSTRING(@txt, 1, 4)='INTE') AND 
		SUBSTRING(@txt, 1, 5)<>'PROV_' AND CHARINDEX(';', @txt, 1) > 0 )
	
		begin
		SET @TP=7
		
		--PROV;V000388;P20100301;Q1010;TT032580351;S14288      se utiliza en estampa
		--INTE;SORKA77013  se utiliza para etiquetado interior decoletajes, lapeados, etc. 

		DECLARE @N AS INT=0 
		DECLARE @N1 AS INT=0
		DECLARE @C AS char(40)=''
		DECLARE @ID AS Char(30)=''
		DECLARE @P AS Char(30)=''
		DECLARE @VCAP AS Char(30)=''

		SET @CTD='0'
		SET @L ='000000'
		SET @Cli ='00000000'
		
		SET @N=CHARINDEX(';', @txt, 1)+1
		
		SET @TXT=REPLACE(@TXT,CHAR(4),'') --EOT


		SET @EMPR='ORK'
		
		--QUE EMPRESA ES ESTA UBICACIÓN?
		SELECT    @EMPR=Ubi_Dataareaid 
		FROM     dbo.ubicacion
		WHERE   (id_ubicacion = @Ubi)

		--PROV;V000388;P20100301;Q1010;TT032580351;S14288   
		WHILE @N<LEN(@TXT)
			BEGIN
				SET @ID=''
				SET @N1=CHARINDEX(';', @txt, @N)
				IF @N>0 AND @N1=0 SET @N1=LEN(@TXT)+1
				SET @C =SUBSTRING(@TXT, @N, @N1-@N)

				SET @ID=SUBSTRING(@C, 1, 1)
			
				if @ID='P' set @A=SUBSTRING(@C, 2, len(@c)) 
				if @ID='Q' set @CTD=SUBSTRING(@C, 2, len(@c)) 
				if @ID='H' OR @ID='T' set @L=SUBSTRING(@C, 2, len(@c)) 
				if @ID='S' set @S=SUBSTRING(@C, 2, len(@c))						-- PARA LAS ETIQUETAS INTERNAS, COGEMOS LA SERIE
				if @ID='V' set @V=SUBSTRING(@C, 2, len(@c)) 

				SET @N=@N1+1
			END

		--INTE;SORKA77013  ETIQUETADO INTERIOR DE DECOLETAJE, LAPEADO
		IF SUBSTRING(@txt, 1, 4)='INTE' 
			BEGIN
				--COGEMOS LOS DATOS REGISTRADOS AL IMPRIMIR LA ETIQUETA
				SELECT TOP 1 @A=Referencia, @L=NLote, @CTD=Cantidad, @V=Proveedor         --CAMPOS DISPONIBLES Referencia, NSerie, NLote, Cantidad, Proveedor, Fecha, Tipo
				from [SRVSPS2007\SQL2005x32].[ORKLB000].dbo.VEtiquetas 
				where nserie = @S
				ORDER BY FECHA DESC
			END
		
		
		set @LotCap=@S
		-- SI ES DE CAPTOR
		IF SUBSTRING(@txt, 1, 9)='INTE;SCAP' 
		BEGIN
				SELECT   @Usu=SUBSTRING(descripcion,1,10)--limitamos a 10 la descripcion, bestela error
				FROM     dbo.ubicacion
				WHERE   (id_ubicacion = @Ubi)

				--EL USUARIO DEBE SER EL NOMBRE DEL RECURSO DEL AX
				-- 9/3/2017 no estamos insertando THI1 como recurso, pq el apartado anterior estaba anulado. lo activo otra vez. iker.  INTE;SCAP115049  
				--iñigo y blanca han venido. hay foto etiquta en mov ies



				set @S=SUBSTRING(@S, 4, LEN(@S))
				
				SELECT        @L=Lote, @A=Referencia, @CTD=Cantidad--, Compañia
				FROM            SRVCAPTORDB.Captor3.dbo.VCAPTOR_Lotes AS A
				WHERE        ( A.Compañia = '01' AND A.Lote=@S)

				set @LotCap=@S
				set @S=@ubi

				SET @V='000000' -- CONSIDERAMOS QUE SI ES INTERNA Y DE CAPTOR EL PROVEEDOR ES ORKLI
		END

		

		set @A=isnull(@A,'')
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		set @Cli='00000000'
		SET @V=ISNULL(@V,'000000')
		IF @V='' SET @V='000000'

		EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
				@empresa = @EMPR,
				@linea = @Usu, -- @Ubi, 
				@P = @A,
				@Q = @CTD ,
				@V = @V,
				@serie = @S,
				@H = @L
		
		
			SET @VCAP='PROV:' + @V -- + ';TRAT:16' + ';POS:8'    --CUIDADO EN APLICAR ESTO, YA QUE LUEGO NO ENCUENTRA EL CIRCUITO DE ESTE PROVEEDOR. CAMBIAR NOMBRE DE VARIALBE.


		EXEC	@return_value = PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			@Empresa = N'01',
			@Maquina = @Usu,
			@NLote =@LotCap,
			@Referencia = @A,
			@Cantidad = @CTD,
			@DatosAdicionales = N''		--DEBERÍA DE SER EL SIGUIENTE PARA AÑADIR EL PROVEEDOR. DE MOMENTO NADA. @DatosAdicionales = @VCAP

		
		
		--MIRAMOS SI LA REF Y PROVEEDOR DISPONE DE CIRCUITO O NO 
		SET @CIR=''
		
		SELECT @CIR= id_circuito
		FROM  dbo.circuito
		WHERE (referencia_id = @A) AND (Proveedor_id = @V) AND (ubicacion = @Ubi) AND    (activo = 1)

		SET @CIR=isnull(@CIR,'')	--SI NO EXISTE MANTENEMOS EN ''

		IF @CIR=''
		BEGIN
			SET @A=''  --ANULAMOS LA REFERENCIA PARA EVITAR QUE SE PUEDA DAR CONSUMO DE UNA REFERENCIA QUE SEA DE OTRO PROVEEDOR QUE TENGA KANBAN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			set @A=isnull(@A,'')
		END


		SET @S=@L + '/' + @S +@CIR
		SET @L ='000000'

	end

	
--*************************************************************************************************************************************************

	if left(@txt,5)='[)>{R'
	begin
		SET @TP=8
		SET @txt='[)>{RS}06{GS}2L1{GS}V001700{GS}1PXXX{GS}16K004330{GS}Q1{GS}PF-4002-4{GS}1JUNXXXXXXXXXXXX{GS}7Q1058{GS}7Q16GT{GS}T147/99{GS}6DD10/9/99{GS}S2{GS}2P4{RS}{EOT}'



		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A='Prueba GTL'
	end


--*************************************************************************************************************************************************


	--ETIQUETA "MALA" CON SOLO ARTÍCULO DE SKINTER
	--A2-609
	if SUBSTRING(@txt, 1, 1)='A' AND CHARINDEX('-', @txt, 1) =3
	begin
		SET @TP=9
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************


	--ETIQUETA EAN 13
	--8477637477
	if LEN(@TXT)=13 AND (SUBSTRING(@txt, 1, 2)='84' OR SUBSTRING(@txt, 1, 2)='44')
	begin
		SET @TP=10
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************

	--LECTURAS RFID DE LOS PORTICOS
	--114208092289
	if LEN(@TXT)=12 AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID'
	begin
		SET @TP=11

		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)


		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 


		set @TipCont=isnull(@TipCont,0)
		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 
			IF @Ubi='UWOR'
				BEGIN
				--si es en la ubicación BOSCH, LE AÑADIMOS EL NÚMERO DE PALET AL SERIE
			
				set @S=@L + @S --SUBSTRING(@S,1,2) + '0000'--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

				END
			ELSE
				BEGIN 
				--LECTURAS DE SABAF
			
				set @S=@L --MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO


				END
		
		END
		ELSE
		BEGIN
		IF @TipCont=1 
			begin

				set @S=@L + @S--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
					set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

			end


		END
				
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			DECLARE @ActAnt0 bit
			DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
				set @S='000000'
				set @L='000000'
			end
			--*****************************
			
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END

			

	end

	
	--LECTURAS RFID DE LOS PORTICOS CON CÓDIGO HEXADECIMAL
	--114208092289
	if (LEN(@TXT)=22 OR LEN(@TXT)=24 ) AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID' AND LEFT(@TXT,4)<>'PROV'
	begin
		SET @TP=110



		declare @sql nvarchar(MAX), @ch varchar(200)
		--select @v = '313931323434323637393338'
		select @sql = 'SELECT @ch = convert(varchar, 0x' + @TXT + ')'
		EXEC sp_executesql @sql, N'@ch varchar(30) OUTPUT', @ch OUTPUT
		--SELECT @ch AS ConvertedToASCII
		SET @txt=@ch
		
		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)

		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 



		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END
		IF @TipCont<>2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			--set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END

					
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END


	end

	
--*************************************************************************************************************************************************
	
	--control de stocks DISPONIBILIDAD
	if LEFT(@TXT,3)='CAE' --AND LEN(@TXT)=12 left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND 
	BEGIN
		SET @TP=12
		begin 
			set @A=substring(@TXT,1,6)
			
			
			SELECT      @UbiTip=Ubi_Modo   --EVEN  DISP
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi


			SET @L='000000'
			SET @Cli ='00000000'
			SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
			SET @S = @Ubi + '_' + @TXT 


			IF @Sen='E' AND @UbiTip='DISP'
			BEGIN
				DELETE FROM [dbo].[tarjeta]
				WHERE
				id_lectura=@L + '_' + @S AND SENTIDO='S'
			END





		end
	END

	
--*************************************************************************************************************************************************

	--LECTURA DE ALBARANES (CONSIGNA)
	--AL00109472_1_134340    
	if  LEFT(@TXT,2)='AL'
	begin
			DECLARE @CLI1 AS VARCHAR(25)=''
			

			SET @TP=13
			
			SET @ALB = SUBSTRING(@txt, 1, CHARINDEX('_', @txt, 1)-1)
			SET @ALBLIN = SUBSTRING(@txt,  CHARINDEX('_', @txt, 1)+1,1)
			SET @L = SUBSTRING(@txt,  CHARINDEX('_', @txt,  CHARINDEX('_', @txt, 1)+1)+1, len(@txt))
			
			--000500 ES UN LOTE FICTICIO DEL AX
			SET @L=ISNULL(@L,'000500')
			IF @L='' SET @L='000500'


			SET @S = @txt 

			--Cogemos el artículo según el lote de fabricación. 
			SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			--CANTIDADES SEGÚN EL ALBARAN??????????????????????????????????????????????????????????


			--Cogemos el cliente, según el artículo y ubicación donde estamos leyendo.
			SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor, @CLI1=cliente_id 
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) AND   (activo = 1)

			IF @CLI<>@CLI1
			begin
				set @CLI=@CLI1
				SET @L='000000'
			end
			



	end

	--Si es sólo lote de la etiqueta, cogemos el lote y apartir de este dato, consultará en vista de OFs
	if left(@txt,1)='H' AND LEN(@TXT)=7
	begin
		--H113688
		SET @TP=15
		SET @L =SUBSTRING(@txt, 2, 6)
		SET @S ='000000'
	end 
	
--*************************************************************************************************************************************************

	IF  LEFT(@TXT,4)='RFID'
	BEGIN
		SET @TP=16
		SET @L ='000000'--SUBSTRING(@txt, 1, 8)
		SET @S =SUBSTRING(@txt, 1, 8)
		set @cade = @TXT + ' en Ubi: ' + @Ubi + ' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '
		declare @cade1 as varchar(50)

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=REFERENCIA_ID, @Cli=cliente_id FROM dbo.circuito WHERE id_circuito = @TXT
		SET @L='000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 



		
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			--DECLARE @ActAnt0 bit
			--DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
			end
			--*****************************
			




		SELECT        @cade1=mail
		FROM            dbo.circuito
		WHERE        (id_circuito = @TXT)


		set @cade1=isnull(@cade1,'')

		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		IF @cade1<>''
		BEGIN
			EXEC	[dbo].[PEnvCorreo]
			@Des = @cade1,
			@Tem = N'Lectura de Etiqueta TEST RFID',
			@Cue =@cade
		END

	END

	
--*************************************************************************************************************************************************

	--ELABORACIÓN, SEGUIMINETO TRAZABILIDAD THIELENHAUS....
	
	IF  LEFT(@TXT,2)='SO' AND @TP=0 --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	BEGIN
		SET @TP=17

			SET @S=SUBSTRING(@TXT, 2, LEN(@TXT)+1)
		EXEC	PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazab]
		@serie =@S,--'ORKA50136',--
		@linea =@Ubi


		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S=@L + '/' + @S
		SET @L ='000000'
	END


	--V001857PE-20262Q4093H126511K154572   la etiqueta de vilardell
	--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	--PTE


	--ETIQUETA GS1, SEGÚN NUEVA NORMA. ENERO 2016
	--[)>06V000782PA5-437Q450H2/5357S
	
	if left(@txt,3)='[)>'
		BEGIN
		SET @TP=18
			print @txt;
			SET @L=''
			SET @A=''
			set @CIR=''

			SELECT @A=DBO.GS1Data(@txt,'P')
			SELECT @CTD=DBO.GS1Data(@txt,'Q')
			SELECT @L=DBO.GS1Data(@txt,'H')
			IF @L='' SELECT @L=DBO.GS1Data(@txt,'T')
			SELECT @S=DBO.GS1Data(@txt,'S')
			SELECT @V=DBO.GS1Data(@txt,'V')

			--SELECT @16K=DBO.GS1Data(@txt,'16K')
			--SELECT @2L=DBO.GS1Data(@txt,'2L')
			--SELECT @1J=DBO.GS1Data(@txt,'1J')
			--SELECT @5J=DBO.GS1Data(@txt,'5J')
			--SELECT @6J=DBO.GS1Data(@txt,'6J')
			--SELECT @7Q58=DBO.GS1Data(@txt,'7Q')
			--SELECT @16D=DBO.GS1Data(@txt,'16D')
			--SELECT @2P=DBO.GS1Data(@txt,'2P')
			print @L;
			set @L=isnull(@L,'')
			IF @L='' SET @L='0' 
			


			--SET @CTD=REPLACE(@CTD,'.','') gs-n jarriak
			--SET @CTD=REPLACE(@CTD,',','.')gs-n jarriak
			SET @CTD= CONVERT(NUMERIC(9,1),@CTD)

			--revisamos a qué empresa corresponde la ubicación
			SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
			FROM            dbo.ubicacion
			WHERE        (id_ubicacion = @Ubi)
			
			--if @RegTraz=1
			--begin

			--	--REGISTRAMOS DATOS EN EL AX A TRAVÉS DEL PROCEDMINETO DEL SERVIDOR DEL AX.
			--	EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
			--		@empresa = @EMPR,
			--		@linea = @Usu, -- @Ubi,
			--		@P = @A,
			--		@Q = @CTD ,
			--		@V = @V,
			--		@serie = @S,
			--		@H = @L
		


			--	----REGISTRAMOS LOS DATOS EN CAPTOR
			--	--como da error si el lote no existe...


			--	SET @V='PROV:' + @V
			--	set @LotCap=@L + '_' + @S

			--	EXEC	@return_value =PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			--			@Empresa = N'01',
			--			@Maquina = @Usu,
			--			@NLote =@LotCap,
			--			@Referencia = @A,
			--			@Cantidad =@CTD,
			--			@DatosAdicionales = @V

			--END

			--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
			--Cogemos el cod cliente de la ubiación
			SET @A=ISNULL(@A,'')
			SET @CIR=''
			--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 


			if  @ubi='U45' OR @ubi='U49' OR @ubi='U51' OR @ubi='U52' OR @ubi='U53' 

				BEGIN 
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi)  AND activo=1 and Proveedor_id=@V
								
				END
			ELSE
				BEGIN
				
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1
				END
			--PRINT 'CLIENTE = ' + @CLI;
			--PRINT 'CIRCUITO =' + @CIR;

			set @ant=@V




			SET @CIR=ISNULL(@CIR,'')

			--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
			IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
			--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

			BEGIN
				SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
				WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			END


			SET @S=RTRIM(@S) + '_' + @L
			SET @L='000000'

		END
		---------------*******************************************************************

	IF  LEFT(@TXT,3)='UTI' AND @TP=0 and (@Ubi='UUTI' or @Ubi='UUTIDEK') --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--UTI_P1210-1115-1-10_Q4_V000535
	BEGIN
		SET @TP=19
		
		DECLARE @Usuario nvarchar(50),
		@Referencia nvarchar(20),
		@ReferenciaDescripcion nvarchar(60),
		@Proveedor nvarchar(20),
		@Cantidad int,
		@Ubicacion nvarchar(600),
		@FechaSolicitud nvarchar(10),
		@FechaPrevista nvarchar(10),
		@Precio decimal(14,6),
		@Url nvarchar(1),
		@Resultado int
		
		DECLARE @SelectedValue int=0
		DECLARE @SelectedValueTXT VARCHAR(50)



		SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,CHARINDEX('_U', @txt, 1)-CHARINDEX('_V', @txt, 1)-2)
		SET @Ubicacion = SUBSTRING(@txt, CHARINDEX('_U', @txt, 1)+2,LEN(@TXT))

		SET @Cli =isnull(@Cli,'')
		SET @Ubicacion =isnull(@Ubicacion,'')

		SET @Cli = SUBSTRIng(@Cli ,1,6)


		set @URLPlano='1'--'http://srvsps2007/Procesos/UtilesPDF/' + @A + '.pdf'

		SET XACT_ABORT ON  
	   
		SELECT @Usuario=[Usu_Dominio]
		FROM [dbo].[sga_usuario]
		where [username]=@Usu

		SET @Usuario =isnull(@Usuario,'orkliord\gm')
		
		DECLARE @UbicacionDes nchar(100)
		
		SELECT @UbicacionDes=[descripcion]
		FROM [EKANBAN].[dbo].[ubicacion]
		WHERE   [id_ubicacion]=@Ubi
		
		SET @UbicacionDes =isnull(@UbicacionDes,'')


		SET @Referencia = @A  --N'1331-0111-0-11'
		SET @ReferenciaDescripcion = ''
		SET @Proveedor = @Cli
		SET @Cantidad = @CTD
		SET @Ubicacion = 'Sekzioa: ' + rtrim(LTRIM( @UbicacionDes)) + ' (' + @Ubicacion + ') Erreferentziak LASER bidez markatu.' 
		SET @FechaSolicitud = null --para que coja el de hoy
		SET @FechaPrevista = null --si es nulo, añade 21 días. 
		SET @Precio = 1 --no se utiliza para nada
		SET @Url = 1 --1 edo 0





		SELECT        @Referencia= ITEMID
		FROM             [PREaxsql].[SQLAXBAK].dbo.INVENTTABLE
		WHERE        (DATAAREAID = N'ork') AND (ITEMID = @A)

		set @Referencia=isnull(@Referencia,'') 

		if @Referencia=''
		begin
			SELECT     @Referencia=   ITEMID
			FROM             [PREaxsql].[SQLAXBAK].dbo.CUSTVENDEXTERNALITEM
			WHERE        (DATAAREAID = N'ORK') AND (EXTERNALITEMID = @A) AND (CUSTVENDRELATION = @Proveedor)
		end

		set @Referencia=isnull(@Referencia,'') 


		


		--EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
		--	'orkliord\aauzmendi',
		--	'1331-0111-0-11',
		--	'',
		--	'000535 MEKALAN',
		--	1,
		--	'UUTI',
		--	null,
		--	null,
		--	1,
		--	1,
		--   @Resultado OUTPUT

		EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
			@Usuario,
			@Referencia,
			@ReferenciaDescripcion,
			@Proveedor,
			@Cantidad,
			@Ubicacion,
			@FechaSolicitud,
			@FechaPrevista,
			@Precio,
			@Url,
		   @Resultado OUTPUT

		 SET @SelectedValueTXT=CASE @Resultado 
					WHEN 0 THEN 'Error dentro del servicio. Tendréis que comunicármelo para que revise que a poder pasar.'
					WHEN 1 THEN 'El pedido se ha generado. Otra cosa que se haya quedado parado, se haya un producido un problema al volcar al AX o haya todo correctamente. Estas situaciones ya se revisaran dentro de la aplicación.'
					WHEN 2 THEN 'Fecha solicitud incorrecta.  La fecha tiene un formato incorrecto o es anterior al día de hoy.'
					WHEN 3 THEN 'Fecha prevista incorrecta.  La fecha tiene un formato incorrecto'
					WHEN 4 THEN 'Fecha Prevista menor que la Fecha Solicitud.'
					WHEN 5 THEN 'Usuario incorrecto. El usuario que se pasa (usuario de red) es incorrecto o no tiene permisos para Compras.'
					WHEN 6 THEN 'Empresa incorrecta. La empresa del usuario no es correcta (No debería pasar que se valida por si acaso).'
					WHEN 7 THEN 'Sección incorrecta. Se valida que la sección del usuario es correcta (No debería pasar que se valida por si acaso).'
					WHEN 8 THEN 'Almacén incorrecto. Se valida que el almacén que se obtiene en base a los datos de la sección del usuario es correcto en base a la empresa en que estamos tratando (No debería pasar que se valida por si acaso).'
					WHEN 9 THEN 'Referencia incorrecta. El código de la referencia no existe en la empresa que estamos tratando.'
					WHEN 10 THEN 'Proveedor incorrecto. El código de proveedor introducido no existe para la empresa que estamos tratando.'
					WHEN 11 THEN 'Precio no encontrado. No hay en el AX un precio definido para ese artículo y proveedor.'
				END
		


		--REGISTRAMOS EL PEDIDO EN CREAR PEDIDOS, PERO YA TRATADO CON ESTADO 99. PARA SEGUIMIENTO
		INSERT INTO [PREaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
		(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra, DocumentoAdjunto,  Observaciones)
		VALUES
		('99',    @Referencia,        'UTILES', substring(@Cli,1,6),  @Usu, @A,   @CTD,      GETDATE()+7,           @Usu,   GETDATE(),   '0',    'PRO', '3',                       '', 0, '','', 0,0,@SelectedValue,  @Ubicacion + @SelectedValueTXT)
		
		IF @Resultado=1--1
		BEGIN 
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1)
			ORDER BY num_kanban
		END

		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END

	---------------*******************************************************************

	IF   @Ubi='URFIdD' --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--pruebas de EMBEBLUE
	BEGIN
		SET @TP=20

		--SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		--SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		--SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,LEN(@TXT))
			
		
		SET @cade=@Usu + ': ' + @Txt
		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		--IF @cade1<>''
		--BEGIN
		--	EXEC	[dbo].[PEnvCorreo]
		--	@Des = 'test_910@embeblue.com; auxiliar@orkli.es; iker@orkli.es',
		--	@Tem = N'Lectura de Etiqueta RFID EMBEBLUE',
		--	@Cue =@cade
		--END

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END
	
		
--*************************************************************************************************************************************************


	if @txt like '%@%' 
	begin
		SET @TP=14
		DECLARE @Notif bit=0

		SELECT       @Notif= Ubi_NotEtiVacias
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)



		set @cade = 'Ubi: ' + @Ubi + ', Texto: ' + @txt +' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '

		if @Notif=1 --@Ubi<>'UCAE'
			begin
			EXEC	[dbo].[PEnvCorreo]
					@Des = N'alert@ekanban.es;',
					@Tem = N'Lectura de Etiquetas Vacías por RFID',
					@Cue = @cade
			end
	end



	
--*************************************************************************************************************************************************
REGISTROLECTURAS:

print 'tregistroLecturas';

INSERT INTO [dbo].[TRegistroLecturas] 
           ([Reg_Fec]
           ,[Reg_Dis]
           ,[Reg_Usu]
           ,[Reg_Ubi]
           ,[Reg_Pan]
           ,[Reg_Txt]
           ,[Reg_Cir]
           ,[Reg_Cli]
           ,[Reg_Art]
           ,[Reg_Lot]
           ,[Reg_Ser]
		   ,[Reg_Ant]
		   ,[Reg_Sen]
		   ,[Reg_Tip]
		   ,[Reg_Ctd]) 
     VALUES
           (getdate()
           ,@Disp
           ,@Usu
		   ,@Ubi
           ,@Pant
           ,@Txt
           ,@CIR
           ,@Cli
           ,@A
           ,@L
           ,@s
		   ,@Ant
		   ,@Sen
		   ,@TP
		   ,@CTD)


	
	--select @L1,  @L2, @L,  @S1,  @S2, @S, @A1,  @A2, @A
	select @L AS LOT,  @S AS SER, @A AS ART, 0 as CTD, '000000' AS PRO, @Cli as CLI,  @L + '_' + @S AS GRAB, @CTD AS CTDLEIDA, @PED AS IDPED
	
	-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @CIR, @Origen = @OrigenLectura, @Usuario = @Usu
	-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES

	commit;
	print getDate();
	SET XACT_ABORT OFF;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
		



END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaPendientes]...';


GO


ALTER PROCEDURE [dbo].[PCtaPendientes] 
(@USU varchar(255)='', @Dis int=0, @CLI varchar(255)='%', @FILAS INT=10,  @PAG INT=1, @OrBy varchar(50)='', @OrDi varchar(1)='1', @Bus varchar(75)='')
AS
BEGIN
	SET NOCOUNT ON;
	
	CREATE TABLE #ClientesCan ( COD VARCHAR(255))
	CREATE TABLE #ProveedoresCan ( COD VARCHAR(255))

	DECLARE @DE INT
	DECLARE @HA INT
--	DECLARE @COND VARCHAR(255)
	DECLARE @CadOrd varchar (50)

	SET @DE=(@PAG-1)*@FILAS+1
	SET @HA=@DE+@FILAS-1

	IF @CLI='ALL' 	SET @CLI='%'

	SET @CadOrd= @OrBy + @OrDi
	SET @Bus='%' + ltrim(rtrim(@Bus)) + '%'

	IF @CadOrd='00' SET @CadOrd='CUSTACCOUNT0' -- PARA PRUEBAS, PARA PODER TENER RESULTADOS


	INSERT INTO [dbo].[#ClientesCan] (COD)
     (
		-- Clientes según los circuitos kanban
		SELECT      distinct  dbo.ubicacion.localizacion
		FROM        dbo.usuario_ubicacion INNER JOIN
					dbo.sga_usuario ON dbo.usuario_ubicacion.id_usuario = dbo.sga_usuario.cod_usuario INNER JOIN
					dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion
		WHERE       (dbo.sga_usuario.username = @USU) AND (dbo.ubicacion.activo = 1) AND (dbo.sga_usuario.activo = 1)
					AND (dbo.ubicacion.localizacion <> '00000000')
	
		UNION

		-- Clientes a los que este usuario en concreto tiene acceso
		SELECT       distinct dbo.usuario_cliente.id_cliente
		FROM         dbo.usuario_cliente INNER JOIN
					 dbo.sga_usuario ON dbo.usuario_cliente.id_usuario = dbo.sga_usuario.cod_usuario
		WHERE        (dbo.sga_usuario.username = @USU)
	 );


	 

	INSERT INTO [dbo].[#ProveedoresCan] (COD)
     (
		-- Proveedores según los circuitos kanban
		--SELECT      distinct  dbo.ubicacion.localizacion
		--FROM        dbo.usuario_ubicacion INNER JOIN
		--			dbo.sga_usuario ON dbo.usuario_ubicacion.id_usuario = dbo.sga_usuario.cod_usuario INNER JOIN
		--			dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion
		--WHERE       (dbo.sga_usuario.username = @USU) AND (dbo.ubicacion.activo = 1) AND (dbo.sga_usuario.activo = 1)
	
		--UNION

		-- Proveedores a los que este usuario en concreto tiene acceso
		SELECT    distinct    dbo.usuario_proveedor.id_proveedor
		FROM      dbo.sga_usuario INNER JOIN
				  dbo.usuario_proveedor ON dbo.sga_usuario.cod_usuario = dbo.usuario_proveedor.id_usuario
		WHERE     (dbo.sga_usuario.username = @USU)
	 );




WITH CTAPTES AS
(
	 
	SELECT        TOP (100) PERCENT  SALESLINE.CUSTACCOUNT AS CO, CUSTTABLE.NAMEALIAS, SALESLINE.SHIPPINGDATEREQUESTED, SALESLINE.SHIPPINGDATERECONFIRMED, SALESLINE.EXTERNALITEMID, 
	SALESLINE.ITEMID, SALESLINE.PURCHORDERFORMNUM, SALESLINE.SALESID,
	  CAST( SALESLINE.LINENUM AS decimal(10,0)) as LINENUM,
	 SALESLINE.QTYORDERED, SALESLINE.REMAINSALESPHYSICAL, 3 AS T
	,
	ROW_NUMBER() OVER (ORDER BY 
	CASE @CadOrd WHEN 'CUSTACCOUNT0' THEN SALESLINE.CUSTACCOUNT END ASC,
	CASE @CadOrd WHEN 'CUSTACCOUNT1' THEN SALESLINE.CUSTACCOUNT END DESC,
	CASE @CadOrd WHEN 'EXTERNALITEMID0' THEN SALESLINE.EXTERNALITEMID END ASC,
	CASE @CadOrd WHEN 'EXTERNALITEMID1' THEN SALESLINE.EXTERNALITEMID END DESC,
	CASE @CadOrd WHEN 'ITEMID0' THEN SALESLINE.ITEMID END ASC,
	CASE @CadOrd WHEN 'ITEMID1' THEN SALESLINE.ITEMID END DESC,
	CASE @CadOrd WHEN 'PACKINGSLIPID0' THEN SALESLINE.SHIPPINGDATERECONFIRMED END ASC,--SHIPPINGDATERECONFIRMED0
	CASE @CadOrd WHEN 'PACKINGSLIPID1' THEN SALESLINE.SHIPPINGDATERECONFIRMED END DESC,
	CASE @CadOrd WHEN 'PURCHASEORDER0' THEN SALESLINE.PURCHORDERFORMNUM END ASC,--PURCHORDERFORMNUM
	CASE @CadOrd WHEN 'PURCHASEORDER1' THEN SALESLINE.PURCHORDERFORMNUM END DESC
	) AS RowNumber  --ORDER BY CASE @RR WHEN 1 THEN cliente_id END DESC, CASE WHEN @RR = 0 THEN UBICACION END
	, 
    (


	CASE WHEN GETDATE() > DATEADD(day, 1, SALESLINE.SHIPPINGDATERECONFIRMED) AND year(SALESLINE.SHIPPINGDATERECONFIRMED) > 2000 THEN 2 ELSE 
	CASE WHEN SALESLINE.SHIPPINGDATEREQUESTED >= SALESLINE.SHIPPINGDATERECONFIRMED THEN 1 ELSE 
	--CASE WHEN SALESLINE.SHIPPINGDATEREQUESTED < SALESLINE.SHIPPINGDATERECONFIRMED THEN 0 ELSE 
	0 
	END 
	END 
	--END
	) AS Color


	FROM            PREAXSQL.SQLAXBAK.dbo.SALESLINE AS SALESLINE INNER JOIN
	PREAXSQL.SQLAXBAK.dbo.CUSTTABLE AS CUSTTABLE ON SALESLINE.DATAAREAID = CUSTTABLE.DATAAREAID AND SALESLINE.CUSTACCOUNT = CUSTTABLE.ACCOUNTNUM
	
	WHERE     (SALESLINE.SALESSTATUS = 1) AND (SALESLINE.REMAINSALESPHYSICAL > 0) AND (SALESLINE.DATAAREAID = N'ork') AND (SALESLINE.ITEMID <> N'PORTES') 
	and  SALESLINE.salestype<>0
	AND    (SALESLINE.CUSTACCOUNT IN (SELECT COD FROM dbo.[#ClientesCan])) AND (SALESLINE.CUSTACCOUNT LIKE @CLI + '%')
		and 
	(
	SALESLINE.EXTERNALITEMID like @Bus
	or
	SALESLINE.ITEMID like @Bus
	or
	SALESLINE.PURCHORDERFORMNUM like @Bus
	or
	SALESLINE.SALESID like @Bus
	)


	--COMO SI FUESEN DE PROVEEDOR
	UNION

	SELECT        PURCHTABLE_1.INVOICEACCOUNT AS CO, PURCHTABLE_1.PURCHNAME, PURCHLINE_1.CREATEDDATETIME AS FEC, PURCHLINE_1.DELIVERYDATE AS ENT, PURCHLINE_1.EXTERNALITEMID, 
                         PURCHLINE_1.ITEMID, PURCHTABLE_1.VENDORREF, PURCHLINE_1.PURCHID AS PED,

						 	  CAST( PURCHLINE_1.LINENUM AS decimal(10,0)) as LIN,
						  PURCHLINE_1.QTYORDERED, PURCHLINE_1.REMAINPURCHPHYSICAL AS CTD, 1 AS T

				, 
				ROW_NUMBER() OVER (ORDER BY 
				CASE @CadOrd WHEN 'CUSTACCOUNT0' THEN PURCHTABLE_1.INVOICEACCOUNT END ASC,
				CASE @CadOrd WHEN 'CUSTACCOUNT1' THEN PURCHTABLE_1.INVOICEACCOUNT END DESC,
				CASE @CadOrd WHEN 'EXTERNALITEMID0' THEN PURCHLINE_1.EXTERNALITEMID END ASC,
				CASE @CadOrd WHEN 'EXTERNALITEMID1' THEN PURCHLINE_1.EXTERNALITEMID END DESC,
				CASE @CadOrd WHEN 'ITEMID0' THEN  PURCHLINE_1.ITEMID END ASC,
				CASE @CadOrd WHEN 'ITEMID1' THEN  PURCHLINE_1.ITEMID END DESC,
				CASE @CadOrd WHEN 'SHIPPINGDATERECONFIRMED0' THEN PURCHLINE_1.DELIVERYDATE END ASC,
				CASE @CadOrd WHEN 'SHIPPINGDATERECONFIRMED1' THEN PURCHLINE_1.DELIVERYDATE END DESC,
				CASE @CadOrd WHEN 'PURCHORDERFORMNUM0' THEN  PURCHTABLE_1.VENDORREF END ASC,
				CASE @CadOrd WHEN 'PURCHORDERFORMNUM1' THEN  PURCHTABLE_1.VENDORREF END DESC
				) AS RowNumber , 0 as Color



				FROM  PREAXSQL.SQLAXBAK.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
										 PREAXSQL.SQLAXBAK.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
										 dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
				WHERE (PURCHLINE_1.PURCHSTATUS = 1) 
				AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) 
				AND (PURCHLINE_1.DATAAREAID = N'ork') 
				AND (PURCHTABLE_1.PURCHASETYPE = 3)
			
				--AND (PURCHTABLE_1.VENDORREF LIKE N'U%') 
				AND (dbo.circuito.Proveedor_id IN (SELECT COD FROM dbo.[#ProveedoresCan]))
				AND (dbo.circuito.Proveedor_id LIKE @CLI + '%')
		and 
	(
	PURCHLINE_1.EXTERNALITEMID like @Bus
	or
	PURCHLINE_1.ITEMID like @Bus
	or
	PURCHTABLE_1.VENDORREF like @Bus
	or
	PURCHLINE_1.PURCHID like @Bus
	)


)
	
	SELECT *  FROM CTAPTES WHERE ROWNUMBER BETWEEN @DE AND @HA AND CO LIKE @CLI + '%' order by T, RowNumber
	
	DROP TABLE #ClientesCan
	DROP TABLE [#ProveedoresCan]


		

	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = '',
		@Reg_opt = N'Cta Pedidos Ptes',
		@Reg_Art = '',
		@Reg_cli = @CLI,
		@Reg_Txt = ''

END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAjustarStock]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAccAjustarStock]
	-- Add the parameters for the stored procedure here
	@CodCircuito varchar(20) = '',
	@NumTarjVerdes integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)=''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @codRespuesta int; declare @resultado varchar(255);
	
	declare @Tresultado table (Resul  varchar(255), MsgTxt  varchar(255));
	
	declare @result table (art varchar(255), artcli varchar(255), t int, r  int, a  int, ae  int, v  int, ss  int,
				 estado int, circuito varchar(255), ultLec datetime, Cont  int, Circ  int, opt int, transito_Activado bit, ctd_Kan int);	
	
    -- Insert statements for procedure here
	INSERT INTO @Tresultado EXEC PAccAjustar @CodCircuito, @NumTarjVerdes, @Usu, @Dis;
	
	select @codRespuesta= Resul, @resultado = MsgTxt from @Tresultado;
	--if @codRespuesta = 0
	--begin
	insert into @result exec PAX_SitTarjetas @codCircuito, @Usu;
	select *, @codRespuesta as Result, @resultado as Msg from @result;
	--end	
	--else
	--begin
		--select * from @Tresultado;
	--end
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @CodCircuito, @Origen = 'AjusteStock', @Usuario = @Usu
	-----20181008 FIN INCLUSION DE TABLA HISTORICOVERDES
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_CtaLeerFicha]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAX_CtaLeerFicha] 
	-- Add the parameters for the stored procedure here
	@codQR varchar(255) = '',
	@ref varchar(255)= '',
	@usuarioID varchar(255) = '',
	@ubicacionID varchar(255) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @REFMODIFICADA VARCHAR(255);
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @REFID VARCHAR(255) = NULL;
	DECLARE @CLIENTEID VARCHAR(255) = NULL;
	declare @circuitoID varchar(255);
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES
	DECLARE @Origen as varchar(25);
	
	SET @Origen = 'LecturaQR'
	-----20181008 FIN DE TABLA HISTORICOVERDES
	
	
	DECLARE @TRESULT TABLE (OFAB VARCHAR(255), ARTICULO VARCHAR(255), ARTICULO_CLIENTE VARCHAR(255), DESCRIP VARCHAR(255), FPROD DATE,
		LINEA VARCHAR(255), ENG VARCHAR(255), DESENG VARCHAR(255), CTDCAJA VARCHAR(255), TENG VARCHAR(255), TDES VARCHAR(255), TCTDCAJA VARCHAR(255));
    -- Insert statements for procedure here
   declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
		PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));
    if @codQR IS NULL OR @codQR = ''
		BEGIN
			--referencia metida manualmente
			SET @REFMODIFICADA = 'P' + 	@referencia;
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @REFMODIFICADA, @usuarioID, @ubicacionID, 1, 'M', ' ', ' ', @Origen
				-----20181008 ORIGEN INCLUIDO PARA LAE TABLA HISTORICOVERDES
			select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
			IF @REFERENCIA IS NOT NULL AND @REFERENCIA <> ''  
				BEGIN
					print @REFERENCIA;
					select @circuitoID = id_circuito from circuito where ubicacion = @ubicacionID and activo = 1 and referencia_id = @REFERENCIA
					INSERT INTO @TRESULT
						EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
					print 'ok';
					--return 0;
				END
			else
				begin
					--referencia en  blanco
					--select @REFERENCIA = referencia_id, @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1;
					--set @REFERENCIA = '';
					--set @circuitoID = '';
					if @ref is null or @ref = ''
					begin
						select @REFERENCIA = referencia_id, @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1;
					end
					else
					begin
						set @REFERENCIA = @ref;
						IF @circuitoID = '' OR @circuitoID IS NULL
						BEGIN
							select @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1 AND referencia_id= @REF;
						END
						IF @circuitoID = '' OR @circuitoID IS NULL
						BEGIN
							-- LA REFERENCIA INTRODUCIDA ES LA REF DEL CLIENTE
							select @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1 AND ref_cliente= @REF;
						END
					end
					INSERT INTO @TRESULT
						EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
					--return 11;
				end
		END
    ELSE
		BEGIN
			--FICHA DESDE EL CODIGO QR
			--print 'Lectura con QR'
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @codQR, @usuarioID, @ubicacionID, 1, 'M', ' ', ' ', @Origen
			select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
			select @circuitoID = id_circuito from circuito where ubicacion = @ubicacionID and activo = 1 and referencia_id = @REFERENCIA
			print @TEXTO
			IF @TEXTO <> '_'
			BEGIN
				IF @OF <> '000000'
				BEGIN
					--PRINT 'OF NO ES 000000'
					SELECT @REFID = ITEMID, @CLIENTEID = CUSTACCOUNT FROM VAX_EKAN_OF WHERE PRODID= @OF
					IF @REFID IS NULL AND @CLIENTEID IS NULL
					BEGIN
						RETURN 6 --orden de fabricación inexisistente
					END
					
				END	
				else
				begin
					-- OF es 000000
					--PRINT 'OF ES 000000'
					--print @REFERENCIA 
					INSERT INTO @TRESULT
					EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
				end
				PRINT @REFID;
				PRINT @CLIENTEID;
				IF @REFID IS NOT NULL AND @CLIENTEID IS NOT NULL
				BEGIN
					INSERT INTO @TRESULT
					EXEC [dbo].PAX_CtaEspecifaciones @OF, @REFERENCIA, @usuarioID, @ubicacionID	
				END
			END
			ELSE
			BEGIN
				RETURN 6;
			END
		END
		PRINT @circuitoID;
		SELECT *, @circuitoID as circuito FROM @TRESULT;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAnularLectura]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[PAccAnularLectura]
	-- Add the parameters for the stored procedure here
	@codQR varchar(255) = '',
	@idUbi varchar(255) = '',
	@idUsu varchar(255) = '',
	@Pant int = 0,
	@Disp varchar(255) = 'M',
	@Ant varchar(50) = ' ',
	@Sen varchar(5) = ' '
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @RESULT AS INT = -1;
	DECLARE @MsgTxt AS VARCHAR(255) = 'OK';
	declare @idCircuito as varchar(255);
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES
	DECLARE @Origen as varchar(25);
	
	SET @Origen = 'LecturaAnulada';
	-----20181008 FIN DE TABLA HISTORICOVERDES

 ----   -- Ejecutamos el proc PAX_PatronLec: con este procedimiento obtenemos el registro de la lectura
	declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
		PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));

	insert into @TTarjeta
		EXEC	[dbo].[PAX_PatronLec] @codQR, @idUsu, @idUbi, @Pant, @Disp, @Ant, @Sen, @Origen
		-----20181008 INCLUSION ORIGEN PARA LA TABLA HISTORICOVERDES 
	
	select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
		, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
	print @TEXTO;
	IF @TEXTO <> '_'
	BEGIN
		IF EXISTS (SELECT *
					FROM tarjeta T
					WHERE T.id_lectura = @TEXTO AND  T.sentido = 'S')
			BEGIN
				print 'eliminar tarjeta';
				select @idCircuito = id_circuito from tarjeta WHERE id_lectura = @TEXTO AND sentido = 'S'; 
				DELETE FROM tarjeta WHERE id_lectura = @TEXTO AND sentido = 'S';
				SET @RESULT = 0;
				
			END
		
	END
	ELSE
	BEGIN
		print '@TEXTO = _ ';
	END 
	
	-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @IDCIRCUITO, @Origen = @Origen, @Usuario = @idUsu
	-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES
	--SELECT @RESULT AS RESULTADO
	select  @idCircuito as idCircuito, @RESULT as Result, @MsgTxt as Msg		
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PCtaEnviados]...';


GO


ALTER PROCEDURE [dbo].[PCtaEnviados] 
(@USU varchar(255)='', @Dis int=0, @CLI varchar(255)='%', @FILAS INT=10,  @PAG INT=1, @OrBy varchar(50)='1', @OrDi varchar(1)='1', @Bus varchar(75)='')
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @FacCon bit

	SELECT        @FacCon=Usu_FacConsig
	FROM            dbo.sga_usuario
	WHERE        (username = @USU)

	set @FacCon=ISNULL(@FacCon,'')

	CREATE TABLE #ClientesCan2 ( COD VARCHAR(255), TIP BIT, MSG VARCHAR(150))

	DECLARE @DE INT
	DECLARE @HA INT
	--DECLARE @COND VARCHAR(255)
	DECLARE @CadOrd varchar (50)

	SET @DE=(@PAG-1)*@FILAS+1
	SET @HA=@DE+@FILAS-1

	IF @CLI='ALL' 	SET @CLI='%'



	SET @CadOrd= @OrBy + @OrDi
	SET @Bus='%' + ltrim(rtrim(@Bus)) + '%'

	IF @CadOrd='00' SET @CadOrd='INVOICEACCOUNT0' -- PARA PRUEBAS, PARA PODER TENER RESULTADOS


	INSERT INTO [dbo].[#ClientesCan2] (COD, TIP, MSG)
     (
		-- Clientes según los circuitos kanban
		SELECT      distinct dbo.ubicacion.localizacion, dbo.ubicacion.Ubi_MosPenFac, isnull(dbo.ubicacion.Ubi_MsgPenFac,'')
		FROM        dbo.usuario_ubicacion INNER JOIN
					dbo.sga_usuario ON dbo.usuario_ubicacion.id_usuario = dbo.sga_usuario.cod_usuario INNER JOIN
					dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion
		WHERE       (dbo.sga_usuario.username = @USU) AND (dbo.ubicacion.activo = 1) AND (dbo.sga_usuario.activo = 1) 
					AND (dbo.ubicacion.localizacion <> '00000000')
	
		UNION

		-- Clientes a los que este usuario en concreto tiene acceso
		SELECT      distinct  dbo.usuario_cliente.id_cliente, 0, ''
		FROM        dbo.usuario_cliente INNER JOIN
					dbo.sga_usuario ON dbo.usuario_cliente.id_usuario = dbo.sga_usuario.cod_usuario
		WHERE       (dbo.sga_usuario.username = @USU)
	 );

WITH CTAENVIADOS AS
(
	SELECT        TOP (100) PERCENT CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPJOUR.DELIVERYNAME, CUSTPACKINGSLIPJOUR.PACKINGSLIPID, CUSTPACKINGSLIPTRANS.EXTERNALITEMID, 
	CUSTPACKINGSLIPTRANS.ITEMID, CUSTPACKINGSLIPJOUR.PURCHASEORDER, 
	--CUSTPACKINGSLIPTRANS.LINENUM, 
	
 CAST(CUSTPACKINGSLIPTRANS.LINENUM AS decimal(10,0)) as LINENUM,
	
	
	- ISNULL(INVENTTRANS.QTY, - CUSTPACKINGSLIPTRANS.QTY) AS QTY,   	--CUSTPACKINGSLIPTRANS.QTY, cambiado el 24/2 para que muestre la ctd según lote
	INVENTDIM.INVENTBATCHID, 
	DATEADD(MINUTE, DATEDIFF(MINUTE, GETUTCDATE(), GETDATE()), CUSTPACKINGSLIPTRANS.CREATEDDATETIME) as DELIVERYDATE, 

	ROW_NUMBER() OVER (ORDER BY 
CASE @CadOrd WHEN 'INVOICEACCOUNT0' THEN CUSTPACKINGSLIPJOUR.INVOICEACCOUNT END ASC,
CASE @CadOrd WHEN 'INVOICEACCOUNT1' THEN CUSTPACKINGSLIPJOUR.INVOICEACCOUNT END DESC,
CASE @CadOrd WHEN 'EXTERNALITEMID0' THEN CUSTPACKINGSLIPTRANS.EXTERNALITEMID END ASC,
CASE @CadOrd WHEN 'EXTERNALITEMID1' THEN CUSTPACKINGSLIPTRANS.EXTERNALITEMID END DESC,
CASE @CadOrd WHEN 'ITEMID0' THEN CUSTPACKINGSLIPTRANS.ITEMID END ASC,
CASE @CadOrd WHEN 'ITEMID1' THEN CUSTPACKINGSLIPTRANS.ITEMID END DESC,
CASE @CadOrd WHEN 'PACKINGSLIPID0' THEN CUSTPACKINGSLIPJOUR.PACKINGSLIPID END ASC,
CASE @CadOrd WHEN 'PACKINGSLIPID1' THEN CUSTPACKINGSLIPJOUR.PACKINGSLIPID END DESC,
CASE @CadOrd WHEN 'PURCHASEORDER0' THEN CUSTPACKINGSLIPJOUR.PURCHASEORDER END ASC,
CASE @CadOrd WHEN 'PURCHASEORDER1' THEN CUSTPACKINGSLIPJOUR.PURCHASEORDER END DESC,
CASE @CadOrd WHEN 'DELIVERYDATE0' THEN CUSTPACKINGSLIPTRANS.CREATEDDATETIME END ASC,
CASE @CadOrd WHEN 'DELIVERYDATE1' THEN CUSTPACKINGSLIPTRANS.CREATEDDATETIME END DESC
) AS RowNumber, CUSTPACKINGSLIPTRANS.RECID AS RECID, 
CASE WHEN CUSTPACKINGSLIPTRANS.INK_FACTURADOCONSIGNA = 1 THEN 0 ELSE 1 END AS COLOR 

	FROM            PREAXSQL.SQLAXBAK.dbo.INVENTDIM AS INVENTDIM RIGHT OUTER JOIN
	PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
	PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
	CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
	PREAXSQL.SQLAXBAK.dbo.INVENTTRANS AS INVENTTRANS ON CUSTPACKINGSLIPTRANS.INVENTTRANSID = INVENTTRANS.INVENTTRANSID AND 
	CUSTPACKINGSLIPTRANS.DATAAREAID = INVENTTRANS.DATAAREAID LEFT OUTER JOIN
	PREAXSQL.SQLAXBAK.dbo.AOCARRIERTABLE AS AOCARRIERTABLE ON CUSTPACKINGSLIPJOUR.CARRIERCODEREF = AOCARRIERTABLE.CARRIERCODE AND 
	CUSTPACKINGSLIPJOUR.DATAAREAID = AOCARRIERTABLE.DATAAREAID LEFT OUTER JOIN
	PREAXSQL.SQLAXBAK.dbo.DLVMODE AS DLVMODE ON CUSTPACKINGSLIPJOUR.DLVMODE = DLVMODE.CODE AND CUSTPACKINGSLIPJOUR.DATAAREAID = DLVMODE.DATAAREAID ON 
	INVENTDIM.DATAAREAID = INVENTTRANS.DATAAREAID AND INVENTDIM.INVENTDIMID = INVENTTRANS.INVENTDIMID
	
	
	WHERE        (CUSTPACKINGSLIPJOUR.ORDERACCOUNT IN (SELECT COD FROM #ClientesCan2)) 
	AND (CUSTPACKINGSLIPJOUR.ORDERACCOUNT LIKE @CLI + '%') --INVOICEACCOUNT
	AND CUSTPACKINGSLIPTRANS.QTY> 0 and (CUSTPACKINGSLIPTRANS.DATAAREAID = N'ork') 
	AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, - 6, GETDATE()))
	and 
	(
	CUSTPACKINGSLIPJOUR.PACKINGSLIPID like @Bus
	or
	CUSTPACKINGSLIPTRANS.EXTERNALITEMID like @Bus
	or
	CUSTPACKINGSLIPTRANS.ITEMID like @Bus
	or
	CUSTPACKINGSLIPJOUR.PURCHASEORDER like @Bus
	)
)

		--kentzeko
		--SELECT * FROM CTAENVIADOS WHERE ROWNUMBER BETWEEN @DE AND @HA 

	SELECT  ORDERACCOUNT as INVOICEACCOUNT, DELIVERYNAME, PACKINGSLIPID, EXTERNALITEMID, ITEMID, PURCHASEORDER, LINENUM, QTY, isnull(INVENTBATCHID,'') as INVENTBATCHID, DELIVERYDATE, RowNumber, 
	RECID, TIP, CASE WHEN COLOR=0 or @FacCon=0 THEN 0 ELSE 1 END AS LINK, COLOR, MSG as TXTVAL
	
	FROM CTAENVIADOS inner join  #ClientesCan2 on CTAENVIADOS.ORDERACCOUNT=#ClientesCan2.COD
	WHERE ROWNUMBER BETWEEN @DE AND @HA AND CTAENVIADOS.ORDERACCOUNT LIKE @CLI  --CTAENVIADOS.ORDERACCOUNT
	order by RowNumber
	
	DROP TABLE #ClientesCan2

	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = '',
		@Reg_opt = N'Cta Albaranes Enviados',
		@Reg_Art = '',
		@Reg_cli = @CLI,
		@Reg_Txt = ''

END




/*
--PARA MARCAR TODOS LOS ENVÍOS A UN CLIENTE COMO FACTURADOS


UPDATE CUSTPACKINGSLIPTRANS_1
SET CUSTPACKINGSLIPTRANS_1.INK_FACTURADOCONSIGNA = 1
FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS_1 INNER JOIN
                         SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS_1.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
                         CUSTPACKINGSLIPTRANS_1.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID
WHERE        (CUSTPACKINGSLIPJOUR.INVOICEACCOUNT = N'02420300')



--PARA CAMBIAR EL ESTADO A UN ALBARÁN

UPDATE CUSTPACKINGSLIPTRANS_1
SET CUSTPACKINGSLIPTRANS_1.INK_FACTURADOCONSIGNA = 1
FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS_1 
WHERE          (PACKINGSLIPID = N'AL00137460')



*/
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccRegLectura]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAccRegLectura]
	-- Add the parameters for the stored procedure here
	@tagID as varchar(255),
	@imei as varchar(255),
	@idUbi as varchar(255),
	@idUsu as varchar(255),
	@antena as varchar(255) = '',
	@numSerie as varchar(255) = '',
	@sentido as varchar(255) = '',
	@tipoLectura as varchar(5) = '',
	@subUbicacion as varchar(255) = NULL,
	@Pant as int = 0
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	 
	--BEGIN TRY
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMEROSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @IDREFERENCIA VARCHAR(255) = '';
	DECLARE @IDCLIENTE VARCHAR(255)='';
	DECLARE @IDCIRCUITO VARCHAR(255) = '';
	DECLARE @IDLECTURA VARCHAR(255) = '';
	DECLARE @RESULT AS INT = -1;
	DECLARE @MsgTxt AS VARCHAR(255) = 'OK';
	DECLARE @SENTIDOLECTURA VARCHAR(5) = '';
	DECLARE @TIPOAVISO INT = 0;
	DECLARE @MSGAVISO VARCHAR(255)='';
	
	
	
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
								
	if @subUbicacion = ''
	begin
		set @subUbicacion = NULL;
	end
	
     --LECTURAS DE MOVIL
	IF @tipoLectura = 'M'
	BEGIN
		IF @tagID <> '' AND @idUbi <> '' AND @idUsu <> ''
		BEGIN	
			-- Ejecutamos el proc PAX_PatronLec: con este procedimiento obtenemos el registro de la lectura
			--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
			begin  tran t1;
			
				-----20181008 INCLUSION DE TABLA HISTORICOVERDES
				DECLARE @Origen as varchar(25);
	
				SET @Origen = 'RegistrarLectura'
				-----20181008 FIN DE TABLA HISTORICOVERDES
	
			declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
				PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));
			
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @tagID, @idUsu, @idUbi, @Pant, 'M', ' ', ' ', @Origen
				-----20181008 INCLUSION DE ORIGEN PARA LA TABLA HISTORICOVERDES
			select TOP(1) @OF = lot, @NUMEROSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
				, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta 
			COMMIT TRANSACTION t1;
			
			print @texto;
			
			IF @OF ='000000'
				BEGIN 
					print 'of desde patronLec';
					SET @IDREFERENCIA = @REFERENCIA;
					SET @IDCLIENTE = @CLIENTE;
				END
			ELSE
				BEGIN
					SELECT TOP(1) @IDREFERENCIA = V_OF.ITEMID, @IDCLIENTE = V_OF.CUSTACCOUNT  FROM [dbo].VAX_EKAN_OF  AS V_OF WITH (NOLOCK) WHERE V_OF.PRODID = @OF
				END
						
						
			
			IF @TEXTO <> '_' --
			BEGIN
				IF NOT EXISTS(SELECT 1 
					FROM [dbo].tarjeta T  WITH (NOLOCK)
					WHERE T.id_lectura = @TEXTO) -- NO EXISTE UNA TARJETA CON ESA INFORMACION
				BEGIN
					--IF @OF ='000000'
					--	BEGIN 
					--		print 'of desde patronLec';
					--		SET @IDREFERENCIA = @REFERENCIA;
					--		SET @IDCLIENTE = @CLIENTE;
					--	END
					--ELSE
					--	BEGIN
					--		SELECT TOP(1) @IDREFERENCIA = V_OF.ITEMID, @IDCLIENTE = V_OF.CUSTACCOUNT  FROM [dbo].VAX_EKAN_OF  AS V_OF WITH (NOLOCK) WHERE V_OF.PRODID = @OF
					--	END
					PRINT 'REFERENCIA OF:' + @IDREFERENCIA + ' IDCLIENTE:' + @IDCLIENTE;
					IF @IDREFERENCIA is not null AND @IDCLIENTE is not null
					BEGIN
						print 'barruan';
						print @idcliente;
						print @idReferencia;
						--Buscar circuito por ubicacion, ref y cliente
						--SELECT * FROM ubicacion u WHERE id_ubicacion = @idUbi 
						select @IDCIRCUITO = id_circuito FROM [dbo].Circuito c WITH (NOLOCK) WHERE c.ubicacion = @idUbi AND c.cliente_id =@IDCLIENTE AND c.referencia_id =@IDREFERENCIA AND c.activo = 1
						PRINT 'CIRCUITO:' + @IDCIRCUITO;
						IF @IDCIRCUITO <> ''
							BEGIN
								--mirar si hay registros tipo ajuste pendientes de ser modificados
								SELECT @IDLECTURA = T.id_lectura FROM [dbo].Tarjeta t WITH (NOLOCK)  WHERE t.id_circuito=@IDCIRCUITO AND t.estado='A_MODIF' AND t.gestionado IS NULL ORDER BY t.fecha_lectura ASC
								IF @IDLECTURA = '' -- no existe ningun registro de tipo ajuste pendiente
								BEGIN
									--print 'insert into tarjeta'
									
									INSERT INTO [dbo].tarjeta WITH (ROWLOCK) (id_lectura, sentido, estado, fecha_lectura, id_circuito, movil, num_lecturas, ultima_lectura, kill_tag, antena, tipo, referencia_id, cliente_id, ubicacion, sububicacion) VALUES 
											(@TEXTO, 'S', 'P', GETDATE(), @IDCIRCUITO, @imei, 1, GETDATE(), 0, NULL, 'M', @IDREFERENCIA, @IDCLIENTE, @idUbi, @subUbicacion );
									SET @RESULT=  0;
									set @MsgTxt = 'Lectura correctamente registrada';
									
									set @TIPOAVISO = 1;
								END
								ELSE
								BEGIN
									--MODIFICAR REGISTRO @IDLECTURA
									
									UPDATE [dbo].tarjeta WITH (ROWLOCK)  SET gestionado='Ajuste' where id_lectura = @IDLECTURA
									INSERT INTO [dbo].tarjeta WITH (ROWLOCK)	 (id_lectura, sentido, estado, fecha_lectura, gestionado, id_circuito, movil, tipo, kill_tag, referencia_id, cliente_id, ubicacion, sububicacion) VALUES 
										(@TEXTO, 'S', 'A', GETDATE(), 'Ajuste', @IDCIRCUITO, @imei, 'M', 0, @IDREFERENCIA, @IDCLIENTE, @idUbi, @subUbicacion );
									SET @RESULT=  1;
									set @TIPOAVISO = 3;
									set @MsgTxt = 'Referencia leida, se ha incrementado el nº de lecturas';
									
								END 
							END
							else
							begin
		
								set  @MsgTxt = 'La referencia leída no pertenece a la ubicación seleccionada';
								
								set @TIPOAVISO = 2;
								SET @RESULT=  5;
								
							end  
					END
					
				END
				ELSE
				BEGIN
					select @IDCIRCUITO = id_circuito FROM [dbo].Circuito c WITH (NOLOCK) WHERE c.ubicacion = @idUbi AND c.cliente_id =@IDCLIENTE AND c.referencia_id =@IDREFERENCIA AND c.activo = 1
					IF @IDCIRCUITO <> '' begin
						--la etiqueta ya ha sido leida
						UPDATE [dbo].tarjeta  WITH (ROWLOCK) SET num_lecturas = num_lecturas + 1, fecha_lectura = GETDATE() WHERE TARJETA.id_lectura = @TEXTO
						SET @RESULT=  8;
						set @MsgTxt = 'Etiqueta leida, cantidad de lecturas actualizada';
						set @TIPOAVISO = 3;
					end
					else
						begin

						set  @MsgTxt = 'La referencia leída no pertenece a la ubicación seleccionada';
						set @TIPOAVISO = 2;
						SET @RESULT=  5;
					end
				END
			END	
			ELSE
			BEGIN
				set @MsgTxt = 'No existe ninguna OF para la etiqueta';
				set @TIPOAVISO = 4;
				SET @RESULT=  6;
			END 
		END
	END
	select  @IDCIRCUITO as circuito, @REFERENCIA as referencia, @RESULT as Result, @MsgTxt as Msg, @TIPOAVISO as Tipo_Aviso, @MsgTxt as Mensaje_Aviso;
	----select  'PRECOZ4' as circuito, '20900-51' as referencia, 0 as Result, 'OK' as Msg, '' as Tipo_Aviso, '' as Mensaje_Aviso;
	COMMIT TRANSACTION
----END TRY
----BEGIN CATCH 
----	ROLLBACK TRANSACTION
----	--select  '' as circuito, '' as referencia, -1 as Result, 'deadlock' as Msg, '2' as Tipo_Aviso, 'Error-Intente otra vez' as Mensaje_Aviso; 
----	IF ERROR_NUMBER() = 1205 -- Deadlock Error Number
----	BEGIN
----		WAITFOR DELAY '00:00:00.500' -- Wait for 5 ms
----		GOTO RETRY -- Go to Label RETRY
----	END
----	ELSE
----		select  '' as circuito, '' as referencia, -1 as Result, 'deadlock' as Msg, '2' as Tipo_Aviso, 'Error-Intente otra vez' as Mensaje_Aviso; 
----END CATCH

	-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @IDCIRCUITO, @Origen = 'RegistrarLectura', @Usuario = @idUsu
	-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES
	SET XACT_ABORT OFF;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Creando [dbo].[circuito].[Cir_LecMinParaCons].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cantidad mínima a considerar para que una lectura se considere como consumo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'circuito', @level2type = N'COLUMN', @level2name = N'Cir_LecMinParaCons';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[VAX_EKANSTOCKCONSIGPROV].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[21] 4[51] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PURCHLINE_1"
            Begin Extent = 
               Top = 60
               Left = 661
               Bottom = 190
               Right = 936
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PURCHTABLE_1"
            Begin Extent = 
               Top = 29
               Left = 354
               Bottom = 159
               Right = 666
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 12
               Left = 31
               Bottom = 384
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuario_proveedor"
            Begin Extent = 
               Top = 209
               Left = 547
               Bottom = 339
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sga_usuario"
            Begin Extent = 
               Top = 237
               Left = 1108
               Bottom = 367
               Right = 1299
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         W', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANSTOCKCONSIGPROV';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[VAX_EKANSTOCKCONSIGPROV].[MS_DiagramPane2]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane2', @value = N'idth = 2445
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANSTOCKCONSIGPROV';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaDiaSugLanz]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaDiaSugLanz]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaEnTrans]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaEnTrans]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PEnvConsumos]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PEnvConsumos]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAccConsumir]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAccConsumir]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAccRegConsumo]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAccRegConsumo]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAX_ActTarOptimas]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAX_ActTarOptimas]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaConsumidas]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaConsumidas]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaConsumosPorUbicacion]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaConsumosPorUbicacion]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaFicha]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaFicha]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaUltimosEnvios]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaUltimosEnvios]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PEnvSitTarOPT]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PEnvSitTarOPT]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PRevLazPedAut]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PRevLazPedAut]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PTestingKK]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PTestingKK]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PEnvSitPanel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PEnvSitPanel]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAccGetAllUsers]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAccGetAllUsers]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCamIdiUser]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCamIdiUser]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PEnvUsers]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PEnvUsers]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PModIdiUser]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PModIdiUser]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PValidAcceso]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PValidAcceso]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PValidAcceso2]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PValidAcceso2]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PValidPriv]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PValidPriv]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAX_ActualizarUbicacionesUsuario]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAX_ActualizarUbicacionesUsuario]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaGetUbicaciones]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaGetUbicaciones]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAX_CambioCircuito]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAX_CambioCircuito]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaTarOptimoConConsumosHistoricos]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaTarOptimoConConsumosHistoricos]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAX_LanzarPedidoProcesoCompleto]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAX_LanzarPedidoProcesoCompleto]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PCtaAccReports]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PCtaAccReports]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Actualizando [dbo].[PAccesoValidoByUserID]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[PAccesoValidoByUserID]';


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'La parte de transacción de la actualización de la base de datos se realizó correctamente.'
COMMIT TRANSACTION
END
ELSE PRINT N'Error de la parte de transacción de la actualización de la base de datos.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Comprobando los datos existentes con las restricciones recién creadas';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [FK_h4a4i8acrq6mun1132lhiru9h];

ALTER TABLE [dbo].[circuito] WITH CHECK CHECK CONSTRAINT [FK_iejhi9irkh7tx1ji2y8t5k47k];

ALTER TABLE [dbo].[circuito] WITH CHECK CHECK CONSTRAINT [FK_3vn6jckmib1of620by1jg35a6];

ALTER TABLE [dbo].[circuito] WITH CHECK CHECK CONSTRAINT [FK_gpat2w8x7tswwv6tmkaq9u1po];

ALTER TABLE [dbo].[usuario_cliente] WITH CHECK CHECK CONSTRAINT [FK_usuario_cliente_sga_usuario1];

ALTER TABLE [dbo].[usuario_proveedor] WITH CHECK CHECK CONSTRAINT [FK_usuario_proveedor_sga_usuario];

ALTER TABLE [dbo].[sga_usuario] WITH CHECK CHECK CONSTRAINT [FKDEDB797C40F68DC8];

ALTER TABLE [dbo].[sga_usuario] WITH CHECK CHECK CONSTRAINT [FKDEDB797CDAB65181];

ALTER TABLE [dbo].[sga_usuario_puesto] WITH CHECK CHECK CONSTRAINT [FKEB3B4B51F04FDB86];

ALTER TABLE [dbo].[usuario_ubicacion] WITH CHECK CHECK CONSTRAINT [FK5577C8CE315BB8B3];

ALTER TABLE [dbo].[dispositivo_ubicacion] WITH CHECK CHECK CONSTRAINT [FK_hsx52j13m5icy3spj1hormj1y];

ALTER TABLE [dbo].[usuario_ubicacion] WITH CHECK CHECK CONSTRAINT [FK5577C8CE8CC191D5];

ALTER TABLE [dbo].[ubicacion_sububicacion] WITH CHECK CHECK CONSTRAINT [FK_ubicacion_sububicacion_ubicacion];

ALTER TABLE [dbo].[pruebapro_circuito] WITH CHECK CHECK CONSTRAINT [FK_pruebapro_3vn6jckm];

ALTER TABLE [dbo].[pruebapro_circuito] WITH CHECK CHECK CONSTRAINT [FK_pruebapro_gpat2w8x7];

ALTER TABLE [dbo].[pruebapro_circuito] WITH CHECK CHECK CONSTRAINT [FK_pruebapro_iejhi9irkh7];

ALTER TABLE [dbo].[pruebapro_dispositivo_ubicacion] WITH CHECK CHECK CONSTRAINT [FK_pruebapro_atj79pqko6guiswhr];

ALTER TABLE [dbo].[pruebapro_dispositivo_ubicacion] WITH CHECK CHECK CONSTRAINT [FK_pruebapro_hsx52j13m5icy3s];

ALTER TABLE [dbo].[pruebapro_sga_usuario] WITH CHECK CHECK CONSTRAINT [FKpruebapro_DEDB797C40F68DC8];

ALTER TABLE [dbo].[pruebapro_sga_usuario] WITH CHECK CHECK CONSTRAINT [FKpruebapro_DEDB797CDAB65181];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [FK_pruebapro_h4a4i8acrq6mun1132lhiru9h];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [FKaa_h4a4i8acrq6mun1132lhiru9h_Historico];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [fkaa_Historicosububicacion];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [fkaa_sububicacion];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [FK2aa_h4a4i8acrq6mun1132lhiru9h];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [FK2aa_Historicoh4a4i8acrq6mun1132lhiru9h];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [fk2aa_Historicosububicacion];

ALTER TABLE [dbo].[pruebapro_tarjeta] WITH CHECK CHECK CONSTRAINT [fk2aa_sububicacion];

ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [FK_h4a4i8acrq6mun1132lhiru9h_Historico];

ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [fk_Historicosububicacion];

ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [FK2_h4a4i8acrq6mun1132lhiru9h];

ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [FK2_Historicoh4a4i8acrq6mun1132lhiru9h];

ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [fk2_Historicosububicacion];

ALTER TABLE [dbo].[tarjeta] WITH CHECK CHECK CONSTRAINT [fk2_sububicacion];


GO
PRINT N'Actualización completada.';


GO

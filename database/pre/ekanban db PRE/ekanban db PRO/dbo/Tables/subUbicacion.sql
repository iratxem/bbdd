﻿CREATE TABLE [dbo].[subUbicacion] (
    [id_SubUbicacion] VARCHAR (255) NOT NULL,
    [activo]          TINYINT       NOT NULL,
    [descripcion]     VARCHAR (255) NULL,
    CONSTRAINT [PK_subUbicacion] PRIMARY KEY CLUSTERED ([id_SubUbicacion] ASC)
);


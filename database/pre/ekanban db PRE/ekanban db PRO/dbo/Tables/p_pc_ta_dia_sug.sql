﻿CREATE TABLE [dbo].[p_pc_ta_dia_sug] (
    [id_circuito]     VARCHAR (255) NOT NULL,
    [ctd]             INT           NULL,
    [fec]             DATETIME      NULL,
    [tipo_contenedor] VARCHAR (255) NULL,
    [txt]             VARCHAR (255) NULL,
    [tipo_circuito]   INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([id_circuito] ASC)
);


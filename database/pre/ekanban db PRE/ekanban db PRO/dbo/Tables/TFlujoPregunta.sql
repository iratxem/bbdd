﻿CREATE TABLE [dbo].[TFlujoPregunta] (
    [IdFlujo]               INT           NOT NULL,
    [IdPregunta]            INT           NOT NULL,
    [Fase]                  INT           NULL,
    [Activo]                BIT           NULL,
    [Obligatorio]           BIT           NULL,
    [OrigenDatos]           VARCHAR (255) NULL,
    [SentenciaSQL]          VARCHAR (255) NULL,
    [Orden]                 INT           NULL,
    [TextoPregunta]         VARCHAR (255) NULL,
    [TipoRespuesta]         VARCHAR (100) NULL,
    [ToolTip]               VARCHAR (255) NULL,
    [ValorPorDefecto]       VARCHAR (255) NULL,
    [CondicionaSigPregunta] BIT           NULL,
    [Condicion]             VARCHAR (255) NULL,
    [UltimaPregunta]        BIT           NULL,
    CONSTRAINT [PK_FlujoPregunta] PRIMARY KEY CLUSTERED ([IdFlujo] ASC, [IdPregunta] ASC)
);


﻿CREATE TABLE [dbo].[p_situacion_panel_ubicacion] (
    [id_circuito]   VARCHAR (255) NOT NULL,
    [ref_cliente]   VARCHAR (255) NULL,
    [referencia_id] VARCHAR (255) NULL,
    [status]        INT           NULL,
    [valor_status]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id_circuito] ASC)
);


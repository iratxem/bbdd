﻿CREATE TABLE [dbo].[p_consumos_historicos] (
    [cli]    VARCHAR (255) NOT NULL,
    [etiq]   VARCHAR (255) NOT NULL,
    [art]    VARCHAR (255) NOT NULL,
    [año]    VARCHAR (255) NULL,
    [ctdsem] VARCHAR (255) NULL,
    [emp]    VARCHAR (255) NULL,
    [sem]    VARCHAR (255) NULL,
    [color]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([cli] ASC, [etiq] ASC, [art] ASC)
);


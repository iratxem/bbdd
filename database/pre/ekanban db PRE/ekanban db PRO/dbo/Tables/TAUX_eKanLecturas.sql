﻿CREATE TABLE [dbo].[TAUX_eKanLecturas] (
    [Tip] VARCHAR (255) NULL,
    [Mov] VARCHAR (255) NULL,
    [Ant] VARCHAR (255) NOT NULL,
    [Fec] DATETIME      NULL,
    [Txt] VARCHAR (255) NOT NULL,
    [Ges] VARCHAR (255) NULL,
    CONSTRAINT [PK_TAUX_eKanLecturas_1] PRIMARY KEY CLUSTERED ([Txt] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TAUX_eKanLecturas]
    ON [dbo].[TAUX_eKanLecturas]([Tip] ASC);


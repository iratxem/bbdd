﻿CREATE TABLE [dbo].[THistoricoVerdes] (
    [IdCircuito]         VARCHAR (255) NOT NULL,
    [Fecha]              DATETIME      NOT NULL,
    [CtdVerdes]          INT           NULL,
    [CtdAmarillas]       INT           NULL,
    [CtdRojas]           INT           NULL,
    [CtdAmarillasVerdes] INT           NULL,
    [CtdSobreStock]      INT           NULL,
    [tag]                BIT           NULL,
    [origen]             VARCHAR (255) NULL,
    [usuario]            VARCHAR (255) NULL,
    CONSTRAINT [PK_THistoricoVerdes] PRIMARY KEY CLUSTERED ([IdCircuito] ASC, [Fecha] ASC)
);




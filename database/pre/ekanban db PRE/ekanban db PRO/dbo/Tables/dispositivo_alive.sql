﻿CREATE TABLE [dbo].[dispositivo_alive] (
    [id_dispositivo]  VARCHAR (255) NOT NULL,
    [ip]              VARCHAR (255) NULL,
    [ultima_conexion] DATETIME      NULL,
    [version]         VARCHAR (255) NULL,
    [Dis_EnError]     BIT           NULL,
    [Dis_SIM]         NCHAR (100)   NULL,
    [Dis_IPLocal]     NCHAR (100)   NULL,
    [Dis_NotErrMin]   INT           NULL,
    [Dis_NotErrMail]  NCHAR (100)   NULL,
    [Dis_NotErrSta]   INT           NULL,
    [Dis_Tipo]        NCHAR (4)     NULL,
    [Dis_ActAnt0]     BIT           NULL,
    [Dis_ActAnt1]     BIT           NULL,
    [Fecha]           DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([id_dispositivo] ASC, [Fecha] ASC)
);


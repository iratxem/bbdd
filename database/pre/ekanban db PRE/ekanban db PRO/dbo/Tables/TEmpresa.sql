﻿CREATE TABLE [dbo].[TEmpresa] (
    [Emp_Idd]    INT         NOT NULL,
    [Emp_Nom]    NCHAR (50)  NULL,
    [Emp_Log]    IMAGE       NULL,
    [Emp_Tel]    NCHAR (30)  NULL,
    [Emp_EMa]    NCHAR (30)  NULL,
    [Emp_LogURL] NCHAR (150) NULL,
    CONSTRAINT [PK_TEmpresa] PRIMARY KEY CLUSTERED ([Emp_Idd] ASC)
);


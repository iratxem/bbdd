﻿CREATE TABLE [dbo].[p_tarjetas_en_stock_consigna] (
    [alb]     VARCHAR (255) NOT NULL,
    [ctd]     FLOAT (53)    NULL,
    [color]   INT           NULL,
    [fec]     DATETIME      NULL,
    [ent]     DATETIME      NULL,
    [linenum] FLOAT (53)    NULL,
    [link]    INT           NULL,
    [lot]     VARCHAR (255) NULL,
    [recid]   VARCHAR (255) NULL,
    [txtval]  VARCHAR (255) NULL,
    [tip]     INT           NULL,
    PRIMARY KEY CLUSTERED ([alb] ASC)
);


﻿CREATE TABLE [dbo].[orden_fabricacion] (
    [orden_fabricacion] VARCHAR (255) NOT NULL,
    [fecha_creacion]    DATETIME      NULL,
    [fecha_entrega]     DATETIME      NULL,
    [id_cliente]        VARCHAR (255) NULL,
    [id_referencia]     VARCHAR (255) NULL,
    [id_ubicacion]      VARCHAR (255) NULL,
    [lote]              INT           NOT NULL,
    [nombre_cliente]    VARCHAR (255) NULL,
    [num_serie]         INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([orden_fabricacion] ASC)
);


﻿CREATE TABLE [dbo].[ReferenciasConsumoAutomatico] (
    [referencia_id]         VARCHAR (255)    NOT NULL,
    [ubicacion]             VARCHAR (100)    NOT NULL,
    [IdCircuito]            VARCHAR (100)    NULL,
    [CtdActivacionConsigna] NUMERIC (30, 12) NULL,
    CONSTRAINT [PK_ReferenciasConsumoAutomatico] PRIMARY KEY CLUSTERED ([referencia_id] ASC, [ubicacion] ASC)
);


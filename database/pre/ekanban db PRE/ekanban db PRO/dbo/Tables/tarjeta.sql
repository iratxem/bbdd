﻿CREATE TABLE [dbo].[tarjeta] (
    [id_lectura]        VARCHAR (255) NOT NULL,
    [antena]            VARCHAR (255) NULL,
    [estado]            VARCHAR (255) NULL,
    [fecha_lectura]     DATETIME      NULL,
    [gestionado]        VARCHAR (255) NULL,
    [movil]             VARCHAR (255) NULL,
    [tipo]              VARCHAR (255) NULL,
    [cliente_id]        VARCHAR (255) NULL,
    [id_circuito]       VARCHAR (255) NULL,
    [referencia_id]     VARCHAR (255) NULL,
    [ubicacion]         VARCHAR (255) NULL,
    [num_lecturas]      INT           NULL,
    [ultima_lectura]    DATETIME      NULL,
    [sentido]           VARCHAR (10)  NOT NULL,
    [kill_tag]          BIT           NULL,
    [kill_enviado]      DATETIME      NULL,
    [Tar_GestionadoUSU] NCHAR (30)    NULL,
    [Tar_GestionadoFEC] DATETIME      NULL,
    [Tar_CTDRealLeida]  VARCHAR (25)  NULL,
    [Tar_IdPedido]      NCHAR (50)    NULL,
    [lote]              VARCHAR (255) NULL,
    [Tar_FecEntSol]     DATETIME      NULL,
    [Tar_FacFec]        DATETIME      NULL,
    [Tar_Facturado]     BIT           CONSTRAINT [DF_tarjeta_Tar_Facturado] DEFAULT ((0)) NOT NULL,
    [sububicacion]      VARCHAR (255) NULL,
    CONSTRAINT [PK__tarjeta__E9687596114A936A] PRIMARY KEY CLUSTERED ([id_lectura] ASC, [sentido] ASC) WITH (ALLOW_PAGE_LOCKS = OFF),
    CONSTRAINT [FK_h4a4i8acrq6mun1132lhiru9h] FOREIGN KEY ([cliente_id], [id_circuito], [referencia_id], [ubicacion]) REFERENCES [dbo].[circuito] ([cliente_id], [id_circuito], [referencia_id], [ubicacion]),
    CONSTRAINT [fk_sububicacion] FOREIGN KEY ([sububicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion])
);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[ActGesEntradas]
   ON  [dbo].[tarjeta]
   AFTER  INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;




	UPDATE [dbo].[tarjeta]
		SET [gestionado] = 'Entrada', estado='E', sentido='E' 
		WHERE 
		sentido='E'  and gestionado is null
		AND ubicacion 
		NOT IN 
        (SELECT        id_ubicacion
        FROM            dbo.ubicacion AS ubicacion_1
        WHERE        (Ubi_Modo = N'DISP'))




	--UPDATE [dbo].[tarjeta]
	--	SET  estado='P', sentido='S' 
	--	WHERE 
	--	sentido='E'  and gestionado is null
	--	AND ubicacion 
	--	IN 
 --       (SELECT        id_ubicacion
 --       FROM            dbo.ubicacion AS ubicacion_1
 --       WHERE        (Ubi_Modo = N'DISP'))










	UPDATE [dbo].[tarjeta]
		SET [gestionado] = 'Inventario', estado='I'
		WHERE sentido='I'  and gestionado is null

END

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Si una lectura de tarjeta se ha lanzado a pedido de compra para su facturación o no.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tarjeta', @level2type = N'COLUMN', @level2name = N'Tar_Facturado';


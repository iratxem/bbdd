﻿CREATE TABLE [dbo].[dispositivo] (
    [id_dispositivo]      VARCHAR (255)  NOT NULL,
    [activo]              BIT            NOT NULL,
    [imei]                VARCHAR (255)  NULL,
    [num_serie]           VARCHAR (255)  NULL,
    [descripcion]         VARCHAR (255)  NULL,
    [Dis_UltAccFec]       DATETIME       NULL,
    [Dis_UltAccUsu]       NCHAR (50)     NULL,
    [Dis_UltAccVer]       NCHAR (20)     NULL,
    [ultimo_inventario]   DATETIME       NULL,
    [ip]                  VARCHAR (255)  NULL,
    [ultima_conexion]     DATETIME       NULL,
    [version]             VARCHAR (255)  NULL,
    [ant_inventario]      BIT            NULL,
    [realizar_inventario] BIT            NULL,
    [con_sensor]          BIT            NULL,
    [Dis_EnError]         BIT            NULL,
    [Dis_SIM]             NCHAR (100)    NULL,
    [Dis_IPLocal]         NCHAR (100)    NULL,
    [Dis_NotErrMin]       INT            NULL,
    [Dis_NotErrMail]      NCHAR (100)    NULL,
    [Dis_NotErrSta]       INT            NULL,
    [fecha_inicio]        DATETIME       NULL,
    [Dis_Obs]             NVARCHAR (MAX) NULL,
    [Dis_Tipo]            NCHAR (4)      NULL,
    [Dis_ActAnt0]         BIT            NULL,
    [Dis_ActAnt1]         BIT            NULL,
    CONSTRAINT [PK__disposit__FD7B94E53493CFA7] PRIMARY KEY CLUSTERED ([id_dispositivo] ASC) WITH (ALLOW_PAGE_LOCKS = OFF)
);


GO
CREATE NONCLUSTERED INDEX [Id_dispositivo]
    ON [dbo].[dispositivo]([id_dispositivo] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
CREATE NONCLUSTERED INDEX [Indice_NumSerie]
    ON [dbo].[dispositivo]([num_serie] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[ActErrores]
   ON  [dbo].[dispositivo]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	DECLARE @NBase_Cursor varchar(255)
	DECLARE @Dis as varchar(255)
	DECLARE @EnError as bit
	DECLARE @EnErrorAnt as bit
	DECLARE @FecEstAnt as datetime
	DECLARE @FecAct as datetime 
	DECLARE @IP as varchar(20)
	DECLARE @Ver as varchar(20)
	DECLARE @IPAnt as varchar(20)
	DECLARE @VerAnt as varchar(20)
	declare @TipInc as int
	DECLARE @NotMin as int
	DECLARE @NotMail as varchar (100)
	DECLARE @NotSt as int
	DECLARE @UltCon as datetime
	declare @SegErr as int
	DECLARE @UltIni as datetime
	DECLARE @AntIni as datetime


	set @FecAct=getdate()
	
	--Cogemos los segundos según los parámetros. A partir de no tener una señal del portico en estos segundos, se considera que no responde. Cambio IP?
	SELECT      @SegErr=valor
	FROM        dbo.sga_parametros
	WHERE       (nombre = 'SegundosErrorArco')

	set @SegErr=isnull(@SegErr,60)

	--actualiza a error, aquellos dispositivos que no han mostrado estar alive durante los últimos x segundos.
	UPDATE [dbo].[dispositivo]
	SET Dis_EnError = CASE WHEN DATEDIFF(second, ultima_conexion, @FecAct) > @SegErr THEN 1 ELSE 0 END

	CREATE TABLE #TempErr (Disp VARCHAR(50), Ult datetime, EnErr bit)

	DECLARE  CDatos CURSOR FOR
		SELECT [id_dispositivo]
		FROM [dbo].[dispositivo]
   
	OPEN CDatos

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
		SET @Dis = @NBase_Cursor
		set @TipInc=0


		--situación actual?
		SELECT       @EnError=Dis_EnError, @IP=ip, @Ver=version, @NotMin=Dis_NotErrMin, @NotMail=Dis_NotErrMail, @NotSt=Dis_NotErrSta, @UltCon=ultima_conexion, @UltIni=fecha_inicio
		FROM            dbo.dispositivo
		WHERE        (id_dispositivo = @Dis)

		Set @ip=isnull(@ip,'')
		set @Ver=isnull(@Ver,'')
		set @NotMin=isnull(@NotMin,5)
		set @NotMail=isnull(@NotMail,'alert@ekanban.es')
		set @NotSt=isnull(@NotSt,0)
		set @UltIni=isnull(@UltIni,getdate())
		

		--situación anterior?
		SELECT     @FecEstAnt= RegPor_Fec, @EnErrorAnt=RegPor_Err, @IPAnt=RegPor_IP, @VerAnt=RegPor_Ver, @AntIni=RegPor_UltIni
		FROM            dbo.TRegistroEstPort
		WHERE        RegPor_Dis = @Dis
		ORDER BY RegPor_Fec ASC

		set @FecEstAnt=isnull(@FecEstAnt,getdate())
		set @EnErrorAnt=isnull(@EnErrorAnt,0)
		set @EnError=isnull(@EnError,0)
		set @AntIni=isnull(@AntIni,getdate())
		
		
		-- PARA REGISTRAR EL FICHERO DE ESTADO DE PORTICOS
		if @EnError<>@EnErrorAnt or @ip<>@IPAnt or @ver<>@VerAnt or @AntIni<>@UltIni
		begin		
			UPDATE [dbo].[TRegistroEstPort]
			SET [RegPor_Has] = @FecAct
			WHERE [RegPor_Dis]=@Dis and RegPor_Fec=@FecEstAnt

			if @EnError<>@EnErrorAnt  set @TipInc=1
			if @ip<>@IPAnt  set @TipInc=2
			if @ver<>@VerAnt  set @TipInc=3
		
			INSERT INTO [dbo].[TRegistroEstPort]
					   ([RegPor_Fec]
					   ,[RegPor_Dis]
					   ,[RegPor_Des]
					   ,[RegPor_Err]
					   ,[RegPor_IP]
					   ,[RegPor_Ver]
					   ,[RegPor_Est]
					   ,[RegPor_UltIni])
				 VALUES
					   (@FecAct
					   ,@Dis
					   ,@FecAct
					   ,@EnError
					   ,@ip
					   ,@Ver
					   ,@TipInc
					   ,@UltIni)
		end
	
		--EN ERROR
		-- PARA REGISTRAR SI ES UN ERROR DE TENER QUE NOTIFICAR POR CORREO O NO
		if @EnError=1 and @NotSt=0 and DATEDIFF(minute, @UltCon, @FecAct) >= @NotMin
		begin
			UPDATE [dbo].[dispositivo]
			SET [Dis_NotErrSta] = 1
			WHERE [id_dispositivo]=@Dis
		end

		--SIN ERROR
		--TODAVÍA SIN HABER NOTIFIFICADO
		if @EnError=0 and (@NotSt=1) --es que no hemos llegado a notificar por correo pero ha recuperado la conexión
		begin
			UPDATE [dbo].[dispositivo]
			SET [Dis_NotErrSta] = 0
			WHERE [id_dispositivo]=@Dis
		end

		
		--SIN ERROR
		--YA NOTIFIFICADO, HAY QUE AVISAR QUE SE HA RECUPERADO
		if @EnError=0 and (@NotSt=2) --@NotSt=1 or   (LE DEJAMOS QUE LA OTRA INSTANCIA LO NOTIFIQUE POR CORREO)
		begin
			UPDATE [dbo].[dispositivo]
			SET [Dis_NotErrSta] = -1
			WHERE [id_dispositivo]=@Dis
		end

		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor
	END
close CDatos
deallocate CDatos

DROP TABLE #TempErr


EXEC	[dbo].[PValidarLecPorticos]

----- Actualizar dispositivo_alive

	INSERT INTO [dbo].[dispositivo_alive] 
			(id_dispositivo, ip, ultima_conexion, version, Dis_EnError, Dis_SIM, Dis_IPLocal, Dis_NotErrMin, 
			Dis_NotErrMail, Dis_NotErrSta, Dis_Tipo, Dis_ActAnt0, Dis_ActAnt1, fecha)
	SELECT d.id_dispositivo, d.ip, d.ultima_conexion, d.version, d.Dis_EnError, d.Dis_SIM, d.Dis_IPLocal, d.Dis_NotErrMin, 
			d.Dis_NotErrMail, d.Dis_NotErrSta, d.Dis_Tipo, d.Dis_ActAnt0, d.Dis_ActAnt1, CURRENT_TIMESTAMP
	FROM [dbo].[dispositivo] d
	inner join inserted i on d.id_dispositivo=i.id_dispositivo 
	
----- Fin Actualizar dispositivo_alive

END

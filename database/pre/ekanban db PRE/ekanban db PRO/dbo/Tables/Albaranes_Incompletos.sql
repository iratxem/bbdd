﻿CREATE TABLE [dbo].[Albaranes_Incompletos] (
    [IdCircuito] VARCHAR (255)    NOT NULL,
    [CtdEnviada] NUMERIC (28, 12) NULL,
    CONSTRAINT [PK_Albaranes_Incompletos] PRIMARY KEY CLUSTERED ([IdCircuito] ASC)
);


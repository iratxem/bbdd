﻿CREATE TABLE [dbo].[TOptimos] (
    [IdCircuito]          VARCHAR (100) NOT NULL,
    [Fecha]               DATETIME      NOT NULL,
    [CtdOptimaNew]        NUMERIC (18)  NULL,
    [IdUbicacion]         VARCHAR (255) NULL,
    [NumOPT_CircuitoActu] INT           NULL,
    [Ctd_Tarjetas]        INT           NULL,
    [Referencia_id]       NCHAR (100)   NULL,
    [Entrega_Dias]        INT           NULL,
    [LeadTime]            INT           NULL,
    CONSTRAINT [PK_TOptimos] PRIMARY KEY CLUSTERED ([IdCircuito] ASC, [Fecha] ASC)
);


﻿CREATE TABLE [dbo].[Optimos] (
    [IdCircuito]   VARCHAR (255) NOT NULL,
    [FechaCalculo] DATE          NOT NULL,
    [OPT]          INT           NULL,
    [Ubicacion]    VARCHAR (255) NULL,
    CONSTRAINT [PK_Optimos] PRIMARY KEY CLUSTERED ([IdCircuito] ASC, [FechaCalculo] ASC)
);


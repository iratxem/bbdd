﻿CREATE TABLE [dbo].[usuario_cliente] (
    [id]         NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [id_usuario] NUMERIC (19)  NOT NULL,
    [id_cliente] VARCHAR (255) NOT NULL,
    [prioridad]  INT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_usuario_cliente_sga_usuario1] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario])
);


﻿CREATE TABLE [dbo].[TipoRespuestaFlujo] (
    [Tipo]        VARCHAR (100) NOT NULL,
    [Descripcion] VARCHAR (255) NULL,
    [Origen]      VARCHAR (255) NULL,
    [Sentencia]   VARCHAR (255) NULL,
    CONSTRAINT [PK_TipoRespuestaFlujo] PRIMARY KEY CLUSTERED ([Tipo] ASC)
);


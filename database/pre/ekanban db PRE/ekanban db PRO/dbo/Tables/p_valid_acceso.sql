﻿CREATE TABLE [dbo].[p_valid_acceso] (
    [cod_usuario]        NUMERIC (19)    NOT NULL,
    [rol_pedpro]         BIT             NULL,
    [codigocliente]      VARCHAR (255)   NULL,
    [emp_ema]            VARCHAR (255)   NULL,
    [nombre]             VARCHAR (255)   NULL,
    [apellidos]          VARCHAR (255)   NULL,
    [logotipo]           VARCHAR (255)   NULL,
    [emp_nom]            VARCHAR (255)   NULL,
    [ptr_rol]            VARBINARY (255) NULL,
    [emp_tel]            VARCHAR (255)   NULL,
    [tienecircuitos]     BIT             NULL,
    [id_ubicacion]       VARCHAR (255)   NULL,
    [usu_id]             VARCHAR (255)   NULL,
    [vista_datos_reales] BIT             NULL,
    [usu_idi]            VARCHAR (255)   NULL,
    [emp_log]            VARCHAR (255)   NULL,
    [rol_des]            VARCHAR (255)   NULL,
    [roldes]             VARCHAR (255)   NULL,
    [ubi_desloc]         VARCHAR (255)   NULL,
    [descripcion]        VARCHAR (255)   NULL,
    [localizacion]       VARCHAR (255)   NULL,
    [timezone]           VARCHAR (255)   NULL,
    [usu]                VARCHAR (255)   NULL,
    PRIMARY KEY CLUSTERED ([cod_usuario] ASC)
);


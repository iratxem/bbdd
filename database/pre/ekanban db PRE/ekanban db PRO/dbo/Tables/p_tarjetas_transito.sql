﻿CREATE TABLE [dbo].[p_tarjetas_transito] (
    [alb] VARCHAR (255) NOT NULL,
    [ctd] FLOAT (53)    NULL,
    [fec] DATETIME      NULL,
    [ent] DATETIME      NULL,
    [lot] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([alb] ASC)
);


﻿CREATE TABLE [dbo].[v_of] (
    [prodid]               VARCHAR (255) NOT NULL,
    [qtysched]             FLOAT (53)    NOT NULL,
    [ectd_ctd]             FLOAT (53)    NOT NULL,
    [remaininventphysical] FLOAT (53)    NOT NULL,
    [dataareaid]           VARCHAR (255) NULL,
    [ectd_fec]             DATETIME      NOT NULL,
    [scheddate]            DATETIME      NOT NULL,
    [custaccount]          VARCHAR (255) NOT NULL,
    [externalitemid]       VARCHAR (255) NULL,
    [itemid]               VARCHAR (255) NOT NULL,
    [prodgroupid]          VARCHAR (255) NULL,
    [prodstatus]           INT           NOT NULL,
    [wrkctrid]             VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([prodid] ASC)
);


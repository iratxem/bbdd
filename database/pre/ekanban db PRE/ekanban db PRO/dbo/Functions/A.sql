﻿
CREATE FUNCTION [dbo].[A]
(@Circuito as nvarchar(50) )--, @UBI AS NVARCHAR(50)=''
RETURNS int
AS
BEGIN
	DECLARE @A as int
	declare @Art as nvarchar(25)
	DECLARE @PEDPTES AS INT
	DECLARE @CtdKan AS INT
	declare @Tipo as int
	declare @cons as bit
	declare @Dias as int
	declare @Horas as int
	declare @Emp as nvarchar(3)
	
	SET @A=0

	begin
		SELECT @Art=referencia_id,@CtdKan=cantidad_piezas, @Tipo=tipo_circuito, @cons=Cir_Consigna, @Dias=entrega_dias, @Horas=entrega_horas, @Emp=Cir_Empresa
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end

	set @CtdKan=isnull(@CtdKan,1)
	set @Tipo=isnull(@Tipo,1)
	set @cons=isnull(@cons,0)
	set @Dias=isnull(@Dias,0)
	set @Horas=isnull(@Horas,0)


	IF @Tipo=1 --AMARILLAS EN PROVEEDORES
	BEGIN
		if @cons=0  --SI NO ES CONSIGNA, COGEMOS LOS PED COMPRA NORMAL
		BEGIN
			SELECT @PEDPTES=SUM(PURCHLINE_1.REMAINPURCHPHYSICAL) 
			FROM   SRVAXSQL.SQLAX.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
				   SRVAXSQL.SQLAX.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
				   dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
			WHERE  (PURCHLINE_1.PURCHSTATUS = 1)  AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND 
				   (dbo.circuito.id_circuito = @Circuito) AND (PURCHLINE_1.DATAAREAID = N'ork') AND  (PURCHTABLE_1.PURCHASETYPE = 3) AND (PURCHTABLE_1.VENDORREF LIKE N'U%') --(PURCHTABLE_1.DOCUMENTSTATUS = 0) AND

			SET @PEDPTES=isnull(@PEDPTES, 0)
			SET @A=@PEDPTES/@CtdKan
		END

		ELSE --SI ES ARTÍCULO DE CONSIGNA
		
		BEGIN
				DECLARE @CTDPED AS INT
				DECLARE @CTDREC AS INT


				SELECT        @CTDPED=SUM(PURCHLINE_1.PURCHQTY),  @CTDREC=SUM(PURCHLINE_1.INK_ENTRADASCONSIGNA) 
				FROM            SRVAXSQL.SQLAX.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
				SRVAXSQL.SQLAX.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
				dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id
				WHERE        (PURCHLINE_1.PURCHSTATUS = 1) AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND (dbo.circuito.id_circuito = @Circuito) AND (PURCHLINE_1.DATAAREAID = N'ork') AND 
				(PURCHTABLE_1.VENDORREF LIKE N'%') AND (PURCHLINE_1.PURCHASETYPE = 5) AND (PURCHLINE_1.REMAINPURCHPHYSICAL > 0)
				--GROUP BY PURCHLINE_1.REMAINPURCHPHYSICAL
				HAVING        (SUM(PURCHLINE_1.PURCHQTY) > 0)  AND (SUM(PURCHLINE_1.PURCHQTY) - SUM(PURCHLINE_1.INK_ENTRADASCONSIGNA) > 0)
				---ORDER BY SUM(PURCHLINE_1.PURCHQTY) DESC


				set @CTDPED=isnull(@CTDPED, 0)
				set @CTDREC=isnull(@CTDREC, 0)


				SET @PEDPTES=@CTDPED-@CTDREC

				
			set @PEDPTES=isnull(@PEDPTES, 0)
			if @PEDPTES<0 set @PEDPTES=0


			SET @A=@PEDPTES/@CtdKan
		END

	END

	--AMARILLAS EN CLIENTES
	IF @Tipo=3
	BEGIN
		SELECT @PEDPTES=  SUM(dbo.VAX_EKAN_PENORD.REMAINSALESPHYSICAL)
		FROM   dbo.VAX_EKAN_PENORD INNER JOIN
			   dbo.circuito ON dbo.VAX_EKAN_PENORD.CUSTACCOUNT = dbo.circuito.cliente_id AND dbo.VAX_EKAN_PENORD.ITEMID = dbo.circuito.referencia_id AND 
			   (SUBSTRING(dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM, 1, 3) = dbo.circuito.ubicacion OR
			   SUBSTRING(dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM, 1, 2) = dbo.circuito.Cir_UbiAnt OR
			   dbo.VAX_EKAN_PENORD.PURCHORDERFORMNUM LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%') AND dbo.VAX_EKAN_PENORD.SALESTYPE = dbo.circuito.Cir_StdSalesType
		GROUP BY dbo.circuito.id_circuito, dbo.circuito.cliente_id, dbo.circuito.referencia_id
		HAVING (dbo.circuito.id_circuito = @Circuito)

		set @PEDPTES=isnull(@PEDPTES, 0)
		SET @A=@PEDPTES/@CtdKan
	END

	--AMARILLAS EN CIRCUITOS INTERNOS  
	IF @Tipo=2
	BEGIN

	--Campo: INK_DIARIOKANBAN

		SELECT        @PEDPTES=SUM(REMAININVENTPHYSICAL)
		FROM            SRVAXSQL.SQLAX.dbo.PRODTABLE
		WHERE        (PRODSTATUS = 4 and INK_DIARIOKANBAN=@Circuito)
		GROUP BY ITEMID, PRODSTATUS, DATAAREAID, INK_DIARIOKANBAN
		HAVING        (ITEMID = @Art) AND (DATAAREAID = N'ork')



		SET @PEDPTES=isnull(@PEDPTES, 0)
		SET @A=@PEDPTES/@CtdKan


	END

	--AMARILLAS EN CIRCUITOS EXTERNOS 
	IF  @Tipo=4
	BEGIN
		SELECT      @A=  COUNT(estado)
		FROM            dbo.tarjeta
		WHERE          (estado = 'G' OR  estado = 'A_MODIF')  AND (id_circuito = @Circuito) AND (DATEADD(HH, @Horas, Tar_FecEntSol) > GETDATE())

		set @A=isnull(@A, 0)
	
	END
	
	RETURN @A
END



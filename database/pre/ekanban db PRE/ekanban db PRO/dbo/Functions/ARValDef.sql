﻿



CREATE FUNCTION [dbo].[ARValDef]
(@AR as nvarchar(50)='', @Var as nvarchar(1)='1', @Usu as nvarchar(50)='', @Cli as nvarchar(50)='', @Ubi as nvarchar(50)='', @Cir as nvarchar(50)='' )
RETURNS nvarchar(500)
AS
BEGIN
	
	
	DECLARE @Ret as nvarchar(500)
	DECLARE @RetV1 as nvarchar(500)
	DECLARE @RetV2 as nvarchar(500)
	DECLARE @RetV3 as nvarchar(500)
	
	
	--COGEMOS LOS VALORES POR DEFECTO DE LA TABLA, SIN PERSONALIZACIONES
	SELECT        @RetV1=AccRep_V1Def, @RetV2=AccRep_V2Def, @RetV3=AccRep_V3Def
	FROM            dbo.TAccionesReports
	WHERE        (AccRep_Idd = @AR)

	set @RetV1=isnull(@RetV1, '')
	set @RetV2=isnull(@RetV2, '')
	set @RetV3=isnull(@RetV3, '')

	IF @Var=1 SET @Ret=@RetV1
	IF @Var=2 SET @Ret=@RetV2
	IF @Var=3 SET @Ret=@RetV3

	if @AR='ACTMAICON' and @Var='1' SELECT @Ret=mail FROM  dbo.circuito WHERE   (id_circuito = @Cir)
	
	if @AR IN ('ETIIDENT', 'ETIIDENT1', 'ETIBARLUR', 'ETIJOSNEA', 'ETIGIEZBE', 'ETIOITXI') and @Var='1' set @Ret=dbo.CTDETI(@Cir)
	if @AR IN ('ETIIDENT', 'ETIIDENT1', 'ETIBARLUR', 'ETIJOSNEA', 'ETIGIEZBE', 'ETIOITXI')  and @Var='2' set @Ret=CONVERT(nvarchar(100), getdate(), 103)
	if @AR IN ('ETIIDENT', 'ETIIDENT1', 'ETIBARLUR', 'ETIJOSNEA', 'ETIGIEZBE', 'ETIOITXI')  and @Var='3' set @Ret=dbo.r(@Cir)


	set @Ret=isnull(@Ret, '')

	RETURN @Ret
END






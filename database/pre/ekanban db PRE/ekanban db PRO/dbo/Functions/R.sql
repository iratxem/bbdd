﻿

CREATE FUNCTION [dbo].[R]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @R as int
	declare @Tipo as int

	--EN LAS TARJETAS ROJAS, NO DIFERENCIAMOS SI ES UN CIRCUITO DE CLIENTE O DE PROVEEDOR. 
	
	--ROJAS
	SELECT      @R=COUNT(dbo.tarjeta.id_lectura)
	FROM            dbo.circuito INNER JOIN
				dbo.tarjeta ON dbo.circuito.id_circuito = dbo.tarjeta.id_circuito
	WHERE        (dbo.tarjeta.estado = 'A' OR
				dbo.tarjeta.estado = 'P') AND (dbo.tarjeta.sentido = 'S') AND (dbo.circuito.id_circuito = @Circuito) AND (dbo.tarjeta.gestionado IS NULL OR
				dbo.tarjeta.gestionado = '')
	GROUP BY dbo.circuito.id_circuito

	set @R=isnull(@R, 0)



	--PARA MIRAR LOS CIRCUITOS DE DISPONIBILIDAD
	declare @Modo as varchar(30)
	SELECT @Modo=dbo.ubicacion.Ubi_Modo
	FROM   dbo.circuito INNER JOIN
	dbo.ubicacion ON dbo.circuito.ubicacion = dbo.ubicacion.id_ubicacion
	WHERE (dbo.circuito.id_circuito = @Circuito)

	set @Modo=isnull(@Modo, 0)

	if @Modo='DISP'
	BEGIN

	set @R=0 --RESETEAR DATOS ANTERIORES
	SELECT      @R=COUNT(dbo.tarjeta.id_lectura)--[dbo].[T](@Circuito)-
	FROM            dbo.circuito INNER JOIN
				dbo.tarjeta ON dbo.circuito.id_circuito = dbo.tarjeta.id_circuito
	WHERE        (dbo.tarjeta.estado = 'A' OR
				dbo.tarjeta.estado = 'P') AND (dbo.tarjeta.sentido = 'E') AND (dbo.circuito.id_circuito = @Circuito) AND (dbo.tarjeta.gestionado IS NULL OR
				dbo.tarjeta.gestionado = '')
	GROUP BY dbo.circuito.id_circuito

	set @R=isnull(@R, 0)
	IF @R<0 SET @R=0

	END








	RETURN @R

END




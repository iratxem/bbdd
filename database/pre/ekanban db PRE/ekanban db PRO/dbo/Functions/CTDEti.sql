﻿


CREATE FUNCTION [dbo].[CTDEti]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @CTD as int
	
	--EN LAS TARJETAS ROJAS, NO DIFERENCIAMOS SI ES UN CIRCUITO DE CLIENTE O DE PROVEEDOR. 
	
	--ROJAS
	SELECT  @CTD=cantidad_piezas
	FROM    dbo.circuito
	WHERE  (id_circuito = @Circuito)

	set @CTD=isnull(@CTD, 0)

	RETURN @CTD

END





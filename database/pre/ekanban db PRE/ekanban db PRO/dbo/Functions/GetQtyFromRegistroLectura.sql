﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetQtyFromRegistroLectura] 
	-- Add the parameters for the stored procedure here
	(@idLectura varchar(255) = '')
RETURNS VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
    DECLARE @QTY AS VARCHAR(255) = '0' ;
	SELECT        TOP (1) @QTY = Reg_Ctd
		FROM            dbo.TRegistroLecturas
		WHERE        Ltrim(RTrim(Reg_Lot)) + '_' +  rtrim(Ltrim(Reg_Ser)) = @idLectura
		ORDER BY Reg_Fec
	
	RETURN @QTY;
END

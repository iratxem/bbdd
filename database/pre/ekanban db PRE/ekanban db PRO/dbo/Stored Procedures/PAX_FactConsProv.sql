﻿
CREATE PROCEDURE [dbo].[PAX_FactConsProv]
AS
BEGIN

	/*
	UPDATE     tarjeta
	SET                Tar_Facturado = 0
	WHERE        (fecha_lectura > CONVERT(DATETIME, '2015-11-25 06:00:00', 102)) AND (id_circuito LIKE 'EST%')
	*/
	SET NOCOUNT ON;
	SET XACT_ABORT ON;  

	
	DECLARE @NumPed AS VARCHAR(50)
	--DECLARE @LineasPed AS int

	DECLARE @FactFEC as datetime
	set @FactFEC=getdate()

	DECLARE @NBase_Cursor varchar(50)
	DECLARE	@return_value int
	
	DECLARE  CDatos CURSOR FOR
	SELECT  id_circuito 
	FROM            dbo.circuito
	WHERE        (activo = 1) AND (tipo_circuito = 1) AND (Cir_Consigna = 1) AND (Cir_ConFacturarAut = 1) --AND id_circuito='EST'


	OPEN CDatos  -- dispone de todos los códigos de circuito .

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
		
		set @NumPed=@NBase_Cursor + '_' + CONVERT(nvarchar(100), getdate(), 112) 
		
		UPDATE [dbo].[tarjeta]
		SET [Tar_FacFec] = @FactFEC
		,[Tar_Facturado] = 1
		where
		(dbo.tarjeta.id_circuito = @NBase_Cursor) 
		--AND (NOT (dbo.TRegistroLecturas.Reg_Ctd IS NULL)) 
		AND (dbo.tarjeta.Tar_Facturado = 0) 
		AND (estado = 'P'  OR estado = 'T' OR estado = 'A') 

		INSERT INTO [srvaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
		(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, 
		InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra, FechaConsumo)
			
			SELECT      0 AS STA, dbo.tarjeta.id_circuito AS CIR, SUBSTRING(dbo.tarjeta.id_lectura, 8, 5100) AS FRO,
			 dbo.circuito.Proveedor_id AS PRO, 
			 SUBSTRING(dbo.tarjeta.id_lectura, 8, 18) AS PED, --Failoa ematne zuena elaborazioan, 5100 luzera jartzean, -qtxx hartzen duelako, y es por kenketa de txt. 
			 dbo.circuito.referencia_id AS REF,
			
			CONVERT(NUMERIC(9,3),REPLACE(
			isnull(
			(SELECT        TOP (1) Reg_Ctd
			FROM            dbo.TRegistroLecturas
			WHERE        (Reg_Ser = SUBSTRING(dbo.tarjeta.id_lectura, 8, 5100))
			ORDER BY Reg_Fec),0)
			,',','.'))
			AS CTD,

			GETDATE() AS HOY, 'iker' AS USU, @FactFEC AS FEC, dbo.circuito.reserva_auto AS RES, 'PRO' AS TIP, 3 AS ID, 'MP.CH' AS ALM, dbo.circuito.Cir_ConFacturarAut AS FAC, 
			dbo.circuito.Cir_PedMarco AS MAR, dbo.circuito.Cir_ConFacturarAgrup AS FACT, 1 AS EDD, ISNULL(dbo.circuito.Cir_RegistrarPedCompra, 0) AS REGIS,
			CONVERT(VARCHAR(200), dbo.tarjeta.fecha_lectura, 120) as FECLEC

			FROM            dbo.tarjeta INNER JOIN
			dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito

			WHERE        (dbo.tarjeta.id_circuito = @NBase_Cursor) AND  [Tar_FacFec] >= @FactFEC


		-- Para mostrar en pantalla si lo hacemos manual. NO FUNCIONAL
		select * from  [dbo].[tarjeta]
		where  [Tar_FacFec] = @FactFEC AND dbo.tarjeta.id_circuito = @NBase_Cursor

		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor

	END

	--set @LineasPed= @@CURSOR_ROWS;

	close CDatos
	deallocate CDatos
	SET XACT_ABORT OFF;

	--****************************************************
	EXEC	[dbo].[PAX_IntegrarPedidos]
	--****************************************************
	
	
	WAITFOR TIME '06:30';

	DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>AXean Sortu diren Eskaerak</H1>' +
    N'<table border="1">' +
	N'<tr><th>Grabaketa Eguna</th>' +
	N'<th>Ubik.</th>' +
	N'<th>Kodea</th>' +
    N'<th>Izena</th>' +
	N'<th>Izena 2</th>' +
	N'<th>Erabilitako Eguna</th>' +
	N'<th>Txartela</th>' +
	N'<th>Kgs</th>' +
	N'<th>Eskaera AX</th>' +
	N'<th>Linea AX</th>' +
	N'<th>Msg</th>' +
	N'<th>Albaran</th>' +
	N'<th>Linea</th>' +

    --N'<th>Artikulua</th>' +
	--N'<th>Bezeroaren Erref</th>' +
	--N'<th>Txartelak</th>' +
	--N'<th>OPTIMOAK</th>' +
	--N'<th>Diferentzia</th>' +
	--	N'<th>Pzak/Kanban</th>' +
	N'</tr>' +
    CAST ( ( SELECT DISTINCT "td/@align" =  'center', td =convert(varchar(50),  dbo.VTAUX_CREAR_PEDIDO.FechaVolc , 120) ,   ''--convert(varchar(50),  dbo.TRegistroOptHis.id_Fec , 120)
		,td = dbo.circuito.ubicacion,   ''
		,td = dbo.VTAUX_CREAR_PEDIDO.ITEMID,   ''
		,td =   dbo.circuito.descripcion,   ''
		,td =   dbo.circuito.ref_cliente,   ''
		,td =  dbo.VTAUX_CREAR_PEDIDO.FechaConsumo,   ''
		,td =  dbo.VTAUX_CREAR_PEDIDO.Name,   ''
		,td =  cast(dbo.VTAUX_CREAR_PEDIDO.SALESQTY as int),   ''
		,td =  isnull(dbo.VTAUX_CREAR_PEDIDO.SALESID,0),   ''
		,td =  CONVERT(NUMERIC(2,0),isnull(dbo.VTAUX_CREAR_PEDIDO.LINENUM,0)),   ''
		,td =  isnull(dbo.VTAUX_CREAR_PEDIDO.ERRTXT,0),   ''
		,td =  isnull(dbo.VTAUX_CREAR_PEDIDO.PackingSlipId,0),   ''
		,td =  CONVERT(NUMERIC(2,0),isnull(dbo.VTAUX_CREAR_PEDIDO.PackingSlipLINENUM,0)),   ''
		--,td =  dbo.circuito.referencia_id,   ''
		--,td =    dbo.circuito.ref_cliente,   ''
		--,"td/@align" =  'center', td =   dbo.TRegistroOptHis.num_kanbanAct,   ''
		--,"td/@align" =  'center', td =   dbo.TRegistroOptHis.num_kanbanOpt,   ''
		--,"td/@align" =  'center',  "td/@bgcolor" =  case when ((dbo.TRegistroOptHis.num_kanbanOpt*100/dbo.TRegistroOptHis.num_kanbanAct)>50 and (dbo.TRegistroOptHis.num_kanbanOpt*100/dbo.TRegistroOptHis.num_kanbanAct)<120) then 'green' ELSE 'yellow' END, td = dbo.TRegistroOptHis.num_kanbanDif,  '' --case when [Dis_EnError]=0 then 'OK' ELSE 'ERR' END--   
		--,"td/@align" =  'right', td =   dbo.TRegistroOptHis.cantidad_piezas,   ''
		--  ,td = convert(varchar(50), [ultima_conexion], 120),   ''
		--  ,td = 'https://' + [ip] + ':10000',   ''
		--  ,"td/@align" =  'center', td = [version], '',td=convert(varchar(50), [fecha_inicio], 120)
		FROM            dbo.VTAUX_CREAR_PEDIDO INNER JOIN
								 dbo.circuito ON dbo.VTAUX_CREAR_PEDIDO.Journalname = dbo.circuito.id_circuito
		WHERE     (dbo.VTAUX_CREAR_PEDIDO.FechaVolc = @FactFEC)
		order by dbo.circuito.ubicacion, dbo.VTAUX_CREAR_PEDIDO.ITEMID, dbo.VTAUX_CREAR_PEDIDO.FechaConsumo
		--, CONVERT(NUMERIC(2,0),isnull(dbo.VTAUX_CREAR_PEDIDO.LINENUM,0)),   dbo.VTAUX_CREAR_PEDIDO.Name
		--isnull(dbo.VTAUX_CREAR_PEDIDO.SALESID,0)



	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;


	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='jestensoro@orkli.es; jolasagasti@orkli.es; eunanue@orkli.es; ptejeira@orkli.es; upenasco@orkli.es; egoitze@orkli.es; nantxia@orkli.es; mlecea@orkli.es',
	  @subject='eKanban: Fakturatzeko AXean Grabaturiko Txartelak',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'



END


	
	/*



	

	*/
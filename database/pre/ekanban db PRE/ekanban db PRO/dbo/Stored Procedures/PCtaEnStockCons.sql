﻿




CREATE PROCEDURE [dbo].[PCtaEnStockCons] 
	(@Cir varchar(255), @USU varchar(255)='', @Dis int=0)
AS
BEGIN
	
	declare @Tipo as int


	begin
		SELECT        @Tipo=tipo_circuito
		FROM            dbo.circuito
		WHERE        id_circuito = @Cir
	end

	set @Tipo=isnull(@Tipo,1)


	--EN PROVEEDORES NO GESTIONAMOS LA CTD EN TRANSITO, O CONSUMIDO, O CONFIRMADO O EN ALMACÉN
	if @Tipo=1
	begin
		SELECT '000000' AS ALB, GETDATE() AS FEC, GETDATE() AS ENT, 0 AS CTD, 'ABC' AS LOT, 0 as RECID, 0 as LINENUM, 1 as TIP, '1' AS LINK, '1' AS COLOR, 'Confirmar?' AS TXTVAL
	end

	--TRANSITO EN CLIENTES
	if @Tipo=3
	begin
		SELECT        dbo.VAX_EKAN_ALBA.PACKINGSLIPID AS ALB,  dbo.VAX_EKAN_ALBA.CREATEDDATETIME AS FEC,  

		DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME))

		AS ENT, 
		dbo.VAX_EKAN_ALBA.QTY AS CTD, dbo.VAX_EKAN_ALBA.INVENTBATCHID AS LOT, dbo.VAX_EKAN_ALBA.RECID AS RECID, dbo.VAX_EKAN_ALBA.LINENUM, '1' AS TIP, '1' AS LINK, '1' AS COLOR, 'Confirmar?' AS TXTVAL



		FROM            dbo.circuito INNER JOIN
		dbo.VAX_EKAN_ALBA ON dbo.circuito.cliente_id = dbo.VAX_EKAN_ALBA.ORDERACCOUNT AND dbo.circuito.referencia_id = dbo.VAX_EKAN_ALBA.ITEMID AND 
		(dbo.circuito.ubicacion = SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 3) OR
		SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 2) = dbo.circuito.Cir_UbiAnt OR
		dbo.VAX_EKAN_ALBA.PURCHASEORDER LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%')
		WHERE        (dbo.VAX_EKAN_ALBA.QTY > 0) AND (dbo.circuito.id_circuito = @Cir) AND (dbo.VAX_EKAN_ALBA.INK_FACTURADOCONSIGNA = 0) and
		DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME))<getdate()

		order by dbo.VAX_EKAN_ALBA.CREATEDDATETIME
	END




END






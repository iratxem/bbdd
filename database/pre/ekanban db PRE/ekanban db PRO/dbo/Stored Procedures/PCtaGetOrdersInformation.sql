﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaGetOrdersInformation] 
	-- Add the parameters for the stored procedure here
	@usuario varchar(255), @pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT t.id_lectura, t.fecha_lectura, t.num_lecturas, t.sentido, c.id_circuito, c.cliente_id, c.ubicacion, c.referencia_id as referencia_id, c.ref_cliente,
			t.Tar_FecEntSol
	FROM ((SELECT DISTINCT UU.id_ubicacion FROM  sga_usuario U INNER JOIN usuario_ubicacion UU ON U.cod_usuario = UU.id_usuario 
		WHERE U.username = @usuario and U.password =@pwd
	) AS T1
		INNER JOIN circuito C ON C.ubicacion = T1.id_ubicacion) INNER JOIN tarjeta T ON C.id_circuito = T.id_circuito 
		 inner join ubicacion ub on T1.id_ubicacion = ub.id_ubicacion
	WHERE C.activo = 1 AND (t.estado = 'G' OR  t.estado = 'A_MODIF') AND (DATEADD(HH, c.entrega_horas, Tar_FecEntSol) > GETDATE())
		and ub.activo = 1
	ORDER BY t.ubicacion, t.referencia_id
END

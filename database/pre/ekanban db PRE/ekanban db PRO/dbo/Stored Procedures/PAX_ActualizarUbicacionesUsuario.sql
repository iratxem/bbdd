﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_ActualizarUbicacionesUsuario] 
	-- Add the parameters for the stored procedure here
	@codUser numeric(19,0),
	@ubiaciones varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--borrar todas las ubicaciones del usuario e insertar todos (separados por |).
	delete from usuario_ubicacion where id_usuario = @codUser;
	
	DECLARE @start INT, @end INT 
	declare @sububicacion varchar(255)
    SELECT @start = 1, @end = CHARINDEX('|', @ubiaciones) 
    declare @index int = 1;
    WHILE @start < LEN(@ubiaciones) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@ubiaciones) + 1
       
        set @sububicacion= SUBSTRING(@ubiaciones, @start, @end - @start) ;
        if exists (select * from ubicacion where id_ubicacion=@sububicacion)
        begin
			print 'ok   ' + @sububicacion;
			insert into usuario_ubicacion (id_ubicacion, id_usuario, prioridad) values (@sububicacion, @codUser, @index);
		end
        else
        begin
			print 'ko   ' + @sububicacion;
        end
        SET @start = @end + 1 
        SET @end = CHARINDEX('|', @ubiaciones, @start)
        
    END 
	
END

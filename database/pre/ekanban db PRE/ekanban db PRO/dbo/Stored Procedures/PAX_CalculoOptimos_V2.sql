﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CalculoOptimos_V2] 
	-- Add the parameters for the stored procedure here
	@Usuario varchar(255) = '', @fechaHasta date = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @idCircuito varchar(50)
	DECLARE	@return_value int
	DECLARE @Regis varchar(70);
	DECLARE @CTD_MESES_LECTURA INT = 0;
	DECLARE @CTD_OPT INT;
	DECLARE @CTD_KANBAN INT;
	DECLARE @FECHA_DESDE DATETIME;
	DECLARE @ULTIMA_LECTURA DATETIME;
	DECLARE @PRIMERA_LECTURA DATETIME;
	DECLARE @CONSUMO_MEDIO INT;
	DECLARE @IDUBICACION VARCHAR(255);
	DECLARE @NUMOPTCIRCUITO INT;
	DECLARE @NUM_KANBAN INT;
	DECLARE @REFERENCIA_ID varchar(50);
	DECLARE @ENTREGADIAS INT;
	DECLARE @ENTREGAHORAS INT;
	DECLARE @LUNM INT, @LUNT INT;
	DECLARE @MARM INT, @MART INT;
	DECLARE @MIEM INT, @MIET INT;
	DECLARE @JUEM INT, @JUET INT;
	DECLARE @VIEM INT, @VIET INT;
	DECLARE @SABM INT, @SABT INT;
	DECLARE @DOMM INT, @DOMT INT; 
	declare @leadTime int;
	DECLARE @DIAS INT  = 0; DECLARE @PLAZO INT = 0;
	
	set @Regis=CONVERT(nvarchar(50), GETDATE(), 120) 

	DECLARE  CDatos CURSOR FOR
		SELECT  id_circuito, num_kanban, ubicacion,  num_kanbanOPT, num_kanban, referencia_id, entrega_dias, entrega_horas,
			Cir_LunM, Cir_LunT, Cir_MarM, Cir_MarT, Cir_MieM, Cir_MieT, Cir_JueM, Cir_JueT, Cir_VieM, Cir_VieT, Cir_SabM, Cir_SabT, Cir_DomM, Cir_DomT,
			LeadTime
		FROM    dbo.circuito
		WHERE        (activo = 1)

	OPEN CDatos  -- dispone de todos los códigos de circuito .

	FETCH NEXT FROM CDatos INTO @idCircuito, @CTD_KANBAN, @IDUBICACION, @NUMOPTCIRCUITO, @NUM_KANBAN, @REFERENCIA_ID, @ENTREGADIAS, @ENTREGAHORAS, @LUNM, @LUNT, @MARM, @MART, @MIEM, @MIET,
				@JUEM, @JUET, @VIEM, @VIET, @SABM, @SABT, @DOMM, @DOMT, @leadTime;
	WHILE @@fetch_status = 0 
	BEGIN
	
	--***************************************************************************************
		if @fechaHasta IS NOT NULL
		BEGIN
		select @PRIMERA_LECTURA = MIN(FECHA_LECTURA), @ULTIMA_LECTURA = MAX(FECHA_LECTURA), @CTD_MESES_LECTURA = DATEDIFF (month, MIN(fecha_lectura), MAX(fecha_lectura)) 
			from tarjeta where id_circuito = @idCircuito and fecha_lectura < @fechaHasta; 
		END
		ELSE
		BEGIN
			select @PRIMERA_LECTURA = MIN(FECHA_LECTURA), @ULTIMA_LECTURA = MAX(FECHA_LECTURA), @CTD_MESES_LECTURA = DATEDIFF (month, MIN(fecha_lectura), MAX(fecha_lectura)) 
				from tarjeta where id_circuito = @idCircuito
		END
		if @CTD_MESES_LECTURA < 3 or @CTD_MESES_LECTURA is null or @CTD_MESES_LECTURA = ''
		begin
			SET @CTD_OPT = @CTD_KANBAN;
		end
		ELSE
		BEGIN
			--si la diferencia es mayor que 3 meses -> 
			IF @CTD_MESES_LECTURA > 12 SET @FECHA_DESDE = DATEADD(MONTH, -12, @ULTIMA_LECTURA);
			ELSE SET @FECHA_DESDE = @PRIMERA_LECTURA;
			set @PLAZO = @leadTime;
			if @PLAZO is null set @PLAZO = 0;
			select @CTD_OPT = ceiling(T.ctd_opt)
			from (
			select COUNT(*) AS CTD_CONSUMO, CAST(SUM(consumoDia) / cast((datediff(day, @FECHA_DESDE, @fechaHasta)) as float) as numeric(18,6)) AS PROMEDIO,  
					STDEV(CONSUMODIA) AS VARIABILIDAD, AVG(consumoDia) * PLAZO AS STOCK_POR_MEDIO,
				SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911  AS STOCK_POR_VARIABILIDAD, (AVG(consumoDia) * PLAZO) + (SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911) as total
				, ctd_Caja, 
				CAST(cast((dbo.RoundMult(ceiling(((((CAST(SUM(consumoDia) / cast((datediff(day, @FECHA_DESDE, @fechaHasta)) as float) as numeric(18,6)) * PLAZO) +
				 (SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911)) ))) , ctd_Caja)) as numeric(18,0)) as int)
				AS CTD_OPT1,
				CAST(SUM(consumoDia) / cast((datediff(day, @FECHA_DESDE, @fechaHasta) + 1) as float)as numeric(18,6)) * PLAZO   + (SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911) as ctd_opt,
				stdev(consumoDia) as tt3, sum(consumoDia) as consumo
			FROM 
			(select @PLAZO as PLAZO , t.id_circuito,  CONVERT(date, fecha_lectura) as fecha, c.cantidad_piezas, CAST( COUNT(*) as float) as consumoDia,
				c.cantidad_piezas as ctd_Caja, datediff(day, @FECHA_DESDE, @fechaHasta) as dias
			from tarjeta t inner join circuito c on t.id_circuito = c.id_circuito
			where t.id_circuito=@idCircuito  and t.sentido ='s' and t.estado <> 'A_MODIF' -- AND T.id_lectura NOT LIKE 'AJUSTE%'
			AND T.fecha_lectura >= @FECHA_DESDE and t.fecha_lectura <= @fechaHasta
			group by C.entrega_dias,   c.entrega_horas, CONVERT(date, fecha_lectura), t.id_circuito, c.cantidad_piezas) AS QUERY
			GROUP BY PLAZO, ctd_Caja) as T
		END
		--print @ctd_opt;
		
		INSERT INTO TOPTIMOS (IDCIRCUITO, FECHA, CTDOPTIMANew, IDUBICACION, numOPT_CircuitoActu, Ctd_Tarjetas, Referencia_id,Entrega_Dias, LeadTime) VALUES (@idCircuito, @fechaHasta, @CTD_OPT, @IDUBICACION, @NUMOPTCIRCUITO, @NUM_KANBAN, @REFERENCIA_ID, @PLAZO, @leadTime);
		
		UPDATE [dbo].[circuito]
		SET [num_kanbanOPT] = @CTD_OPT
		WHERE [id_circuito]=@idCircuito

		-- En el histórico, ponemos todo a vigente 0
		UPDATE [dbo].[TRegistroOptHis]
		SET [id_Vig] = 0       
		WHERE [id_circuito] =@idCircuito
		
	--***************************************************************************************
		
		FETCH NEXT FROM CDatos INTO @idCircuito, @CTD_KANBAN, @IDUBICACION, @NUMOPTCIRCUITO, @NUM_KANBAN, @REFERENCIA_ID, @ENTREGADIAS, @ENTREGAHORAS, @LUNM, @LUNT, @MARM, @MART, @MIEM, @MIET,
				@JUEM, @JUET, @VIEM, @VIET, @SABM, @SABT, @DOMM, @DOMT, @leadTime;

	END
	close CDatos
	deallocate CDatos
END

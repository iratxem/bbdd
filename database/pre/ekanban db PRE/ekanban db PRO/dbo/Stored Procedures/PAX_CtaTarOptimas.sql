﻿
-- =============================================
-- Author:		IKER
-- Create date: 6-5-2014
-- Description:	Procedimiento para obtener la cantidad optima según circuito
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CtaTarOptimas]
(
@Circuito varchar(255)='', @Usu AS varchar(255)='', @Sal as int=0, @Reg as varchar(50)=' '
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @Tipo as int
	DECLARE @Cliente AS varchar(255)
	DECLARE @Proveedor AS varchar(255)
	DECLARE @Articulo AS varchar(255)
	DECLARE @Ubic AS varchar(255)
	DECLARE @MedDia  as real
	declare @CtdKan as int
	declare @DiaTrans as int
	declare @DiaTrans1 as int
	declare @HoraTrans as int
	declare @DefKan as int
	DECLARE @OPT AS int
	DECLARE @CONSKANDIA AS real
	DECLARE @UbiAnt as varchar(3)=''
	DECLARE @NumEnv as int=0
	declare @leadTime int;

	if @sal=0
	begin
		SELECT        cliente_id AS CLI, ubicacion AS UBI, referencia_id AS ART, num_kanban AS ACT, 1 AS MEDDIA, 1 AS CTDKAN, 1 AS ConsTarDia, 1 AS NumEnvios, 1 AS DemoraDias, 1 AS TRANSD, 1 AS TRANSH, 
		num_kanbanOPT AS OPT, num_kanbanOPT - num_kanban AS DIF
		FROM            dbo.circuito
		WHERE        (id_circuito = @Circuito)

		-- si es 0, que lo muestre en pantalla. situación por defecto. En funcionamiento hasta la fecha. 27/1/2015
		--select  @Cliente as CLI, @Ubic as UBI, @Articulo as ART, @DefKan as ACT, @MedDia as MEDDIA, @CtdKan as CTDKAN, @CONSKANDIA AS ConsTarDia, @NumEnv AS NumEnvios, 5-@NumEnv as DemoraDias, @DiaTrans as TRANSD, @HoraTrans as TRANSH, @OPT as OPT, @OPT-@DefKan as DIF
	end
	else
	begin 


			set @MedDia =0

		begin
			SELECT        @Cliente= cliente_id, @Proveedor=Proveedor_id, @Articulo=referencia_id, @CtdKan=cantidad_piezas, @DiaTrans=entrega_dias, @HoraTrans=entrega_horas, @DefKan=num_kanban, @Ubic=ubicacion,  @Tipo=tipo_circuito, @UbiAnt=Cir_UbiAnt,
							@NumEnv=CASE WHEN Cir_LunM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_MarM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_MieM = 0 THEN 1 ELSE 0 END + CASE WHEN Cir_JueM = 0 THEN 1 ELSE 0 END + CASE WHEN
							  Cir_VieM = 0 THEN 1 ELSE 0 END , @leadTime = LeadTime
			FROM            dbo.circuito
			WHERE        id_circuito = @Circuito
		end
	
			--considerando las ubicaciones y cogiendo los envíos según albarán, en vez de facturas como anteriormente.

		/*Comentado 02/05/2018 - nuevo metodo aplicado
		if @tipo=3
		--Significa que es circuito de tipo cliente. 
		begin
			SET @UBIANT=ISNULL(@UBIANT,'')
			SET @UBIANT=LTRIM(RTRIM(@UBIANT))

			SELECT         @MedDia=AVG(CtdSem) / 5 
				FROM            (
	
					SELECT        TOP (40) CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
							THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
					FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
							SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
							CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
							SRVAXSQL.SQLAX.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
							CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
					WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0)) OR
							(SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%') AND (CUSTPACKINGSLIPTRANS.DELIVERYDATE > DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 12, 0))
					GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
							'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
							CUSTPACKINGSLIPTRANS.DATAAREAID
					HAVING       (SUBSTRING(CUSTPACKINGSLIPJOUR.ORDERACCOUNT,1,6) = substring(@Cliente,1,6)) 
			 AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) AND (DATEPART(week, 
							CUSTPACKINGSLIPTRANS.DELIVERYDATE) <> DATEPART(week, GETDATE()))
					ORDER BY ART, AÑO, SEM DESC

	/**
			SELECT        TOP (40)  CUSTPACKINGSLIPJOUR.ORDERACCOUNT AS CLI, @Ubic AS UBI, CUSTPACKINGSLIPTRANS.ITEMID AS ART, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS AÑO, 
									 DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, 
									 CUSTPACKINGSLIPTRANS.DELIVERYDATE))) AS ETIQ, SUM(CUSTPACKINGSLIPTRANS.QTY) AS CTDSEM, CASE WHEN DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(week, getdate()) 
									 THEN 0 ELSE 1 END AS COLOR, CUSTPACKINGSLIPTRANS.DATAAREAID AS EMP
			FROM            SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
									 SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND 
									 CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID LEFT OUTER JOIN
									 SRVAXSQL.SQLAX.dbo.SALESTABLE AS SALESTABLE_1 ON SALESTABLE_1.SALESID = CUSTPACKINGSLIPTRANS.ORIGSALESID AND 
									 CUSTPACKINGSLIPJOUR.DATAAREAID = SALESTABLE_1.DATAAREAID
			WHERE        (SUBSTRING(SALESTABLE_1.PURCHORDERFORMNUM, 1, 3) = @Ubic) OR
									 (SALESTABLE_1.PURCHORDERFORMNUM LIKE @UBIANT + '%')
			GROUP BY CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.ITEMID, DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE), DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE), 
									 'Y' + LTRIM(RTRIM(STR(DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE))), 
									 CUSTPACKINGSLIPTRANS.DATAAREAID
			HAVING        (CUSTPACKINGSLIPJOUR.ORDERACCOUNT = @Cliente) AND (SUM(CUSTPACKINGSLIPTRANS.QTY) > 0) AND (CUSTPACKINGSLIPTRANS.ITEMID = @Articulo) 
							AND (DATEPART(year, CUSTPACKINGSLIPTRANS.DELIVERYDATE) = DATEPART(YEAR, GETDATE()))
       					    AND (DATEPART(week, CUSTPACKINGSLIPTRANS.DELIVERYDATE) <> DATEPART(week, GETDATE()))
			ORDER BY ART, AÑO, SEM DESC
			**/

			) AS ten


			end


		if @tipo=1 --tipo proveedor
		BEGIN
			SELECT         @MedDia=AVG(CtdSem) / 5 
			FROM            (
				SELECT        TOP (100) PERCENT PURCHTABLE_1.INVOICEACCOUNT AS CLI, 1 AS UBI, PURCHLINE_1.ITEMID AS ART, DATEPART(year, PURCHLINE_1.DELIVERYDATE) AS AÑO, DATEPART(week, 
				PURCHLINE_1.DELIVERYDATE) AS SEM, 'Y' + LTRIM(RTRIM(STR(DATEPART(year, PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, PURCHLINE_1.DELIVERYDATE))) AS ETIQ, 
				SUM(PURCHLINE_1.PURCHQTY) AS CTDSEM, '1' AS COLOR, PURCHTABLE_1.DATAAREAID AS EMP
				FROM            SRVAXSQL.SQLAX.dbo.PURCHTABLE AS PURCHTABLE_1 INNER JOIN
				SRVAXSQL.SQLAX.dbo.PURCHLINE AS PURCHLINE_1 ON PURCHTABLE_1.PURCHID = PURCHLINE_1.PURCHID
				GROUP BY PURCHTABLE_1.INVOICEACCOUNT, PURCHLINE_1.ITEMID, DATEPART(year, PURCHLINE_1.DELIVERYDATE), DATEPART(week, PURCHLINE_1.DELIVERYDATE), 'Y' + LTRIM(RTRIM(STR(DATEPART(year, 
				PURCHLINE_1.DELIVERYDATE)))) + ' W' + LTRIM(STR(DATEPART(week, PURCHLINE_1.DELIVERYDATE))), PURCHTABLE_1.DATAAREAID
				HAVING        (PURCHTABLE_1.DATAAREAID = N'ork') AND (PURCHLINE_1.ITEMID = @Articulo) AND (DATEPART(year, PURCHLINE_1.DELIVERYDATE) = DATEPART(YEAR, GETDATE())) AND (DATEPART(week, 
				PURCHLINE_1.DELIVERYDATE) <> 53) AND (PURCHTABLE_1.INVOICEACCOUNT = @Proveedor)
 				) as a
		END


		set @MedDia=isnull(@MedDia,0)

		if @MedDia=0 set @MedDia=1

		SET @CONSKANDIA=@MedDia/@CtdKan

		set @DiaTrans1=@DiaTrans
		if @DiaTrans1=0 set @DiaTrans1=1
		if @DiaTrans1>5 set @DiaTrans1=@DiaTrans1-2

		if @tipo=1 	SET @OPT=@MedDia/@CtdKan*@DiaTrans1*1.2
		if @tipo=3 	SET @OPT=@MedDia/@CtdKan*(5 - @NumEnv + @DiaTrans1) * 1.25

		set @OPT=isnull(@OPT,0)

		if @OPT=0 set @OPT=1
		
		*/
		--nuevo metodo 02/05/2018
		
		
		declare @fechaHasta datetime = getdate();
		DECLARE @ULTIMA_LECTURA DATETIME;
		DECLARE @PRIMERA_LECTURA DATETIME;
		DECLARE @CONSUMO_MEDIO INT;
		DECLARE @IDUBICACION VARCHAR(255);
		DECLARE @NUMOPTCIRCUITO INT;
		DECLARE @CTD_MESES_LECTURA INT = 0;
		DECLARE @CTD_OPT INT;
		DECLARE @FECHA_DESDE DATETIME;
		
		
		DECLARE @DIAS INT  = 0; DECLARE @PLAZO INT = 0;
			
		if @fechaHasta IS NOT NULL
		BEGIN
		select @PRIMERA_LECTURA = MIN(FECHA_LECTURA), @ULTIMA_LECTURA = MAX(FECHA_LECTURA), @CTD_MESES_LECTURA = DATEDIFF (month, MIN(fecha_lectura), MAX(fecha_lectura)) 
			from tarjeta where id_circuito = @Circuito and fecha_lectura < @fechaHasta; 
		END
		ELSE
		BEGIN
			select @PRIMERA_LECTURA = MIN(FECHA_LECTURA), @ULTIMA_LECTURA = MAX(FECHA_LECTURA), @CTD_MESES_LECTURA = DATEDIFF (month, MIN(fecha_lectura), MAX(fecha_lectura)) 
				from tarjeta where id_circuito = @Circuito
		END
		if @CTD_MESES_LECTURA < 3 or @CTD_MESES_LECTURA is null or @CTD_MESES_LECTURA = ''
		begin
			SET @OPT = @DefKan;
		end
		ELSE
		BEGIN
			--si la diferencia es mayor que 3 meses -> 
			IF @CTD_MESES_LECTURA > 12 SET @FECHA_DESDE = DATEADD(MONTH, -12, @ULTIMA_LECTURA);
			ELSE SET @FECHA_DESDE = @PRIMERA_LECTURA;
			set @PLAZO = @leadTime;
			if @PLAZO is null set @PLAZO = 0;
			select @OPT = ceiling(T.ctd_opt)
			from (
			select COUNT(*) AS CTD_CONSUMO, CAST(SUM(consumoDia) / cast((datediff(day, @FECHA_DESDE, @fechaHasta)) as float) as numeric(18,6)) AS PROMEDIO,  
					STDEV(CONSUMODIA) AS VARIABILIDAD, AVG(consumoDia) * PLAZO AS STOCK_POR_MEDIO,
				SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911  AS STOCK_POR_VARIABILIDAD, (AVG(consumoDia) * PLAZO) + (SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911) as total
				, ctd_Caja, 
				CAST(cast((dbo.RoundMult(ceiling(((((CAST(SUM(consumoDia) / cast((datediff(day, @FECHA_DESDE, @fechaHasta)) as float) as numeric(18,6)) * PLAZO) +
				 (SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911)) ))) , ctd_Caja)) as numeric(18,0)) as int)
				AS CTD_OPT1,
				CAST(SUM(consumoDia) / cast((datediff(day, @FECHA_DESDE, @fechaHasta) + 1) as float)as numeric(18,6)) * PLAZO   + (SQRT(PLAZO) * STDEV(CONSUMODIA) * 2.053748911) as ctd_opt,
				stdev(consumoDia) as tt3, sum(consumoDia) as consumo
			FROM 
			(select @PLAZO as PLAZO , t.id_circuito,  CONVERT(date, fecha_lectura) as fecha, c.cantidad_piezas, CAST( COUNT(*) as float) as consumoDia,
				c.cantidad_piezas as ctd_Caja, datediff(day, @FECHA_DESDE, @fechaHasta) as dias
			from tarjeta t inner join circuito c on t.id_circuito = c.id_circuito
			where t.id_circuito=@Circuito  and t.sentido ='s' and t.estado <> 'A_MODIF' -- AND T.id_lectura NOT LIKE 'AJUSTE%'
			AND T.fecha_lectura >= @FECHA_DESDE and t.fecha_lectura <= @fechaHasta
			group by C.entrega_dias,   c.entrega_horas, CONVERT(date, fecha_lectura), t.id_circuito, c.cantidad_piezas) AS QUERY
			GROUP BY PLAZO, ctd_Caja) as T
		END

		--para visualizar, mientras se ejecuta. 
		select  @Cliente as CLI, @Ubic as UBI, @Articulo as ART, @DefKan as ACT, @MedDia as MEDDIA, @CtdKan as CTDKAN, @CONSKANDIA AS ConsTarDia, @NumEnv AS NumEnvios, 5-@NumEnv as DemoraDias, @DiaTrans as TRANSD, @HoraTrans as TRANSH, @OPT as OPT, @OPT-@DefKan as DIF

		--Actualizamos el campo de la tabla circuito, para acceso rápido
		UPDATE [dbo].[circuito]
		SET [num_kanbanOPT] = @OPT
		WHERE [id_circuito]=@Circuito

		-- En el histórico, ponemos todo a vigente 0
		UPDATE [dbo].[TRegistroOptHis]
		SET [id_Vig] = 0       
		WHERE [id_circuito] =@Circuito
		
		--insertamos un nuevo registro con vigente 1
		INSERT INTO [dbo].[TRegistroOptHis]
           ([id_Reg]
           ,[id_Fec]
           ,[id_Vig]
           ,[id_circuito]
           ,[cliente_id]
           ,[ubicacion]
           ,[referencia_id]
           ,[num_kanbanAct]
           ,[num_kanbanOpt]
           ,[num_kanbanDif]
           ,[MedDia]
           ,[cantidad_piezas]
           ,[MedDiaTarKanban]
           ,[NumEnvios]
           ,[DemoraDias]
           ,[DiasTrans]
           ,[HorasTrans])

		select  
			  @Reg as Registro
			, getdate()
			, 1 as v
			, @Circuito as c
			, @Cliente as CLI
			, @Ubic as UBI
			, @Articulo as ART
			, @DefKan as ACT
			, @OPT as OPT
			, @OPT-@DefKan as DIF
			, @MedDia as MEDDIA
			, @CtdKan as CTDKAN
			, @CONSKANDIA AS ConsTarDia
			, @NumEnv AS NumEnvios
			, 5-@NumEnv as DemoraDias
			, @DiaTrans as TRANSD
			, @HoraTrans as TRANSH

	end



	--EXEC	[dbo].[PRegistro]
	--@Reg_Usu = N'_',
	--@Reg_Ubi = @Ubic,
	--@Reg_opt = N'Cta/Act Optimos',
	--@Reg_Art = @Articulo,
	--@Reg_cli = @Cliente,
	--@Reg_Txt = @OPT


END


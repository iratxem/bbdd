﻿
-- =============================================
-- Author:		Mireia
-- Create date: 18/01/2018
-- Description:	Devuelve los registros de la tabla version_app ordenados por la fecha de creación.  
-- =============================================
CREATE PROCEDURE [dbo].[PCtaAppVersiones]
	@isNuevaApp bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    if @isNuevaApp <> 1 
    begin
		SELECT top(1) [num_version]
			  ,[descripcion]
			  ,[fecha_creacion]
			  ,[URLDescarga]
			FROM [EKANBAN].[dbo].[version_app]
			where (isAppNueva = @isNuevaApp or isappnueva is null)
			ORDER BY [fecha_creacion] DESC
    end
    else
    begin
		SELECT top(1) [num_version]
			  ,[descripcion]
			  ,[fecha_creacion]
			  ,[URLDescarga]
			FROM [EKANBAN].[dbo].[version_app]
			where isAppNueva = @isNuevaApp
			ORDER BY [fecha_creacion] DESC
    end
    
	
END


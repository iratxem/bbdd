﻿
CREATE PROCEDURE [dbo].[PAX_ActTarOptimas]
(
@Usuario AS varchar(255)=''
)
AS
BEGIN
	

SET NOCOUNT ON;
	
	DECLARE @NBase_Cursor varchar(50)
	DECLARE	@return_value int
	DECLARE @Regis varchar(70)

	set @Regis=CONVERT(nvarchar(50), GETDATE(), 120) 

	DECLARE  CDatos CURSOR FOR
		SELECT  id_circuito --    referencia_id --
		FROM    dbo.circuito
		WHERE        (activo = 1)

	OPEN CDatos  -- dispone de todos los códigos de circuito .

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
	
	--***************************************************************************************
	

	EXEC	@return_value = [dbo].[PAX_CtaTarOptimas]
		@Circuito = @NBase_Cursor,
		@Usu = @Usuario,
		@Sal = 1,
		@Reg = @Regis
	

	--***************************************************************************************
		
		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor

	END

close CDatos
deallocate CDatos

fin:


END



﻿

CREATE PROCEDURE [dbo].[PValidarLecPorticos] 
--DE MOMENTO SE LANZA CADA VEZ QUE HAY ACTUALIZACIÓN DE DISPOSITIVOS, COMO TRIGUER DE SEGUIMINETO DE ERRORES, AL FINAL DEL CODIGO
--ES DEMASIADO, Y SE PODRÍA PONER EN ACTUALIZACIÓN DE TARJETA, PERO NO GARANTIZARÍAMOS QUE SE BORRE AL TIEMPO, SI ES QUE NO HAY MÁS LECTURAS. 

AS

BEGIN

	--DECLARE	@return_value int
	DECLARE @Resul as varchar(100)
	DECLARE @Emaitza as int
	DECLARE @TConsLecPor as int
	DECLARE @NMinLecPor as int
	declare @Lecturas as int
	DECLARE @HayLecMin as varchar(100)
	DECLARE @Cli as varchar(100)
	DECLARE @Art as varchar(100)
	DECLARE @Ubi as varchar(100)

	set @Emaitza=1  --por defecto como error
	set @HayLecMin=''

	--Recuperamos los parámetros para considerar o no válido las lecturas de palet por el portico
	SELECT        @TConsLecPor=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'TMaxEntreLecturasPor')

	SELECT        @NMinLecPor=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'NMinLecturasPor')

	set @TConsLecPor=isnull(@TConsLecPor,0)
	set @NMinLecPor=isnull(@NMinLecPor,0)

	--REVISAMOS QUE TENEMOS CONEXIÓN DURANTE LOS ÚLTIMOS X SEGUNDOS, PARA RECIBIR LECTURAS, SI ES QUE NO. YA PROBAREMOS MÁS ADELANTE.
	--DE MOMENTO SIN IMPLANTAR, YA QUE NO SABEMOS SI VA A SER DE UN ARCO O DE OTRO. CONSIDERAMOS QUE SI HA LLEGADO LECTURA INICIAL, DEBERÍA DE 
	--HABER TENIDO CONNEXIÓN. SI VUELVE A ENVIAR, YA REACTIVAREMOS. 
	
	--PARA ACTUALIZAR A GESTIONADO, AQUELLAS LECTURAS DE CIRCUITOS A NIVEL DE PALET, QUE TENGAN MENOS LECTURAS AL DEFINIDO Y QUE HAYA PASADO YA EL TIEMPO MAXIMO
	--ENTRE LECTURAS.
	
	--TRATAMIENTO ESPECIAL PARA CIRCUITOS DE RIELLO: ENTRA EL PALET POR EL ARCO Y SALEN CAJAS. PARA DAR BIEN LOS CONSUMOS UTILIZAMOS EL CAMPO CIR_LECMIN
	
	--update tarjeta set gestionado='Entrada Palet', estado='E'
	--where id_lectura in (
	--	select t.id_lectura
	--	from dbo.tarjeta t inner join dbo.circuito c on t.id_circuito = c.id_circuito
	--	where t.tipo='P' and c.ubicacion ='URIE' and (t.estado = 'P') and (t.gestionado IS NULL OR t.gestionado = '')
		
	--		and c.cir_lecMinParaCons < (
	--			select count(*) --,max(t2.ultima_lectura),MIN(T2.ULTIMA_LECTURA), dateadd(minute, (datediff(minute, 0, t2.ultima_lectura) / 2) * 2, 0)
	--				from tarjeta t2 where 
	--				 T2.ubicacion='URIE' and t2.cliente_id = t.cliente_id and t2.ubicacion = t.ubicacion
	--				 AND T2.referencia_id = T.referencia_id AND T2.id_circuito = T.id_circuito and 
	--				  (t2.estado = 'P' or t2.estado='E') and (t2.gestionado IS NULL OR t2.gestionado = '' or t2.gestionado='Entrada Palet') 
	--				and dateadd(minute, (datediff(minute, 0, t2.ultima_lectura) / 5) * 5, 0) = dateadd(minute, (datediff(minute, 0, t.ultima_lectura) / 2) * 2, 0)
	--				group by  dateadd(minute, (datediff(minute, 0, t2.ultima_lectura) / 5) * 5, 0) --(DATEDIFF(MINUTE, 0, t2.ultima_lectura) / 60)
	--				--having abs(DATEDIFF(SECOND, max(t2.ultima_lectura), min(t2.ultima_lectura))) <= 60 --AND DATEDIFF(SECOND, GETDATE(), MIN(T2.ULTIMA_LECTURA)) <= 60
	--							)
	--				)
				
	
	--update tarjeta set gestionado=NULL, estado='P'
	--where id_lectura in (
	--	select t.id_lectura
	--	from dbo.tarjeta t inner join dbo.circuito c on t.id_circuito = c.id_circuito
	--	where t.tipo='P' and c.ubicacion ='URIE' and (t.estado = 'E') and (t.gestionado IS NULL OR t.gestionado = '' or t.gestionado='Entrada Palet')
	--		and t.ultima_lectura >= DATEADD(MINUTE, -2, getdate())
	--		and c.cir_lecMinParaCons >= (
	--			select count(*) --,max(t2.ultima_lectura),MIN(T2.ULTIMA_LECTURA), dateadd(minute, (datediff(minute, 0, t2.ultima_lectura) / 2) * 2, 0)
	--				from tarjeta t2 where 
	--				 T2.ubicacion='URIE' and t2.cliente_id = t.cliente_id and t2.ubicacion = t.ubicacion
	--				 AND T2.referencia_id = T.referencia_id AND T2.id_circuito = T.id_circuito and 
	--				  (t2.estado = 'P' or t2.estado='E') and (t2.gestionado IS NULL OR t2.gestionado = '' or t2.gestionado='Entrada Palet') 
	--				and dateadd(minute, (datediff(minute, 0, t2.ultima_lectura) / 5) * 5, 0) = dateadd(minute, (datediff(minute, 0, t.ultima_lectura) / 2) * 2, 0)
	--				group by  dateadd(minute, (datediff(minute, 0, t2.ultima_lectura) / 5) * 5, 0) --(DATEDIFF(MINUTE, 0, t2.ultima_lectura) / 60)
	--				--having abs(DATEDIFF(SECOND, max(t2.ultima_lectura), min(t2.ultima_lectura))) <= 60 --AND DATEDIFF(SECOND, GETDATE(), MIN(T2.ULTIMA_LECTURA)) <= 60
	--				)
	--				)
		--update tarjeta set estado='E', gestionado='Entrada Palet' 
		--where id_lectura in (
		--select b.id_lectura
		--from (
		--	select c.Cir_LecMinParaCons, a.id_lectura, count(*) over ( partition by a.fechaAgrupada ) as ctdEntradas
		--	from (
		--	select t.id_circuito, t.id_lectura,
		--	  SUBSTRING(t.id_lectura, 1, CHARINDEX( '_', t.id_lectura, 1) - 1) + '_' + cast(dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0) as varchar(255)) as fechaAgrupada 
		--	from tarjeta t
		--		inner join tarjeta t2 on 
		--			t.referencia_id = t2.referencia_id and
		--			t.cliente_id = t2.cliente_id and
		--			t.ubicacion = t2.ubicacion and
		--			SUBSTRING(t.id_lectura, 1, CHARINDEX( '_', t.id_lectura, 1) - 1) = SUBSTRING(t2.id_lectura, 1, CHARINDEX( '_', t2.id_lectura, 1) - 1) 
		--			and	DATEDIFF(MINUTE, t.ultima_lectura, t2.ultima_lectura) <= 5
		--	where convert(date, t.ultima_lectura) = CONVERT(date,  GETDATE()) and
		--	 t.ubicacion='urie' and (t.estado='P')
		--	group by t.referencia_id, t.cliente_id,t.id_circuito,  t.ubicacion,  SUBSTRING(t.id_lectura, 1, CHARINDEX( '_', t.id_lectura, 1) - 1), t.id_lectura, 
		--	dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0)	
		--	) as a inner join circuito c on c.id_circuito = a.id_circuito) as b
		--where b.ctdEntradas > b.Cir_LecMinParaCons
		--)

		--update tarjeta set estado ='P', gestionado = NULL 
		--where id_lectura in (
		--select b.id_lectura
		--from (
		--	select c.Cir_LecMinParaCons, a.id_lectura, count(*) over ( partition by a.fechaAgrupada ) as ctdEntradas
		--	from (
		--	select t.id_circuito, t.id_lectura,
		--	  SUBSTRING(t.id_lectura, 1, CHARINDEX( '_', t.id_lectura, 1) - 1) + '_' + cast(dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0) as varchar(255)) as fechaAgrupada 
		--	from tarjeta t
		--		inner join tarjeta t2 on 
		--			t.referencia_id = t2.referencia_id and
		--			t.cliente_id = t2.cliente_id and
		--			t.ubicacion = t2.ubicacion and
		--			SUBSTRING(t.id_lectura, 1, CHARINDEX( '_', t.id_lectura, 1) - 1) = SUBSTRING(t2.id_lectura, 1, CHARINDEX( '_', t2.id_lectura, 1) - 1) 
		--			and	DATEDIFF(MINUTE, t.ultima_lectura, t2.ultima_lectura) <= 5
		--	--where t.ultima_lectura >= DATEADD(MINUTE, -15, '2018-03-28 14:40:39.470')	and t.ultima_lectura <= '2018-03-28 17:50:39.470'
		--	where 
		--	convert(date, t.ultima_lectura) = CONVERT(date,  GETDATE()) and 
		--	 t.ubicacion='urie' and (t.estado='E') and t.gestionado='Entrada Palet'
		--	group by t.referencia_id, t.cliente_id,t.id_circuito,  t.ubicacion,  SUBSTRING(t.id_lectura, 1, CHARINDEX( '_', t.id_lectura, 1) - 1), t.id_lectura, 
		--	dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0)	
		--	) as a inner join circuito c on c.id_circuito = a.id_circuito) as b
		--where b.ctdEntradas <= b.Cir_LecMinParaCons)


	select @HayLecMin=id_lectura, @Lecturas=dbo.tarjeta.num_lecturas, @cli=dbo.circuito.cliente_nombre, @Art=dbo.circuito.referencia_id, @NMinLecPor=dbo.circuito.Cir_LecMinParaCons, @Ubi=dbo.circuito.ubicacion
	from 	dbo.tarjeta INNER JOIN  dbo.circuito 
	ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito AND dbo.tarjeta.num_lecturas < dbo.circuito.Cir_LecMinParaCons
	WHERE        (dbo.tarjeta.tipo = 'P') AND (dbo.circuito.tipo_contenedor = 2) AND (dbo.tarjeta.estado = 'P') AND (dbo.tarjeta.gestionado IS NULL OR
                         dbo.tarjeta.gestionado = '') AND (DATEADD(ss, @TConsLecPor, dbo.tarjeta.ultima_lectura) < GETDATE())
                 and dbo.circuito.ubicacion <> 'URIE'
	

	set @HayLecMin= isnull(@HayLecMin,'')
	set @Lecturas= isnull(@Lecturas,0)
	set @NMinLecPor=isnull(@NMinLecPor,1)


	declare @cad varchar(200)
	set @cad = 'Se han realizado ' + CONVERT(CHAR(3),@Lecturas) + 'lecturas de '+ @HayLecMin + ' en ' + @cli + ' (' + @Ubi + '), y el mínimo debe ser de ' + CONVERT(CHAR(3),@NMinLecPor)  + '. '
	SET @cad= @cad + ' Revisar si realmente se debe considerar o no la lectura. Artículo ' + @Art + '.'

	if @HayLecMin<>''  
	begin 
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'mmujika@orkli.es; dallodoli@orkli.it; alert@ekanban.es', 
		@Tem = N'Faltan Lecturas Mínimas',
		@Cue = @cad
	end 



	--PARA ANULAR LECTURAS QUE NO HAYA CTD MÍNIMA EN TIEMPO MÁXIMO
	update   dbo.tarjeta 
	SET gestionado = 'GesAUT_NumLecMin', estado='T'
	from
	dbo.tarjeta INNER JOIN  dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito
	WHERE      id_lectura=@HayLecMin AND sentido='S'
	


	--PARA RECUPERAR LECTURAS VIEJAS, QUE SE ANULARON PORQUE HAYA LECTURA NUEVA
	update   dbo.tarjeta 
	SET gestionado = '', estado='P', dbo.tarjeta.num_lecturas=1
	from
	dbo.tarjeta
	WHERE        (gestionado = 'GesAUT_NumLecMin') AND (DATEADD(ss, @TConsLecPor, ultima_lectura) > GETDATE()) -- (estado = 'T') AND
	
END


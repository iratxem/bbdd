﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PAX_PruebaVerdes 
	-- Add the parameters for the stored procedure here
	@fecha as datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @idCircuito varchar(255);
	declare @verdes int;
	declare @rojas int;
	declare @amarillas int;
	declare @amarillasVerdes int;
	declare @ss int;
	declare @tarjetas int;
	DECLARE cursorCircuitos CURSOR FOR
	select c.id_circuito 
	from circuito c
	where c.activo = 1

	OPEN cursorCircuitos
	FETCH NEXT FROM cursorCircuitos INTO @idCircuito

	WHILE (@@FETCH_STATUS = 0)
    BEGIN
		select @tarjetas = num_kanban
		from circuito
		where id_circuito = @idCircuito
		
		--rojas
		select @rojas =  COUNT(*)
			from tarjeta t
			where (t.Tar_GestionadoFEC is null or convert(date, t.Tar_GestionadoFEC) > convert(date, @fecha))
				and t.estado<> 'a_modif' and convert(date, t.fecha_lectura) <= convert(date, @fecha)
				and t.id_circuito = @idCircuito
				
		--amarillas
		select @amarillas = COUNT(*)
		from tarjeta t inner join circuito c on t.id_circuito = c.id_circuito
		where convert(date, t.Tar_GestionadoFEC) <= convert(date, @fecha)
			and t.estado<> 'a_modif' and convert(date, t.fecha_lectura) <= convert(date, @fecha)
			and convert(date, dateadd(HOUR, c.entrega_horas, t.Tar_FecEntSol)) >=  convert(date, @fecha)	
			and t.id_circuito = @idCircuito
		
		--Sobrestock
		select @ss = COUNT(*)
		from tarjeta t inner join circuito c on t.id_circuito = c.id_circuito
		where t.estado= 'a_modif' and convert(date, t.fecha_lectura) <= convert(date, @fecha)
			and t.id_circuito = @idCircuito and (gestionado is null or gestionado ='')
		
		SET @rojas=isnull(@rojas, 0);
		SET @amarillasVerdes = ISNULL(@amarillasVerdes, 0);
		SET @amarillas=isnull(@amarillas, 0);
		SET @ss=isnull(@ss, 0);
		
		SET @verdes =@tarjetas-@amarillasVerdes-@amarillas-@rojas
		
		set @verdes = @verdes + @ss;
		
		IF @verdes < 0
		BEGIN
			SET @verdes = 0;
		END
		
		insert into THistoricoVerdes (IdCircuito, Fecha, CtdVerdes, ctdAmarillas, ctdRojas, ctdAmarillasVerdes, ctdSobreStock, tag) values (
			@idCircuito, @fecha, @verdes, @amarillas, @rojas, @amarillasVerdes, @ss, 1);
			
		FETCH NEXT FROM cursorCircuitos INTO @idCircuito
    end
    
	CLOSE cursorCircuitos
	DEALLOCATE cursorCircuitos
END

﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PValidAcceso2] 
	-- Add the parameters for the stored procedure here
	@username varchar(255), @pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @nombre varchar(255);
	declare @idioma varchar(255);
	declare @userID int;
	declare @apellidos varchar(255);
	declare @idEmpresa int;
	declare @tokenAuten int;
	declare @tokenRefresco int;
	declare @logoEmpresa varchar(255);
	declare @nombreEmpresa varchar(255);
	declare @telEmpresa varchar(255);
	declare @emailEmpresa varchar(255);
	declare @ptrRol int;
	declare @rolDesc varchar(255);
	declare @timeZone varchar(255);
	declare @pasarelaActiva bit;
	declare @ftpServer varchar(255);
	declare @ftpUsername  varchar(255);
	declare @ftpPassword varchar(255);
	declare @permisoPasarela bit;
	declare @superuser tinyint;
	declare @CodPuesto int;

	select @nombre=nombre, @idioma = Usu_Idi, @userID = cod_usuario, @apellidos = apellidos, @idEmpresa = Usu_Emp, @tokenAuten = TokenAuten_CaducidadMins,
		@tokenRefresco = TokenRefresco_CaducidadMins, @ptrRol = ptr_rol,
		@timeZone = case  isnull(Usu_TimeZone,'') when '' then 'Europe/Madrid' else Usu_TimeZone end, @pasarelaActiva = usu_PasarelaActivada,
		@ftpServer = FTP_Server, @ftpUsername = FTP_Username, @ftpPassword = ftp_password, @permisoPasarela = usu_subirDatosAFtp
	from sga_usuario 
	where username =@username and password =@pwd AND activo = 1;

	if @idEmpresa is not null and @idEmpresa <> ''
	begin
		select @logoEmpresa = Emp_LogURL, @nombreEmpresa = Emp_Nom, @telEmpresa = Emp_Tel, @emailEmpresa = Emp_EMa from TEmpresa where Emp_Idd = @idEmpresa;
	end

	if @ptrRol is not null 
	begin
	 select @rolDesc = descripcion, @superuser = superusuario from sga_rol where cod_rol = @ptrRol;
	end
	
	select @username as usu, @idioma as Usu_Idi, @timeZone as Timezone, @nombre as nombre, @logoEmpresa as Emp_Log, @nombreEmpresa as Emp_Nom, @telEmpresa as Emp_Tel, @emailEmpresa as Emp_Ema, @userID as cod_usuario,
		@apellidos as apellidos, @rolDesc as RolDes, @superuser as Super_User, u.Ubi_DesLoc, u.id_ubicacion, u.descripcion, u.localizacion, su.id_subUbicacion as subUbicacion, su.descripcion as subUbicacionDesc, us.orden,
		@tokenAuten as AuthTokenExpirationMins, @tokenRefresco RefreshTokenExpirationMins, @pasarelaActiva as Pasarela_Activada, @ftpServer as servidor_FTP, @ftpUsername as ftp_Username, @ftpPassword as ftp_Password, @permisoPasarela as Permiso_Pasarela
	from (usuario_ubicacion uu inner join ubicacion u on u.id_ubicacion = uu.id_ubicacion ) LEFT OUTER JOIN 
		(ubicacion_sububicacion us  inner join subUbicacion su on us.id_subUbicacion = su.id_subUbicacion AND SU.ACTIVO = 1 and US.activo = 1) 
				on u.id_ubicacion = us.id_ubicacion 
	where  uu.id_usuario = @userID AND U.activo = 1 
	ORDER BY uu.prioridad, u.Ubi_DesLoc, uu.id_ubicacion;
END

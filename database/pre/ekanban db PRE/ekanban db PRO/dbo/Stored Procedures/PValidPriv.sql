﻿



-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PValidPriv]
(
@Usu AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT        dbo.sga_usuario.username, dbo.sga_rol_privilegio.cod_privilegio
	FROM            dbo.sga_usuario INNER JOIN
	dbo.sga_rol ON dbo.sga_usuario.ptr_rol = dbo.sga_rol.cod_rol INNER JOIN
	dbo.sga_rol_privilegio ON dbo.sga_rol.cod_rol = dbo.sga_rol_privilegio.ptr_rol
	WHERE        (dbo.sga_usuario.username = @Usu)


	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usu,
		@Reg_Ubi = '',
		@Reg_opt = N'[PValidPriv]',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Usu
		
END







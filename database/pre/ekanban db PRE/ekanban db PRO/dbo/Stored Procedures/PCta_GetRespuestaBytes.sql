﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCta_GetRespuestaBytes]
	-- Add the parameters for the stored procedure here
	@idMatricula int,
	@IdPregunta varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select respuestaBytes from MatriculaRespuesta where IdMatricula = @idMatricula and IdPregunta = @IdPregunta;
END

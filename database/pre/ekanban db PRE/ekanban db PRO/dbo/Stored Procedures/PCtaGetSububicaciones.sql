﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaGetSububicaciones]
	-- Add the parameters for the stored procedure here
	@idUbic varchar(255) = '',
	@mode varchar = '1'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @mode = '1'
	begin
		select su.* 
		from subUbicacion su inner join ubicacion_sububicacion uu on su.id_SubUbicacion = uu.id_subUbicacion 
		where uu.id_ubicacion= @idUbic and su.activo = 1;
	end
	else
	begin
		select * from subUbicacion
		where activo=1 and id_SubUbicacion not in (select id_subUbicacion from ubicacion_sububicacion where activo= 1 and id_ubicacion= @idUbic)
	end
END

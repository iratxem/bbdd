﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PEnvErrRFID]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NBase_Cursor varchar(255)
	DECLARE @Dis as varchar(255)
	DECLARE @EnError as int
	DECLARE	@return_value int
	DECLARE @Desc as varchar(100)
	DECLARE @Dest as varchar(400)
	
	CREATE TABLE #TempErr2 (Disp VARCHAR(50))
	DECLARE  CDatos2 CURSOR FOR
		SELECT [id_dispositivo]
		FROM [dbo].[dispositivo]
   
	OPEN CDatos2

	FETCH NEXT FROM CDatos2
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
		SET @Dis = @NBase_Cursor
		
		SELECT @EnError= Dis_NotErrSta, @Dest=Dis_NotErrMail, @Desc=id_dispositivo + ': ' + descripcion + '(Ult. Con. ' + convert(varchar(80), ultima_conexion, 120) + ')'
		FROM dbo.dispositivo
		WHERE (id_dispositivo = @Dis)

		set @EnError=ISNULL(@EnError,0)
		set @Dest=ISNULL(@Dest,'alert@ekanban.es')

		-- PARA REGISTRAR SI ES UN ERROR DE TENER QUE NOTIFICAR POR CORREO O NO
		if @EnError=1 
		begin
			select '1' as ik, @dis
			EXEC	[dbo].[PEnvCorreo]
			@Des = @Dest, 
			@Tem = N'ERROR EN RFID',
			@Cue = @Desc 


			UPDATE [dbo].[dispositivo]
			SET [Dis_NotErrSta] = 2
			WHERE [id_dispositivo]=@Dis
		end


		if @EnError=-1 
		begin
			select '-1' as ik, @dis
			EXEC	[dbo].[PEnvCorreo]
			@Des = @Dest,
			@Tem = N'RFID RECUPERADO',
			@Cue = @Desc 


			UPDATE [dbo].[dispositivo]
			SET [Dis_NotErrSta] = 0
			WHERE [id_dispositivo]=@Dis
		end


		FETCH NEXT FROM CDatos2
		INTO @NBase_Cursor
	END

	
close CDatos2
deallocate CDatos2
DROP TABLE #TempErr2


--PARA GARANTIZAR QUE LOS ARCOS ESTÁN OK, Y EL SERVICIO EN MARCHA
--LE DEJAMOS 1 MINUTOS AL QUE ESTÉ CONECTADO. SI HA PASADO MÁS TIEMPO, CORREO CADA 5 MINUTOS

SET @EnError=0

SELECT        @EnError=1 
	FROM            dbo.dispositivo
	HAVING        (MAX(DATEADD(mi, 1, ultima_conexion)) < GETDATE())

SET @EnError=ISNULL(@EnError,0)


if @EnError=1 
		begin
			UPDATE [dbo].[dispositivo]
			SET Dis_EnError = CASE WHEN DATEDIFF(second, ultima_conexion, GETDATE()) > 60 THEN 1 ELSE 0 END

			EXEC	[dbo].[PEnvCorreo]
			@Des = 'eunanue@orkli.es; jsolis@orkli.es; IKER@ORKLI.ES; mmujika@orkli.es; aarrondo@orkli.es',
			@Tem = 'TOMCAT EN SERVIDOR SRVDMZ CON PROBLEMAS !!',
			@Cue = 'Conectarse al servidor SRVDMZ con el programa Escritorio Remoto. User: ulma Pwd: UL20maxx e iniciar JAVA (Startup-shortcut) '
		END

END

﻿
CREATE PROCEDURE [dbo].[PAccAccReports]
(
@Usuario AS varchar(255)='', @Cliente varchar(255)='',@Ubicacion varchar(255)='', 
@Circuito varchar(255)='', @AccRep varchar(9)='', @Tipo varchar(3)='PDF', 
@Var1 varchar(255)='', @Var2 varchar(255)='', @Var3 varchar(255)=''
)
AS

BEGIN
	DECLARE @Rep varchar(250)
	DECLARE @Ser varchar(25)=''
	DECLARE @ResOK INT=0
	DECLARE @Errmsg varchar(250)=''
	DECLARE @Url varchar(250)=''
	SET NOCOUNT ON;
	
	--REPORTING
	if @Tipo='PDE' OR @Tipo='PDF' OR @Tipo='EXC'
	BEGIN
		SELECT   @ser=AccRep_Ser, @Rep=AccRep_Rep
		FROM     dbo.TAccionesReports
		WHERE   (AccRep_Idd = @AccRep)
	END
	SET @SER=ISNULL(@SER, '')
	SET @Rep=ISNULL(@Rep, '')



	--***********************************************
	/*
	SELECT *
	FROM [dbo].[TAccionesReports]
	where accrep_tip='acc'
	ORDER BY ACCREP_TIP, ACCREP_IDD
	*/

	--ACCIONES
	if @AccRep='ACTMAICON' 
	begin
		UPDATE [dbo].[circuito]
		SET [mail] = @Var1
		WHERE   (id_circuito = @Circuito)
		
		SET @ResOK=1
	end
	if @AccRep='DARCONSUM' SET @ResOK=0
	

	if @AccRep='RESETCIRC' and left(@Circuito,4)='RFID'
	begin
		DELETE FROM [dbo].[tarjeta]
		WHERE        (id_circuito = @Circuito)
		SET @ResOK=1
	end

	

	if @AccRep='UPDMODUBI' --and left(@Circuito,10)='RFIDTEST00'
	begin
		UPDATE [EKANBAN].[dbo].[ubicacion]
		SET [Ubi_Modo] = @Var1
		WHERE [id_ubicacion]=@Ubicacion
		
		SET @ResOK=1
	end

	if @AccRep='ACFINCIR'
	begin
		IF @Var1 <> '' AND @Var1 IS NOT NULL
		BEGIN
			UPDATE EKANBAN.dbo.circuito SET activo = 0 WHERE id_circuito = @VAR1 and ubicacion = @Ubicacion
			SET @ResOK=1;
			set @Errmsg = 'Lote ' + @Var1 + ' finalizado con éxito';
		END
	end

	if @AccRep='ACNCIRC'
	begin
		declare @referencia_ID varchar(255);
		declare @refCliente varchar(255);
		declare @cantidad_piezas int; declare @entregaDias int; declare @entregaHoras int;
		declare @numKanban int, @clienteID varchar(255), @nombreCliente varchar(255), @descripcion varchar(255);
		declare @pos1 int; declare @pos2 int; declare @pos3 int;
		declare @idCircuito varchar(255);
		SELECT @pos1 =CHARINDEX('#', @Var1, 1); 
		begin try
			set @numKanban = cast(SUBSTRING(@var1 ,1 , @pos1 - 1 ) as int);
			set @cantidad_piezas = cast(SUBSTRING(@var1 ,@pos1 + 1 , LEN(@Var1)) as int);
			set @referencia_ID = @Var2;
			set @idCircuito = @Var3;
			set @refCliente = @Var3;
			if exists(select * from circuito where id_circuito = @idCircuito)
			begin
				set @Errmsg = 'El lote ' + @idCircuito + ' ya existe';
			end
			else
			begin
				INSERT INTO [dbo].[circuito]
						   ([cliente_id],[id_circuito],[referencia_id],[activo],[cantidad_piezas],[cliente_nombre],[descripcion],[entrega_dias]
						   ,[entrega_horas],[mail],[num_kanban],[ref_cliente],[reserva_auto],[ubicacion],[tipo_circuito],[tipo_contenedor], Cir_UbiAnt, Cir_LanPedAut, Cir_LunM, Cir_LunT,
						   Cir_MarM, Cir_MarT, Cir_MieM, Cir_MieT, Cir_JueM, Cir_JueT, Cir_VieM, Cir_VieT, Proveedor_id, Cir_HoraM, Cir_HoraT, Cir_SabM, Cir_SabT,
						   Cir_DomM, Cir_DomT, Cir_RAnt, Cir_RFec, Cir_Consigna, Cir_ConSalesType, Cir_ConInventLocationId, Cir_ConFacturarAut, Cir_ConNumFactCli, Cir_StdSalesType,
						   Cir_StdInventLocationId, Cir_PedMarco, Cir_AlbaranarAut, Cir_IntLun, Cir_IntMar, Cir_IntMie, Cir_IntJue, Cir_IntVie, Cir_IntSab, Cir_IntDom, Cir_LanPed5MI,
						   Cir_ConFacturarAgrup, Cir_RegistrarPedCompra, Cir_LecMinParaCons, Cir_ReaprovAgrup, Cir_Empresa, Cir_DupConsCir, Cir_NumPedCli)
				select top(1) cliente_id, @idCircuito as id_circuito, @referencia_ID as referencia_id , 1 , @cantidad_piezas as cantidad_piezas,
								cliente_nombre,@descripcion as descripcion, entrega_dias as entrega_dias
							   ,[entrega_horas],[mail],@numKanban as num_kanban,@refCliente as ref_cliente,[reserva_auto], @Ubicacion as ubicacion,tipo_circuito,tipo_contenedor, Cir_UbiAnt, Cir_LanPedAut, Cir_LunM, Cir_LunT,
							   Cir_MarM, Cir_MarT, Cir_MieM, Cir_MieT, Cir_JueM, Cir_JueT, Cir_VieM, Cir_VieT, Proveedor_id, Cir_HoraM, Cir_HoraT, Cir_SabM, Cir_SabT,
							   Cir_DomM, Cir_DomT, Cir_RAnt, Cir_RFec, Cir_Consigna, Cir_ConSalesType, Cir_ConInventLocationId, Cir_ConFacturarAut, Cir_ConNumFactCli, Cir_StdSalesType,
							   Cir_StdInventLocationId, Cir_PedMarco, Cir_AlbaranarAut, Cir_IntLun, Cir_IntMar, Cir_IntMie, Cir_IntJue, Cir_IntVie, Cir_IntSab, Cir_IntDom, Cir_LanPed5MI,
							   Cir_ConFacturarAgrup, Cir_RegistrarPedCompra, Cir_LecMinParaCons, Cir_ReaprovAgrup, Cir_Empresa, Cir_DupConsCir, Cir_NumPedCli
				from circuito
				where ubicacion = @Ubicacion
				order by id_circuito desc;
				SET @ResOK=1;
				set @Errmsg = 'Lote ' + @idCircuito + ' generado correctamente';
			end
		end try
		begin catch
			set @Errmsg = 'Error creando circuito';
		end catch
		
	end

	/*
	exec SRVAXSQL.ORKLB000.dbo.[ReportPDF_Obtener_Path] @Servidor = N'SRVAX',
		@Report = N'900_Adierazleak.rpt',
		@Cliente = N'00000001',
		@Division = N'01',
		@Links = N'1',
		@Empresa = N'ORK', 
		@Formato = N'EXCEL'


	*/


	--RESULTADOS para Acciones, como para Reporting
	SELECT  @Tipo as Tipo, 
			@ResOK as ResOK, 
			@Errmsg as Errmsg,  
			@Url as Url,
			@ser AS servidor,  
			@Rep AS report,  
			@Cliente AS cliente,  
			@Ubicacion AS ubicacion,  
			@Circuito AS circuito,  
			@Usuario AS usuario,  
			'01' AS division,  
			'1' AS links,  
			'9' AS permiso,  
			'ORK' AS empresa,  
			@Tipo AS tipo,  
			@Var1 AS var1,  
			@Var2 AS var2,  
			@Var3 AS var3,  
			'4' AS var4,  
			'5' AS var5,  
			'6' AS var6,  
			'7' AS var7
		
		
		/*
		EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usuario,
		@Reg_Ubi = '',
		@Reg_opt = N'[PCtaAccReports]',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Usuario
		*/

END









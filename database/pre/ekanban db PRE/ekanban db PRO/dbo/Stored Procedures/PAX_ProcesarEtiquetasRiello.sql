﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_ProcesarEtiquetasRiello] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     --Insert statements for procedure here
	update tarjeta set estado='E', gestionado='Entrada Palet' 
		where id_lectura in (
		select b.id_lectura
		from (
			select c.Cir_LecMinParaCons, a.id_lectura, count(*) over ( partition by a.fechaAgrupada ) as ctdEntradas
			from (
			select t.id_circuito, t.id_lectura,
			  SUBSTRING(t.id_lectura,  CHARINDEX( '_', t.id_lectura, 1) + 1, 6) + '_' + cast(dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0) as varchar(255)) as fechaAgrupada 
			from tarjeta t
				inner join tarjeta t2 on 
					t.referencia_id = t2.referencia_id and
					t.cliente_id = t2.cliente_id and
					t.ubicacion = t2.ubicacion and
					SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6) = SUBSTRING(t2.id_lectura, CHARINDEX( '_', t2.id_lectura, 1) + 1, 6) 
					and	DATEDIFF(MINUTE, t.ultima_lectura, t2.ultima_lectura) <= 5
			where convert(date, t.ultima_lectura) = CONVERT(date,  GETDATE()) 
				and
			 t.ubicacion='urie' and t.sentido ='S' --and (t.estado='P')
			group by t.referencia_id, t.cliente_id,t.id_circuito,  t.ubicacion,  SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6), t.id_lectura, 
			dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0)	
			) as a inner join circuito c on c.id_circuito = a.id_circuito) as b
		where b.ctdEntradas > b.Cir_LecMinParaCons
		)
		----update tarjeta set estado='E', gestionado='Entrada Palet' 
		----	where id_lectura in (
		----	select b.id_lectura
		----	from (
		----	select c.Cir_LecMinParaCons, a.id_lectura, count(*) over ( partition by a.fechaAgrupada ) as ctdEntradas
		----	 from (
		----				select t.id_circuito, t.id_lectura,
		----				  SUBSTRING(t.id_lectura,  CHARINDEX( '_', t.id_lectura, 1) + 1, 6) as fechaAgrupada 
		----				from tarjeta t
		----					inner join tarjeta t2 on 
		----						t.referencia_id = t2.referencia_id and
		----						t.cliente_id = t2.cliente_id and
		----						t.ubicacion = t2.ubicacion and
		----						SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6) = SUBSTRING(t2.id_lectura, CHARINDEX( '_', t2.id_lectura, 1) + 1, 6) --lote bera
		----						and	DATEDIFF(MINUTE, t.ultima_lectura, t2.ultima_lectura) <= 5
		----				where convert(date, t.ultima_lectura) = CONVERT(date,  GETDATE()) 
		----					and t.ubicacion='urie' and t.sentido ='S' --and (t.estado='P')
		----				group by t.referencia_id, t.cliente_id,t.id_circuito,  t.ubicacion,  
		----				SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6), t.id_lectura) as A
		----					inner join circuito c on c.id_circuito = a.id_circuito 
		----	) as B
		----	where b.ctdEntradas > b.Cir_LecMinParaCons)

		update tarjeta set estado ='P', gestionado = NULL 
		where id_lectura in (
		select b.id_lectura
		from (
			select c.Cir_LecMinParaCons, a.id_lectura, count(*) over ( partition by a.fechaAgrupada ) as ctdEntradas
			from (
			select t.id_circuito, t.id_lectura,
			  SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6) + '_' + cast(dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0) as varchar(255)) as fechaAgrupada 
			from tarjeta t
				inner join tarjeta t2 on 
					t.referencia_id = t2.referencia_id and
					t.cliente_id = t2.cliente_id and
					t.ubicacion = t2.ubicacion and
					SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6) = SUBSTRING(t2.id_lectura, CHARINDEX( '_', t2.id_lectura, 1) + 1, 6)
					and	DATEDIFF(MINUTE, t.ultima_lectura, t2.ultima_lectura) <= 5
			--where t.ultima_lectura >= DATEADD(MINUTE, -15, '2018-03-28 14:40:39.470')	and t.ultima_lectura <= '2018-03-28 17:50:39.470'
			where 
			convert(date, t.ultima_lectura) = CONVERT(date,  GETDATE()) and 
			  t.ultima_lectura >= DATEADD(MINUTE, -10, getdate()) and
			 t.ubicacion='urie'  and (t.estado='E') and (t.gestionado='Entrada Palet' OR T.gestionado='Entrada')
			group by t.referencia_id, t.cliente_id,t.id_circuito,  t.ubicacion,  SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6), t.id_lectura, 
			dateadd(minute, datediff(minute, 0, t.ultima_lectura) / 10 * 10, 0)	
			) as a inner join circuito c on c.id_circuito = a.id_circuito) as b
		where b.ctdEntradas <= b.Cir_LecMinParaCons)
		
		--update tarjeta set estado='P', gestionado=NULL 
		--	where id_lectura in (
		--	select b.id_lectura
		--	from (
		--	select c.Cir_LecMinParaCons, a.id_lectura, count(*) over ( partition by a.fechaAgrupada ) as ctdEntradas
		--	 from (
		--				select t.id_circuito, t.id_lectura,
		--				  SUBSTRING(t.id_lectura,  CHARINDEX( '_', t.id_lectura, 1) + 1, 6) as fechaAgrupada 
		--				from tarjeta t
		--					inner join tarjeta t2 on 
		--						t.referencia_id = t2.referencia_id and
		--						t.cliente_id = t2.cliente_id and
		--						t.ubicacion = t2.ubicacion and
		--						SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6) = SUBSTRING(t2.id_lectura, CHARINDEX( '_', t2.id_lectura, 1) + 1, 6) --lote bera
		--						and	DATEDIFF(MINUTE, t.ultima_lectura, t2.ultima_lectura) <= 5
		--				where convert(date, t.ultima_lectura) = CONVERT(date,  GETDATE()) 
		--					and t.ubicacion='urie' and t.sentido ='S' and (t.estado='E') and (t.gestionado='Entrada Palet' OR T.gestionado='Entrada')
		--				group by t.referencia_id, t.cliente_id,t.id_circuito,  t.ubicacion,  
		--				SUBSTRING(t.id_lectura, CHARINDEX( '_', t.id_lectura, 1) + 1, 6), t.id_lectura) as A
		--					inner join circuito c on c.id_circuito = a.id_circuito 
		--	) as B
		--	where b.ctdEntradas <= b.Cir_LecMinParaCons)
END

﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAccGetAllUsers] 
	-- Add the parameters for the stored procedure here
	@codUsuario numeric(19,0) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    if @codUsuario > 0 
    begin
		select * from sga_usuario where cod_usuario= @codUsuario;
    end
    else
    begin
		select * from sga_usuario;
	end
END

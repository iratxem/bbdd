﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_ConsumoAutomatico] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--DECLARE ENVIOS CURSOR FOR
	--select r.referencia_id, r.ubicacion, r.IdCircuito, c.tipo_contenedor, c.cantidad_piezas,  a.PACKINGSLIPID, c.cliente_id, a.QTY, a.[WEIGHT]
	--from [EKANBAN].[dbo].[VAX_EKAN_ALBA] A inner join ReferenciasConsumoAutomatico r on a.ITEMID = r.referencia_id
	--	inner join circuito C on r.IdCircuito = c.id_circuito and c.activo = 1
	--where a.PACKINGSLIPID not in 
	--	(select Albaran from albaranesProcesados P where p.referencia = a.ITEMID) --and a.PACKINGSLIPID='AL00194487'
		
	DECLARE ENVIOS CURSOR FOR
	select r.referencia_id, r.ubicacion, r.IdCircuito, c.tipo_contenedor, c.cantidad_piezas,  T.PACKINGSLIPID, t.recid, c.cliente_id, T.QTY--, T2.[WEIGHT]
	from (SRVAXSQL.SQLAX.DBO.inventtrans as t inner join 
		SRVAXSQL.SQLAX.DBO.INVENTDIM as t2 on t.INVENTDIMID = t2.INVENTDIMID and t.DATAAREAID = t2.DATAAREAID) 
				 inner join ReferenciasConsumoAutomatico r on T.ITEMID = r.referencia_id
		inner join circuito C on r.IdCircuito = c.id_circuito and c.activo = 1
	where T.recid not in 
		(select recid from albaranesProcesados P where p.referencia = T.ITEMID and p.RecID is not null)
		AND t.DATAAREAID='ork' and MOVEMENTTYPE in (2, 12, 14,15) and t2.INVENTLOCATIONID='exp.ch' and 
			((IdCircuito like 'presblock%' and t.datephysical > '2018-09-04') )
		--or (IdCircuito like 'STRAUB%' AND T.datephysical > '2018-09-24')
	order by t.qty desc -- ordenamos por la cantidad para que primero procese los ajustes, o movimientos de tipo entrada (aumento de stock) -> no se generan tarjetas sólo se toca la ctd pendiente
		

	DECLARE @ALBARAN VARCHAR(100);
	declare @recid bigint;
	DECLARE @REFERENCIA VARCHAR(100);
	DECLARE @UBICACION VARCHAR(100);
	DECLARE @CLIENTEID VARCHAR(100);
	DECLARE @QTY NUMERIC(28,12);
	DECLARE @PESO NUMERIC(28,12);
	DECLARE @IDCIRCUITO VARCHAR(100);
	DECLARE @TIPOCONTENEDOR NUMERIC(19,0);
	DECLARE @CANTIDAD_PIEZAS INT;
	DECLARE @IDLECTURA VARCHAR(255);
	DECLARE @CNT INT = 0;
	declare @pendiente int = 0;
	DECLARE @CTD_CONSIGNA INT = 0;
	declare @LECTURAID varchar(255) = NULL;
	DECLARE @ASUNTO VARCHAR(255) = NULL;
	DECLARE @DESC VARCHAR(400) = NULL;
	
	OPEN ENVIOS
	FETCH NEXT FROM ENVIOS
		INTO @REFERENCIA, @UBICACION, @IDCIRCUITO, @TIPOCONTENEDOR, @CANTIDAD_PIEZAS, @ALBARAN, @recid, @CLIENTEID, @QTY
	WHILE @@fetch_status = 0 
	BEGIN
		set @CNT = 0;
		
		--SI TIPO CONTENEDOR = 4 -> PIEZAS -> GENERAR UNA TARJETA ROJA POR CADA PIEZA ENVIADA
		IF @TIPOCONTENEDOR = 4
		BEGIN
			PRINT 'TIPO PIEZAS';
			WHILE @CNT < @QTY
			begin
				--COMPROBAMOS QUE NO HAYA SOBRESTOCK
				set @IDLECTURA = NULL;
				SELECT @IDLECTURA = T.id_lectura FROM [dbo].Tarjeta t WITH (NOLOCK)  WHERE t.id_circuito=@IDCIRCUITO AND t.estado='A_MODIF' AND t.gestionado IS NULL ORDER BY t.fecha_lectura ASC
				PRINT @IDLECTURA;
				IF @IDLECTURA = '' or @IDLECTURA IS NULL -- no existe ningun registro de tipo ajuste pendiente
				BEGIN
						--print 'insert into tarjeta'
					SET @LECTURAID = 'EXP_AUT_' + @ALBARAN + '_' + CONVERT(NVARCHAR(30), GETDATE(), 126);
					INSERT INTO [dbo].tarjeta WITH (ROWLOCK) (id_lectura, sentido, estado, fecha_lectura, id_circuito, movil, num_lecturas, ultima_lectura, kill_tag, antena, tipo, referencia_id, cliente_id, ubicacion) VALUES 
						(@LECTURAID, 'S', 'P', GETDATE(), @IDCIRCUITO, NULL, 1, GETDATE(), 0, NULL, 'M', @REFERENCIA, @CLIENTEID, @UBICACION );
					INSERT INTO DBO.TRegistroLecturas WITH (ROWLOCK) (REG_FEC, REG_DIS, Reg_Usu, Reg_Ubi, Reg_Pan, Reg_Txt, Reg_Cir, Reg_Cli, Reg_Art, Reg_Lot, Reg_Ser, Reg_Ctd) VALUES
							(GETDATE(), 'M', 'ConsumoAuto', @UBICACION, 0, 'ConsumoAuto', @IDCIRCUITO, @CLIENTEID, @REFERENCIA, '', SUBSTRING(@LECTURAID, 8, 5100), @CANTIDAD_PIEZAS);
							
					PRINT 'INSERTAR TARJETA';
				END
				ELSE
				BEGIN
					--MODIFICAR REGISTRO @IDLECTURA	
					SET @LECTURAID = 'EXP_AUT_' + @ALBARAN + '_' + CONVERT(NVARCHAR(30), GETDATE(), 126);
					UPDATE [dbo].tarjeta WITH (ROWLOCK)  SET gestionado='Ajuste' where id_lectura = @IDLECTURA
					INSERT INTO [dbo].tarjeta WITH (ROWLOCK)	 (id_lectura, sentido, estado, fecha_lectura, gestionado, id_circuito, movil, tipo, kill_tag, referencia_id, cliente_id, ubicacion) VALUES 
							(@LECTURAID, 'S', 'A', GETDATE(), 'Ajuste', @IDCIRCUITO, NULL, 'M', 0, @REFERENCIA, @CLIENTEID, @UBICACION );
					INSERT INTO DBO.TRegistroLecturas WITH (ROWLOCK) (REG_FEC, REG_DIS, Reg_Usu, Reg_Ubi, Reg_Pan, Reg_Txt, Reg_Cir, Reg_Cli, Reg_Art, Reg_Lot, Reg_Ser, Reg_Ctd) VALUES
							(GETDATE(), 'M', 'ConsumoAuto', @UBICACION, 0, 'ConsumoAuto', @IDCIRCUITO, @CLIENTEID, @REFERENCIA, '', SUBSTRING(@LECTURAID, 8, 5100), @CANTIDAD_PIEZAS);
					PRINT 'INSERTAR TARJETA';
				END 
				SET @CNT = @CNT + 1;
			end
		END
		IF @TIPOCONTENEDOR <> 4
		BEGIN
			DECLARE @QTY_PENDIENTE NUMERIC(28,12) = 0;
			--txartel gorri bat sortzeko zenbat gelditzen den lortzen dugu
			SELECT @QTY_PENDIENTE = CtdEnviada FROM Albaranes_Incompletos WHERE IdCircuito = @IDCIRCUITO;
			IF (@QTY_PENDIENTE IS NULL) set @QTY_PENDIENTE = 0;
			DECLARE @PENDIENTES int = @QTY_PENDIENTE + @QTY; -- el valor QTY vendrá en negativo en movimientos de salida y positivo en entradas
			IF @PENDIENTES <= 0
			BEGIN 
				DECLARE @NUMTARJETAS float = CAST(@PENDIENTES AS FLOAT) / CAST(@cantidad_piezas AS FLOAT);
				if @NUMTARJETAS < 0 set @NUMTARJETAS = @NUMTARJETAS * -1;
				set @NUMTARJETAS = CEILING(@NUMTARJETAS);
				WHILE @CNT < @NUMTARJETAS
				BEGIN
					--COMPROBAMOS QUE HAYA TARJETAS VERDES
					DECLARE @TARJETASVERDES INT = DBO.V(@IDCIRCUITO);
					IF @TARJETASVERDES <= 0 
					BEGIN
						--SI NO HAY TARJETAS VERDES -> ENVIO DE CORREO Y PASAMOS AL SIGUIENTE
						SET @ASUNTO = 'EXP.CH - Consumo incorrecto de la ref.:' + @REFERENCIA;
						SET @DESC = 'No hay cantidad disponible en el circuito, no se crearán tarjetas rojas, revisar el circuito manualmente y actualizar la cantidad pendiente en la BBDD. Cantidad del movimiento:' + convert(varchar(255), @QTY);
						EXEC DBO.PEnvCorreo 'mlecea@orkli.es; eunanue@orkli.es; jolasagasti@orkli.es', @ASUNTO , @DESC;
						GOTO SIGUIENTE;
					END
					--COMPROBAMOS SI HAY QUE ACTIVAR LAS CARACTERISTICAS DE CONSIGNA EN EL CIRCUITO
					select @CTD_CONSIGNA = CtdActivacionConsigna from ReferenciasConsumoAutomatico WHERE IdCircuito = @IDCIRCUITO AND referencia_id = @REFERENCIA AND ubicacion = @UBICACION; 
					IF @CTD_CONSIGNA >= 0 AND  @CTD_CONSIGNA - @CANTIDAD_PIEZAS <= 0 
					BEGIN
						UPDATE circuito SET Cir_LanPedAut = 1, Cir_ConFacturarAgrup = 1, Cir_PedMarco= '*' where id_circuito= @IDCIRCUITO and referencia_id= @REFERENCIA and ubicacion=@UBICACION;
						SET @DESC  = 'Se ha modificado la referencia ' + @REFERENCIA + '. Se han activado los parametros para que genere pedidos de manera automatica' ;
						EXEC DBO.PEnvCorreo 'mlecea@orkli.es; eunanue@orkli.es; jolasagasti@orkli.es', 'Circuito de Consigna EXP-CH modificado', @DESC; 
					end
					UPDATE ReferenciasConsumoAutomatico SET CtdActivacionConsigna = (@CTD_CONSIGNA - @CANTIDAD_PIEZAS) WHERE IdCircuito = @IDCIRCUITO AND referencia_id = @REFERENCIA AND ubicacion = @UBICACION;
						
					
					--COMPROBAMOS QUE NO HAYA SOBRESTOCK
					set @IDLECTURA = NULL;
					SELECT @IDLECTURA = T.id_lectura FROM [dbo].Tarjeta t WITH (NOLOCK)  WHERE t.id_circuito=@IDCIRCUITO AND t.estado='A_MODIF' AND t.gestionado IS NULL ORDER BY t.fecha_lectura ASC
					IF @IDLECTURA = '' or @IDLECTURA IS NULL -- no existe ningun registro de tipo ajuste pendiente
					BEGIN
						SET @LECTURAID = 'EXP_AUT_' + @ALBARAN + '_' + CONVERT(NVARCHAR(30), GETDATE(), 126);
							--print 'insert into tarjeta'
						INSERT INTO [dbo].tarjeta WITH (ROWLOCK) (id_lectura, sentido, estado, fecha_lectura, id_circuito, movil, num_lecturas, ultima_lectura, kill_tag, antena, tipo, referencia_id, cliente_id, ubicacion) VALUES 
						(@LECTURAID, 'S', 'P', GETDATE(), @IDCIRCUITO, NULL, 1, GETDATE(), 0, NULL, 'M', @REFERENCIA, @CLIENTEID, @UBICACION );
						
						INSERT INTO DBO.TRegistroLecturas WITH (ROWLOCK) (REG_FEC, REG_DIS, Reg_Usu, Reg_Ubi, Reg_Pan, Reg_Txt, Reg_Cir, Reg_Cli, Reg_Art, Reg_Lot, Reg_Ser, Reg_Ctd) VALUES
							(GETDATE(), 'M', 'ConsumoAuto', @UBICACION, 0, 'ConsumoAuto', @IDCIRCUITO, @CLIENTEID, @REFERENCIA, '', SUBSTRING(@LECTURAID, 8, 5100), @CANTIDAD_PIEZAS);
						PRINT 'INSERTAR TARJETA';
					END
					ELSE
					BEGIN
						--MODIFICAR REGISTRO @IDLECTURA	
						SET @LECTURAID = 'EXP_AUT_' + @ALBARAN + '_' + CONVERT(NVARCHAR(30), GETDATE(), 126);
						UPDATE [dbo].tarjeta WITH (ROWLOCK)  SET gestionado='Ajuste' where id_lectura = @IDLECTURA
						INSERT INTO [dbo].tarjeta WITH (ROWLOCK)	 (id_lectura, sentido, estado, fecha_lectura, gestionado, id_circuito, movil, tipo, kill_tag, referencia_id, cliente_id, ubicacion) VALUES 
								(@LECTURAID, 'S', 'A', GETDATE(), 'Ajuste', @IDCIRCUITO, NULL, 'M', 0, @REFERENCIA, @CLIENTEID, @UBICACION );
						INSERT INTO DBO.TRegistroLecturas WITH (ROWLOCK) (REG_FEC, REG_DIS, Reg_Usu, Reg_Ubi, Reg_Pan, Reg_Txt, Reg_Cir, Reg_Cli, Reg_Art, Reg_Lot, Reg_Ser, Reg_Ctd) VALUES
							(GETDATE(), 'M', 'ConsumoAuto', @UBICACION, 0, 'ConsumoAuto', @IDCIRCUITO, @CLIENTEID, @REFERENCIA, '',SUBSTRING(@LECTURAID, 8, 5100), @CANTIDAD_PIEZAS);
						--print 'sobrestock';
					END 
					
					SET @CNT = @CNT + 1;
					set @PENDIENTES = @PENDIENTES + (@CANTIDAD_PIEZAS);
					
				END
				SET @QTY_PENDIENTE = @PENDIENTES;		
				--SET @QTY_PENDIENTE =  ( @CANTIDAD_PIEZAS * @CNT) - (@QTY) + @QTY_PENDIENTE ;
			END
			ELSE 
				SET @QTY_PENDIENTE = @QTY_PENDIENTE + @QTY;		
			
			
			--ACTUALIZAR/INSERTAR LA CANTIDAD PENDIENTE
			
			IF EXISTS (SELECT * FROM Albaranes_Incompletos WHERE IdCircuito = @IDCIRCUITO)
				UPDATE Albaranes_Incompletos SET CtdEnviada = @QTY_PENDIENTE WHERE IdCircuito = @IDCIRCUITO;
			ELSE
				INSERT INTO Albaranes_Incompletos (IdCircuito, CtdEnviada) VALUES (@IDCIRCUITO, @QTY_PENDIENTE);
			
		END	
		INSERT INTO AlbaranesProcesados (Albaran, Fecha, referencia, recid) VALUES (@ALBARAN, GETDATE(), @REFERENCIA, @recid);
		
		
		--Control de consigna (comentado -> tratamiento en del bucle)
		----IF @CTD_CONSIGNA >= 0 AND @REFERENCIA='CS7000' -- MOMENTUZ CS7000 BAKARRA, SOBRESTOCK HANDIA DAGOELAKO
		----BEGIN
		----	DECLARE @PDTE_CONSIGNA INT = @CTD_CONSIGNA - @QTY;
		----	UPDATE ReferenciasConsumoAutomatico SET CtdActivacionConsigna = @PDTE_CONSIGNA WHERE IdCircuito = @IDCIRCUITO AND referencia_id = @REFERENCIA AND ubicacion = @UBICACION;
		----	IF @PDTE_CONSIGNA <= 0 
		----	BEGIN
		----		SET @desc  = 'Modificar el circuito ' + @IDCIRCUITO + ', para activar la propiedad de generación automatica de pedidos' ;
		----		EXEC DBO.PEnvCorreo 'mlecea@orkli.es; eunanue@orkli.es; jolasagasti@orkli.es', 'Revisar circuito Consigna EXP-CH', @desc; 
		----	END
		----END
SIGUIENTE:
		FETCH NEXT FROM ENVIOS
		INTO @REFERENCIA, @UBICACION, @IDCIRCUITO, @TIPOCONTENEDOR, @CANTIDAD_PIEZAS, @ALBARAN, @recid, @CLIENTEID, @QTY
	END

	deallocate envios;
END

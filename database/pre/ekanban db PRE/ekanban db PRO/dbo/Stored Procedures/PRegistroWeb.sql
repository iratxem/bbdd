﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRegistroWeb] 
	-- Add the parameters for the stored procedure here
	(@Name varchar(200) = '',
	@Company varchar(200) = '',
	@Website varchar(200) = '',
	@Tel varchar(200) = '',
	@Email varchar(200) = ''
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Resul INT=1
	DECLARE @Msg varchar(250)=''


	INSERT INTO [dbo].[TRegistroWeb]
           ([RegWeb_Date]
           ,[RegWeb_Name]
           ,[RegWeb_Company]
           ,[RegWeb_Website]
           ,[RegWeb_Tel]
           ,[RegWeb_Email])
      
     VALUES
           (GETDATE()
           ,@Name
           ,@Company
           ,@Website
           ,@Tel
           ,@Email)

	Set @Resul=0
	set @Msg='se han insertado los datos'

	--RESULTADOS para Acciones, como para Reporting
	SELECT  'PAccAlbEnv' as Tipo, 
			@Resul as Resul, 
			@Msg as Msg  


	DECLARE	@return_value int
	Declare @Texto varchar(400)

	set @Texto = 'Se ha registrado un nuevo contacto en la Web ekanban:' + CHAR(13) + CHAR(13)
	set @Texto = @Texto + 'Nombre: ' + @Name + CHAR(13)
	set @Texto = @Texto + 'Empresa: ' + @Company + CHAR(13)
	set @Texto = @Texto + 'Web: ' + @Website + CHAR(13)
	set @Texto = @Texto + 'Tel: ' + @Tel + CHAR(13)
	set @Texto = @Texto + 'eMail: ' + @Email + CHAR(13)
	

	EXEC	@return_value = [dbo].[PEnvCorreo]
			@Des = N'eunanue@orkli.es;info@ekanban.es; iker@orkli.es',
			@Tem = N'Nuevo Registro Web en eKanban',
			@Cue = @Texto


END


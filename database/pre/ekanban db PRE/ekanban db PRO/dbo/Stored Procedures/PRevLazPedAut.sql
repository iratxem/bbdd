﻿
CREATE PROCEDURE [dbo].[PRevLazPedAut]
	(@USU varchar(255)='', @Man bit=0)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @NBase_Cursor varchar(50)
	DECLARE @Cir varchar (100)=''
	DECLARE @IntegM VARCHAR(5)=''
	DECLARE @IntegT VARCHAR(5)=''
	DECLARE @DiasM int=0
	DECLARE @DiasT int=0
	DECLARE @DiasCons int=0
	DECLARE @R int=0
	DECLARE	@return_value int
	DECLARE @FechaPed VARCHAR(30)
	DECLARE @Integrar bit
	DECLARE @User varchar(255)=''

	SET DATEFIRST 1

	
	SELECT        @Integrar=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'ActivarIntegracionAut')

	SET @Integrar=isnull(@Integrar,0)

	--Si por parámetro no estamos integrando en automático y no es un lanzamiento manual, vamos a fin.
	if @Integrar<>1 and @Man=0 goto fin

	IF @Man=0 -- NO ES MANUAL=>SÍ ES AUTOMÁTICO
	 begin
		--No es manual, por lo que cogemos todos los circuitos automáticos que correspondan a esta hora, y también las que son de cada 5 minutos. 
		DECLARE  CDatos CURSOR FOR

		SELECT  id_circuito, @USU AS Expr1
		FROM    dbo.circuito
		WHERE   

		(
		(activo = 1) AND (Cir_LanPedAut = 1) AND (ISNULL(Cir_LanPed5MI, 0) = 0) AND 
		(
		   (DATEPART(weekday, GETDATE()) = 1) AND (Cir_IntLun = 1) OR
                         (DATEPART(weekday, GETDATE()) = 2) AND (Cir_IntMar = 1) OR
                         (DATEPART(weekday, GETDATE()) = 3) AND (Cir_IntMie = 1) OR
                         (DATEPART(weekday, GETDATE()) = 4) AND (Cir_IntJue = 1) OR
                         (DATEPART(weekday, GETDATE()) = 5) AND (Cir_IntVie = 1) OR
                         (DATEPART(weekday, GETDATE()) = 6) AND (Cir_IntSab = 1) OR
                         (DATEPART(weekday, GETDATE()) = 7) AND (Cir_IntDom = 1)
		)

		and
		(
		(CASE WHEN CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, GETDATE()) - 0) + Cir_HoraM, GETDATE())) > - 3 AND
				   CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, GETDATE()) - 0) + Cir_HoraM, GETDATE())) < 3 
				   THEN 'M' ELSE '' END) = 'M' 
				OR
		(CASE WHEN CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, GETDATE()) - 1) + Cir_HoraT, GETDATE())) > - 3 AND 
				   CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, GETDATE()) - 1) + Cir_HoraT, GETDATE())) < 3 
				   THEN 'T' ELSE '' END) = 'T'
				   ) 
			)	   
				   OR
  			    ( (ISNULL(Cir_LanPed5MI, 0) = 1) AND (activo = 1))

     end
	else
		--SÍ ES MANUAL
		--NO CONSIDERA SI ES LA HORA DE INTEGRACIÓN O NO.
	 begin
			--Al haber indicado la integración manual (sin considerar hora de integración), cogemos todos los circuitos que sean activos y con integración automática de pedidos
			DECLARE  CDatos CURSOR FOR
			SELECT          id_circuito, @USU
			FROM            dbo.circuito
			WHERE        ((Cir_LanPedAut = 1) or  (ISNULL(Cir_LanPed5MI, 0) = 1)) AND (activo = 1)
	 end
 
	OPEN CDatos  -- dispone de todos los códigos de circuito a integrar.

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor, @User

	WHILE @@fetch_status = 0 
	BEGIN
		SET @Cir = @NBase_Cursor

		set @R=dbo.r(@cir)	

		--SI HAY ROJAS, Y ES HORA DE INTEGRAR...
		IF @R>0 --and @User=@USU  --Control de que en caso de que se lance el cdatos, por dos usuarios, solo se cogan los del mismo usuario.
		BEGIN

			--COGEMOS LOS DÍAS A AÑADIR A LA ENTREGA, SEGÚN EL DÍA DE HOY
			SELECT     @DiasM = CASE DATEPART(dw, GETDATE())
			WHEN 1 THEN cir_lunM WHEN 2 THEN cir_marM WHEN 3 THEN cir_mieM WHEN 4 THEN cir_jueM WHEN 5 THEN cir_vieM WHEN 6 THEN cir_sabM WHEN 7 THEN cir_DomM END , 
			@DiasT = CASE DATEPART(dw, GETDATE())
			WHEN 1 THEN cir_lunT WHEN 2 THEN cir_marT WHEN 3 THEN cir_mieT WHEN 4 THEN cir_jueT WHEN 5 THEN cir_vieT WHEN 6 THEN cir_SabT WHEN 7 THEN cir_DomT END
			FROM         dbo.circuito
			WHERE     (id_circuito = @Cir)
		

			IF @Man=0 -- ES LANZAMIENTO AUTOMÁTICO, PORQUE HA LLEGADO A LA HORA DE INTEGARCIÓN DE PEDIDOS, o es cada 5 minutos
			begin		
				SELECT  @IntegM =   CASE WHEN CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, GETDATE()) - 0) + Cir_HoraM, GETDATE())) > - 20 AND CONVERT(int, DATEDIFF(minute, 
							  CONVERT(datetime, CONVERT(int, GETDATE()) - 0) + Cir_HoraM, GETDATE())) < 20 THEN 'M' ELSE '' END,
						@IntegT =   CASE WHEN CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, 
							  GETDATE()) - 1) + Cir_HoraT, GETDATE())) > - 20 AND CONVERT(int, DATEDIFF(minute, CONVERT(datetime, CONVERT(int, GETDATE()) - 1) + Cir_HoraT, GETDATE())) < 20 THEN 'T' ELSE '' END 
				FROM                      
					   dbo.circuito
				WHERE     (id_circuito = @Cir)
			end


			if @IntegT='T' 
				begin
					set @DiasCons=isnull(@DiasT, 0)
					set @FechaPed = DATEADD(dd, @DiasCons, CONVERT(datetime, CONVERT(int, GETDATE()) - 1))
				end
		
			if @IntegM='M' 
				--Tiene prioridad el de mañana, por si se ponen las dos horas iguales.
				begin
					set @DiasCons=isnull(@DiasM,0)
					set @FechaPed = DATEADD(dd, @DiasCons, CONVERT(datetime, CONVERT(int, GETDATE()) - 0))

				end
			if @IntegM='' and @IntegT=''
				--En caso de que no se haya cogido la hora fija de mañana o tarde, bien porque sea lanzamiento manual o bien por ser lanzamiento cada 5 minutos
				begin
				--CONSIDERAMOS QUE ANTES DE LAS 12 ES MAÑANA Y DESPUÉS DE LAS 12 ES TARDE. 
				--OTRA OPCIÓN    CONVERT(datetime, FLOOR(CONVERT(real, GETDATE())))
					if DATEPART(hh, GETDATE())<12 
					begin
						set @DiasCons=isnull(@DiasM,0)
						set @FechaPed = DATEADD(dd, @DiasCons, CONVERT(datetime, CONVERT(int, GETDATE()) - 0))
					end
					else
					begin
						set @DiasCons=isnull(@DiasT, 0)
						set @FechaPed = DATEADD(dd, @DiasCons, CONVERT(datetime, CONVERT(int, GETDATE()) - 1))
					end
				end


			--en caso de que haya un error para que lo grabe para hoy
			--puede ser el caso de que no hayamos puesto ningún valor en los dias a considerar, o que por la hora de lanzamiento, no sea mañana ni tarde, y @FechaPed esté vacío
			set @FechaPed=isnull(@FechaPed,getdate())
			
			EXEC	@return_value=[dbo].[PAX_LanzamientoPedidos]
			@CodCircuito = @Cir,
			@NumTarjRojas = @R,
			@NumTarjetas = @R,
			@FechEntregaConfirmada = @FechaPed,  
			@Usuario = @USU
		END
		
		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor, @User

	END

close CDatos
deallocate CDatos

fin:


END


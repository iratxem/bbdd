﻿CREATE VIEW dbo.VAX_STOCKCUSTCONSIG
AS
SELECT   TOP (100) PERCENT CUSTPACKINGSLIPJOUR.PACKINGSLIPID, CUSTPACKINGSLIPJOUR.INVOICEACCOUNT, CUSTPACKINGSLIPJOUR.DELIVERYNAME, CUSTPACKINGSLIPTRANS.DELIVERYDATE, CUSTPACKINGSLIPTRANS.LINENUM, CUSTPACKINGSLIPTRANS.ITEMID, CUSTPACKINGSLIPTRANS.EXTERNALITEMID, 
             CUSTPACKINGSLIPTRANS.ORDERED, CUSTPACKINGSLIPTRANS.QTY, INVENTDIM.INVENTBATCHID, CUSTPACKINGSLIPTRANS.DATAAREAID, CUSTPACKINGSLIPJOUR.PURCHASEORDER, CUSTPACKINGSLIPTRANS.INK_FACTURADOCONSIGNA, CUSTPACKINGSLIPJOUR.ORDERACCOUNT, CUSTPACKINGSLIPTRANS.RECID, 
             dbo.circuito.id_circuito, dbo.circuito.ubicacion, dbo.circuito.Cir_Consigna
FROM     SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPTRANS AS CUSTPACKINGSLIPTRANS INNER JOIN
             SRVAXSQL.SQLAX.dbo.CUSTPACKINGSLIPJOUR AS CUSTPACKINGSLIPJOUR ON CUSTPACKINGSLIPTRANS.PACKINGSLIPID = CUSTPACKINGSLIPJOUR.PACKINGSLIPID AND CUSTPACKINGSLIPTRANS.DATAAREAID = CUSTPACKINGSLIPJOUR.DATAAREAID INNER JOIN
             dbo.circuito ON CUSTPACKINGSLIPTRANS.ITEMID = dbo.circuito.referencia_id AND CUSTPACKINGSLIPJOUR.ORDERACCOUNT = dbo.circuito.cliente_id LEFT OUTER JOIN
             SRVAXSQL.SQLAX.dbo.INVENTDIM AS INVENTDIM RIGHT OUTER JOIN
             SRVAXSQL.SQLAX.dbo.InventTrans AS INVENTTRANS ON INVENTDIM.DATAAREAID = INVENTTRANS.DATAAREAID AND INVENTDIM.INVENTDIMID = INVENTTRANS.INVENTDIMID ON CUSTPACKINGSLIPTRANS.INVENTTRANSID = INVENTTRANS.INVENTTRANSID AND 
             CUSTPACKINGSLIPTRANS.DATAAREAID = INVENTTRANS.DATAAREAID
WHERE   (CUSTPACKINGSLIPTRANS.INK_FACTURADOCONSIGNA = 0) AND (CUSTPACKINGSLIPJOUR.ORDERACCOUNT LIKE N'02420301%') AND (CUSTPACKINGSLIPTRANS.DATAAREAID = N'ork') AND (CUSTPACKINGSLIPTRANS.QTY > 0) AND (dbo.circuito.Cir_Consigna = 1)
ORDER BY CUSTPACKINGSLIPJOUR.INVOICEACCOUNT, CUSTPACKINGSLIPTRANS.EXTERNALITEMID, CUSTPACKINGSLIPJOUR.PACKINGSLIPID DESC, CUSTPACKINGSLIPTRANS.LINENUM

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[54] 2[2] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CUSTPACKINGSLIPTRANS"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 427
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CUSTPACKINGSLIPJOUR"
            Begin Extent = 
               Top = 9
               Left = 484
               Bottom = 206
               Right = 851
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 9
               Left = 908
               Bottom = 425
               Right = 1204
            End
            DisplayFlags = 280
            TopColumn = 30
         End
         Begin Table = "INVENTDIM"
            Begin Extent = 
               Top = 9
               Left = 1261
               Bottom = 206
               Right = 1529
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "INVENTTRANS"
            Begin Extent = 
               Top = 9
               Left = 1586
               Bottom = 405
               Right = 2011
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_STOCKCUSTCONSIG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'    Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 2460
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_STOCKCUSTCONSIG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_STOCKCUSTCONSIG';


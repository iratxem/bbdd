﻿CREATE VIEW dbo.VEnvTraza
AS
SELECT        TOP (100) PERCENT dbo.circuito.cliente_id, dbo.circuito.referencia_id, dbo.circuito.ref_cliente, dbo.VAX_EKAN_ALBA.DELIVERYDATE, dbo.VAX_EKAN_ALBA.PACKINGSLIPID, dbo.VAX_EKAN_ALBA.LINENUM, 
                         dbo.VAX_EKAN_ALBA.QTY, dbo.VAX_EKAN_ALBA.INVENTBATCHID,
                             (SELECT        MIN(Reg_Fec) AS Expr1
                               FROM            dbo.TRegistroLecturas
                               WHERE        (Reg_Lot = dbo.VAX_EKAN_ALBA.INVENTBATCHID)
                               GROUP BY Reg_Dis) AS RegistroInicial,
                             (SELECT        fecha_lectura
                               FROM            dbo.tarjeta
                               WHERE        (sentido = 'S') AND (id_lectura = tarjeta_1.id_lectura)) AS RegistroEntrada, tarjeta_1.fecha_lectura, tarjeta_1.id_lectura, tarjeta_1.sentido, tarjeta_1.num_lecturas
FROM            dbo.circuito INNER JOIN
                         dbo.VAX_EKAN_ALBA ON dbo.circuito.cliente_id = dbo.VAX_EKAN_ALBA.INVOICEACCOUNT AND dbo.circuito.referencia_id = dbo.VAX_EKAN_ALBA.ITEMID LEFT OUTER JOIN
                         dbo.tarjeta AS tarjeta_1 ON dbo.circuito.cliente_id = tarjeta_1.cliente_id AND dbo.circuito.id_circuito = tarjeta_1.id_circuito AND dbo.circuito.referencia_id = tarjeta_1.referencia_id AND 
                         dbo.circuito.ubicacion = tarjeta_1.ubicacion AND dbo.VAX_EKAN_ALBA.INVENTBATCHID = SUBSTRING(tarjeta_1.id_lectura, 1, 6)
WHERE        (dbo.circuito.cliente_id = '02229000') AND (dbo.VAX_EKAN_ALBA.DATAAREAID = N'ork') AND (tarjeta_1.sentido = 's' OR
                         tarjeta_1.sentido IS NULL)
ORDER BY dbo.VAX_EKAN_ALBA.PACKINGSLIPID, dbo.VAX_EKAN_ALBA.LINENUM

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[31] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "circuito"
            Begin Extent = 
               Top = 20
               Left = 233
               Bottom = 530
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "VAX_EKAN_ALBA"
            Begin Extent = 
               Top = 55
               Left = 22
               Bottom = 260
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "tarjeta_1"
            Begin Extent = 
               Top = 36
               Left = 589
               Bottom = 475
               Right = 782
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2280
         Width = 2265
         Width = 1725
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 8550
         Alias = 2970
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEnvTraza';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEnvTraza';

